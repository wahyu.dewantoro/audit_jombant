
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="jombang/assets/img/logo-fav.png">
    <title>E-Audit</title>
    <link rel="stylesheet" type="text/css" href="jombang/assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="jombang/assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
    <link rel="stylesheet" href="jombang/assets/css/app.css" type="text/css"/>
    <script src='https://www.google.com/recaptcha/api.js'></script>
  </head>
  <body class="be-splash-screen">
    <div class="be-wrapper be-login">
      <div class="be-content">
        <div class="main-content container-fluid">
          <div class="splash-container">
            <div class="card card-border-color card-border-color-danger">
              <div class="card-header"><img class="logo-img" src="jombang/assets/img/home.png" alt="logo" ><span class="splash-description"></span>
                <h3>E-AUDIT Kab. Jombang</h3>
              </div>
              <div class="card-body">
                
                <form method="post" action="proses.php">
                  <div class="form-group">
                    <select class="form-control" name="tahun" id="tahun" required>
                                                <option value="2019">2019</option>
                                                <option value="2018">2018</option>
                        
                    </select>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="username" type="text" name="username"  placeholder="Username" autocomplete="off">
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="password" name="password" type="password" placeholder="Password">
                  </div>
                   <!-- <div class="g-recaptcha" data-sitekey="6Lch3oMUAAAAAEYU8qTJihg2kl1X69SHSPZ8ZPGh"></div> -->
                  <div class="form-group login-submit"> <button class="btn btn-primary btn-xl">Masuk</button> </div>
                </form>
              </div>
            </div>
            <!-- <div class="splash-footer"><span>Don't have an account? <a href="pages-sign-up.html">Sign Up</a></span></div> -->
          </div>
        </div>
      </div>
    </div>
    <script src="jombang/assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="jombang/assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="jombang/assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="jombang/assets/js/app.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        //-initialize the javascript
        App.init();
      });
      
    </script>
  </body>

<!-- Mirrored from foxythemes.net/preview/products/beagle/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 13 Jul 2018 11:57:04 GMT -->
</html>
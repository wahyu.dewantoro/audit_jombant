<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Anggaran extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->load->model('Manggaran');
        $this->load->library('form_validation');
        $this->menu_db = $this->load->database('menu', true);
    }

    private function cekAkses($var = null)
    {
        $url = 'Anggaran';
        return cek($this->id_pengguna, $url, $var);
    }


    function _config()
    {
        $tahun = get_userdata('tahun_anggaran');
        return $this->menu_db->query("select tahun,linked_server,nama_db from ms_config where tahun=?", array($tahun))->row();
    }

    public function realisasi(){
        $kdopd = urldecode($this->input->get('opd', TRUE));
        $opd = $this->db->get('TB_SPE_REF_SUB_UNIT')->result();

        $anggaran=[];

        if($kdopd<>''){

            // $cf=$this->_config();
            $xlo =  $kdopd;
            /* $D = get_userdata('tahun_anggaran') . '1231';
            $Tahun=get_userdata('tahun_anggaran');
            $Kd_Urusan=$xlo[0];
            $Kd_Bidang=$xlo[1];
            $Kd_Unit=$xlo[2];
            $Kd_Sub=$xlo[3]; */
            $anggaran=$this->db->query("select a.*,realisasi,anggaran - realisasi selisih
            from (
            select Akun_Akrual_3,Nm_Akrual_3, case when sum(Anggaran_Perubahan_1) >0 then sum(Anggaran_Perubahan_1) else sum(Anggaran_Awal) end anggaran
            from TB_SPE_ANGGARAN
            where Kd_SKPD='$xlo'
            group by Akun_Akrual_3,Nm_Akrual_3
            ) a 
            left join (
            select Akun_Akrual_3,Nm_Akrual_3,sum(debet) - sum(kredit) realisasi
            from tb_spe_detail_lra
            where Kd_SKPD='$xlo'
            group by Akun_Akrual_3,Nm_Akrual_3
            ) b on a.Akun_Akrual_3=b.Akun_Akrual_3 
            order by a.Akun_Akrual_3 asc")->result();

            /* $anggaran = $this->db->query("SELECT Kd_Rek_1,
            Kd_Rek_2,
            Kd_Rek_3,
            Nm_Rek_1,
            Nm_Rek_2,
            Nm_Rek_3,
            abs(sum(Anggaran)) Anggaran,
            abs(sum(Realisasi)) Realisasi,
            abs(sum(selisih)) Selisih  FROM   OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptLRAAK $Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, ''$D''')
                group by Kd_Rek_1,
                Kd_Rek_2,
                Kd_Rek_3,
                Nm_Rek_1,
                Nm_Rek_2,
                Nm_Rek_3")->result_array(); */
        }

        $data    = array(
            'lra' => $anggaran,
            'nonbtl'=>$anggaran,
            'title'=> 'Realisasi Anggaran',
            'kdopd'=>$kdopd,
            'opd'=>$opd
        );
        $this->template->load('layout', 'anggaran/view_realisasi_new', $data);
    }

    public function xrealisasi()
    {
        $url  = 'realisasi';
        $akses = cek($this->id_pengguna, $url, 'read');
        $q    = urldecode($this->input->get('q', TRUE));

        if ($q <> '') {
            $cetak = base_url() . 'anggaran/cetak_realisasi?q=' . urlencode($q);
        } else {
            $cetak = base_url() . 'anggaran/cetak_realisasi';
        }
        $anggaran = $this->Manggaran->dataRealisai($q);
        $data    = array(
            'anggaran_data' => $anggaran,
            'title'         => 'Realisasi Anggaran',
            'cetak'         => $cetak
        );
        $this->template->load('layout', 'anggaran/view_realisasi', $data);
    }



    function readrealisasi()
    {
        $url  = 'Anggaran.realisasi';
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $jenis  = urldecode($this->input->get('jenis', true));
        $akun   = urldecode($this->input->get('akun', true));

        $query = $this->Manggaran->GetLeveldua($nm_unit, $akun);

        $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $exp = explode('?', $actual_link);
        /*echo $exp[1];
        die();*/
        if (!empty($_SERVER['HTTP_REFERER'])) {
            $back = $_SERVER['HTTP_REFERER'];
        } else {
            $back = base_url() . 'anggaran/realisasi';
        }

        if ($akun == '4') {
            $jns = 'Pendapatan';
        } else {
            $jns = 'Belanja';
        }

        $data = array(
            'title'         => 'Detail Realisasi Anggaran',
            'anggaran_data' => $query,
            'kembali'       => $back,
            'nm_unit'       => $nm_unit,
            'jenis'         => $jns . ' (' . ucwords($jenis) . ')',
            'jns'           => $jns,
            'cetak' => base_url() . 'anggaran/cetak_readrealisasi?' . $exp[1]
        );
        $this->template->load('layout', 'anggaran/view_indexduarealisasi', $data);
    }


    function cetak_readrealisasi()
    {
        $url  = 'Anggaran.realisasi';
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $jenis  = urldecode($this->input->get('jenis', true));
        $akun   = urldecode($this->input->get('akun', true));

        $query = $this->Manggaran->GetLeveldua($nm_unit, $akun);

        if (!empty($_SERVER['HTTP_REFERER'])) {
            $back = $_SERVER['HTTP_REFERER'];
        } else {
            $back = base_url() . 'anggaran/realisasi';
        }

        if ($akun == '4') {
            $jns = 'Pendapatan';
        } else {
            $jns = 'Belanja';
        }

        $data = array(
            'title'         => 'Detail Realisasi Anggaran',
            'anggaran_data' => $query,
            'kembali'       => $back,
            'nm_unit'       => $nm_unit,
            'jenis'         => $jns . ' (' . ucwords($jenis) . ')',
            'jns'           => $jns
        );


        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=Anggaran.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        $this->load->view('anggaran/view_indexduarealisasi', $data);
        // $this->template->load('layout','anggaran/view_indexduarealisasi',$data);
    }

    public function cetak_realisasi()
    {
        $url  = 'Anggaran.realisasi';
        $akses = cek($this->id_pengguna, $url, 'read');
        $q    = urldecode($this->input->get('q', TRUE));

        if ($q <> '') {
            $cetak = base_url() . 'anggaran/cetak_realisasi?q=' . urlencode($q);
        } else {
            $cetak = base_url() . 'anggaran/cetak_realisasi';
        }

        $anggaran = $this->Manggaran->dataRealisai($q);
        $data    = array(
            'anggaran_data' => $anggaran,
            'title'         => 'Anggaran VS Realisasi',
            'cetak'         => $cetak
        );



        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=Anggaran.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        $this->load->view('anggaran/view_realisasi', $data);
        // $this->template->load('layout','anggaran/view_realisasi',$data);



        /*$akses =$this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        $anggaran             = $this->Manggaran->dataRealisai($q);

        $data = array(
            'anggaran_data' => $anggaran,
        );
        $this->load->view('anggaran/cetak_realisasi',$data);*/
    }
    public function cetak_realisasi2()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        $anggaran             = $this->Manggaran->dataRealisai($q);

        $data = array(
            'anggaran_data' => $anggaran,
        );
        $this->load->view('anggaran/cetak_realisasi2', $data);
    }
    public function index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        if ($q <> '') {

            $cetak = base_url() . 'anggaran/cetak_index?q=' . urlencode($q);
        } else {
            $cetak = base_url() . 'anggaran/cetak_index';
        }

        $anggaran                    = $this->Manggaran->getAllData($q);

        $data = array(
            'anggaran_data' => $anggaran,
            'q'             => $q,
            'title'         => 'Anggaran',
            'cetak'         => $cetak,
            'script' => 'anggaran/scriptindex'
        );
        $this->template->load('layout', 'anggaran/view_index', $data);
    }

    public function cetak_index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        $anggaran = $this->Manggaran->getAllData($q);

        $data = array(
            'anggaran_data' => $anggaran,

        );
        $this->load->view('anggaran/cetak_index', $data);
    }
    public function cetak_index2()
    {
        $akses  = $this->cekAkses('read');
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $jenis  = urldecode($this->input->get('jenis', true));
        $akun   = urldecode($this->input->get('akun', true));

        $query = $this->Manggaran->GetLeveldua($nm_unit, $akun);

        if ($akun == '4') {
            $var = 'Anggaran Pendapatan';
        } else {
            $var = 'Anggaran Belanja';
        }


        $data = array(
            'title'        => 'Data Anggaran',
            'anggaran_data' => $query,
            'nm_unit'      => $nm_unit,
            'jenis'        => $jenis,
            'var'          => $var
        );
        $this->load->view('anggaran/cetak_index2', $data);
    }

    public function read()
    {
        $akses  = $this->cekAkses('read');
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $jenis  = urldecode($this->input->get('jenis', true));
        $akun   = urldecode($this->input->get('akun', true));
        $query  = $this->Manggaran->GetLeveldua($nm_unit, $akun);

        if (!empty($_SERVER['HTTP_REFERER'])) {
            $back = $_SERVER['HTTP_REFERER'];
        } else {
            $back = base_url() . 'anggaran';
        }

        if ($akun == '4') {
            $var = 'Pendapatan';
        } else {
            $var = 'Belanja';
        }

        $data = array(
            'title'        => 'Detail Anggaran',
            'anggaran_data' => $query,
            'kembali'      => $back,
            'nm_unit'      => $nm_unit,
            'jenis'        => $jenis,
            'var'          => $var,
            'cetak'        => base_url() . 'anggaran/cetak_index2?nm_unit=' . urlencode($nm_unit) . '&jenis=' . urlencode($jenis) . '&akun=' . $akun
        );
        $this->template->load('layout', 'anggaran/view_indexdua', $data);
    }

    public function cetak()
    {
        $akses         = $this->cekAkses('read');
        $q             = urldecode($this->input->get('q', TRUE));
        $start         = intval($this->input->get('start'));
        $kd_skpd       = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit   = urldecode($this->input->get('nm_sub_unit', true));
        $akun_akrual_3 = urldecode($this->input->get('akun_akrual_3', true));

        if ($akun_akrual_3 <> '') {
            $this->db->where('akun_akrual_3', $akun_akrual_3);
        }
        $config['total_rows']        = $this->Manggaran->total_rows($kd_skpd, $nm_sub_unit, $q);

        if ($akun_akrual_3 <> '') {
            $this->db->where('akun_akrual_3', $akun_akrual_3);
        }
        $anggaran                    = $this->Manggaran->get_limit_data($kd_skpd, $nm_sub_unit, $q);
        if ($akun_akrual_3 <> '') {
            $this->db->where('akun_akrual_3', $akun_akrual_3);
        }

        $total = $this->Manggaran->getSumdetail($kd_skpd, $nm_sub_unit, $q);
        $data = array(
            'anggaran_data' => $anggaran,
            'kd_skpd'       => $kd_skpd,
            'nm_sub_unit'   => $nm_sub_unit,
            'awal'          => number_format($total->awal, '2', ',', '.'),
            'satu'          => number_format($total->satu, '2', ',', '.'),
            'dua'           => number_format($total->dua, '2', ',', '.'),
        );

        $this->load->view('anggaran/excel', $data);
    }
    public function cetak2()
    {
        $akses         = $this->cekAkses('read');
        $q             = urldecode($this->input->get('q', TRUE));
        $start         = intval($this->input->get('start'));
        $kd_skpd       = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit   = urldecode($this->input->get('nm_sub_unit', true));
        $akun_akrual_3 = urldecode($this->input->get('akun_akrual_3', true));

        if ($akun_akrual_3 <> '') {
            $this->db->where('akun_akrual_3', $akun_akrual_3);
        }
        $config['total_rows']        = $this->Manggaran->total_rows($kd_skpd, $nm_sub_unit, $q);

        if ($akun_akrual_3 <> '') {
            $this->db->where('akun_akrual_3', $akun_akrual_3);
        }
        $anggaran                    = $this->Manggaran->get_limit_data($kd_skpd, $nm_sub_unit, $q);
        if ($akun_akrual_3 <> '') {
            $this->db->where('akun_akrual_3', $akun_akrual_3);
        }

        $total = $this->Manggaran->getSumdetail($kd_skpd, $nm_sub_unit, $q);
        $data = array(
            'anggaran_data' => $anggaran,
            'kd_skpd'       => $kd_skpd,
            'nm_sub_unit'   => $nm_sub_unit,
            'awal'          => number_format($total->awal, '2', ',', '.'),
            'satu'          => number_format($total->satu, '2', ',', '.'),
            'dua'           => number_format($total->dua, '2', ',', '.'),
        );

        $this->load->view('anggaran/excel2', $data);
    }
    public function cetaktiga()
    {
        $akses         = $this->cekAkses('read');
        $q             = urldecode($this->input->get('q', TRUE));
        $start         = intval($this->input->get('start'));
        $kd_skpd       = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit   = urldecode($this->input->get('nm_sub_unit', true));
        $akun_akrual_3 = urldecode($this->input->get('akun_akrual_3', true));
        $nm_akrual_3   = urldecode($this->input->get('nm_akrual_3', true));
        $akun_akrual_5 = urldecode($this->input->get('akun_akrual_5', true));

        if ($akun_akrual_5 <> '' || $q <> '') {
            $config['base_url']  = base_url() . 'anggaran/readtiga?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&akun_akrual_3=' . urlencode($akun_akrual_3) . '&nm_akun_akrual_3=' . urlencode($nm_akrual_3) . '&akun_akrual_5=' . urlencode($akun_akrual_5) . '&q=' . urlencode($q);
            $config['first_url'] = base_url() . 'anggaran/readtiga?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&akun_akrual_3=' . urlencode($akun_akrual_3) . '&nm_akun_akrual_3=' . urlencode($nm_akrual_3) . '&akun_akrual_5=' . urlencode($akun_akrual_5) . '&q=' . urlencode($q);
            $cetak               = base_url() . 'anggaran/cetaktiga?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&akun_akrual_3=' . urlencode($akun_akrual_3) . '&nm_akun_akrual_3=' . urlencode($nm_akrual_3) . '&akun_akrual_5=' . urlencode($akun_akrual_5) . '&q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'anggaran/readtiga?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&akun_akrual_3=' . urlencode($akun_akrual_3) . '&nm_akun_akrual_3=' . urlencode($nm_akrual_3);
            $config['first_url'] = base_url() . 'anggaran/readtiga?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&akun_akrual_3=' . urlencode($akun_akrual_3) . '&nm_akun_akrual_3=' . urlencode($nm_akrual_3);
            $cetak               = base_url() . 'anggaran/cetaktiga?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&akun_akrual_3=' . urlencode($akun_akrual_3) . '&nm_akun_akrual_3=' . urlencode($nm_akrual_3);
        }

        $config['per_page']          = 2000;
        $config['page_query_string'] = TRUE;

        if ($akun_akrual_5 <> '') {
            $this->db->where('akun_akrual_5', $akun_akrual_5);
        }

        $config['total_rows']        = $this->Manggaran->total_rows_tiga($kd_skpd, $nm_sub_unit, $akun_akrual_3, $q);
        if ($akun_akrual_5 <> '') {
            $this->db->where('akun_akrual_5', $akun_akrual_5);
        }
        $anggaran                    = $this->Manggaran->get_limit_data_tiga($kd_skpd, $nm_sub_unit, $akun_akrual_3, $config['per_page'], $start, $q);
        /*echo $this->db->last_query();
        die();*/

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        if ($akun_akrual_5 <> '') {
            $this->db->where('akun_akrual_5', $akun_akrual_5);
        }
        $total = $this->Manggaran->getSumdetail_tiga($kd_skpd, $nm_sub_unit, $akun_akrual_3, $q);


        $data = array(
            'anggaran_data' => $anggaran,
            'q'             => $q,

            'title'         => 'Detail Anggaran',
            'akses'         => $akses,
            // 'action'        => base_url() . 'anggaran/readtiga?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&nm_akun_akrual_3='. urlencode($nm_akrual_3),
            // 'kembali'       => base_url().'anggaran/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit),
            'kd_skpd'       => $kd_skpd,
            'nm_sub_unit'   => $nm_sub_unit,
            'akun_akrual_5' => $akun_akrual_5,
            'akun_akrual_3' => $akun_akrual_3,
            'nm_akrual_3'   => $nm_akrual_3,
            'awal'          => number_format($total->awal, '2', ',', '.'),
            'satu'          => number_format($total->satu, '2', ',', '.'),
            'dua'           => number_format($total->dua, '2', ',', '.'),
            // 'cetak'         =>$cetak,
        );
        $this->load->view('anggaran/exceltiga', $data);
    }

    public function cetaktiga2()
    {
        $akses         = $this->cekAkses('read');
        $q             = urldecode($this->input->get('q', TRUE));
        $start         = intval($this->input->get('start'));
        $kd_skpd       = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit   = urldecode($this->input->get('nm_sub_unit', true));
        $akun_akrual_3 = urldecode($this->input->get('akun_akrual_3', true));
        $nm_akrual_3   = urldecode($this->input->get('nm_akrual_3', true));
        $akun_akrual_5 = urldecode($this->input->get('akun_akrual_5', true));

        if ($akun_akrual_5 <> '' || $q <> '') {
            $config['base_url']  = base_url() . 'anggaran/readtiga?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&akun_akrual_3=' . urlencode($akun_akrual_3) . '&nm_akun_akrual_3=' . urlencode($nm_akrual_3) . '&akun_akrual_5=' . urlencode($akun_akrual_5) . '&q=' . urlencode($q);
            $config['first_url'] = base_url() . 'anggaran/readtiga?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&akun_akrual_3=' . urlencode($akun_akrual_3) . '&nm_akun_akrual_3=' . urlencode($nm_akrual_3) . '&akun_akrual_5=' . urlencode($akun_akrual_5) . '&q=' . urlencode($q);
            $cetak               = base_url() . 'anggaran/cetaktiga?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&akun_akrual_3=' . urlencode($akun_akrual_3) . '&nm_akun_akrual_3=' . urlencode($nm_akrual_3) . '&akun_akrual_5=' . urlencode($akun_akrual_5) . '&q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'anggaran/readtiga?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&akun_akrual_3=' . urlencode($akun_akrual_3) . '&nm_akun_akrual_3=' . urlencode($nm_akrual_3);
            $config['first_url'] = base_url() . 'anggaran/readtiga?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&akun_akrual_3=' . urlencode($akun_akrual_3) . '&nm_akun_akrual_3=' . urlencode($nm_akrual_3);
            $cetak               = base_url() . 'anggaran/cetaktiga?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&akun_akrual_3=' . urlencode($akun_akrual_3) . '&nm_akun_akrual_3=' . urlencode($nm_akrual_3);
        }

        $config['per_page']          = 2000;
        $config['page_query_string'] = TRUE;

        if ($akun_akrual_5 <> '') {
            $this->db->where('akun_akrual_5', $akun_akrual_5);
        }

        $config['total_rows']        = $this->Manggaran->total_rows_tiga($kd_skpd, $nm_sub_unit, $akun_akrual_3, $q);
        if ($akun_akrual_5 <> '') {
            $this->db->where('akun_akrual_5', $akun_akrual_5);
        }
        $anggaran                    = $this->Manggaran->get_limit_data_tiga($kd_skpd, $nm_sub_unit, $akun_akrual_3, $config['per_page'], $start, $q);
        /*echo $this->db->last_query();
        die();*/

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        if ($akun_akrual_5 <> '') {
            $this->db->where('akun_akrual_5', $akun_akrual_5);
        }
        $total = $this->Manggaran->getSumdetail_tiga($kd_skpd, $nm_sub_unit, $akun_akrual_3, $q);


        $data = array(
            'anggaran_data' => $anggaran,
            'q'             => $q,

            'title'         => 'Detail Anggaran',
            'akses'         => $akses,
            // 'action'        => base_url() . 'anggaran/readtiga?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_3='.urlencode($akun_akrual_3).'&nm_akun_akrual_3='. urlencode($nm_akrual_3),
            // 'kembali'       => base_url().'anggaran/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit),
            'kd_skpd'       => $kd_skpd,
            'nm_sub_unit'   => $nm_sub_unit,
            'akun_akrual_5' => $akun_akrual_5,
            'akun_akrual_3' => $akun_akrual_3,
            'nm_akrual_3'   => $nm_akrual_3,
            'awal'          => number_format($total->awal, '2', ',', '.'),
            'satu'          => number_format($total->satu, '2', ',', '.'),
            'dua'           => number_format($total->dua, '2', ',', '.'),
            // 'cetak'         =>$cetak,
        );
        $this->load->view('anggaran/exceltiga2', $data);
    }
}

/* End of file Anggaran.php */
/* Location: ./application/controllers/Anggaran.php */

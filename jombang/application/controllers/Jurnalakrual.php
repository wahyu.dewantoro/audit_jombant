<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Jurnalakrual extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->load->model('Mja');
        $this->load->library('form_validation');
    }

    private function cekAkses($var = null)
    {
        $url = 'Jurnalakrual';
        return cek($this->id_pengguna, $url, $var);
    }

    public function index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        if ($q <> '') {
            $config['base_url']  = base_url() . 'jurnalakrual?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'jurnalakrual?q=' . urlencode($q);
            $cetak = base_url() . 'jurnalakrual/cetak_index?q=' . urlencode($q);
            $cetak2 = base_url() . 'jurnalakrual/cetak_index2?q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'jurnalakrual';
            $config['first_url'] = base_url() . 'jurnalakrual';
            $cetak = base_url() . 'jurnalakrual/cetak_index';
            $cetak2 = base_url() . 'jurnalakrual/cetak_index2';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows']        = $this->Mja->getRow($q);
        $jurnalakrual                = $this->Mja->getAllData($q);

        // getSumNilai
        $total = $this->Mja->getSumNilai($q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        $subunit = $this->Mja->getSUbunit();


        $data = array(
            'jurnalakrual_data' => $jurnalakrual,
            'q'                 => $q,
            'pagination'        => $this->pagination->create_links(),
            'total_rows'        => $config['total_rows'],
            'start'             => $start,
            'title'             => 'Jurnal Akrual',
            'akses'             => $akses,
            'subunit'           => $subunit,
            'cetak'                 => $cetak,
            'cetak2'                 => $cetak2,
            'jum_debet'             => number_format($total->jum_debet, '2', ',', '.'),
            'jum_kredit'            => number_format($total->jum_kredit, '2', ',', '.'),
            'script' => 'report/datatables',
        );
        $this->template->load('layout', 'jurnalakrual/view_index', $data);
    }
    public function cetak_index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $jurnalakrual                = $this->Mja->getAllData($q);
        $total = $this->Mja->getSumNilai($q);
        $data = array(
            'jurnalakrual_data' => $jurnalakrual,
            'jum_debet'             => number_format($total->jum_debet, '2', ',', '.'),
            'jum_kredit'            => number_format($total->jum_kredit, '2', ',', '.'),
        );
        $this->load->view('jurnalakrual/cetak_index', $data);
    }
    public function cetak_index2()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $jurnalakrual                = $this->Mja->getAllData($q);
        $total = $this->Mja->getSumNilai($q);
        $data = array(
            'jurnalakrual_data' => $jurnalakrual,
            'jum_debet'             => number_format($total->jum_debet, '2', ',', '.'),
            'jum_kredit'            => number_format($total->jum_kredit, '2', ',', '.'),
        );
        $this->load->view('jurnalakrual/cetak_index2', $data);
    }
    public function read()
    {
        $akses         = $this->cekAkses('read');
        $q             = urldecode($this->input->get('q', TRUE));
        $kd_skpd       = urldecode($this->input->get('kd_skpd', TRUE));
        $nm_sub_unit   = urldecode($this->input->get('nm_sub_unit', TRUE));
        $akun_akrual_5 = urldecode($this->input->get('akun_akrual_5', true));
        $start         = intval($this->input->get('start'));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));

        if ($tanggal1 == '') {
            $tanggal1 = '01/01/' . date('Y');
        }

        if ($tanggal2 == '') {
            $tanggal2 = date('d/m/Y');
        }

        if ($q <> '' || $akun_akrual_5 <> '' || $tanggal1 <> '' || $tanggal2 <> '') {
            $config['base_url']  = base_url() . 'jurnalakrual/read?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&akun_akrual_5=' . urlencode($akun_akrual_5) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2) . '&q=' . urlencode($q);
            $config['first_url'] = base_url() . 'jurnalakrual/read?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&akun_akrual_5=' . urlencode($akun_akrual_5) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2) . '&q=' . urlencode($q);
            $cetak               = base_url() . 'jurnalakrual/cetak?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&akun_akrual_5=' . urlencode($akun_akrual_5) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2) . '&q=' . urlencode($q);
            $cetak2               = base_url() . 'jurnalakrual/cetak2?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&akun_akrual_5=' . urlencode($akun_akrual_5) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2) . '&q=' . urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'jurnalakrual/read?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit);
            $config['first_url'] = base_url() . 'jurnalakrual/read?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit);
            $cetak = base_url() . 'jurnalakrual/cetak?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit);
            $cetak2 = base_url() . 'jurnalakrual/cetak2?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit);
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;

        if ($akun_akrual_5 <> '') {
            $this->db->where('akun_akrual_5', $akun_akrual_5);
        }

        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }

        $config['total_rows']        = $this->Mja->total_rows($kd_skpd, $nm_sub_unit, $q);
        if ($akun_akrual_5 <> '') {
            $this->db->where('akun_akrual_5', $akun_akrual_5);
        }

        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }

        $jurnalakrual                = $this->Mja->get_limit_data($kd_skpd, $nm_sub_unit, $config['per_page'], $start, $q);


        $this->load->library('pagination');
        $this->pagination->initialize($config);



        $akrual5 = $this->db->query("select distinct akun_akrual_5,nm_akrual_5
									from tb_spe_jurnal_akrual
									where kd_skpd='$kd_skpd' and nm_sub_unit='$nm_sub_unit'
									order by akun_akrual_5 asc")->result();

        if ($akun_akrual_5 <> '') {
            $this->db->where('akun_akrual_5', $akun_akrual_5);
        }

        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }

        $total = $this->Mja->getSumNilaidetail($kd_skpd, $nm_sub_unit, $q);


        $data = array(
            'jurnalakrual_data' => $jurnalakrual,
            'q'                 => $q,
            'pagination'        => $this->pagination->create_links(),
            'total_rows'        => $config['total_rows'],
            'start'             => $start,
            'title'             => 'Jurnal Akrual',
            'akses'             => $akses,
            'kembali'           => base_url() . 'jurnalakrual',
            'kd_skpd'           => $kd_skpd,
            'nm_sub_unit'       => $nm_sub_unit,
            'action'            => base_url() . 'jurnalakrual/read?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit),
            'akrual5'           => $akrual5,
            'akun_akrual_5'     => $akun_akrual_5,
            'tanggal1' => $tanggal1,
            'tanggal2' => $tanggal2,
            'jum_debet'         => number_format($total->jum_debet, '2', ',', '.'),
            'jum_kredit'        => number_format($total->jum_kredit, '2', ',', '.'),
            'cetak'                => $cetak,
            'cetak2'                => $cetak2,
        );
        $this->template->load('layout', 'jurnalakrual/view_indexdua', $data);
    }

    public function cetak()
    {
        $akses         = $this->cekAkses('read');
        $q             = urldecode($this->input->get('q', TRUE));
        $kd_skpd       = urldecode($this->input->get('kd_skpd', TRUE));
        $nm_sub_unit   = urldecode($this->input->get('nm_sub_unit', TRUE));
        $akun_akrual_5 = urldecode($this->input->get('akun_akrual_5', true));
        $start         = intval($this->input->get('start'));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($akun_akrual_5 <> '') {
            $this->db->where('akun_akrual_5', $akun_akrual_5);
        }
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $jurnalakrual       = $this->Mja->get_all_data($kd_skpd, $nm_sub_unit, $q);
        if ($akun_akrual_5 <> '') {
            $this->db->where('akun_akrual_5', $akun_akrual_5);
        }
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $total = $this->Mja->getSumNilaidetail($kd_skpd, $nm_sub_unit, $q);
        $data = array(
            'jurnalakrual_data' => $jurnalakrual,
            'jum_debet'         => number_format($total->jum_debet, '2', ',', '.'),
            'jum_kredit'        => number_format($total->jum_kredit, '2', ',', '.'),
            'kd_skpd'       => $kd_skpd,
            'nm_sub_unit'   => $nm_sub_unit,
        );
        $this->load->view('jurnalakrual/excel', $data);
    }
    public function cetak2()
    {
        $akses         = $this->cekAkses('read');
        $q             = urldecode($this->input->get('q', TRUE));
        $kd_skpd       = urldecode($this->input->get('kd_skpd', TRUE));
        $nm_sub_unit   = urldecode($this->input->get('nm_sub_unit', TRUE));
        $akun_akrual_5 = urldecode($this->input->get('akun_akrual_5', true));
        $start         = intval($this->input->get('start'));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($akun_akrual_5 <> '') {
            $this->db->where('akun_akrual_5', $akun_akrual_5);
        }
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $jurnalakrual       = $this->Mja->get_all_data($kd_skpd, $nm_sub_unit, $q);
        if ($akun_akrual_5 <> '') {
            $this->db->where('akun_akrual_5', $akun_akrual_5);
        }
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $total = $this->Mja->getSumNilaidetail($kd_skpd, $nm_sub_unit, $q);
        $data = array(
            'jurnalakrual_data' => $jurnalakrual,
            'jum_debet'         => number_format($total->jum_debet, '2', ',', '.'),
            'jum_kredit'        => number_format($total->jum_kredit, '2', ',', '.'),
            'kd_skpd'       => $kd_skpd,
            'nm_sub_unit'   => $nm_sub_unit,
        );
        $this->load->view('jurnalakrual/excel2', $data);
    }
}

/* End of file Jurnalakrual.php */
/* Location: ./application/controllers/Jurnalakrual.php */

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Keuangan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->load->model('Mkeuangan');
        $this->load->library('form_validation');
    }

    private function cekAkses($var = null)
    {
        $url = 'keuangan';
        return cek($this->id_pengguna, $url, $var);
    }

    function index()
    {
        $akses = $this->cekAkses('read');
        $kdopd = urldecode($this->input->get('opd', TRUE));
        $opd = $this->db->get('TB_SPE_REF_SUB_UNIT')->result();

        $lravertikal = [];
        $lrahorizontal = [];
        $neracavertikal = [];
        $lo = [];
        $lpe = [];
        $lralak = [];
        $neracavslak = [];
        $lravssal = [];
        $lolpeneraca = [];
        $lolraneraca = [];
        $dataSAL=[];
        $dataLAK=[];
        if ($kdopd <> '') {
            $xlo = explode('.', $kdopd);
            $d = get_userdata('tahun_anggaran') . '1231';
            $lravertikal = $this->Mkeuangan->dataLraVertikal(get_userdata('tahun_anggaran'), $xlo[0], $xlo[1], $xlo[2], $xlo[3], $d);
            $lrahorizontal = $this->Mkeuangan->dataLraHorizontal(get_userdata('tahun_anggaran'), $xlo[0], $xlo[1], $xlo[2], $xlo[3], $d);
            $neracavertikal = $this->Mkeuangan->dataNeracaVertikal(get_userdata('tahun_anggaran'), $xlo[0], $xlo[1], $xlo[2], $xlo[3], $d);
            $lo = $this->Mkeuangan->dataLo(get_userdata('tahun_anggaran'), $xlo[0], $xlo[1], $xlo[2], $xlo[3], $d);
            $lpe = $this->Mkeuangan->dataLPE(get_userdata('tahun_anggaran'), $xlo[0], $xlo[1], $xlo[2], $xlo[3], $d);
            $lralak = $this->Mkeuangan->dataLRALAK(get_userdata('tahun_anggaran'), $xlo[0], $xlo[1], $xlo[2], $xlo[3], $d);
            $neracavslak = $this->Mkeuangan->neracavslak(get_userdata('tahun_anggaran'), $xlo[0], $xlo[1], $xlo[2], $xlo[3], $d);
            $lravssal = $this->Mkeuangan->lravssal(get_userdata('tahun_anggaran'), $xlo[0], $xlo[1], $xlo[2], $xlo[3], $d);
            $lolpeneraca = $this->Mkeuangan->lolpeneraca(get_userdata('tahun_anggaran'), $xlo[0], $xlo[1], $xlo[2], $xlo[3], $d);
            $lolraneraca = $this->Mkeuangan->lolraneraca(get_userdata('tahun_anggaran'), $xlo[0], $xlo[1], $xlo[2], $xlo[3], $d);
            $dataSAL=$this->Mkeuangan->dataSAL(get_userdata('tahun_anggaran'), $xlo[0], $xlo[1], $xlo[2], $xlo[3], $d);
            $dataLAK=$this->Mkeuangan->dataLAK(get_userdata('tahun_anggaran'), $xlo[0], $xlo[1], $xlo[2], $xlo[3], $d);
        }
       
        $data     = array(
            'title' => 'Prosedur Analitis',
            'opd' => $opd,
            'kdopd' => $kdopd,
            'lravertikal' => $lravertikal,
            'neracavertikal' => $neracavertikal,
            'lo' => $lo,
            'lpe' => $lpe,
            'lrahorizontal' => $lrahorizontal,
            'lralak' => $lralak,
            'neracavslak' => $neracavslak,
            'lravssal' => $lravssal,
            'lolpeneraca' => $lolpeneraca,
            'lolraneraca' => $lolraneraca,
            'dataSAL'=>$dataSAL,
            'dataLAK'=>$dataLAK,
        );
        
        $this->template->load('layout', 'keuangan/view_pa_new', $data);
        // echo get_userdata('tahun_anggaran');
    }

    public function xindex()
    {
        $akses = $this->cekAkses('read');
        $q    = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        $kdopd = urldecode($this->input->get('opd', TRUE));
        $opd = $this->db->get('TB_SPE_REF_SUB_UNIT')->result();

        $lo = [];
        $lpe = [];
        $lra = [];
        $neraca = [];
        $neracaSAP = [];
        if ($kdopd <> '') {
            $xlo = explode('.', $kdopd);
            $d = get_userdata('tahun_anggaran') . '1231';
            $lo = $this->Mkeuangan->rptLO(get_userdata('tahun_anggaran'), $xlo[0], $xlo[1], $xlo[2], $xlo[3], $d);
            $lpe = $this->Mkeuangan->rptLPE(get_userdata('tahun_anggaran'), $xlo[0], $xlo[1], $xlo[2], $xlo[3], $d);
            $neraca = $this->Mkeuangan->RptNeraca(get_userdata('tahun_anggaran'), $xlo[0], $xlo[1], $xlo[2], $xlo[3], $d);
            $lra = $this->Mkeuangan->RptLra(get_userdata('tahun_anggaran'), $xlo[0], $xlo[1], $xlo[2], $xlo[3], $d);
            $neracaSAP = $this->Mkeuangan->RptNeracSAP(get_userdata('tahun_anggaran'), $xlo[0], $xlo[1], $xlo[2], $xlo[3], $d);
        }

        $data     = array(
            'title' => 'Prodedur Analitis',
            'opd' => $opd,
            'kdopd' => $kdopd,
            'pendapatanlra' => $this->Mkeuangan->getPendapatanLRA($kdopd),
            'belanjalra' => $this->Mkeuangan->getBelanjaLRA($kdopd),
            'penerimaanBiayaLra' => $this->Mkeuangan->getPenerimaanBiayaLRA($kdopd),
            'pengeluaranBiayaLra' => $this->Mkeuangan->getPengeluaranBiayaLRA($kdopd),
            'lra' => $lra,
            'lo' => $lo,
            'lpe' => $lpe,
            'neraca' => $neraca,
            'neracasap' => $neracaSAP
        );

        $this->template->load('layout', 'keuangan/view_pa', $data);
    }

    function test()
    {
        $sql="exec dbo.RptLPSAL_OPD ?,?,?,?,?,?";
        $param=array(2018,'20181231',4,2,1,1);
        /* $sql = "SELECT *    
            FROM   OPENQUERY(
            jbg,  
            'SET FMTONLY OFF; SET NOCOUNT ON; EXEC jbg_2018.dbo.rptlo 2018,4,2,1,1,''20181231''')"; */
        $data = $this->db->query($sql,$param);
        $result = $data->result_array();
        echo $this->db->last_query();
        echo "<pre>";
        print_r($result);
    }
}

/* End of file Lradetail.php */
/* Location: ./application/controllers/Lradetail.php */

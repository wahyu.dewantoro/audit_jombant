<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Neraca extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->load->model('Mneraca');
        $this->load->library('form_validation');
    }

    private function cekAkses($var = null)
    {
        $url = 'Neraca';
        return cek($this->id_pengguna, $url, $var);
    }

    public function index()
    {
        $akses = $this->cekAkses('read');
        $query = $this->Mneraca->getall();
        $data = array(
            'neraca_data' => $query,
            'title' => 'Neraca',
            'cetak' => base_url() . 'neraca/cetak'
        );

        $this->template->load('layout', 'neraca/view_index', $data);
    }

    function cetak()
    {
        $akses = $this->cekAkses('read');
        $query = $this->Mneraca->getall();
        $data = array(
            'neraca_data' => $query,
            'title'      => 'Neraca',
        );
        $this->load->view('neraca/view_index', $data);
        $this->load->view('neraca/script_cetak');
    }


    function read()
    {
        $akses  = $this->cekAkses('read');
        $jenis  = urldecode($this->input->get('jenis', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));

        switch ($jenis) {
            case 'saldoawalaset':
                $query = $this->Mneraca->saldoawalaset($nm_unit);
                $view = 'neraca/saldoawalaset';
                $title = 'Saldo Awal Aset';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum(saldo_awal_debet)-sum(saldo_awal_kredit)) nilai from tb_spe_saldo_awal where akun_akrual_1='1' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                break;

            case 'mutasiaset':
                $query = $this->Mneraca->mutasiAset($nm_unit);
                $view = 'neraca/mutasiaset';
                $title = 'Mutasi Aset';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum([Mutasi_Debet])-sum([Mutasi_Kredit])) nilai from TB_SPE_DETAIL_NERACA where akun_akrual_1='1' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                break;

            case 'saldoawalkewajiban':
                $query = $this->Mneraca->saldoawalKewajiban($nm_unit);
                $view = 'neraca/saldoawalkewajiban';
                $title = 'Saldo Awal Kewajiban';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum(saldo_awal_debet)-sum(saldo_awal_kredit)) nilai from tb_spe_saldo_awal where akun_akrual_1='2' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                break;

            case 'mutasikewajiban':
                $query = $this->Mneraca->mutasikewajiban($nm_unit);
                $view = 'neraca/mutasiaset';
                $title = 'Mutasi Kewajiban';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum([Mutasi_Debet])-sum([Mutasi_Kredit])) nilai from TB_SPE_DETAIL_NERACA where akun_akrual_1='2' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                break;

            default:
                // case 'mutasiekuitas' :
                $query = $this->Mneraca->mutasiekuitas($nm_unit);
                $view = 'neraca/mutasiaset';
                $title = 'Ekuitas';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum([Mutasi_Debet])-sum([Mutasi_Kredit])) nilai from TB_SPE_DETAIL_NERACA where akun_akrual_1='3' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                break;
        }

        $data = array(
            'title' => $title,
            'neraca_data' => $query,
            'nm_unit' => $nm_unit,
            'kembali' => base_url() . 'neraca',
            'cetak' => base_url() . 'neraca/cetakdetail?jenis=' . urlencode($jenis) . '&nm_unit=' . urlencode($nm_unit),
            'akun' => $label->akun_akrual_1 . ' - ' . $label->nm_akrual_1 . ' (Rp.' . number_format($label->nilai, 0, '', '.') . ')'
        );
        $this->template->load('layout', $view, $data);
    }

    function cetakdetail()
    {
        $akses  = $this->cekAkses('read');
        $jenis  = urldecode($this->input->get('jenis', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));

        switch ($jenis) {
            case 'saldoawalaset':
                $query = $this->Mneraca->saldoawalaset($nm_unit);
                $view = 'neraca/saldoawalaset';
                $title = 'Saldo Awal Aset';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum(saldo_awal_debet)-sum(saldo_awal_kredit)) nilai from tb_spe_saldo_awal where akun_akrual_1='1' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                break;

            case 'mutasiaset':
                $query = $this->Mneraca->mutasiAset($nm_unit);
                $view = 'neraca/mutasiaset';
                $title = 'Mutasi Aset';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum([Mutasi_Debet])-sum([Mutasi_Kredit])) nilai from TB_SPE_DETAIL_NERACA where akun_akrual_1='1' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                break;

            case 'saldoawalkewajiban':
                $query = $this->Mneraca->saldoawalKewajiban($nm_unit);
                $view = 'neraca/saldoawalkewajiban';
                $title = 'Saldo Awal Kewajiban';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum(saldo_awal_debet)-sum(saldo_awal_kredit)) nilai from tb_spe_saldo_awal where akun_akrual_1='2' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                break;

            case 'mutasikewajiban':
                $query = $this->Mneraca->mutasikewajiban($nm_unit);
                $view = 'neraca/mutasiaset';
                $title = 'Mutasi Kewajiban';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum([Mutasi_Debet])-sum([Mutasi_Kredit])) nilai from TB_SPE_DETAIL_NERACA where akun_akrual_1='2' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                break;

            default:
                // case 'mutasiekuitas' :
                $query = $this->Mneraca->mutasiekuitas($nm_unit);
                $view = 'neraca/mutasiaset';
                $title = 'Ekuitas';
                $label = $this->db->query("SELECT akun_akrual_1 ,nm_akrual_1 ,abs(sum([Mutasi_Debet])-sum([Mutasi_Kredit])) nilai from TB_SPE_DETAIL_NERACA where akun_akrual_1='3' and nm_unit='$nm_unit' group by nm_unit ,akun_akrual_1 ,nm_akrual_1 ")->row();
                break;
        }

        $data = array(
            'title'      => $title,
            'neraca_data' => $query,
            'nm_unit'    => $nm_unit,
            'akun'       => $label->akun_akrual_1 . ' - ' . $label->nm_akrual_1 . ' (Rp.' . number_format($label->nilai, 0, '', '.') . ')'
        );
        $this->load->view($view, $data);
        $this->load->view('neraca/script_cetak');
    }

    /* public function cetak_index()
    {
        $akses =$this->cekAkses('read');
        $q2     = urldecode($this->input->get('q2', TRUE));
        $q     = urldecode($this->input->get('q', TRUE));
        $tanggal1      =urldecode($this->input->get('tanggal1',true));
        $tanggal2      =urldecode($this->input->get('tanggal2',true));
        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $neraca                      = $this->Mneraca->getAllData($q,$q2);
        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $total=$this->Mneraca->getSum($q,$q2);
        $data = array(
            'neraca_data' => $neraca,
            'debet'=>number_format($total->debet,'2',',','.'),
            'kredit'=>number_format($total->kredit,'2',',','.'),
        );
        $this->load->view('neraca/cetak_index',$data);
    }
    public function cetak_index2()
    {
        $akses =$this->cekAkses('read');
        $q2     = urldecode($this->input->get('q2', TRUE));
        $q     = urldecode($this->input->get('q', TRUE));
        $tanggal1      =urldecode($this->input->get('tanggal1',true));
        $tanggal2      =urldecode($this->input->get('tanggal2',true));
        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $neraca                      = $this->Mneraca->getAllData($q,$q2);
        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $total=$this->Mneraca->getSum($q,$q2);
        $data = array(
            'neraca_data' => $neraca,
            'debet'=>number_format($total->debet,'2',',','.'),
            'kredit'=>number_format($total->kredit,'2',',','.'),
        );
        $this->load->view('neraca/cetak_index2',$data);
    }
    public function read()
    {
         $akses         =$this->cekAkses('read');
         $q             = urldecode($this->input->get('q', TRUE));
         $start         = intval($this->input->get('start'));
         $kd_skpd       =urldecode($this->input->get('kd_skpd',true));
         $nm_sub_unit   =urldecode($this->input->get('nm_sub_unit',true));

         $akun_akrual_5 =urldecode($this->input->get('akun_akrual_5',true));
         $tanggal1      =urldecode($this->input->get('tanggal1',true));
         $tanggal2      =urldecode($this->input->get('tanggal2',true));

        if($tanggal1==''){
            $tanggal1='01/01/'.date('Y');
        }

        if($tanggal2==''){
            $tanggal2=date('d/m/Y');
        }

        if ($q <> '' || $akun_akrual_5<>''|| $tanggal1 <>'' || $tanggal2 <>'') {

            $config['base_url']  = base_url() . 'neraca/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_5='.urlencode($akun_akrual_5).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2).'&q='.urlencode($q);
            $config['first_url'] = base_url() . 'neraca/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_5='.urlencode($akun_akrual_5).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2).'&q='.urlencode($q);
            $cetak = base_url() . 'neraca/cetak?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_5='.urlencode($akun_akrual_5).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2).'&q='.urlencode($q);
            $cetak2 = base_url() . 'neraca/cetak2?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&akun_akrual_5='.urlencode($akun_akrual_5).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2).'&q='.urlencode($q);
        } else {
            $config['base_url']  = base_url() . 'neraca/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit);
            $config['first_url'] = base_url() . 'neraca/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit);
            $cetak = base_url() . 'neraca/cetak?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit);
            $cetak2 = base_url() . 'neraca/cetak2?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit);
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        if ($akun_akrual_5 <>'') {
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $config['total_rows'] = $this->Mneraca->total_rows($kd_skpd,$nm_sub_unit,$q);
        if ($akun_akrual_5 <>'') {
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }

        $neraca = $this->Mneraca->get_limit_data($kd_skpd,$nm_sub_unit,$config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $akrual5=$this->db->query("select distinct nm_akrual_5,akun_akrual_5
                                    from tb_spe_detail_neraca
                                    where kd_skpd='$kd_skpd' and nm_sub_unit='$nm_sub_unit'")->result();
         if ($akun_akrual_5 <>'') {
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }

        $total=$this->Mneraca->getSum($kd_skpd,$nm_sub_unit,$q);
        $data = array(
            'neraca_data'   => $neraca,
            'q'             => $q,
            'pagination'    => $this->pagination->create_links(),
            'total_rows'    => $config['total_rows'],
            'start'         => $start,
            'title'         => 'Neraca',
            'akses'         => $akses,
            'kd_skpd'       =>$kd_skpd,
            'nm_sub_unit'   =>$nm_sub_unit,
            'action'        =>base_url() . 'neraca/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit),
            'kembali'       =>base_url() . 'neraca',
            'akrual5'       =>$akrual5,

            'akun_akrual_5' =>$akun_akrual_5,
            'tanggal1' =>$tanggal1,
            'tanggal2' =>$tanggal2,
             'debet'=>number_format($total->debet,'2',',','.'),
            'kredit'=>number_format($total->kredit,'2',',','.'),
            'cetak'             =>$cetak,
            'cetak2'             =>$cetak2

        );
        $this->template->load('layout','neraca/view_indexdua',$data);

    }

    public function cetak()
    {
         $akses         =$this->cekAkses('read');
         $q             = urldecode($this->input->get('q', TRUE));
         $start         = intval($this->input->get('start'));
         $kd_skpd       =urldecode($this->input->get('kd_skpd',true));
         $nm_sub_unit   =urldecode($this->input->get('nm_sub_unit',true));

         $akun_akrual_5 =urldecode($this->input->get('akun_akrual_5',true));
         $tanggal1      =urldecode($this->input->get('tanggal1',true));
         $tanggal2      =urldecode($this->input->get('tanggal2',true));
        if ($akun_akrual_5 <>'') {
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }

        $neraca = $this->Mneraca->get_all_data($kd_skpd,$nm_sub_unit,$q);
         if ($akun_akrual_5 <>'') {
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $total=$this->Mneraca->getSum($kd_skpd,$nm_sub_unit,$q);
        $data = array(
            'neraca_data'   => $neraca,
             'debet'=>number_format($total->debet,'2',',','.'),
            'kredit'=>number_format($total->kredit,'2',',','.'),
            'kd_skpd'       => $kd_skpd,
            'nm_sub_unit'   => $nm_sub_unit,

        );
        $this->load->view('neraca/excel',$data);

    }

    public function cetak2()
    {
         $akses         =$this->cekAkses('read');
         $q             = urldecode($this->input->get('q', TRUE));
         $start         = intval($this->input->get('start'));
         $kd_skpd       =urldecode($this->input->get('kd_skpd',true));
         $nm_sub_unit   =urldecode($this->input->get('nm_sub_unit',true));

         $akun_akrual_5 =urldecode($this->input->get('akun_akrual_5',true));
         $tanggal1      =urldecode($this->input->get('tanggal1',true));
         $tanggal2      =urldecode($this->input->get('tanggal2',true));
        if ($akun_akrual_5 <>'') {
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }

        $neraca = $this->Mneraca->get_all_data($kd_skpd,$nm_sub_unit,$q);
         if ($akun_akrual_5 <>'') {
            $this->db->where('akun_akrual_5',$akun_akrual_5);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];
            $this->db->where("(tgl_bukti between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $total=$this->Mneraca->getSum($kd_skpd,$nm_sub_unit,$q);
        $data = array(
            'neraca_data'   => $neraca,
             'debet'=>number_format($total->debet,'2',',','.'),
            'kredit'=>number_format($total->kredit,'2',',','.'),
            'kd_skpd'       => $kd_skpd,
            'nm_sub_unit'   => $nm_sub_unit,

        );
        $this->load->view('neraca/excel2',$data);

    }*/
}

/* End of file Neraca.php */
/* Location: ./application/controllers/Neraca.php */

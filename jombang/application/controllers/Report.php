<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Report extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->load->model('Mreport');
    }

    private function cekAkses($var = null)
    {
        $url = 'Report';
        return cek($this->id_pengguna, $url, $var);
    }

    private function subunit(){
        return $this->db->query("select kd_skpd,nm_sub_unit from SPE_REF_SUB_UNIT")->result();
    }

    public function index()
    {
        $akses = $this->cekAkses('read');
        $data = array(
            'data' => $this->Mreport->getList(),
            'title' => 'Report'
        );
        $this->template->load('layout', 'report/view_index', $data);
    }

    public function limaduatigakurangsejuta()
    {

        $akses=cek($this->id_pengguna,'limaduatigakurangsejuta','read');
        // $akses = $this->cekAkses('read');
        $q=urldecode($this->input->get('q'));
        if($q<>''){
            $this->db->where('kd_skpd',$q);
        }
        $res=$this->Mreport->getLimaDuaDua();
        $data  = array(
            'data'    => $res,
            'title'   => 'Belanja Modal kurang dari 1 juta',
            'script'  => 'report/datatables',
            'cetak'   => base_url() . 'report/cetak_limaduatiga',
            'subunit'=>$this->subunit(),
            'q'=>$q,
            'action'=>'limaduatigakurangsejuta',
        );
        $this->template->load('layout', 'report/view_limaduatiga', $data);
    }
    function cetak_limaduatiga()
    {
        $akses = $this->cekAkses('read');
        $data = array(
            'data'    => $this->Mreport->getLimaDuaDua(),
            'start'     => 0
        );
        $this->load->view('report/cetak_limaduatiga', $data);
    }
    function limaduadualebihtigapuluh()
    {
        $akses=cek($this->id_pengguna,'limaduadualebihtigapuluh','read');
        $q=urldecode($this->input->get('q'));

        if($q<>''){
            $this->db->where('kd_skpd',$q);
        }
        $res=$this->Mreport->getLimaDuaTiga();
        $data  = array(
            'subunit'=>$this->subunit(),
            'q'=>$q,
            'action'=>'limaduadualebihtigapuluh',
            'data' => $res,
            'title' => 'Belanja Barang dan Jasa Lebih dari 30 jt',
            'script' => 'report/datatables',
            'cetak'   => base_url() . 'report/cetak_limaduadua',

        );
        $this->template->load('layout', 'report/view_limaduadua', $data);
    }
    function cetak_limaduadua()
    {
        $akses = $this->cekAkses('read');
        $data = array(
            'data' => $this->Mreport->getLimaDuaTiga(),
            'start'     => 0
        );
        $this->load->view('report/cetak_limaduadua', $data);
    }
    function detailLimaDuaDua()
    {
        $kd_skpd = $this->input->get('kd_skpd', true);
        $nm_sub_unit = $this->input->get('nm_sub_unit', true);
        $kode_rek_3 = $this->input->get('kode_rek_3', true);
        $no_sp2d = $this->input->get('no_sp2d', true);

        $data = array(
            'title' => "Belanja Barang dan Jasa Lebih dari 30 jt ( " . $nm_sub_unit . " )",
            'data' => $this->Mreport->getDetailDuaTiga($kd_skpd, $kode_rek_3, $no_sp2d),
            'script' => 'report/datatables',
            'kembali' => base_url() . 'limaduadualebihtigapuluh',
            'cetak' => base_url() . 'report/cetak_detailLimaDuaDua?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&kode_rek_3=' . urlencode($kode_rek_3)
        );
        $this->template->load('layout', 'report/view_detaillimaduadua', $data);
    }
    function cetak_detailLimaDuaDua()
    {
        $akses = $this->cekAkses('read');
        $kd_skpd = $this->input->get('kd_skpd', true);
        $nm_sub_unit = $this->input->get('nm_sub_unit', true);
        $kode_rek_3 = $this->input->get('kode_rek_3', true);
        $no_sp2d = $this->input->get('no_sp2d', true);
        $data = array(
            'data' => $this->Mreport->getDetailDuaTiga($kd_skpd, $kode_rek_3, $no_sp2d),
            'start'     => 0
        );
        $this->load->view('report/cetak_detailLimaDuaDua', $data);
    }

    public function sp2dyangbelumspj()
    {

     $akses=cek($this->id_pengguna,'sp2dtuup','read');
        $data  = array(
            'data'    => $this->Mreport->getsp2dyangbelumspj(),
            'title'   => 'SP2D UP & TU',
            'script'  => 'report/datatables',
            'kembali' => base_url() . 'report',
            'cetak'   => base_url() . 'report/cetak_sp2dyangbelumspj',
        );
        $this->template->load('layout', 'report/view_sp2dbelumspj', $data);
    }
    function cetak_sp2dyangbelumspj()
    {
        $akses = $this->cekAkses('read');
        $data = array(
            'data'    => $this->Mreport->getsp2dyangbelumspj(),
            'start'     => 0
        );
        $this->load->view('report/cetak_sp2dyangbelumspj', $data);
    }

    function sp2dakrual()
    {
        $akses = $this->cekAkses('read');
        $data = array(
            'data'  => $this->Mreport->sp2dAkrual(),
            'script' => 'report/datatables',
            'title' => 'Monitoring Akun pada SP2D'
        );

        $this->template->load('layout', 'report/view_sp2dakrual', $data);
    }

    function detailakrualsp2d()
    {
        $q     = urldecode($this->input->get('q', true));
        $nm_unit     = urldecode($this->input->get('nm_unit', true));

        $akses = $this->cekAkses('read');
        $data = array(
            'data'    => $this->Mreport->sp2dAkrualdetail($q, $nm_unit),
            'script'  => 'report/datatables',
            'title'   => 'Monitoring Akun pada SP2D',
            'action'  => base_url() . 'report/detailakrualsp2d?q=' . urlencode($q),
            'kembali' => 'report/sp2dakrual',
            'subunit'     => $this->db->query("select distinct kode_unit kd_skpd,nm_unit
                                        from tb_spe_sp2d
                                        where kd_rek_gabung='$q'
                                        order by nm_unit")->result(),
            'q' => $q,
            'nm_unit' => $nm_unit
        );

        $this->template->load('layout', 'report/view_sp2dakrualdetail', $data);
    }
}

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Spd extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->load->model('Mspd');
        $this->load->library('form_validation');
    }

    private function cekAkses($var = null)
    {
        $url = 'Spd';
        return cek($this->id_pengguna, $url, $var);
    }

    public function index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($tanggal1 == '') {
            $tanggal1 = '01/01/' . date('Y');
        }

        if ($tanggal2 == '') {
            $tanggal2 = date('d/m/Y');
        }
        if ($q <> '' || $tanggal1 <> '' || $tanggal2 <> '') {
            $config['base_url']  = base_url() . 'spd?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'spd?q=' . urlencode($q);
            $cetak               = base_url() . 'spd/cetak_index?q=' . urlencode($q) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2);
            $cetak2              = base_url() . 'spd/cetak_index2?q=' . urlencode($q) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2);
        } else {
            $config['base_url']  = base_url() . 'spd';
            $config['first_url'] = base_url() . 'spd';
            $cetak               = base_url() . 'spd/cetak_index';
            $cetak2               = base_url() . 'spd/cetak_index2';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_spd between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $config['total_rows']        = $this->Mspd->gettotal($q);
        // echo $this->db->last_query();
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_spd between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $spd                      = $this->Mspd->getAlldata($q);
        // echo $this->db->last_query();


        $this->load->library('pagination');
        $this->pagination->initialize($config);
        // $total=$this->Mspd->getSumNilai($q);
        $subunit = $this->Mspd->getSUbunit();


        /* echo "<pre>";
        print_r($spd);
        echo "</pre>";
        die();
*/
        $data = array(
            'spd_data'      => $spd,
            'q'             => $q,
            // 'pagination' => $this->pagination->create_links(),
            // 'total_rows' => $config['total_rows'],
            'start'         => $start,
            'title'         => 'Surat Penyediaan Dana (SPD)',
            'akses'         => $akses,
            'subunit'       => $subunit,
            'cetak'         => $cetak,
            'cetak2'        => $cetak2,
            // 'jum_nilai'  =>number_format($total->jum_nilai,'0',',','.'),
            'tanggal1'      => $tanggal1,
            'tanggal2'      => $tanggal2,
            'script'        => 'report/datatables'
        );
        $this->template->load('layout', 'spd/view_index', $data);
    }

    public function cetak_index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_spd between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $spd                      = $this->Mspd->getAlldata($q);
        $total = $this->Mspd->getSumNilai($q);
        $data = array(
            'spd_data' => $spd,
            'jum_nilai'             => number_format($total->jum_nilai, '0', ',', '.'),
        );
        $this->load->view('spd/cetak_index', $data);
    }

    public function cetak_index2()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_spd between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $spd                      = $this->Mspd->getAlldata($q);
        $total = $this->Mspd->getSumNilai($q);
        $data = array(
            'spd_data' => $spd,
            'jum_nilai'             => number_format($total->jum_nilai, '0', ',', '.'),
        );
        $this->load->view('spd/cetak_index2', $data);
    }
    public function read()
    {
        $akses       = $this->cekAkses('read');
        $kd_skpd     = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit     = urldecode($this->input->get('nm_unit', true));
        $start       = intval($this->input->get('start'));
        $tanggal1    = urldecode($this->input->get('tanggal1', true));
        $tanggal2    = urldecode($this->input->get('tanggal2', true));
        $q           = urldecode($this->input->get('q', TRUE));

        if ($q <> '' || $tanggal1 <> '' || $tanggal2 <> '') {
            /*$config['base_url']  = base_url() . 'spd/read?kd_skpd=' . urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&nm_unit=' . urlencode($nm_unit);
            $config['first_url'] = base_url() . 'spd/read?kd_skpd=' . urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&nm_unit=' . urlencode($nm_unit);*/
            $cetak               = base_url() . 'spd/excel?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2);
            $cetak2               = base_url() . 'spd/excel2?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2);
        } else {
            /*   $config['base_url']  = base_url() . 'spd/read?kd_skpd=' . urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit);
            $config['first_url'] = base_url() . 'spd/read?kd_skpd=' . urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit);*/
            $cetak               = base_url() . 'spd/excel?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit);
            $cetak2               = base_url() . 'spd/excel2?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit);
        }

        $config['per_page']          = 1000;
        $config['page_query_string'] = TRUE;

        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_spd between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $config['total_rows']        = $this->Mspd->total_rows($kd_skpd, $nm_sub_unit, $nm_unit, $q);

        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_spd between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $spd = $this->Mspd->get_limit_data($config['per_page'], $start, $kd_skpd, $nm_sub_unit, $nm_unit, $q);

        /*$this->load->library('pagination');
        $this->pagination->initialize($config);*/

        $data = array(
            'spd_data'     => $spd,
            'q'            => $q,
            /*'pagination' => $this->pagination->create_links(),
            'total_rows'   => $config['total_rows'],*/
            'start'        => $start,
            'title'        => 'Surat Penyediaan Dana (SPD)',
            'akses'        => $akses,
            'kd_skpd'      => $kd_skpd,
            'nm_unit'      => $nm_unit,
            'nm_sub_unit'  => $nm_sub_unit,
            'kembali'      => base_url() . 'spd',
            'cetak'        => $cetak,
            'cetak2'       => $cetak2,
            'tanggal1'     => $tanggal1,
            'tanggal2'     => $tanggal2,
            'script'       => 'report/datatables'
        );
        $this->template->load('layout', 'spd/view_indexdua', $data);
    }


    function excel()
    {
        $akses = $this->cekAkses('read');
        $kd_skpd = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_spd between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $spd = $this->Mspd->get_all_data($kd_skpd, $nm_sub_unit, $nm_unit);
        $data = array(
            'spd_data'    => $spd,
            'nm_unit'   => $nm_unit,
            'kd_skpd'   => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'tanggal1'           => $tanggal1,
            'tanggal2'           => $tanggal2,
        );
        $this->load->view('spd/excel', $data);
    }
    function excel2()
    {
        $akses = $this->cekAkses('read');
        $kd_skpd = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_spd between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $spd = $this->Mspd->get_all_data($kd_skpd, $nm_sub_unit, $nm_unit);
        $data = array(
            'spd_data'    => $spd,
            'nm_unit'   => $nm_unit,
            'kd_skpd'   => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
        );
        $this->load->view('spd/excel2', $data);
    }
    function readdua()
    {
        $akses      = $this->cekAkses('read');
        $kd_skpd    = urldecode($this->input->get("kd_skpd", true));
        $nm_sub_unit = urldecode($this->input->get("nm_sub_unit", true));
        $nm_unit    = urldecode($this->input->get("nm_unit", true));
        $no_spd     = urldecode($this->input->get("no_spd", true));
        $start      = intval($this->input->get('start'));
        $tanggal1   = urldecode($this->input->get('tanggal1', true));
        $tanggal2   = urldecode($this->input->get('tanggal2', true));

        $spd   = $this->Mspd->getLevelDua($kd_skpd, $nm_sub_unit, $nm_unit, $no_spd);
        $cetak = base_url() . 'spd/exceldua?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&no_spd=' . urlencode($no_spd);
        $cetak2 = base_url() . 'spd/exceldua2?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&no_spd=' . urlencode($no_spd);

        $spda = $this->db->query("select distinct tgl_spd,no_spd 
                                from tb_spe_spd
                                where no_spd='$no_spd'")->row();

        $data = array(
            'spd_data'   => $spd,
            'start'      => $start,
            'title'      => 'Surat Penyediaan Dana (SPD)',
            'kd_skpd'    => $kd_skpd,
            'nm_unit'    => $nm_unit,
            'nm_sub_unit' => $nm_sub_unit,
            'no_spd'     => $no_spd,
            'kembali'    => base_url() . 'spd/read?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2),
            'script'     => 'report/datatables',
            'cetak'      => $cetak,
            'cetak2'     => $cetak2,
            'spd'        => $spda
        );

        $this->template->load('layout', 'spd/view_indextiga', $data);
    }

    function exceldua()
    {
        $akses = $this->cekAkses('read');
        $kd_skpd = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $no_spd = urldecode($this->input->get('no_spd', true));
        $spd = $this->Mspd->getLevelDua($kd_skpd, $nm_sub_unit, $nm_unit, $no_spd);
        $data = array(
            'spd_data'    => $spd,
            'nm_unit'     => $nm_unit,
            'kd_skpd'     => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'no_spd'       => $no_spd,
        );
        $this->load->view('spd/exceldua', $data);
    }
    function exceldua2()
    {
        $akses = $this->cekAkses('read');
        $kd_skpd = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $no_spd = urldecode($this->input->get('no_spd', true));
        $spd = $this->Mspd->getLevelDua($kd_skpd, $nm_sub_unit, $nm_unit, $no_spd);
        $data = array(
            'spd_data'    => $spd,
            'nm_unit'     => $nm_unit,
            'kd_skpd'     => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'no_spd'       => $no_spd,
        );
        $this->load->view('spd/exceldua2', $data);
    }
}

/* End of file Spd.php */
/* Location: ./application/controllers/Spd.php */

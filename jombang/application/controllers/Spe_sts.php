<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Spe_sts extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->load->model('MSpe_sts');
        $this->load->library('form_validation');
    }

    private function cekAkses($var = null)
    {
        $url = 'Spe_sts';
        return cek($this->id_pengguna, $url, $var);
    }

    public function index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($tanggal1 == '') {
            $tanggal1 = '01/01/' . date('Y');
        }

        if ($tanggal2 == '') {
            $tanggal2 = date('d/m/Y');
        }
        if ($q <> '' || $tanggal1 <> '' || $tanggal2 <> '') {
            $cetak = base_url() . 'spe_sts/cetak_index?q=' . urlencode($q) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2);
            $cetak2 = base_url() . 'spe_sts/cetak_index2?q=' . urlencode($q) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2);
        } else {
            $cetak = base_url() . 'spe_sts/cetak_index';
            $cetak2 = base_url() . 'spe_sts/cetak_index2';
        }
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sts between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $spe_sts                     = $this->MSpe_sts->getalldata($q);
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sts between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }


        $subunit = $this->MSpe_sts->getSubunit();
        $data = array(
            'spe_sts_data' => $spe_sts,
            'q'            => $q,
            'start'        => $start,
            'title'        => 'Surat Tanda Setoran (STS)',
            'akses'        => $akses,
            'subunit'      => $subunit,
            'cetak'        => $cetak,
            'script'       => 'report/datatables',
            'cetak2'       => $cetak2,
            // 'nilai'        =>number_format($total->nilai,'0',',','.'),
            'tanggal1'     => $tanggal1,
            'tanggal2'     => $tanggal2,

        );
        $this->template->load('layout', 'spe_sts/view_index', $data);
    }

    public function cetak_index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sts between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $spe_sts                      = $this->MSpe_sts->getalldata($q);
        $subunit = $this->MSpe_sts->getSubunit();
        $total = $this->MSpe_sts->getSum($q);
        $data = array(
            'spe_sts_data' => $spe_sts,
            'nilai'        => number_format($total->nilai, '0', ',', '.'),

        );
        $this->load->view('spe_sts/cetak_index', $data);
    }

    public function cetak_index2()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sts between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $spe_sts                      = $this->MSpe_sts->getalldata($q);
        $subunit = $this->MSpe_sts->getSubunit();
        $total = $this->MSpe_sts->getSum($q);
        $data = array(
            'spe_sts_data' => $spe_sts,
            'nilai'        => number_format($total->nilai, '0', ',', '.'),
        );
        $this->load->view('spe_sts/cetak_index2', $data);
    }

    function readsetengah()
    {
        $akses       = $this->cekAkses('read');
        // $res=$this->input->get();
        $kd_skpd = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));

        $start       = intval($this->input->get('start', true));
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sts between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $sts = $this->MSpe_sts->getListRektiga($kd_skpd, $nm_unit, $nm_sub_unit, $tt1, $tt2);
        $cetak = base_url() . 'spe_sts/cetak_setengah?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2);
        $cetak2 = base_url() . 'spe_sts/cetak_setengah2?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2);
        $data = array(
            'title'       => 'Surat Tanda Setoran (STS)',
            'akses'       => $akses,
            'data'        => $sts,
            'kd_skpd'     => $kd_skpd,
            'nm_unit'     => $nm_unit,
            'nm_sub_unit' => $nm_sub_unit,
            'kembali'     => base_url() . 'spe_sts',
            'start'       => $start,
            'script' => 'report/datatables',
            'cetak'         => $cetak,
            'cetak2'         => $cetak2,
            'tanggal1'           => $tanggal1,
            'tanggal2'           => $tanggal2,
        );
        $this->template->load('layout', 'spe_sts/view_indexsetengah', $data);
    }
    public function cetak_setengah()
    {
        $akses = $this->cekAkses('read');
        $kd_skpd = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));

        $start       = intval($this->input->get('start', true));
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sts between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $sts = $this->MSpe_sts->getListRektiga($kd_skpd, $nm_unit, $nm_sub_unit, $tt1, $tt2);
        $data = array(
            'spe_sts_data' => $sts,
            'nm_unit'   => $nm_unit,
            'kd_skpd'   => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'tanggal1'           => $tanggal1,
            'tanggal2'           => $tanggal2,
        );
        $this->load->view('spe_sts/cetak_setengah', $data);
    }
    public function cetak_setengah2()
    {
        $akses = $this->cekAkses('read');
        $kd_skpd = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));

        $start       = intval($this->input->get('start', true));
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sts between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $sts = $this->MSpe_sts->getListRektiga($kd_skpd, $nm_unit, $nm_sub_unit, $tt1, $tt2);
        $data = array(
            'spe_sts_data' => $sts,
            'nm_unit'   => $nm_unit,
            'kd_skpd'   => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
        );
        $this->load->view('spe_sts/cetak_setengah2', $data);
    }
    public function read()
    {
        $akses      = $this->cekAkses('read');
        $start      = intval($this->input->get('start'));
        $kd_skpd    = urldecode($this->input->get("kd_skpd", true));
        $nm_sub_unit = urldecode($this->input->get("nm_sub_unit", true));
        $nm_unit    = urldecode($this->input->get("nm_unit", true));
        $no_sts     = urldecode($this->input->get("no_sts", true));
        $tanggal1   = urldecode($this->input->get('tanggal1', true));
        $tanggal2   = urldecode($this->input->get('tanggal2', true));
        $cetak      = base_url() . 'spe_sts/cetak?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&no_sts=' . urlencode($no_sts);
        $cetak2     = base_url() . 'spe_sts/cetak2?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&no_sts=' . urlencode($no_sts);
        $spe_sts    = $this->MSpe_sts->get_limit_data($kd_skpd, $nm_sub_unit, $nm_unit, $no_sts);

        $sts = $this->db->query("select distinct no_sts,tgl_sts,ket_program,ket_kegiatan,keterangan
                                from tb_spe_sts
                                where no_sts='$no_sts'")->row();

        $data = array(
            'spe_sts_data' => $spe_sts,
            'start'       => $start,
            'title'       => 'Surat Tanda Setoran (STS)',
            'kd_skpd'     => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'nm_unit'     => $nm_unit,
            'no_sts'      => $no_sts,
            'kembali'     => base_url() . 'spe_sts/readsetengah?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2),
            'script'      => 'report/datatables',
            'cetak'       => $cetak,
            'cetak2'      => $cetak2,
            'sts' => $sts
        );
        $this->template->load('layout', 'spe_sts/view_indexdua', $data);
    }

    function cetak()
    {
        $akses = $this->cekAkses('read');
        $kd_skpd = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $no_sts = urldecode($this->input->get('no_sts', true));
        $spe_sts = $this->MSpe_sts->get_limit_data($kd_skpd, $nm_sub_unit, $nm_unit, $no_sts);
        $data = array(
            'spe_sts_data' => $spe_sts,
            'nm_unit'     => $nm_unit,
            'kd_skpd'     => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'no_sts'       => $no_sts,
        );
        $this->load->view('spe_sts/cetak_excel', $data);
    }
    function cetak2()
    {
        $akses = $this->cekAkses('read');
        $kd_skpd = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $no_sts = urldecode($this->input->get('no_sts', true));
        $spe_sts = $this->MSpe_sts->get_limit_data($kd_skpd, $nm_sub_unit, $nm_unit, $no_sts);
        $data = array(
            'spe_sts_data' => $spe_sts,
            'nm_unit'     => $nm_unit,
            'kd_skpd'     => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'no_sts'       => $no_sts,
        );
        $this->load->view('spe_sts/cetak_excel2', $data);
    }
}

/* End of file tb_spe_sts.php */
/* Location: ./application/controllers/Spe_sts.php */

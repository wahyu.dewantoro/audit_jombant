<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Spp extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->load->model('Mspp');
        $this->load->library('form_validation');
    }

    private function cekAkses($var = null)
    {
        $url = 'Spp';
        return cek($this->id_pengguna, $url, $var);
    }

    public function index()
    {
        $akses    = $this->cekAkses('read');
        $q        = urldecode($this->input->get('q', TRUE));
        $start    = intval($this->input->get('start'));
        $tanggal1 = urldecode($this->input->get('tanggal1', true));
        $tanggal2 = urldecode($this->input->get('tanggal2', true));

        if ($tanggal1 == '') {
            $tanggal1 = '01/01/' . date('Y');
        }

        if ($tanggal2 == '') {
            $tanggal2 = date('d/m/Y');
        }
        if ($q <> '' || $tanggal1 <> '' || $tanggal2 <> '') {
            $cetak               = base_url() . 'spp/cetak_index?q=' . urlencode($q) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2);
            $cetak2               = base_url() . 'spp/cetak_index2?q=' . urlencode($q) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2);
        } else {
            $cetak               = base_url() . 'spp/cetak_index';
            $cetak2               = base_url() . 'spp/cetak_index2';
        }
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_spp between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $spp     = $this->Mspp->getAllData($q);
        $subunit = $this->Mspp->getSUbunit();
        $data = array(
            'spp_data' => $spp,
            'q'        => $q,
            'start'    => $start,
            'title'    => 'Surat Permintaan Pembayaran (SPP)',
            'akses'    => $akses,
            'subunit'  => $subunit,
            'cetak'    => $cetak,
            'cetak2'   => $cetak2,
            'tanggal1' => $tanggal1,
            'tanggal2' => $tanggal2,
            'script' => 'report/datatables'
        );
        $this->template->load('layout', 'spp/view_index', $data);
    }

    public function cetak_index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_spp between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $spp                      = $this->Mspp->getAllData($q);

        $data = array(
            'spp_data' => $spp,
            'q'        => $q,
            'start'    => $start,
            'title'    => 'Surat Permintaan Pembayaran (SPP)',

        );

        $this->load->view('spp/cetak_index', $data);
    }

    public function cetak_index2()
    {
        $akses = $this->cekAkses('read');
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_spp between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $spp                      = $this->Mspp->getAllData($q);

        $data = array(
            'spp_data' => $spp,
            'q'        => $q,
            'start'    => $start,
            'title'    => 'Surat Permintaan Pembayaran (SPP)',

        );

        $this->load->view('spp/cetak_index2', $data);
    }
    public function read()
    {
        $akses   = $this->cekAkses('read');

        $nm_unit    = urldecode($this->input->get('nm_unit', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $kd_skpd    = urldecode($this->input->get('kd_skpd', true));

        $tanggal1   = urldecode($this->input->get('tanggal1', true));
        $tanggal2   = urldecode($this->input->get('tanggal2', true));
        $q          = urldecode($this->input->get('q'));

        if ($q <> '') {
            $this->db->where('jenis_spp', $q);
        }
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_spp between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $spp   = $this->Mspp->get_limit_data($nm_unit, $nm_sub_unit, $kd_skpd);
        $start = intval($this->input->get('start'));
        $cetak = base_url() . 'spp/cetakdua?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&q=' . urlencode($q) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2);
        $cetak2 = base_url() . 'spp/cetakdua2?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&q=' . urlencode($q) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2);

        $data  = array(
            'spp_data'   => $spp,
            'title'      => 'Surat Permintaan Pembayaran (SPP)',
            'akses'      => $akses,
            'nm_unit'    => $nm_unit,
            'kd_skpd'    => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'kembali'    => base_url() . 'spp',
            'start'      => $start,
            'script'     => 'report/datatables',
            'cetak'      => $cetak,
            'cetak2'     => $cetak2,
            'tanggal1'   => $tanggal1,
            'tanggal2'   => $tanggal2,
        );
        $this->template->load('layout', 'spp/view_indexdua', $data);
    }

    function cetakdua()
    {
        $akses      = $this->cekAkses('read');
        $kd_skpd    = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit    = urldecode($this->input->get('nm_unit', true));
        $tanggal1   = urldecode($this->input->get('tanggal1', true));
        $tanggal2   = urldecode($this->input->get('tanggal2', true));
        $q          = $this->input->get('q');

        if ($q <> '') {
            $this->db->where('jenis_spp', $q);
        }
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_spp between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $spp     = $this->Mspp->get_limit_data($nm_unit, $nm_sub_unit, $kd_skpd);
        $data = array(
            'spp_data'  => $spp,
            'nm_unit'   => $nm_unit,
            'kd_skpd'   => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'tanggal1'           => $tanggal1,
            'tanggal2'           => $tanggal2,
        );

        $this->load->view('spp/cetak_indexdua', $data);
    }

    function cetakdua2()
    {
        $akses   = $this->cekAkses('read');
        $kd_skpd = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        $q = $this->input->get('q');

        if ($q <> '') {
            $this->db->where('jenis_spp', $q);
        }
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_spp between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $spp     = $this->Mspp->get_limit_data($nm_unit, $nm_sub_unit, $kd_skpd);
        $data = array(
            'spp_data'  => $spp,
            'nm_unit'   => $nm_unit,
            'kd_skpd'   => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit
        );

        $this->load->view('spp/cetak_indexdua2', $data);
    }

    function readdua()
    {
        $akses      = $this->cekAkses('read');
        $nm_unit    = urldecode($this->input->get('nm_unit', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $kd_skpd    = urldecode($this->input->get('kd_skpd', true));
        $no_spp     = urldecode($this->input->get('no_spp', true));
        $start      = intval($this->input->get('start'));
        $tanggal1   = urldecode($this->input->get('tanggal1', true));
        $tanggal2   = urldecode($this->input->get('tanggal2', true));
        $cetak      = base_url() . 'spp/cetaktiga?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&no_spp=' . urlencode($no_spp);
        $cetak2     = base_url() . 'spp/cetaktiga2?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&no_spp=' . urlencode($no_spp);

        $spp        = $this->Mspp->getdetailspp($nm_unit, $nm_sub_unit, $kd_skpd, $no_spp);

        $rspp = $this->db->query("select distinct no_spp,tgl_spp,ket_program,ket_kegiatan
                                from tb_spe_spp
                                where no_spp='$no_spp'")->row();
        $data = array(
            'spp_data'   => $spp,
            'title'      => 'Surat Permintaan Pembayaran (SPP)',
            'akses'      => $akses,
            'nm_unit'    => $nm_unit,
            'kd_skpd'    => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'no_spp'     => $no_spp,
            'kembali'    => base_url() . 'spp/read?nm_unit=' . urlencode($nm_unit) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&kd_skpd=' . urlencode($kd_skpd) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2),
            'start'      => $start,
            'script'     => 'report/datatables',
            'cetak'      => $cetak,
            'cetak2'     => $cetak2,
            'spp'        => $rspp
        );

        $this->template->load('layout', 'spp/view_indextiga', $data);

        /*

         */
    }


    function cetaktiga()
    {
        $akses       = $this->cekAkses('read');
        $nm_unit     = urldecode($this->input->get('nm_unit', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $kd_skpd     = urldecode($this->input->get('kd_skpd', true));
        $no_spp      = urldecode($this->input->get('no_spp', true));

        $spp = $this->Mspp->getdetailspp($nm_unit, $nm_sub_unit, $kd_skpd, $no_spp);
        $data = array(
            'spp_data'    => $spp,
            'nm_unit'     => $nm_unit,
            'kd_skpd'     => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'no_spp'       => $no_spp,
            'start'         => 0
        );

        $this->load->view('spp/cetak_indextiga', $data);
    }
    function cetaktiga2()
    {
        $akses       = $this->cekAkses('read');
        $nm_unit     = urldecode($this->input->get('nm_unit', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $kd_skpd     = urldecode($this->input->get('kd_skpd', true));
        $no_spp      = urldecode($this->input->get('no_spp', true));

        $spp = $this->Mspp->getdetailspp($nm_unit, $nm_sub_unit, $kd_skpd, $no_spp);
        $data = array(
            'spp_data'    => $spp,
            'nm_unit'     => $nm_unit,
            'kd_skpd'     => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'no_spp'       => $no_spp,
            'start'         => 0
        );

        $this->load->view('spp/cetak_indextiga2', $data);
    }

    function excel()
    {
        $akses         = $this->cekAkses('read');
        $res           = $this->input->get();
        $kd_skpd       = urldecode($res['kd_skpd']);
        $nm_sub_unit   = urldecode($res['nm_sub_unit']);
        $jenis_spp     = urldecode($res['jenis_spp']);
        $start         = intval($this->input->get('start'));
        $kd_rek_gabung = urldecode($this->input->get('kd_rek_gabung'));
        $q             = urldecode($this->input->get('q', true));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($kd_rek_gabung) {
            $this->db->where("kd_rek_gabung", $kd_rek_gabung);
        }
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];

            $this->db->where("(tgl_spp between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $spp = $this->Mspp->get_limit_data($kd_skpd, $nm_sub_unit, $jenis_spp, $q);
        if ($kd_rek_gabung) {
            $this->db->where("kd_rek_gabung", $kd_rek_gabung);
        }
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];

            $this->db->where("(tgl_spp between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }

        $total = $this->Mspp->getSumdetail($kd_skpd, $nm_sub_unit, $q, $jenis_spp);
        if (isset($total->nilai)) {
            $nilai = $total->nilai;
        } else {
            $nilai = 0;
        }
        $data = array(
            'spp_data'      => $spp,
            'nilai'         => number_format($nilai, '2', ',', '.'),
        );
        $this->load->view('spp/excel', $data);
    }
}

/* End of file Spp.php */
/* Location: ./application/controllers/Spp.php */

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sppd extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->load->model('MSppd');
        $this->load->library('form_validation');
    }

    private function cekAkses($var = null)
    {
        $url = 'Sppd';
        return cek($this->id_pengguna, $url, $var);
    }

    public function index()
    {
        $akses    = $this->cekAkses('read');
        $q        = urldecode($this->input->get('q', TRUE));
        $start    = intval($this->input->get('start'));
        $tanggal1 = urldecode($this->input->get('tanggal1', true));
        $tanggal2 = urldecode($this->input->get('tanggal2', true));

        if ($tanggal1 == '') {
            $tanggal1 = '01/01/' . date('Y');
        }

        if ($tanggal2 == '') {
            $tanggal2 = date('d/m/Y');
        }

        if ($q <> '' || $tanggal1 <> '' || $tanggal2 <> '') {
            $cetak = base_url() . 'sppd/cetak_index?q=' . urlencode($q) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2);
            $cetak2 = base_url() . 'sppd/cetak_index2?q=' . urlencode($q) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2);
        } else {
            $cetak = base_url() . 'sppd/cetak_index';
            $cetak2 = base_url() . 'sppd/cetak_index2';
        }

        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $sppd = $this->MSppd->getAllData($q);

        $subunit = $this->MSppd->getSubunit();
        $data = array(
            'sppd_data' => $sppd,
            'q'         => $q,
            'start'     => $start,
            'title'     => 'Surat Perintah Pencairan Dana (SP2D)',
            'akses'     => $akses,
            'subunit'   => $subunit,
            'cetak'     => $cetak,
            'cetak2'    => $cetak2,
            'tanggal1'  => $tanggal1,
            'tanggal2'  => $tanggal2,
            'script'   => 'report/datatables'
        );
        $this->template->load('layout', 'sppd/view_index', $data);
    }

    public function cetak_index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $sppd  = $this->MSppd->getAllData($q);
        // $total=$this->MSppd->getSumNilai($q);
        $start = intval($this->input->get('start'));
        $data = array(
            'sppd_data' => $sppd,
            'start'      => $start,
            'tanggal1'           => $tanggal1,
            'tanggal2'           => $tanggal2,
        );
        $this->load->view('sppd/cetak_index', $data);
    }

    public function cetak_index2()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $sppd  = $this->MSppd->getAllData($q);
        // $total=$this->MSppd->getSumNilai($q);
        $start = intval($this->input->get('start'));
        $data = array(
            'sppd_data' => $sppd,
            'start'      => $start,
        );
        $this->load->view('sppd/cetak_index2', $data);
    }
    public function read()
    {
        $akses       = $this->cekAkses('read');
        $nm_unit     = urldecode($this->input->get('nm_unit', true));
        $kd_skpd     = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));

        $q = $this->input->get('q');

        if ($q <> '') {
            $this->db->where('jenis_sp2d', $q);
        }
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $sppd        = $this->MSppd->get_limit_data($nm_unit, $kd_skpd, $nm_sub_unit);
        $start       = intval($this->input->get('start'));
        $data = array(
            'sppd_data'   => $sppd,
            'title'       => 'Surat Perintah Pencairan Dana (SP2D)',
            'akses'       => $akses,
            'nm_unit'     => $nm_unit,
            'nm_sub_unit' => $nm_sub_unit,
            'kd_skpd'     => $kd_skpd,
            'kembali'     => base_url() . 'sppd',
            'start'       => $start,
            'script' => 'report/datatables',
            'cetak'         => base_url() . 'sppd/cetakdua?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&q=' . urlencode($q) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2),
            'cetak2'         => base_url() . 'sppd/cetakdua2?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&q=' . urlencode($q) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2),
            'tanggal1'           => $tanggal1,
            'tanggal2'           => $tanggal2,

        );

        $this->template->load('layout', 'sppd/view_indexdua', $data);
    }

    function cetakdua()
    {
        $akses   = $this->cekAkses('read');
        $kd_skpd = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $q = $this->input->get('q');
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));

        if ($q <> '') {
            $this->db->where('jenis_sp2d', $q);
        }
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $sppd        = $this->MSppd->get_limit_data($nm_unit, $kd_skpd, $nm_sub_unit);
        $data = array(
            'sppd_data' => $sppd,
            'title'     => 'Surat Perintah Pencairan Dana (SP2D)',
            'nm_unit'   => $nm_unit,
            'kd_skpd'   => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'start'     => 0,
            'tanggal1'           => $tanggal1,
            'tanggal2'           => $tanggal2,
        );

        $this->load->view('sppd/cetak_indexdua', $data);
    }
    function cetakdua2()
    {
        $akses   = $this->cekAkses('read');
        $kd_skpd = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        $q = $this->input->get('q');

        if ($q <> '') {
            $this->db->where('jenis_sp2d', $q);
        }
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $sppd        = $this->MSppd->get_limit_data($nm_unit, $kd_skpd, $nm_sub_unit);
        $data = array(
            'sppd_data' => $sppd,
            'title'     => 'Surat Perintah Pencairan Dana (SP2D)',
            'nm_unit'   => $nm_unit,
            'kd_skpd'   => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'start'     => 0,
        );

        $this->load->view('sppd/cetak_indexdua2', $data);
    }

    function readdua()
    {
        $akses      = $this->cekAkses('read');
        $kd_skpd    = $this->input->get('kd_skpd', true);
        $nm_unit    = $this->input->get('nm_unit', true);
        $nm_sub_unit = $this->input->get('nm_sub_unit', true);
        $no_sp2d    = $this->input->get('no_sp2d', true);

        $tanggal1   = urldecode($this->input->get('tanggal1', true));
        $tanggal2   = urldecode($this->input->get('tanggal2', true));
        $start      = intval($this->input->get('start'));
        $sppd = $this->MSppd->getdetailsp2d($kd_skpd, $nm_unit, $nm_sub_unit, $no_sp2d);

        $rsppd = $this->db->query("select distinct tgl_sp2d,no_sp2d
                                from tb_spe_sp2d
                                where no_sp2d='$no_sp2d'")->row();
        $data = array(
            'sppd_data'  => $sppd,
            'title'      => 'Surat Perintah Pencairan Dana (SP2D)',
            'akses'      => $akses,
            'nm_unit'    => $nm_unit,
            'kd_skpd'    => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'no_sp2d'    => $no_sp2d,
            'kembali'    => base_url() . 'sppd/read?nm_unit=' . urlencode($nm_unit) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&kd_skpd=' . urlencode($kd_skpd) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2),
            'start'      => $start,
            'script'     => 'report/datatables',
            'cetak'      => base_url() . 'sppd/cetaktiga?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&no_sp2d=' . urlencode($no_sp2d),
            'cetak2'     => base_url() . 'sppd/cetaktiga2?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&no_sp2d=' . urlencode($no_sp2d),
            'sppd' => $rsppd
        );

        $this->template->load('layout', 'sppd/view_indextiga', $data);
    }

    function cetaktiga()
    {
        $akses       = $this->cekAkses('read');
        $nm_unit     = urldecode($this->input->get('nm_unit', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $kd_skpd     = urldecode($this->input->get('kd_skpd', true));
        $no_sp2d      = urldecode($this->input->get('no_sp2d', true));

        $sppd = $this->MSppd->getdetailsp2d($kd_skpd, $nm_unit, $nm_sub_unit, $no_sp2d);
        $data = array(
            'sppd_data'   => $sppd,
            'title'       => 'Surat Perintah Pencairan Dana (SP2D)',
            'nm_unit'     => $nm_unit,
            'kd_skpd'     => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'no_sp2d'       => $no_sp2d,
            'start'       => 0,
        );

        $this->load->view('sppd/cetak_indextiga', $data);
    }

    function cetaktiga2()
    {
        $akses       = $this->cekAkses('read');
        $nm_unit     = urldecode($this->input->get('nm_unit', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $kd_skpd     = urldecode($this->input->get('kd_skpd', true));
        $no_sp2d      = urldecode($this->input->get('no_sp2d', true));

        $sppd = $this->MSppd->getdetailsp2d($kd_skpd, $nm_unit, $nm_sub_unit, $no_sp2d);
        $data = array(
            'sppd_data'   => $sppd,
            'title'       => 'Surat Perintah Pencairan Dana (SP2D)',
            'nm_unit'     => $nm_unit,
            'kd_skpd'     => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'no_sp2d'       => $no_sp2d,
            'start'       => 0,
        );

        $this->load->view('sppd/cetak_indextiga2', $data);
    }
    /*public function read()
    {
        $akses       = $this->cekAkses('read');
        $res         =$this->input->get();
        $kd_skpd     =urldecode($res['kd_skpd']);
        $nm_sub_unit =urldecode($res['nm_sub_unit']);
        $jenis_sp2d  =urldecode($this->input->get('jenis_sp2d',true));

        $start       = intval($this->input->get('start'));
        $kd_rek_gabung =urldecode($this->input->get('kd_rek_gabung'));
        $q             =urldecode($this->input->get('q',true));
        $tanggal1      =urldecode($this->input->get('tanggal1',true));
        $tanggal2      =urldecode($this->input->get('tanggal2',true));


        if($tanggal1==''){
            $tanggal1='01/01/'.date('Y');
        }

        if($tanggal2==''){
            $tanggal2=date('d/m/Y');
        }

         if ($kd_rek_gabung<>'' || $q<> '' || $tanggal1 <> '' || $tanggal2<>'') {
            $config['base_url']  = base_url() . 'sppd/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&jenis_sp2d='.urlencode($jenis_sp2d).'&q=' . urlencode($q).'&kd_rek_gabung=' . urlencode($kd_rek_gabung).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2);
            $config['first_url'] = base_url() . 'sppd/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&jenis_sp2d='.urlencode($jenis_sp2d).'&q=' . urlencode($q).'&kd_rek_gabung=' . urlencode($kd_rek_gabung).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2);
            $cetak=base_url() . 'sppd/cetak?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&jenis_sp2d='.urlencode($jenis_sp2d).'&q=' . urlencode($q).'&kd_rek_gabung=' . urlencode($kd_rek_gabung).'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2);
        } else {
            $config['base_url']  = base_url() . 'sppd/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&jenis_sp2d='.urlencode($jenis_sp2d);
            $config['first_url'] = base_url() . 'sppd/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&jenis_sp2d='.urlencode($jenis_sp2d);
            $cetak=base_url() . 'sppd/cetak?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&jenis_sp2d='.urlencode($jenis_sp2d);
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $config['total_rows']        = $this->MSppd->total_rows($kd_skpd,$nm_sub_unit,$jenis_sp2d,$q);
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $sppd                         = $this->MSppd->get_limit_data($kd_skpd,$nm_sub_unit,$jenis_sp2d,$config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        if($kd_rek_gabung ){
            $this->db->where("kd_rek_gabung",$kd_rek_gabung);
        }

        if($tanggal1 <> '' && $tanggal2 <> '' ){
            $t1=explode('/',$tanggal1);
            $tt1=$t1[2].'-'.$t1[1].'-'.$t1[0];

            $t2=explode('/',$tanggal2);
            $tt2=$t2[2].'-'.$t2[1].'-'.$t2[0];

            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))",null,false);
        }
        $total=$this->MSppd->getSumdetail($kd_skpd,$nm_sub_unit,$q,$jenis_sp2d);
        if (isset($total->nilai)) {
            $nilai=$total->nilai;
        } else {
            $nilai=0;
        }
        $rekening=$this->db->query("select distinct kd_rek_gabung,nm_rek_5
                                    from tb_spe_sp2d
                                    where kd_skpd='$kd_skpd' and nm_sub_unit='$nm_sub_unit' and jenis_sp2d='$jenis_sp2d'
                                    order by nm_rek_5 asc")->result();
        $data = array(
			'sppd_data'    => $sppd,
			'q'           => $q,
			'pagination'  => $this->pagination->create_links(),
			'total_rows'  => $config['total_rows'],
			'start'       => $start,
			'title'       => 'Surat Perintah Pencairan Dana (SP2D)',
			'akses'       => $akses,
			'action'      => base_url().'sppd/read?kd_skpd='.urlencode($kd_skpd).'&nm_sub_unit='.urlencode($nm_sub_unit).'&jenis_sp2d='.urlencode($jenis_sp2d),
			'kd_skpd'     =>$kd_skpd,
			'nm_sub_unit' =>$nm_sub_unit,
			'jenis_sp2d'  =>$jenis_sp2d,
			'kembali'     => base_url().'sppd',
            'cetak'=>$cetak,
            'rekening'      =>$rekening,
            'kd_rek_gabung' =>$kd_rek_gabung,
            'nilai'         =>number_format($nilai,'2',',','.'),
            'tanggal1'      =>$tanggal1,
            'tanggal2'      =>$tanggal2,

        );
        // echo $data['kd_skpd'];
        $this->template->load('layout','sppd/view_indexdua',$data);
    }*/

    public function cetak()
    {
        $akses       = $this->cekAkses('read');
        $res   = $this->input->get();
        $kd_skpd     = urldecode($res['kd_skpd']);
        $nm_sub_unit = urldecode($res['nm_sub_unit']);
        $jenis_sp2d  = urldecode($this->input->get('jenis_sp2d', true));

        $start       = intval($this->input->get('start'));
        $kd_rek_gabung = urldecode($this->input->get('kd_rek_gabung'));
        $q             = urldecode($this->input->get('q', true));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($kd_rek_gabung) {
            $this->db->where("kd_rek_gabung", $kd_rek_gabung);
        }

        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];

            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $sppd                         = $this->MSppd->get_all_data($kd_skpd, $nm_sub_unit, $jenis_sp2d, $q);
        if ($kd_rek_gabung) {
            $this->db->where("kd_rek_gabung", $kd_rek_gabung);
        }

        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];

            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $total = $this->MSppd->getSumdetail($kd_skpd, $nm_sub_unit, $q, $jenis_sp2d);
        if (isset($total->nilai)) {
            $nilai = $total->nilai;
        } else {
            $nilai = 0;
        }
        $data = array(
            'sppd_data'    => $sppd,
            'nilai'         => number_format($nilai, '2', ',', '.'),

        );
        $this->load->view('sppd/excel', $data);
    }

    function pihakketiga()
    {
        // $url         = 'Sppd.pihakketiga';
        $url         = 'pihakketiga';
        $akses       = cek($this->id_pengguna, $url, 'read');
        $start       = intval($this->input->get('start'));
        $q           = urldecode($this->input->get('q', true));
        $nm_unit     = urldecode($this->input->get('nm_unit', true));
        $kd_skpd     = null;
        $nm_sub_unit = null;
        $jenis_sp2d  = null;

        if ($q <> '') {
            $config['base_url']  = base_url() . 'sppd/pihakketiga?q=' . urlencode($q) . '&nm_unit=' . urlencode($nm_unit);
            $config['first_url'] = base_url() . 'sppd/pihakketiga?q=' . urlencode($q) . '&nm_unit=' . urlencode($nm_unit);
        } else {
            $config['base_url']  = base_url() . 'sppd/pihakketiga';
            $config['first_url'] = base_url() . 'sppd/pihakketiga';
        }

        $config['per_page']          = 10;
        $config['page_query_string'] = TRUE;

        $config['total_rows']        = $this->MSppd->total_rows($q, $nm_unit);
        $sppd                        = $this->MSppd->get_limit_datamonitor($config['per_page'], $start, $q, $nm_unit);
        $this->load->library('pagination');
        $this->pagination->initialize($config);
 
        $unit = $this->MSppd->getSubunit();
        $data = array(
            'sppd_data'  => $sppd,
            'q'          => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start'      => $start,
            'title'      => 'Monitor Pihak Ke 3 (tiga)',
            'akses'      => $akses,
            'action'     => base_url() . 'pihakketiga',
            'unit'       => $unit,
            'nm_unit'    => $nm_unit,
            'cetak'      => base_url() . 'sppd/cetakmonitortiga?q=' . urlencode($q) . '&nm_unit=' . urlencode($nm_unit)
        );

        $this->template->load('layout', 'sppd/monitorPihaktiga', $data);
    }

    function sppdvsspj()
    {
        $url   = 'monitoringtu';
        $akses = cek($this->id_pengguna, $url, 'read');
        $opd = $this->db->query("select  distinct kd_skpd,case when  ltrim(nm_unit) = ltrim(nm_sub_unit) then  ltrim(nm_unit) else ltrim(nm_unit)+' / '+ ltrim(nm_sub_unit) end nm_sub_unit
                               from tb_spe_sp2d where jenis_sp2d in ('TU','UP')
                               order by  nm_sub_unit asc")->result();
        $q = $this->input->get('q');

        if ($q <> '') {
            $sppd = $this->MSppd->sppdTu($q);
            $spj  = $this->MSppd->spjTu($q);
            $mapping = $this->MSppd->mappingsp2dvsspj($q);
        } else {
            $sppd    = [];
            $spj     = [];
            $mapping = [];
        }

        $data = array(
            'title'   => 'Monitoring  TU',
            'action'  => base_url() . 'monitoringtu',
            'subunit' => $opd,
            'q'       => $q,
            // 'jenis'   =>$jenis,
            'sppd'    => $sppd,
            'spj'     => $spj,
            'script'  => 'sppd/jsvs',
            'mapping' => $mapping
        );
        $this->template->load('layout', 'sppd/sp2dvsspj', $data);
    }

    function prosesmappingspj()
    {
        $url   = 'Sppd.sppdvsspj';
        $akses = cek($this->id_pengguna, $url, 'read');
        $res = $this->input->post();
        if (!empty($res['sppd']) && !empty($res['spj'])) {

            $exp = explode('#', $res['sppd']);

            for ($i = 0; $i < count($res['spj']); $i++) {
                $pxe = $res['spj'][$i];
                $data = array(
                    'SP2D_Tahun'         => $exp[0],
                    'SP2D_Kd_SKPD'       => $exp[1],
                    'SP2D_No_SP2D'       => $exp[2],
                    'SPJ_No_SPJ'         => $pxe,

                );
                set_flashdata('success', 'Data berhasil di simpan.');
                $this->db->insert('SP2D_VS_SPJ', $data);
            }
        } else {
            set_flashdata('warning', 'Data harus memilih SPPD dan SPJ.');
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    function hapusmaping()
    {
        $id = $this->input->get('p');
        $this->db->where('id', $id);
        $this->db->delete('SP2D_VS_SPJ');
        set_flashdata('success', 'Data berhasil di hapus.');
        redirect($_SERVER['HTTP_REFERER']);
    }

    function cetakmonitortiga()
    {
        $q       = urldecode($this->input->get('q', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $sppd = $this->MSppd->get_limit_datamonitor(90000, 0, $q, $nm_unit);

        $data = array(
            'sppd_data'  => $sppd,
            'title'      => 'Monitor Pihak Ke 3 (tiga)',
        );
        $this->load->view('sppd/cetakmonitortiga', $data);
        // $this->template->load('layout','sppd/monitorPihaktiga',$data);

    }
}

/* End of file Sppd.php */
/* Location: ./application/controllers/Sppd.php */

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sppd_potongan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->id_pengguna = $this->session->userdata('audit_id_pengguna');
        $this->load->model('Msppdpot');
        $this->load->library('form_validation');
    }

    private function cekAkses($var = null)
    {
        $url = 'Sppd_potongan';
        return cek($this->id_pengguna, $url, $var);
    }

    public function index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));

        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));

        if ($tanggal1 == '') {
            $tanggal1 = '01/01/' . date('Y');
        }

        if ($tanggal2 == '') {
            $tanggal2 = date('d/m/Y');
        }

        if ($q <> '' || $tanggal1 <> '' || $tanggal2 <> '') {
            $cetak = base_url() . 'sppd_potongan/cetak_index?q=' . urlencode($q) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2);
            $cetak2 = base_url() . 'sppd_potongan/cetak_index2?q=' . urlencode($q) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2);
        } else {
            $cetak = base_url() . 'sppd_potongan/cetak_index';
            $cetak2 = base_url() . 'sppd_potongan/cetak_index2';
        }
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $sppd_potongan               = $this->Msppdpot->getalldata($q);
        $subunit = $this->Msppdpot->getSUbunit();

        $data = array(
            'sppd_potongan_data' => $sppd_potongan,
            'q'                  => $q,
            'start'              => $start,
            'title'              => 'Potongan Surat Perintah Pencairan Dana (SP2D)',
            'akses'              => $akses,
            'subunit'            => $subunit,
            'cetak'              => $cetak,
            'cetak2'             => $cetak2,
            'tanggal1'           => $tanggal1,
            'tanggal2'           => $tanggal2,
            'script'             => 'report/datatables',
        );
        $this->template->load('layout', 'sppd_potongan/view_index', $data);
    }
    public function cetak_index()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        $config['total_rows']        = $this->Msppdpot->gettotal($q);
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $sppd_potongan               = $this->Msppdpot->getalldata($q);
        $total = $this->Msppdpot->getSumNilai($q);
        $data = array(
            'sppd_potongan_data' => $sppd_potongan,
            'jum_nilai'             => number_format($total->jum_nilai, '2', ',', '.'),
        );
        $this->load->view('sppd_potongan/cetak_index', $data);
    }

    public function cetak_index2()
    {
        $akses = $this->cekAkses('read');
        $q     = urldecode($this->input->get('q', TRUE));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        $config['total_rows']        = $this->Msppdpot->gettotal($q);
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $sppd_potongan               = $this->Msppdpot->getalldata($q);
        $total = $this->Msppdpot->getSumNilai($q);
        $data = array(
            'sppd_potongan_data' => $sppd_potongan,
            'jum_nilai'             => number_format($total->jum_nilai, '2', ',', '.'),
        );
        $this->load->view('sppd_potongan/cetak_index2', $data);
    }
    public function read()
    {
        $akses         = $this->cekAkses('read');

        $kd_skpd       = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit   = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit       = urldecode($this->input->get('nm_unit', true));
        $start         = intval($this->input->get('start'));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $sp2d_potongan = $this->Msppdpot->get_limit_data($kd_skpd, $nm_sub_unit, $nm_unit);


        $data = array(
            'sppd_potongan_data' => $sp2d_potongan,
            'start'              => $start,
            'title'              => 'Potongan Surat Perintah Pencairan Dana (SP2D)',
            'kd_skpd'            => $kd_skpd,
            'nm_sub_unit'        => $nm_sub_unit,
            'nm_unit'            => $nm_unit,
            'script'             => 'report/datatables',
            'kembali'            => base_url() . 'sppd_potongan',
            'cetak'                 => base_url() . 'sppd_potongan/cetakdua?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2),
            'cetak2'                 => base_url() . 'sppd_potongan/cetakdua2?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2),
            'tanggal1'           => $tanggal1,
            'tanggal2'           => $tanggal2,
        );
        $this->template->load('layout', 'sppd_potongan/view_indexdua', $data);
    }

    function cetakdua()
    {
        $akses   = $this->cekAkses('read');
        $kd_skpd = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $sp2d_potongan = $this->Msppdpot->get_limit_data($kd_skpd, $nm_sub_unit, $nm_unit);
        $data = array(
            'sppd_potongan_data'  => $sp2d_potongan,
            'nm_unit'   => $nm_unit,
            'kd_skpd'   => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'tanggal1'           => $tanggal1,
            'tanggal2'           => $tanggal2,
        );

        $this->load->view('sppd_potongan/cetak_indexdua', $data);
    }
    function cetakdua2()
    {
        $akses   = $this->cekAkses('read');
        $kd_skpd = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit = urldecode($this->input->get('nm_unit', true));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];
            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $sp2d_potongan = $this->Msppdpot->get_limit_data($kd_skpd, $nm_sub_unit, $nm_unit);
        $data = array(
            'sppd_potongan_data'  => $sp2d_potongan,
            'nm_unit'   => $nm_unit,
            'kd_skpd'   => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit
        );

        $this->load->view('sppd_potongan/cetak_indexdua2', $data);
    }
    public function cetak()
    {
        $akses = $this->cekAkses('read');
        $res   = $this->input->get();
        $Kd_SKPD     = urldecode($res['Kd_SKPD']);
        $Nm_Sub_Unit = urldecode($res['Nm_Sub_Unit']);
        $start       = intval($this->input->get('start'));
        $kd_rek_gabung = urldecode($this->input->get('kd_rek_gabung'));
        $q             = urldecode($this->input->get('q', true));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        if ($kd_rek_gabung) {
            $this->db->where("kd_rek_gabung", $kd_rek_gabung);
        }
        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];

            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $sp2d_potongan = $this->Msppdpot->get_all_data($Kd_SKPD, $Nm_Sub_Unit, $q);
        if ($kd_rek_gabung) {
            $this->db->where("kd_rek_gabung", $kd_rek_gabung);
        }

        if ($tanggal1 <> '' && $tanggal2 <> '') {
            $t1 = explode('/', $tanggal1);
            $tt1 = $t1[2] . '-' . $t1[1] . '-' . $t1[0];

            $t2 = explode('/', $tanggal2);
            $tt2 = $t2[2] . '-' . $t2[1] . '-' . $t2[0];

            $this->db->where("(tgl_sp2d between Convert(datetime, '$tt1' ) and Convert(datetime, '$tt2' ))", null, false);
        }
        $total = $this->Msppdpot->getSumdetail($Kd_SKPD, $Nm_Sub_Unit, $q);
        if (isset($total->nilai)) {
            $nilai = $total->nilai;
        } else {
            $nilai = 0;
        }
        $data = array(
            'sppd_potongan_data' => $sp2d_potongan,
            'nilai'         => number_format($nilai, '2', ',', '.'),

        );
        $this->load->view('sppd_potongan/excel', $data);
    }

    function readdua()
    {
        $akses         = $this->cekAkses('read');
        $kd_skpd       = urldecode($this->input->get('kd_skpd', true));
        $nm_sub_unit   = urldecode($this->input->get('nm_sub_unit', true));
        $nm_unit       = urldecode($this->input->get('nm_unit', true));
        $no_sp2d       = urldecode($this->input->get('no_sp2d', true));
        $start         = intval($this->input->get('start'));
        $tanggal1      = urldecode($this->input->get('tanggal1', true));
        $tanggal2      = urldecode($this->input->get('tanggal2', true));
        $sp2d_potongan = $this->Msppdpot->getDetail($kd_skpd, $nm_sub_unit, $nm_unit, $no_sp2d);
        $rsppd = $this->db->query("select distinct tgl_sp2d,no_sp2d,keterangan
                                from tb_spe_sp2d_potongan
                                where no_sp2d='$no_sp2d'")->row();

        $data = array(

            'sppd' => $rsppd,
            'sppd_potongan_data' => $sp2d_potongan,
            'start'             => $start,
            'title'             => 'Potongan Surat Perintah Pencairan Dana (SP2D)',
            'kd_skpd'           => $kd_skpd,
            'nm_sub_unit'       => $nm_sub_unit,
            'nm_unit'           => $nm_unit,
            'no_sp2d'           => $no_sp2d,
            'script'            => 'report/datatables',
            'kembali'           => base_url() . 'sppd_potongan/read?nm_unit=' . urlencode($nm_unit) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&kd_skpd=' . urlencode($kd_skpd) . '&tanggal1=' . urlencode($tanggal1) . '&tanggal2=' . urlencode($tanggal2),
            'cetak'             => base_url() . 'sppd_potongan/cetaktiga?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&no_sp2d=' . urlencode($no_sp2d),
            'cetak2'            => base_url() . 'sppd_potongan/cetaktiga2?kd_skpd=' . urlencode($kd_skpd) . '&nm_sub_unit=' . urlencode($nm_sub_unit) . '&nm_unit=' . urlencode($nm_unit) . '&no_sp2d=' . urlencode($no_sp2d),
        );

        $this->template->load('layout', 'sppd_potongan/view_indextiga', $data);
    }
    function cetaktiga()
    {
        $akses       = $this->cekAkses('read');
        $nm_unit     = urldecode($this->input->get('nm_unit', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $kd_skpd     = urldecode($this->input->get('kd_skpd', true));
        $no_sp2d      = urldecode($this->input->get('no_sp2d', true));

        $sp2d_potongan = $this->Msppdpot->getDetail($kd_skpd, $nm_sub_unit, $nm_unit, $no_sp2d);
        $data = array(
            'sppd_potongan_data'    => $sp2d_potongan,
            'nm_unit'     => $nm_unit,
            'kd_skpd'     => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'no_sp2d'       => $no_sp2d,
            'start'         => 0
        );

        $this->load->view('sppd_potongan/cetak_indextiga', $data);
    }
    function cetaktiga2()
    {
        $akses       = $this->cekAkses('read');
        $nm_unit     = urldecode($this->input->get('nm_unit', true));
        $nm_sub_unit = urldecode($this->input->get('nm_sub_unit', true));
        $kd_skpd     = urldecode($this->input->get('kd_skpd', true));
        $no_sp2d      = urldecode($this->input->get('no_sp2d', true));

        $sp2d_potongan = $this->Msppdpot->getDetail($kd_skpd, $nm_sub_unit, $nm_unit, $no_sp2d);
        $data = array(
            'sppd_potongan_data'    => $sp2d_potongan,
            'nm_unit'     => $nm_unit,
            'kd_skpd'     => $kd_skpd,
            'nm_sub_unit' => $nm_sub_unit,
            'no_sp2d'       => $no_sp2d,
            'start'         => 0
        );

        $this->load->view('sppd_potongan/cetak_indextiga2', $data);
    }
}

/* End of file Sppd_potongan.php */
/* Location: ./application/controllers/Sppd_potongan.php */

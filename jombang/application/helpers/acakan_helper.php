<?php // if (!defined('BASEPATH')) exit('No direct script access allowed');
use Jenssegers\Blade\Blade;

if (!function_exists('view')) {
	function view($view, $data = [])
	{
		$path = APPPATH . 'views';
		$blade = new Blade($path, $path . '/cache');
		echo $blade->make($view, $data);
	}
}

if (!function_exists('acak')) {
	function acak($str)
	{
		$kunci = '==$#$$@#$jsdjGHFVGHSDF&*672367235HHGDVHVDyhagsvfhg^%$#@#@#&^*(*^&^&}{P{P&*^&*()(';
		$hasil = '';
		for ($i = 0; $i < strlen($str); $i++) {
			$karakter = substr($str, $i, 1);
			$kuncikarakter = substr($kunci, ($i % strlen($kunci)) - 1, 1);
			$karakter = chr(ord($karakter) + ord($kuncikarakter));
			$hasil .= $karakter;
		}
		return urlencode(base64_encode($hasil));
	}
}

if (!function_exists('rapikan')) {
	function rapikan($str)
	{
		$str = base64_decode(urldecode($str));
		$hasil = '';
		$kunci = '==$#$$@#$jsdjGHFVGHSDF&*672367235HHGDVHVDyhagsvfhg^%$#@#@#&^*(*^&^&}{P{P&*^&*()(';
		for ($i = 0; $i < strlen($str); $i++) {
			$karakter = substr($str, $i, 1);
			$kuncikarakter = substr($kunci, ($i % strlen($kunci)) - 1, 1);
			$karakter = chr(ord($karakter) - ord($kuncikarakter));
			$hasil .= $karakter;
		}
		return $hasil;
	}
}

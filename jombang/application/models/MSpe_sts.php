<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MSpe_sts extends CI_Model
{

    public $table = 'tb_spe_sts';
    public $id    = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }


    function getListRektiga($kd_skpd, $nm_unit, $nm_sub_unit, $tanggal1, $tanggal2)
    {
        $query = "SELECT  kd_skpd ,nm_unit ,nm_sub_unit ,tgl_sts ,no_sts ,keterangan , count(*) jum,sum(nilai) nilai
                  FROM SPE_STS
                    where  (tgl_sts between Convert(datetime, '$tanggal1' ) and Convert(datetime, '$tanggal2' )) AND kd_skpd ='$kd_skpd' and nm_unit ='$nm_unit' and nm_sub_unit ='$nm_sub_unit'
                  group by  kd_skpd ,nm_unit ,nm_sub_unit ,tgl_sts ,no_sts ,keterangan";


        return $this->db->query($query)->result();
    }










    // get total rows
    function total_rows($Kd_SKPD, $Nm_Sub_Unit, $q = NULL)
    {
        $this->db->where('Kd_SKPD', $Kd_SKPD);
        $this->db->where('Nm_Sub_Unit', $Nm_Sub_Unit);
        $this->db->where("(Tahun like '%$q%' or  Ket_Program like '%$q%'  or Ket_Kegiatan like '%$q%' or No_STS like '%$q%' or Keterangan like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' )", null, false);

        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($kd_skpd, $nm_sub_unit, $nm_unit, $no_sts)
    {

        $sql = "SELECT kd_skpd,nm_unit,nm_sub_unit,ket_program,ket_kegiatan,tgl_sts,no_sts,keterangan,nilai,kd_rek_gabung,nm_rek_5
                from tb_spe_sts
                where kd_skpd='$kd_skpd'
                and nm_unit='$nm_unit'
                and nm_sub_unit='$nm_sub_unit'
                and no_sts='$no_sts'";
        return $this->db->query($sql)->result();
    }

    function get_all_data($Kd_SKPD, $Nm_Sub_Unit, $q = NULL)
    {
        $this->db->where('Kd_SKPD', $Kd_SKPD);
        $this->db->where('Nm_Sub_Unit', $Nm_Sub_Unit);
        $this->db->where("(Tahun like '%$q%' or  Ket_Program like '%$q%'  or Ket_Kegiatan like '%$q%' or No_STS like '%$q%' or Keterangan like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' )", null, false);
        return $this->db->get($this->table)->result();
    }
    function getdata($limit, $start = 0, $q = NULL)
    {
        $this->db->select('Kd_SKPD, Nm_Sub_Unit, sum(nilai) nilai', false);
        $this->db->where("(Kd_SKPD like '%$q%' or Nm_Sub_Unit like '%$q%')", null, false);
        $this->db->group_by('Kd_SKPD', false);
        $this->db->group_by('Nm_Sub_Unit', false);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
    // function getalldata($q=NULL){
    //     $query="select  kd_skpd ,nm_unit ,nm_sub_unit , count(*) jum,sum(nilai) nilai
    //             from (
    //               SELECT  kd_skpd ,nm_unit ,nm_sub_unit ,tgl_sts ,no_sts ,keterangan , count(*) jum,sum(nilai) nilai
    //               FROM e_audit.dbo.SPE_STS
    //               where (nm_unit like '%$q%' or nm_sub_unit like '%$q%')
    //               group by  kd_skpd ,nm_unit ,nm_sub_unit ,tgl_sts ,no_sts ,keterangan
    //             ) temp
    //             group by  kd_skpd ,nm_unit ,nm_sub_unit";
    //     return $this->db->query($query)->result();


    // }
    function getalldata($q = NULL)
    {
        $table = "(                  SELECT  kd_skpd ,nm_unit ,nm_sub_unit ,tgl_sts ,no_sts ,keterangan , count(*) jum,sum(nilai) nilai
        FROM SPE_STS
        group by  kd_skpd ,nm_unit ,nm_sub_unit ,tgl_sts ,no_sts ,keterangan ) temp";
        $this->db->select("kd_skpd ,nm_unit ,nm_sub_unit ,count(*) jum ,sum(nilai) nilai ", false);
        $this->db->group_by("kd_skpd ,nm_unit ,nm_sub_unit", false);
        $this->db->where("(kd_skpd like '%$q%' or  nm_unit like '%$q%' or nm_sub_unit like '%$q%' )", null, false);
        return $this->db->get($table)->result();
    }
    function gettotal($q = NULL)
    {
        /*//*/
        $this->db->where("(Kd_SKPD like '%$q%' or Nm_Sub_Unit like '%$q%')", null, false);
        $this->db->from($this->table);
        $this->db->group_by('Kd_SKPD', false);
        $this->db->group_by('Nm_Sub_Unit', false);
        $this->db->select('Kd_SKPD, Nm_Sub_Unit, sum(nilai) nilai', false);
        return $this->db->count_all_results();
    }

    function getSum($q = null)
    {
        $query = "select sum(nilai) nilai from (select  kd_skpd ,nm_unit ,nm_sub_unit , count(*) jum,sum(nilai) nilai
                from (
                  SELECT  kd_skpd ,nm_unit ,nm_sub_unit ,tgl_sts ,no_sts ,keterangan , count(*) jum,sum(nilai) nilai
                  FROM SPE_STS
                  where (nm_unit like '%$q%' or nm_sub_unit like '%$q%')
                  group by  kd_skpd ,nm_unit ,nm_sub_unit ,tgl_sts ,no_sts ,keterangan
                ) temp
                group by  kd_skpd ,nm_unit ,nm_sub_unit)a";
        return $this->db->query($query)->row();
    }

    function getSubunit()
    {
        return $this->db->query("SELECT kd_skpd,nm_unit
                                    from tb_spe_ref_unit
                                    order by kd_skpd")->result();
    }

    function getSumdetail($kd_skpd, $nm_sub_unit, $q = NULL)
    {
        $this->db->select("sum(nilai) nilai", false);
        $this->db->where('Kd_SKPD', $kd_skpd);
        $this->db->where('Nm_Sub_Unit', $nm_sub_unit);
        $this->db->where("(Tahun like '%$q%' or  Ket_Program like '%$q%'  or Ket_Kegiatan like '%$q%' or No_STS like '%$q%' or Keterangan like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' )", null, false);
        return $this->db->get($this->table)->row();
    }
}

/* End of file MSpe_sts.php */
/* Location: ./application/models/MSpe_sts.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-29 12:03:48 */
/* http://harviacode.com */

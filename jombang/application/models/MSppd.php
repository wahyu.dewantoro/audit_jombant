<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MSppd extends CI_Model
{

    public $table = 'tb_spe_sp2d';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function total_rows($q = NULL, $nm_unit = null)
    {

        // $this->db->where("(no_sp2d like '%$q%' or keterangan like '%$q%' or nm_penerima like '%$q%' or nilai like '%$q%')",null,false);
        $this->db->where("(no_sp2d like '%$q%' or keterangan like '%$q%' or nm_penerima like '%$q%' or nilai like '%$q%' or npwp like '%$q%')", null, false);
        if ($nm_unit <> '') {
            $this->db->where("nm_unit", $nm_unit);
        }

        $this->db->select("kode_unit,kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan,nm_penerima,rek_penerima,npwp,sum(nilai) nilai", false);
        $this->db->group_by('kode_unit,kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan,nm_penerima,rek_penerima,npwp', false);
        $this->db->where("kd_rek_gabung like '5.2.3%' ", null, false);
        $this->db->where('jenis_sp2d', 'LS');
        // $this->db->limit($limit, $start);
        $this->db->from($this->table);
        return $this->db->count_all_results();
        /*
    	if($kd_skpd != null){
            $this->db->where('kd_skpd',$kd_skpd);
        }

    	if($nm_sub_unit != null){
            $this->db->where('nm_sub_unit',$nm_sub_unit);
        }

    	if($jenis_sp2d != null){
            $this->db->where('jenis_sp2d',$jenis_sp2d);
        }

    	$this->db->where("(Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or Ket_Program like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SP2D like '%$q%' or No_SP2D like '%$q%' or Keterangan like '%$q%' or Jenis_SP2D like '%$q%' or Nilai like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or Nm_Penerima like '%$q%' or Rek_Penerima like '%$q%' or Bank_Penerima like '%$q%' or NPWP like '%$q%' or Tgl_SPM like '%$q%' or No_SPM    	 like '%$q%')",NULL,false);
		$this->db->from($this->table);
        return $this->db->count_all_results();*/
    }

    function get_limit_data($nm_unit, $kd_skpd, $nm_sub_unit)
    {
        $this->db->select("kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan,jenis_sp2d,sum(nilai) nilai,count(*) jum", false);
        $this->db->group_by("kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan,jenis_sp2d");
        $this->db->where('nm_unit', $nm_unit);
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        return $this->db->get('tb_spe_sp2d')->result();
    }

    function getdetailsp2d($kd_skpd, $nm_unit, $nm_sub_unit, $no_sp2d)
    {
        return $this->db->query("SELECT ket_program, ket_kegiatan, tgl_sp2d, no_sp2d,  nilai, kd_rek_gabung, nm_rek_5
                                    FROM tb_spe_sp2d
                                    WHERE kd_skpd ='$kd_skpd' and nm_unit ='$nm_unit' and nm_sub_unit ='$nm_sub_unit' and no_sp2d ='$no_sp2d'")->result();
    }


    // get data with limit and search
    function get_limit_datamonitor($limit, $start = 0, $q = NULL, $nm_unit = null)
    {

        $this->db->where("(no_sp2d like '%$q%' or keterangan like '%$q%' or nm_penerima like '%$q%' or nilai like '%$q%' or npwp like '%$q%' or rek_penerima like '%$q%')", null, false);
        if ($nm_unit <> '') {
            $this->db->where("nm_unit", $nm_unit);
        }
        $this->db->select("kode_unit,kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,tgl_spm,no_spm,keterangan,nm_penerima,rek_penerima,npwp,sum(nilai) nilai", false);
        $this->db->group_by('kode_unit,kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan,nm_penerima,rek_penerima,npwp,tgl_spm,no_spm', false);
        $this->db->where("kd_rek_gabung like '5.2.3%' ", null, false);
        $this->db->order_by('tgl_sp2d','desc');
        $this->db->where('jenis_sp2d', 'LS');
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }



    function get_all_data($kd_skpd, $nm_sub_unit, $jenis_sp2d, $q = NULL)
    {
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where('jenis_sp2d', $jenis_sp2d);
        $this->db->where("(Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or Ket_Program like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SP2D like '%$q%' or No_SP2D like '%$q%' or Keterangan like '%$q%' or Jenis_SP2D like '%$q%' or Nilai like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or Nm_Penerima like '%$q%' or Rek_Penerima like '%$q%' or Bank_Penerima like '%$q%' or NPWP like '%$q%' or Tgl_SPM like '%$q%' or No_SPM    	 like '%$q%')", NULL, false);
        return $this->db->get($this->table)->result();
    }
    function getData($limit, $start = 0, $q = NULL)
    {
        $this->db->select("kd_skpd,nm_sub_unit,jenis_sp2d,count(jenis_sp2d) jumlah,sum(nilai) nilai", false);
        $this->db->group_by('kd_skpd', false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%' or jenis_sp2d like '%$q%')", NULL, false);
        $this->db->group_by('nm_sub_unit', false);
        $this->db->group_by('jenis_sp2d', false);
        $this->db->group_by('kd_skpd', false);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table, false)->result();
    }

    function getAllData($q = NULL)
    {
        $table = "(
                   select kd_skpd,nm_unit,nm_sub_unit,no_sp2d,keterangan,jenis_sp2d,tgl_sp2d,sum(nilai) nilai
                   from tb_spe_sp2d
                   group by kd_skpd,nm_unit,nm_sub_unit,no_sp2d,keterangan,jenis_sp2d,tgl_sp2d
                ) temp";

        $this->db->select("kd_skpd,nm_unit,nm_sub_unit ,sum(case when jenis_sp2d ='up' then 1 end) jnsup ,sum(case when jenis_sp2d ='up' then nilai end) nilaiup ,sum(case when jenis_sp2d ='gu' then 1 end) jnsgu ,sum(case when jenis_sp2d ='gu' then nilai end) nilaigu ,sum(case when jenis_sp2d ='tu' then 1 end) jnstu ,sum(case when jenis_sp2d ='tu' then nilai end) nilaitu ,sum(case when jenis_sp2d ='ls' then 1 end) jnsls ,sum(case when jenis_sp2d ='ls' then nilai end) nilails ,sum(case when jenis_sp2d ='nihil' then 1 end) jnsnihil ,sum(case when jenis_sp2d ='nihil' then nilai end) nilainihil");
        $this->db->group_by("kd_skpd,nm_unit,nm_sub_unit");
        $this->db->where("nm_unit like '%$q%' ", NULL, false);
        return $this->db->get($table, false)->result();
    }

    function getRow($q = NULL)
    {
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%' or jenis_sp2d like '%$q%')", NULL, false);
        $this->db->select("kd_skpd,nm_sub_unit,jenis_sp2d,count(jenis_sp2d) jumlah,sum(nilai) nilai", false);
        $this->db->from($this->table, false);
        $this->db->group_by('nm_sub_unit', false);
        $this->db->group_by('jenis_sp2d', false);
        $this->db->group_by('kd_skpd', false);
        return $this->db->count_all_results();
    }
    function getSumNilai($q = null)
    {
        $this->db->select("sum(nilai) jum_nilai", false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')", null, false);
        return $this->db->get($this->table)->row();
    }

    function getSubunit()
    {
        return $this->db->query("SELECT kd_skpd,nm_unit from tb_spe_ref_unit order by kd_skpd")->result();
    }

    function getSumdetail($kd_skpd, $nm_sub_unit, $q = NULL, $jenis_sp2d)
    {
        $this->db->select("sum(nilai) nilai", false);
        $this->db->where("(Kd_SKPD like '%$kd_skpd%' and Nm_Sub_Unit like '%$nm_sub_unit%' and jenis_sp2d like '%$jenis_sp2d%')", null, false);
        $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SPM like '%$q%' or No_SP2D like '%$q%' or  Jenis_SP2D like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or Nm_Penerima like '%$q%' or Rek_Penerima like '%$q%' or Bank_Penerima like '%$q%' or NPWP like '%$q%' or No_SPM like '%$q%' )", NULL, false);
        // $this->db->group_by('Kd_SKPD',false);
        // $this->db->group_by('Nm_Sub_Unit',false);
        // $this->db->group_by('jenis_sp2d',false);
        return $this->db->get($this->table)->row();
    }


    function listpihak3()
    {
        return $this->db->query("SELECT DISTINCT nm_penerima from tb_spe_sp2d order by nm_penerima asc")->result();
    }

    function pihakTigaByPenerima($var)
    {
        return $this->db->query("select * from tb_spe_sp2d where nm_penerima like '%$var%'")->result();
    }

    function sppdTu($kd_skpd)
    {
        return $this->db->query("SELECT tahun,kd_skpd,tgl_sp2d,no_sp2d,keterangan,sum(nilai) nilai
                                FROM TB_SPE_SP2D
                                WHERE JENIS_SP2D in ('up','tu') AND KD_SKPD='$kd_skpd'
                                and no_sp2d not in (select sp2d_no_sp2d from sp2d_vs_spj)
                                group by tahun,kd_skpd,tgl_sp2d,no_sp2d,keterangan")->result();
    }

    function spjTu($kd_skpd)
    {
        return $this->db->query("SELECT tahun, kd_skpd ,tgl_spj, no_spj ,jenis_spj,keterangan,sum(nilai) nilai
                                from tb_spe_spj where jenis_spj in ('gu','tu') and kd_skpd='$kd_skpd'
                                and no_spj not in (select spj_no_spj from sp2d_vs_spj)
                                group by tahun, kd_skpd ,tgl_spj, no_spj,jenis_spj,keterangan
                                order by tgl_spj asc,jenis_spj asc")->result();
    }

    function mappingsp2dvsspj($q)
    {
        return $this->db->query("SELECT a.id,tgl_sp2d,no_sp2d,b.keterangan ket_sp2d,b.nilai nilai_sp2d,tgl_spj,no_spj,c.keterangan ket_spj,c.nilai nilai_spj
                                from sp2d_vs_spj a
                                join (select kd_skpd,tgl_sp2d,no_sp2d,keterangan,sum(nilai) nilai
                                      from tb_spe_sp2d
                                     where kd_skpd='$q'
                                      group by kd_skpd,tgl_sp2d,no_sp2d,keterangan) b on a.sp2d_no_sp2d=b.no_sp2d and a.sp2d_kd_skpd=b.kd_skpd
                                join (select kd_skpd,tgl_spj,no_spj,keterangan,sum(nilai) nilai
                                      from tb_spe_spj
                                      where kd_skpd='$q'
                                      group by kd_skpd,tgl_spj,no_spj,keterangan
                                      ) c on c.no_spj=a.spj_no_spj and a.sp2d_kd_skpd=c.kd_skpd")->result();
    }
}

/* End of file MSppd.php */
/* Location: ./application/models/MSppd.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-29 13:50:41 */
/* http://harviacode.com */

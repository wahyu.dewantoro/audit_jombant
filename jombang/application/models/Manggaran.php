<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Manggaran extends CI_Model
{

    public $table = 'tb_spe_anggaran';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($kd_skpd, $nm_sub_unit, $q = NULL)
    {
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where("(Kd_SKPD like '%$q%'  or Nm_Sub_Unit like '%$q%' or Akun_Akrual_3 like '%$q%' or Nm_Akrual_3 like '%$q%'  or Anggaran_Awal like '%$q%' or Anggaran_Perubahan_1 like '%$q%' or Anggaran_Perubahan_2 like '%$q%')", null, false);
        $this->db->from($this->table);
        $this->db->select("kd_skpd,ltrim(nm_sub_unit) nm_sub_unit,akun_akrual_3,nm_akrual_3,sum(anggaran_awal) anggaran_awal,sum(anggaran_perubahan_1) anggaran_perubahan_1,sum(anggaran_perubahan_2) anggaran_perubahan_2");
        $this->db->group_by('kd_skpd', false);
        $this->db->group_by('nm_sub_unit', false);
        $this->db->group_by('akun_akrual_3', false);
        $this->db->group_by('nm_akrual_3', false);
        return $this->db->count_all_results();
    }

    function total_rows_tiga($kd_skpd, $nm_sub_unit, $akun_akrual_3, $q = NULL)
    {
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where("(case when akun_akrual_3  is null then '' else akun_akrual_3 end ='$akun_akrual_3')", null, false);
        $this->db->where("(ket_program like '%$q%' or ket_kegiatan like '%$q%' or Kd_SKPD like '%$q%'  or Nm_Sub_Unit like '%$q%' or Akun_Akrual_5 like '%$q%' or Nm_Akrual_5 like '%$q%'  or Anggaran_Awal like '%$q%' or Anggaran_Perubahan_1 like '%$q%' or Anggaran_Perubahan_2 like '%$q%')", null, false);
        $this->db->from($this->table);

        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($kd_skpd, $nm_sub_unit, $limit, $start = 0, $q = NULL)
    {
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where("(Kd_SKPD like '%$q%'  or Nm_Sub_Unit like '%$q%' or Akun_Akrual_3 like '%$q%' or Nm_Akrual_3 like '%$q%'  or Anggaran_Awal like '%$q%' or Anggaran_Perubahan_1 like '%$q%' or Anggaran_Perubahan_2 like '%$q%')", null, false);
        $this->db->select("kd_skpd,ltrim(nm_sub_unit) nm_sub_unit,akun_akrual_3,nm_akrual_3,sum(anggaran_awal) anggaran_awal,sum(anggaran_perubahan_1) anggaran_perubahan_1,sum(anggaran_perubahan_2) anggaran_perubahan_2");
        $this->db->group_by('kd_skpd', false);
        $this->db->group_by('nm_sub_unit', false);
        $this->db->group_by('akun_akrual_3', false);
        $this->db->group_by('nm_akrual_3', false);
        $this->db->limit($limit, $start);

        return $this->db->get($this->table)->result();
    }

    function get_limit_data_tiga($kd_skpd, $nm_sub_unit, $akun_akrual_3, $limit, $start = 0, $q = NULL)
    {
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where("(case when akun_akrual_3  is null then '' else akun_akrual_3 end ='$akun_akrual_3')", null, false);
        $this->db->where("(ket_program like '%$q%' or ket_kegiatan like '%$q%' or Kd_SKPD like '%$q%'  or Nm_Sub_Unit like '%$q%' or Akun_Akrual_5 like '%$q%' or Nm_Akrual_5 like '%$q%'  or Anggaran_Awal like '%$q%' or Anggaran_Perubahan_1 like '%$q%' or Anggaran_Perubahan_2 like '%$q%')", null, false);
        $this->db->select("kd_skpd,ket_program,ket_kegiatan,ltrim(nm_sub_unit) nm_sub_unit,akun_akrual_5,nm_akrual_5,anggaran_awal, anggaran_perubahan_1, anggaran_perubahan_2");
        $this->db->limit($limit, $start);
        $this->db->order_by('akun_akrual_5', 'asc');
        return $this->db->get($this->table)->result();
    }


    function getData($limit, $start = 0, $q = NULL)
    {
        $this->db->select("kd_skpd,nm_sub_unit,sum(anggaran_awal) anggaran_awal,sum(anggaran_perubahan_1) anggaran_perubahan_1,sum(anggaran_perubahan_2) anggaran_perubahan_2", false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')", null, false);
        $this->db->group_by('kd_skpd');
        $this->db->group_by('nm_sub_unit');
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
    function getAllData($q = NULL)
    {
        /*$this->db->order_by("nm_sub_unit", "asc");
        $this->db->select("kd_skpd,ltrim(nm_sub_unit) nm_sub_unit ,sum(anggaran_awal) anggaran_awal,sum(anggaran_perubahan_1) anggaran_perubahan_1,sum(anggaran_perubahan_2) anggaran_perubahan_2",false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')",null,false);
        $this->db->group_by('kd_skpd');
        $this->db->group_by('nm_sub_unit');
        return $this->db->get($this->table)->result();*/

        return $this->db->query("SELECT kd_skpd,nm_unit
                                      ,sum(Anggaran_Awal_4) anggaran_awal_4
                                      ,sum(Anggaran_Awal_5) anggaran_awal_5
                                      ,sum(Anggaran_Perubahan_1_4) anggaran_perubahan_1_4
                                      ,sum(Anggaran_Perubahan_1_5) anggaran_perubahan_1_5
                                      ,sum(Anggaran_Perubahan_2_4) anggaran_perubahan_2_4
                                      ,sum(Anggaran_Perubahan_2_5) anggaran_perubahan_2_5
                                from (
                                    select Kd_SKPD,Nm_Unit, 
                                    case when Akun_Akrual_1='4' then Anggaran_Awal end Anggaran_Awal_4,
                                    case when Akun_Akrual_1='5' then Anggaran_Awal end Anggaran_Awal_5,
                                    case when Akun_Akrual_1='4' then Anggaran_Perubahan_1 end Anggaran_Perubahan_1_4,
                                    case when Akun_Akrual_1='5' then Anggaran_Perubahan_1 end Anggaran_Perubahan_1_5,
                                    case when Akun_Akrual_1='4' then Anggaran_Perubahan_2 end Anggaran_Perubahan_2_4,
                                    case when Akun_Akrual_1='5' then Anggaran_Perubahan_2 end Anggaran_Perubahan_2_5
                                     from 
                                    (
                                    SELECT TB_SPE_REF_UNIT.Kd_SKPD,TB_SPE_ANGGARAN.Nm_Unit
                                          ,Akun_Akrual_1
                                          ,Nm_Akrual_1
                                          ,sum(Anggaran_Awal) Anggaran_Awal
                                          ,sum(Anggaran_Perubahan_1) Anggaran_Perubahan_1
                                          ,sum(Anggaran_Perubahan_2) Anggaran_Perubahan_2
                                      FROM TB_SPE_ANGGARAN 
                                        left join TB_SPE_REF_UNIT on TB_SPE_ANGGARAN.Nm_Unit=TB_SPE_REF_UNIT.Nm_Unit
                                      group by TB_SPE_REF_UNIT.Kd_SKPD
                                        ,TB_SPE_ANGGARAN.Nm_Unit
                                          ,Akun_Akrual_1
                                          ,Nm_Akrual_1
                                    ) lv0
                                )lv1 group by Kd_SKPD,Nm_Unit
                                order by  Kd_SKPD")->result();
    }
    function getRow($q = NULL)
    {
        $this->db->select("kd_skpd,nm_sub_unit,sum(anggaran_awal) anggaran_awal,sum(anggaran_perubahan_1) anggaran_perubahan_1,sum(anggaran_perubahan_2) anggaran_perubahan_2", false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')", null, false);
        $this->db->group_by('kd_skpd');
        $this->db->group_by('nm_sub_unit');
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function getSubunit()
    {
        // return $this->db->query("select distinct nm_sub_unit from spe_anggaran")->result();
        return $this->db->query("SELECT distinct ltrim(nm_unit) nm_sub_unit from spe_anggaran order by nm_sub_unit asc")->result();
    }

    function getSum($q = null)
    {
        $this->db->select("sum(anggaran_awal) awal,sum(anggaran_perubahan_1) satu,sum(anggaran_perubahan_2) dua", false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')", null, false);
        return $this->db->get($this->table)->row();
    }

    function getSumdetail($kd_skpd, $nm_sub_unit, $q = NULL)
    {
        $this->db->select("sum(anggaran_awal) awal,sum(anggaran_perubahan_1) satu,sum(anggaran_perubahan_2) dua", false);
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where("(Kd_SKPD like '%$q%'  or Nm_Sub_Unit like '%$q%' or Akun_Akrual_3 like '%$q%' or Nm_Akrual_3 like '%$q%'  or Anggaran_Awal like '%$q%' or Anggaran_Perubahan_1 like '%$q%' or Anggaran_Perubahan_2 like '%$q%')", null, false);

        return $this->db->get($this->table)->row();
    }

    function getSumdetail_tiga($kd_skpd, $nm_sub_unit, $akun_akrual_3, $q = NULL)
    {
        $this->db->select("sum(anggaran_awal) awal,sum(anggaran_perubahan_1) satu,sum(anggaran_perubahan_2) dua", false);
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where("(case when akun_akrual_3  is null then '' else akun_akrual_3 end ='$akun_akrual_3')", null, false);
        $this->db->where("(ket_program like '%$q%' or ket_kegiatan like '%$q%' or Kd_SKPD like '%$q%'  or Nm_Sub_Unit like '%$q%' or Akun_Akrual_5 like '%$q%' or Nm_Akrual_5 like '%$q%'  or Anggaran_Awal like '%$q%' or Anggaran_Perubahan_1 like '%$q%' or Anggaran_Perubahan_2 like '%$q%')", null, false);
        return $this->db->get($this->table)->row();
    }

    function getDataRealisasi($limit, $start = 0, $q = NULL)
    {
        $this->db->select("tb_spe_anggaran.kd_skpd, tb_spe_anggaran.nm_sub_unit, kd_rek_gabung, nm_rek_5,sum( case when anggaran_perubahan_1 < 0 then anggaran_awal else anggaran_perubahan_1 end ) anggaran, sum(nilai) nilai ", false);
        $this->db->join('tb_spe_sp2d', 'tb_spe_anggaran.kd_skpd=tb_spe_sp2d.kd_skpd  and tb_spe_anggaran.akun_akrual_5=tb_spe_sp2d.kd_rek_gabung', false);
        $this->db->limit($limit, $start);
        $this->db->where("(tb_spe_anggaran.kd_skpd like '%$q%' or tb_spe_anggaran.nm_sub_unit like '%$q%' or kd_rek_gabung like '%$q%' or nm_rek_5 like '%$q%' or anggaran_awal like '%$q%' or anggaran_perubahan_1 like '%$q%' or anggaran_perubahan_2 like '%$q%' or nilai like '%$q%')", null, false);
        $this->db->group_by("tb_spe_anggaran.kd_skpd", false);
        $this->db->group_by("tb_spe_anggaran.nm_sub_unit", false);
        $this->db->group_by("kd_rek_gabung", false);
        $this->db->group_by("nm_rek_5", false);
        // $this->db->group_by("case when anggaran_perubahan_2 is null then case when anggaran_perubahan_1 is null then anggaran_awal else anggaran_perubahan_1 end  else anggaran_perubahan_2 end ",false);

        return $this->db->get('tb_spe_anggaran')->result();
    }

    function getAllDataRealisasi($q = NULL)
    {
        $this->db->select("tb_spe_anggaran.kd_skpd, tb_spe_anggaran.nm_sub_unit, kd_rek_gabung, nm_rek_5,sum( case when anggaran_perubahan_1 < 0 then anggaran_awal else anggaran_perubahan_1 end ) anggaran, sum(nilai) nilai ", false);
        $this->db->join('tb_spe_sp2d', 'tb_spe_anggaran.kd_skpd=tb_spe_sp2d.kd_skpd  and tb_spe_anggaran.akun_akrual_5=tb_spe_sp2d.kd_rek_gabung', false);
        $this->db->where("(tb_spe_anggaran.kd_skpd like '%$q%' or tb_spe_anggaran.nm_sub_unit like '%$q%' or kd_rek_gabung like '%$q%' or nm_rek_5 like '%$q%' or anggaran_awal like '%$q%' or anggaran_perubahan_1 like '%$q%' or anggaran_perubahan_2 like '%$q%' or nilai like '%$q%')", null, false);
        $this->db->group_by("tb_spe_anggaran.kd_skpd", false);
        $this->db->group_by("tb_spe_anggaran.nm_sub_unit", false);
        $this->db->group_by("kd_rek_gabung", false);
        $this->db->group_by("nm_rek_5", false);
        // $this->db->group_by("case when anggaran_perubahan_2 is null then case when anggaran_perubahan_1 is null then anggaran_awal else anggaran_perubahan_1 end  else anggaran_perubahan_2 end ",false);

        return $this->db->get('tb_spe_anggaran')->result();
    }

    function getRowRealisasi($q = null)
    {
        $this->db->select("tb_spe_anggaran.kd_skpd, tb_spe_anggaran.nm_sub_unit, kd_rek_gabung, nm_rek_5,sum( case when anggaran_perubahan_1 < 0 then anggaran_awal else anggaran_perubahan_1 end ) anggaran, sum(nilai) nilai ", false);
        $this->db->join('tb_spe_sp2d', 'tb_spe_anggaran.kd_skpd=tb_spe_sp2d.kd_skpd  and tb_spe_anggaran.akun_akrual_5=tb_spe_sp2d.kd_rek_gabung', false);
        // $this->db->limit($limit, $start);
        $this->db->where("(tb_spe_anggaran.kd_skpd like '%$q%' or tb_spe_anggaran.nm_sub_unit like '%$q%' or kd_rek_gabung like '%$q%' or nm_rek_5 like '%$q%' or anggaran_awal like '%$q%' or anggaran_perubahan_1 like '%$q%' or anggaran_perubahan_2 like '%$q%' or nilai like '%$q%')", null, false);
        $this->db->group_by("tb_spe_anggaran.kd_skpd", false);
        $this->db->group_by("tb_spe_anggaran.nm_sub_unit", false);
        $this->db->group_by("kd_rek_gabung", false);
        $this->db->group_by("nm_rek_5", false);



        $this->db->from('tb_spe_anggaran');
        return $this->db->count_all_results();
    }


    function dataRealisai($q = null)
    {
        return $this->db->query("SELECT ang.kd_skpd,ang.*,realisasi_4,realisasi_5  
                                    from audit_anggaran ang
                                    left join audit_realisasi realisasi on ang.kd_skpd=realisasi.kd_skpd
                                    order by ang.kd_skpd")->result();
    }

    function GetLeveldua($nm_unit, $akun)
    {

        return $this->db->query("SELECT a.*,nilai_realisasi
                                    from (
                                       SELECT akun_akrual_2,nm_akrual_2,sum(anggaran_awal) nilai,sum(anggaran_perubahan_1) nilaisatu
                                            from tb_spe_anggaran
                                            where nm_unit='$nm_unit'
                                            and Akun_Akrual_1='$akun'
                                            group by nm_unit,akun_akrual_2,nm_akrual_2 
                                    ) a
                                    left join (    
                                        SELECT akun_akrual_2,nm_akrual_2,abs(sum(debet)-sum(kredit)) nilai_realisasi
                                                     from tb_spe_detail_lra
                                                     where nm_unit='$nm_unit'
                                                     and Akun_Akrual_1='$akun'
                                                     group by akun_akrual_2,nm_akrual_2
                                       ) b on a.akun_akrual_2=b.akun_akrual_2")->result();

        /*return $this->db->query("SELECT akun_akrual_2,nm_akrual_2,sum(anggaran_awal) nilai,sum(anggaran_perubahan_1) nilaisatu
                                from tb_spe_anggaran
                                where nm_unit='$nm_unit'
                                and Akun_Akrual_1='$akun'
                                group by akun_akrual_2,nm_akrual_2 order by akun_akrual_2 asc")->result();*/
    }

    function GetLeveltiga($nm_unit, $akun)
    {
        return $this->db->query("SELECT a.*,nilai_realisasi
                                    from (
                                       SELECT akun_akrual_3,nm_akrual_3,sum(anggaran_awal) nilai,sum(anggaran_perubahan_1) nilaisatu
                                            from tb_spe_anggaran
                                            where nm_unit='$nm_unit'
                                            and Akun_Akrual_2='$akun'
                                            group by nm_unit,akun_akrual_3,nm_akrual_3 
                                    ) a
                                    left join (    
                                        SELECT akun_akrual_3,nm_akrual_3,abs(sum(debet)-sum(kredit)) nilai_realisasi
                                                     from tb_spe_detail_lra
                                                     where nm_unit='$nm_unit'
                                                     and Akun_Akrual_2='$akun'
                                                     group by akun_akrual_3,nm_akrual_3
                                       ) b on a.akun_akrual_3=b.akun_akrual_3")->result();
        /*        return $this->db->query("SELECT akun_akrual_3,nm_akrual_3,sum(anggaran_awal) nilai,sum(anggaran_perubahan_1) nilaisatu
                                from tb_spe_anggaran
                                where nm_unit='$nm_unit'
                                and Akun_Akrual_2='$akun'
                                group by akun_akrual_3,nm_akrual_3 order by akun_akrual_3 asc")->result();*/
    }

    function GetLevelempat($unit, $akun)
    {
        /*return $this->db->query("SELECT akun_akrual_4,nm_akrual_4,sum(anggaran_awal) nilai,sum(anggaran_perubahan_1) nilaisatu
                                from tb_spe_anggaran
                                where nm_unit='$nm_unit'
                                and Akun_Akrual_3='$akun'
                                group by akun_akrual_4,nm_akrual_4 order by akun_akrual_4 asc")->result();*/
        return $this->db->query("SELECT a.*,b.nilai_realisasi from (
                                                            SELECT akun_akrual_4,nm_akrual_4,sum(anggaran_awal) nilai,sum(anggaran_perubahan_1) nilaisatu
                                                                                            from tb_spe_anggaran
                                                                                            where nm_unit='$unit'
                                                                                            and Akun_Akrual_3='$akun'
                                                                                            group by akun_akrual_4,nm_akrual_4 
                                                            ) a
                                                            left join 
                                                            (
                                                            SELECT akun_akrual_4,nm_akrual_4,abs(sum(debet)-sum(kredit)) nilai_realisasi
                                                                                            from tb_spe_detail_lra
                                                                                            where nm_unit='$unit'
                                                                                            and Akun_Akrual_3='$akun'
                                                                                            group by nm_unit,akun_akrual_4,nm_akrual_4 
                                                            ) b on a.akun_akrual_4=b.akun_akrual_4")->result();
    }
}

/* End of file Manggaran.php */
/* Location: ./application/models/Manggaran.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-29 13:57:23 */
/* http://harviacode.com */

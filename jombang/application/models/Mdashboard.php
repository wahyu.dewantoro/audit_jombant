<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mdashboard extends CI_Model
{


    /*public $id = 'id_inc';
    public $order = 'DESC';*/

    function __construct()
    {
        parent::__construct();
        /*
         $this->table = 'ms_pengguna';
        $this->menu_db = $this->load->database('menu', true);*/
    }

    function getList()
    {
        return $this->db->query("SELECT nama_dashboard,url_dashboard, icon FROM MS_SHORTCUT_DASHBOARD")->result();
    }
}

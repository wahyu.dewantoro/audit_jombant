<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mkeuangan extends CI_Model
{


    function __construct()
    {
        parent::__construct();
        $this->menu_db = $this->load->database('menu', true);
    }

    function _config()
    {
        $tahun = get_userdata('tahun_anggaran');
        return $this->menu_db->query("select tahun,linked_server,nama_db from ms_config where tahun=?", array($tahun))->row();
    }

    /* 
    function getPendapatanLRA($opd)
    {
        $res = $this->db->query("select case when (sum(debet)-sum(kredit)) is null then 0 else (sum(debet)-sum(kredit)) end  nilai 
        from TB_SPE_DETAIL_LRA 
        where kd_skpd like '" . $opd . "%' and Akun_Akrual_1='4'")->row()->nilai;
        if (!empty($res)) {
            return $res;
        } else {
            return null;
        }
    }

    function getBelanjaLRA($opd)
    {
        $res = $this->db->query("select case when (sum(debet)-sum(kredit)) is null then 0 else (sum(debet)-sum(kredit)) end  nilai 
        from TB_SPE_DETAIL_LRA 
        where kd_skpd like '" . $opd . "%' and Akun_Akrual_1='5'")->row()->nilai;
        if (!empty($res)) {
            return $res;
        } else {
            return null;
        }
    } */

    function getPenerimaanBiayaLRA($opd)
    {
        return 0;
    }

    function getPengeluaranBiayaLRA($opd)
    {
        return 0;
    }

    function getNeracaAset($opd)
    {
        $res = $this->db->query("select abs(vsaldoawal_lv0.sumnilai_1)+abs(vneraca_lv0.sumnilai_1) res
        from vneraca_lv0
                                          left join vsaldoawal_lv0 on vneraca_lv0.nm_unit=vsaldoawal_lv0.nm_unit
                                          where vneraca_lv0.kd_skpd='$opd'")->row();
        if (!empty($res)) {
            return $res->res;
        } else {
            return null;
        }
    }

    function getNeracaKewajiban($opd)
    {
        $res = $this->db->query("select abs(vsaldoawal_lv0.sumnilai_2)+abs(vneraca_lv0.sumnilai_2) res
        from vneraca_lv0
                                          left join vsaldoawal_lv0 on vneraca_lv0.nm_unit=vsaldoawal_lv0.nm_unit
                                          where vneraca_lv0.kd_skpd='$opd'")->row();
        if (!empty($res)) {
            return $res->res;
        } else {
            return null;
        }
    }

    function getNeracaEkuitas($opd)
    {
        $res = $this->db->query("select case when vneraca_lv0.sumnilai_3 is null then 0 else vneraca_lv0.sumnilai_3 end res
        from vneraca_lv0
                                          left join vsaldoawal_lv0 on vneraca_lv0.nm_unit=vsaldoawal_lv0.nm_unit
                                          where vneraca_lv0.kd_skpd='$opd'")->row();
        if (!empty($res)) {
            return $res->res;
        } else {
            return null;
        }
    }

    /* function rptLRA($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {
        $this->db->trans_start();
        $this->db->query("delete from  tb_rpt_lra where kd_urusan='$Kd_Urusan' and kd_bidang='$Kd_Bidang' and kd_unit='$Kd_Unit' and kd_sub='$Kd_Sub'");
        $this->db->query("INSERT INTO tb_rpt_lra  exec dbo.RptLRA $Tahun,$Kd_Urusan,$Kd_Bidang,$Kd_Unit,$Kd_Sub,'$D'");
        $this->db->trans_complete();
        return $this->db->query("select * from tb_rpt_lra where kd_urusan='$Kd_Urusan' and kd_bidang='$Kd_Bidang' and kd_unit='$Kd_Unit' and kd_sub='$Kd_Sub'")->result_array();
    } */

    function rptLO($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {

        $cf = $this->_config();

        return $this->db->query("SELECT * FROM  OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON; EXEC " . $cf->nama_db . ".dbo.rptlo $Tahun,$Kd_Urusan,$Kd_Bidang,$Kd_Unit,$Kd_Sub,''$D''')   ")->result_array();
    }


    function rptLPE($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {
        $cf = $this->_config();
        return $this->db->query("SELECT *    
        FROM   OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptLPE $Tahun,$Kd_Urusan,$Kd_Bidang,$Kd_Unit,$Kd_Sub,''$D''')")->result_array();
    }

    function RptNeraca($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {
        $cf = $this->_config();
        $skpd = $Kd_Urusan . $Kd_Bidang . $Kd_Unit . $Kd_Sub;
        return $this->db->query("SELECT * 
        FROM   OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptNeracaAK $Tahun,$Kd_Urusan,$Kd_Bidang,$Kd_Unit,$Kd_Sub,$skpd,''$D''')d ")->result_array();
    }

    function RptNeracSAP($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {
        $cf = $this->_config();
        $skpd = $Kd_Urusan . $Kd_Bidang . $Kd_Unit . $Kd_Sub;
        return $this->db->query("SELECT * 
        FROM   OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptNeracaAKSAP $Tahun,$Kd_Urusan,$Kd_Bidang,$Kd_Unit,$Kd_Sub,$skpd,''$D''')")->result_array();
    }

    function RptLra($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {
        $cf = $this->_config();
        return $this->db->query("SELECT *    
        FROM   OPENQUERY(" . $cf->linked_server . ",  
        'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptLra $Tahun,$Kd_Urusan,$Kd_Bidang,$Kd_Unit,$Kd_Sub,''$D''')")->result_array();
    }

    function dataLraVertikal($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {

        $cf = $this->_config();
        $lrasilpa = $this->db->query("SELECT sum(realisasi) realisasi
        FROM   OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptLraAK $Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, ''$D''')		where Kd_Rek_1='7'")->row()->realisasi;



        $pendapatanlra = $this->db->query("SELECT sum(realisasi) realisasi
        FROM   OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptLraAK $Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, ''$D''')
        where  Kd_Rek_1='4'")->row()->realisasi;

        $belanjalra = $this->db->query("SELECT sum(realisasi) realisasi
        FROM   OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptLraAK $Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, ''$D''')
		where  Kd_Rek_1='5'")->row()->realisasi;

        $data = array(
            array(
                'ket' => 'SiLPA tahun berjalan harus sama dengan total pendapatan dikurangi total belanja dan transfer ditambah total penerimaan pembiayaan dikurangi dengan total pengeluaran ',
                'nilai' => 'SiLPA = Total Pendapatan  –  Total Belanja dan Transfer + Total Penerimaan Pembiayaan  –  Total Pengeluaran Pembiayaan'
            ),
            array(
                'ket' => 'SILPA LRA tahun berjalan',
                'nilai' => $lrasilpa
            ),
            array(
                'ket' => 'Total Pendapatan',
                'nilai' => $pendapatanlra
            ),
            array(
                'ket' => 'Total Belanja & Transfer',
                'nilai' => $belanjalra
            ),
            array(
                'ket' => 'Penerimaan Pembiayaan',
                'nilai' => 0
            ),
            array(
                'ket' => 'Pengeluaran Pembiayaan',
                'nilai' => 0
            ),
            array(
                'ket' => '<b>Selisih</b>',
                'nilai' => ($lrasilpa - (0 - $pendapatanlra +   0 - $belanjalra))
            )
        );

        return $data;
    }

    function dataLraHorizontal($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {

        $cf = $this->_config();
        $skpd = $Kd_Urusan . $Kd_Bidang . $Kd_Unit . $Kd_Sub;
        $dasar = "OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptLraAK $Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, ''$D''')";
        $dasarb = "OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptNeracaAKSAP $Tahun,$Kd_Urusan,$Kd_Bidang,$Kd_Unit,$Kd_Sub,$skpd,''$D''')";

        //silpa
        $this->db->where('Kd_Rek_1', 7);
        $this->db->select('sum(Realisasi) nilai');
        $silpa = $this->db->get($dasar)->row()->nilai;

        // kasda
        $this->db->where('Kd_Rek_1', 1);
        $this->db->where('Kd_Rek_2', 1);
        $this->db->where('Kd_Rek_3', 1);
        $this->db->where('Kd_Rek_4', 1);
        $this->db->select('sum(SaldoX1) nilai');
        $kasda = $this->db->get($dasarb)->row()->nilai;

        $this->db->where('Kd_Rek_1', 1);
        $this->db->where('Kd_Rek_2', 1);
        $this->db->where('Kd_Rek_3', 1);
        $this->db->where('Kd_Rek_4', 3);
        $this->db->select('sum(SaldoX1) nilai');
        $kaspe = $this->db->get($dasarb)->row()->nilai;

        $this->db->where('Kd_Rek_1', 1);
        $this->db->where('Kd_Rek_2', 1);
        $this->db->where('Kd_Rek_3', 1);
        $this->db->where('Kd_Rek_4', 4);
        $this->db->select('sum(SaldoX1) nilai');
        $kb = $this->db->get($dasarb)->row()->nilai;

        $this->db->where('Kd_Rek_1', 1);
        $this->db->where('Kd_Rek_2', 1);
        $this->db->where('Kd_Rek_3', 1);
        $this->db->where('Kd_Rek_4', 9);
        $this->db->select('sum(SaldoX1) nilai');
        $sk = $this->db->get($dasarb)->row()->nilai;

        $this->db->where('Kd_Rek_1', 1);
        $this->db->where('Kd_Rek_2', 1);
        $this->db->where('Kd_Rek_3', 1);
        $this->db->where('Kd_Rek_4', 6);
        $this->db->select('sum(SaldoX1) nilai');
        $bos = $this->db->get($dasarb)->row()->nilai;

        $this->db->where('Kd_Rek_1', 2);
        $this->db->where('Kd_Rek_2', 1);
        $this->db->where('Kd_Rek_3', 1);
        $this->db->select('sum(SaldoX1) nilai');
        $pfk = $this->db->get($dasarb)->row()->nilai;

        $a = array(
            array(
                'ket' => 'SiLPA di LRA harus sama dengan Kas di Kas Daerah ditambah Kas di Bendahara Pengeluaran ditambah Kas di BLUD ditambah Setara Kas dikurangi dengan Utang PFK di neraca.',
                'nilai' => 'SiLPA (LRA) = Kas di Kas Daerah + Kas di Bendahara Pengeluaran + Kas di BLUD + Setara Kas – Utang PFK (Neraca)          '
            ),
            array(
                'ket' => 'Silpa di LRA',
                'nilai' => $silpa
            ),
            array(
                'ket' => 'Kas di kas daerah',
                'nilai' => $kasda
            ),
            array(
                'ket' => 'Kas di bendahara pengeluaran',
                'nilai' => $kaspe
            ),
            array(
                'ket' => 'Kas di BLUD',
                'nilai' => $kb
            ),
            array(
                'ket' => 'Setara kas',
                'nilai' => $sk
            ),
            array(
                'ket' => 'Kas di bendahara dana BOS',
                'nilai' => $bos
            ),
            array(
                'ket' => 'Utang PFK neraca',
                'nilai' => $pfk
            ),
            array(
                'ket' => '<b>Selisih</b>',
                'nilai' => $silpa - ($pfk + $bos + $sk + $kb + $kaspe + $kasda)
            )
        );

        $b = array(
            array(
                'ket' => 'Untuk metode harga perolehan, Pengeluaran Pembiayaan untuk Penyertaan Modal Daerah (LRA) harus tercermin dalam penambahan Nilai Penyertaan Modal Daerah (Neraca)',
                'nilai' => 'Untuk metode harga perolehan, Pengeluaran pembiayaan untuk Penyertaan Modal Daerah (LRA) = penambahan nilai penyertaan modal pemerintah daerah (Neraca). '
            ),
            array(
                'ket' => 'Pengeluaran pembiayaan (penyertaan modal)',
                'nilai' => 0
            ),
            array(
                'ket' => 'Saldo penyertaan modal tahun X-1',
                'nilai' => 0
            ),
            array(
                'ket' => 'Saldo penyertaan modal tahun X',
                'nilai' => 0
            ),
            array(
                'ket' => 'Selisih',
                'nilai' => 0
            )
        );

        
        $c = array(
            array(
                'ket' => 'Penerimaan/Pengeluaran Pembiayaan Pinjaman Jangka Panjang (LRA) = Utang Jangka Panjang + Bagian Lancar Utang Jangka Panjang Tahun berkenaan – Utang Jangka Panjang Tahun sebelumnya - Bagian Lancar Utang Jangka Panjang tahun sebelumnya.',
                'nilai' => 'Penerimaan/Pengeluaran Pembiayaan Pinjaman Jangka Panjang (LRA) = Utang Jangka Panjang + Bagian Lancar Utang Jangka Panjang Tahun berkenaan – Utang Jangka Panjang Tahun sebelumnya (Neraca)'
            ),
            array(
                'ket' => 'Penerimaan/pengeluaran pembiayaan pinjaman jk. panjang (LRA)',
                'nilai' => 0
            ),
            array(
                'ket' => 'Utang jangka panjang (pinjaman)',
                'nilai' => 0
            ),
            array(
                'ket' => 'Bag. lancar utang jangka panjang thn x',
                'nilai' => 0
            ),
            array(
                'ket' => 'Utang jangka panjang tahun x-1 (pinjaman)',
                'nilai' => 0
            ),
            array(
                'ket' => 'Bag. lancar utang jangka panjang thn x-1',
                'nilai' => 0
            ),
            array(
                'ket' => 'Selisih',
                'nilai' => 0
            )
        );

        //belanja tanah           
        $dbt = $this->db->query("SELECT sum(Realisasi) nilai from OPENQUERY(jbg, 'SET FMTONLY OFF; SET NOCOUNT ON; EXEC jbg_2018.dbo.RptLRAak $Tahun,$Kd_Bidang,$Kd_Urusan,$Kd_Unit,$Kd_Sub,''$D''') 
                                 where Kd_Rek_1=5 and Kd_Rek_2=2 and Kd_Rek_3=1")->row()->nilai;

        $dtn=$this->db->query("select sum(SaldoX1) a,case when sum(saldox0)=0 then sum(saldox1) else sum(saldox0) end b  FROM   OPENQUERY(
            jbg,  
             'SET FMTONLY OFF; SET NOCOUNT ON;  exec jbg_2018.dbo.RptNeracaAK $Tahun,$Kd_Urusan,$Kd_Bidang,$Kd_Unit,$Kd_Sub,''%'',''$D''')
             where Kd_Rek_Gab3='1 . 3 . 1'")->row();
        
        $d = array(
            array(
                'ket' => 'Realisasi belanja modal harus sama dengan penambahan aset tetap (dan aset lainnya), jika selisih harus dijelaskan di CALK',
                'nilai' => 'Teliti apakah pengungkapan selisih dalam CaLK sudah cukup memadai. Mungkin ada penerimaan hibah berupa aset dan kapitalisasi biaya. Atau ada kesalahan berupa: salah anggaran selain BM ternyata menghasilkan aset atau aset daerah yg baru ditemukan'
            ),
            array(
                'ket' => 'Realisasi belanja modal tanah',
                'nilai' => $dbt
            ),
            array(
                'ket' => 'Penambahan (penurunan)',
                'nilai' => ($dtn->a)  -  ($dtn->b)
            ),
            array(
                'ket' => 'Aset tanah x',
                'nilai' => $dtn->a
            ),
            array(
                'ket' => 'Aset tanah x-1',
                'nilai' => $dtn->b
            ),
            array(
                'ket' => '<b>Selisih<b>',
                'nilai' => $dbt-( ($dtn->a)  - ($dtn->b))
            )
        );


        return array($a, $b, $c, $d);
    }

    function dataNeracaVertikal($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {

        $cf = $this->_config();
        $aset = $this->db->query("SELECT sum(saldox1) nilai
        FROM   OPENQUERY(
        " . $cf->linked_server . ",  
        'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptNeracaAKSAP $Tahun,$Kd_Urusan,$Kd_Bidang,$Kd_Unit,$Kd_Sub,''%'',''$D''')
        where kd_rek_1='1' and  not ( kd_rek_2=1 and kd_rek_3=1 and kd_rek_4=1) ")->row()->nilai;

        $kewajiban = $this->db->query("SELECT sum(saldox1) nilai
        FROM   OPENQUERY(
        " . $cf->linked_server . ",  
        'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptNeracaAK $Tahun,$Kd_Urusan,$Kd_Bidang,$Kd_Unit,$Kd_Sub,''%'',''$D''')
        where kd_rek_1='2'")->row()->nilai;

        $ekuitas = $this->db->query("SELECT sum(saldox1) nilai
        FROM   OPENQUERY(
        " . $cf->linked_server . ",  
        'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptNeracaAKSAP $Tahun,$Kd_Urusan,$Kd_Bidang,$Kd_Unit,$Kd_Sub,''%'',''$D''')
		where kd_rek_1='3'")->row()->nilai;


        // data b
        $kdskpd = $Kd_Urusan . $Kd_Bidang . $Kd_Unit . $Kd_Sub;
        $dasarb = "OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptNeracaAKSAP $Tahun,$Kd_Urusan,$Kd_Bidang,$Kd_Unit,$Kd_Sub,$kdskpd,''$D''')";


        $dataa = array(
            array(
                'ket' => 'Aset harus sama dengan total kewajiban ditambah dengan total ekuitas.',
                'nilai' => 'Aset = Kewajiban + Ekuitas'
            ),
            array(
                'ket' => 'Aset',
                'nilai' => $aset
            ),
            array(
                'ket' => 'Kewajiban',
                'nilai' => $kewajiban
            ),
            array(
                'ket' => 'Ekuitas',
                'nilai' => $ekuitas
            ),
            array(
                'ket' => '<b>Selisih</b>',
                'nilai' => $aset - ($ekuitas + $kewajiban)
            )
        );


        // bendahara pengeluaran
        $this->db->where('Kd_rek_1', 1);
        $this->db->where('Kd_rek_2', 1);
        $this->db->where('Kd_rek_3', 1);
        $this->db->where('Kd_rek_4', 3);
        $ba = $this->db->get($dasarb)->row();

        $bac = $ba->SaldoX1;
        $bad = $ba->SaldoX0;


        // utang pihak 3
        $this->db->where('Kd_Rek_1', 2);
        $this->db->where('Kd_Rek_2', 1);
        $this->db->where('Kd_Rek_3', 1);
        $this->db->where(" ( Kd_Rek_4 in (1,2,3,4,5,6,7,7) ) ", '', false);
        $this->db->select("sum(SaldoX0) nilai");
        $ca = $this->db->get($dasarb)->row()->nilai;

        $databa = array(
            array(
                'ket' => 'Kas di Bendahara Pengeluaran harus sama dengan sisa Uang Persediaan yang belum disetor ke kasda ditambah dengan Utang PFK di Bendahara Pengeluaran yang belum disetor ke kas negara.',
                'nilai' => 'Kas di Bendahara Pengeluaran = Sisa Uang Persediaan yang Belum Disetor + Utang PFK di Bendahara Pengeluaran'
            ),
            array(
                'ket' => 'Kas di bendahara pengeluaran',
                'nilai' => $bac
            ),
            array(
                'ket' => 'Sisa uang persediaan yang belum di setor',
                'nilai' => $bad
            ),
            array(
                'ket' => 'Utang PFK di bendahara pengeluaran',
                'nilai' => $ca
            ),
            array(
                'ket' => '<b>Selisih</b>',
                'nilai' => $bac - ($bad + $ca)
            )
        );

        $data = array(
            $dataa,
            $databa
        );

        return $data;
    }

    function dataLo($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {
        $cf = $this->_config();
        $kdskpd = $Kd_Urusan . $Kd_Bidang . $Kd_Unit . $Kd_Sub;
        $dasar = "OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON; EXEC " . $cf->nama_db . ".dbo.rptlo $Tahun,$Kd_Urusan,$Kd_Bidang,$Kd_Unit,$Kd_Sub,''$D''')";

        $this->db->where('Kd_Grup', 9);
        $this->db->select('sum(Realisasi) nilai');
        $slo = $this->db->get($dasar)->row()->nilai;

        $this->db->where('Kd_Akrual_1', 8);
        $this->db->where('Kd_Akrual_2 in (1,2,3) ', '', false);
        $this->db->select('sum(Realisasi) nilai');
        $plo = $this->db->get($dasar)->row()->nilai;

        $this->db->where('Kd_Akrual_1', 9);
        $this->db->where('Kd_Akrual_2 in (1,2) ', '', false);
        $this->db->select('sum(Realisasi) nilai');
        $blo = $this->db->get($dasar)->row()->nilai;



        $this->db->where('kd_grup=2 and kd_akrual_1=10 and kd_akrual_2=99 and kd_akrual_3=0', '', false);
        $this->db->select('sum(Realisasi) nilai');
        $no = $this->db->get($dasar)->row()->nilai;

        $this->db->where('kd_grup=5 and kd_akrual_1=10 and kd_akrual_2=99 and kd_akrual_3=0', '', false);
        $this->db->select('sum(Realisasi) nilai');
        $plos = $this->db->get($dasar)->row()->nilai;

        $a = array(
            array(
                'ket' => 'Surplus/Defisit LO harus sama dengan total Pendapatan (LO) dikurangi total Beban (LO) ditambah (dikurangi) total Surplus (Defisit) Kegiatan Non Operasional (LO) ditambah (dikurangi) Pos Luar Biasa (LO)',
                'nilai' => 'Surplus/Defisit LO= Total Pendapatan (LO) - Total Beban (LO) +/- Total Surplus/Defisit Kegiatan Non Operasional (LO) +/- Pos Luar Biasa (LO)'
            ),
            array(
                'ket' => 'Surplus (Defisit) LO',
                'nilai' => $slo
            ),
            array(
                'ket' => 'Total Pendapatan LO',
                'nilai' => $plo
            ),
            array(
                'ket' => 'Total Beban LO',
                'nilai' => -$blo
            ),
            array(
                'ket' => 'Total surplus (Defisit) Kegiatan non operasional',
                'nilai' => $no
            ),
            array(
                'ket' => 'Total POS Luar biasa',
                'nilai' => $plos
            ),
            array(
                'ket' => '<b>Selisih</b>',
                'nilai' => $slo - (($plos) + ($no) + (-$blo) + ($plo))
            )
        );

        $data = array($a);

        return $data;
    }


    function dataLAK($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {
        $a = array(
            array(
                'ket' => 'Arus kas bersih dari aktivitas operasi harus sama dengan arus masuk kas dari aktivitas operasi dikurangi arus keluar kas dari aktivitas operasi.',
                'nilai' => 'Arus Kas Bersih dari Aktivitas Operasi = Arus Masuk Kas dari Aktivitas Operasi -Arus Keluar Kas dari Aktivitas Operasi'
            ),
            array('ket' => 'Arus kas bersih aktivitas operasi', 'nilai' => 0),
            array('ket' => 'Arus kas masuk aktivitas operasi', 'nilai' => 0),
            array('ket' => 'Arus kas keluar aktivitas operasi', 'nilai' => 0),
            array('ket' => 'Selisih', 'nilai' => 0),
        );

        $b = array(
            array(
                'ket' => 'Arus Kas Bersih dari Aktivitas Investasi harus sama dengan Arus Masuk Kas dari Aktivitas Investasi dikurangi Arus Keluar Kas dari Aktivitas Investasi',
                'nilai' => 'Arus Kas Bersih dari Aktivitas Investasi = 
                Arus Masuk Kas dari Aktivitas Investasi -  Arus Keluar Kas dari Aktivitas Investasi'
            ),
            array('ket' => 'Arus kas bersih aktivitas investasi ', 'nilai' => 0),
            array('ket' => 'Arus masuk kas akt. investasi ', 'nilai' => 0),
            array('ket' => 'Arus keluar kas akt. investasi', 'nilai' => 0),
            array('ket' => 'Selisih', 'nilai' => 0)
        );

        $c = array(
            array(
                'ket' => 'Arus Kas Bersih dari Aktivitas Pendanaan harus sama dengan Arus Masuk Kas dari Aktivitas Pendanaan dikurangi Arus Keluar Kas dari Aktivitas Pendanaan',
                'nilai' => 'Arus Kas Bersih dari Aktivitas Pendanaan = Arus Masuk Kas dari Aktivitas Pendanaan -  Arus Keluar Kas dari Aktivitas Pendanaan '
            ),
            array('ket' => 'Arus kas bersih aktivitas pendanaan', 'nilai' => 0),
            array('ket' => 'Arus masuk kas aktivitas pendanaan', 'nilai' => 0),
            array('ket' => 'Arus keluar kas aktivitas pendanaan', 'nilai' => 0),
            array('ket' => 'Selisih', 'nilai' => 0)
        );

        $d = array(
            array(
                'ket' => 'Arus Kas Bersih dari Aktivitas Transitoris harus sama dengan Arus Masuk Kas dari Aktivitas Transitoris ditambah Arus Keluar Kas dari Aktivitas Transitoris',
                'nilai' => 'Arus Kas Bersih dari Aktivitas Transitoris =  Arus Masuk Kas dari Aktivitas Transitoris +  Arus Keluar Kas dari Aktivitas Transitoris'
            ),
            array('ket' => 'Arus kas bersih aktivitas transitoris', 'nilai' => 0),
            array('ket' => 'Arus masuk kas aktivitas transitoris', 'nilai' => 0),
            array('ket' => 'Arus keluar kas aktivitas transitoris', 'nilai' => 0),
            array('ket' => 'Selisih', 'nilai' => 0),
        );

        $e = array(
            array(
                'ket' => 'Kenaikan/Penurunan Kas harus sama dengan Arus Kas Bersih dari Aktivitas Operasi ditambah Arus Kas Bersih dari Aktivitas Investasi ditambah Arus Kas Bersih dari Aktivitas Pendanaan ditambah Arus Kas Bersih dari Aktivitas Transitoris.',
                'nilai' => 'Kenaikan/Penurunan Kas = Arus Kas Bersih dari Aktivitas Operasi + Arus Kas Bersih dari Aktivitas Investasi  + Arus Kas Bersih dari Aktivitas Pendanaan + Arus Kas Bersih dari Aktivitas Transitoris'
            ),
            array('ket' => 'Kenaikan/penurunan kas', 'nilai' => 0),
            array('ket' => 'Arus kas bersih aktivitas operasi', 'nilai' => 0),
            array('ket' => 'Arus kas bersih aktivitas investasi ', 'nilai' => 0),
            array('ket' => 'Arus kas bersih aktivitas pendanaan', 'nilai' => 0),
            array('ket' => 'Arus kas bersih aktivitas transitoris', 'nilai' => 0),
            array('ket' => 'Selisih', 'nilai' => 0),
        );

        $f = array(
            array(
                'ket' => 'Saldo Akhir Kas di BUD harus sama dengan Saldo Awal Kas di BUD ditambah Kenaikan/ Penurunan Kas',
                'nilai' => 'Saldo Akhir Kas di BUD = Saldo Awal Kas di BUD + Kenaikan/Penurunan Kas'
            ),
            array('ket' => 'Saldo akhir kas bud', 'nilai' => 0),
            array('ket' => 'Saldo awal kas bud', 'nilai' => 0),
            array('ket' => 'Kenaikan/penurunan kas', 'nilai' => 0),
            array('ket' => 'Selisih', 'nilai' => 0),
        );

        $g = array(
            array(
                'ket' => 'Saldo Akhir Kas harus sama dengan Saldo Akhir di BUD ditambah Saldo Akhir Kas di Bendahara Pengeluaran ditambah Saldo Akhir Kas Di Bendahara Penerimaan + Saldo Akhir Kas di BLUD',
                'nilai' => 'Saldo Akhir Kas = Saldo Akhir di BUD + Saldo Akhir Kas di Bendahara Pengeluaran + SaldoAkhir Kas di Bendahara Penerimaan + Saldo Akhir Kas di BLUD'
            ),
            array('ket' => 'Saldo akhir kas', 'nilai' => 0),
            array('ket' => 'Saldo akhir kas di bud', 'nilai' => 0),
            array('ket' => 'Saldo akhir kas di bendahara pengeluaran', 'nilai' => 0),
            array('ket' => 'Saldo akhir kas di bendahara penerimaan', 'nilai' => 0),
            array('ket' => 'Saldo akhir kas di blud', 'nilai' => 0),
            array('ket' => 'Saldo akhir kas di bendahara dana bos', 'nilai' => 0),
            array('ket' => 'Selisih', 'nilai' => 0)
        );


        return array($a, $b, $c, $d, $e, $f, $g);
    }


    function dasarSAL($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {
        $this->db->query("delete from LPSAL_OPD where kd_urusan='$Kd_Urusan' and kd_bidang='$Kd_Bidang' and kd_unit='$Kd_Unit' and kd_sub='$Kd_Sub'");
        $this->db->query("INSERT INTO [dbo].[LPSAL_OPD]
        ([Kd_Grup_1]
        ,[Kd_Grup_2]
        ,[Nm_Grup_1]
        ,[Nm_Grup_2]
        ,[Realisasi]
        ,[RealisasiX0]
        ,[Balik]
        ,[BalikX0]
        ,[Nm_PimpDaerah]
        ,[Jab_PimpDaerah],
        Kd_Urusan,Kd_Bidang,Kd_Unit,Kd_Sub)
        exec dbo.RptLPSAL_OPD $Tahun,'$D',$Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub");
        return $this->db->query("select nm_grup_1,nm_grup_2,Realisasi,RealisasiX0 from [LPSAL_OPD] where kd_urusan='$Kd_Urusan' and kd_bidang='$Kd_Bidang' and kd_unit='$Kd_Unit' and kd_sub='$Kd_Sub'")->result_array();
    }

    function dataSAL($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {
        $res = $this->dasarSAL($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D);
        $ak = (!empty($res[0]['Realisasi'])?$res[0]['Realisasi']:0 - !empty($res[1]['Realisasi'])?$res[1]['Realisasi']:0) +  !empty($res[2]['Realisasi'])?$res[2]['Realisasi']:0 + !empty($res[3]['Realisasi'])?$res[3]['Realisasi']:0 + !empty($res[4]['Realisasi'])?$res[4]['Realisasi']:0;

        $a = array(
            array(
                'ket' => 'SAL Akhir harus sama dengan SAL Awal dikurangi Penggunaan SAL sebagai penerimaan pembiayaan tahun berkenaan ditambah Sisa Lebih/Kurang Pembiayaan Anggaran Tahun Berkenaan ditambah Koreksi Kurang/Lebih Kesalahan Pembukuan Tahun Sebelumnya',
                'nilai' => 'SAL Akhir = SAL Awal – Penggunaan SAL sebagai penerimaan pembiayaan tahun berkenaan + Sisa Lebih/Kurang Pembiayaan Anggaran Tahun Berkenaan + Koreksi Kurang/Lebih Kesalahan Pembukuan Tahun Sebelumnya
        '
            ),
            array('ket' => 'SAL Akhir', 'nilai' => $ak),
            array('ket' => 'SAL Awal', 'nilai' => !empty($res[0]['Realisasi'])?$res[0]['Realisasi']:0),
            array('ket' => 'Penggunaan SAL', 'nilai' => !empty($res[1]['Realisasi'])?$res[1]['Realisasi']:0),
            array('ket' => 'SAL Tahun Berjalan', 'nilai' => !empty($res[2]['Realisasi'])?$res[2]['Realisasi']:0),
            array('ket' => 'Koreksi Kurang/Lebih Kesalahan Pembukuan Tahun Sebelumnya', 'nilai' => !empty($res[3]['Realisasi'])?$res[3]['Realisasi']:0),
            array('ket' => 'Selisih', 'nilai' => $ak - ((!empty($res[0]['Realisasi'])?$res[0]['Realisasi']:0 - !empty($res[1]['Realisasi'])?$res[1]['Realisasi']:0) +  !empty($res[2]['Realisasi'])?$res[2]['Realisasi']:0)),
        );

        return array($a);
    }

    function dataLRALAK($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {
        
        $cf=$this->_config();
        // Pendapatan daerah (LRA)
        $pendapatanlra = $this->db->query("SELECT sum(realisasi) realisasi
        FROM   OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptLraAK $Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, ''$D''')
        where  Kd_Rek_1='4'")->row()->realisasi;
        
        $a = array(
            array('ket'=>'Arus Kas Masuk dari Aktivitas Operasi (LAK) harus sama dengan Total Pendapatan Daerah (LRA) dikurangi Pendapatan Asli Daerah Lainnya yang berasal dari Penjualan Aset Tetap dan Aset Lainnya.','nilai'=>'Arus Kas Masuk Dari Aktivitas Operasi (LAK) = Total Pendapatan Daerah (LRA)  – Pendapatan Asli Daerah Lainnya yang Berasal dari Penjualan Aset Tetap dan Aset Lainnya (LRA)  )'),
            array('ket' => 'Arus kas masuk aktivitas operasi (LAK)', 'nilai' => 0),
            array('ket' => 'Pendapatan daerah (LRA)', 'nilai' => $pendapatanlra),
            array('ket' => 'PAD penjualan aset tetap dan aset lainnya', 'nilai' => 0),
            array('ket' => 'Selisih', 'nilai' => 0)
        );

        $b = array(
            array('ket'=>'Arus Kas Keluar dari Aktivitas Operasi harus sama dengan Belanja Operasi ditambah Belanja Tak Terduga (di LRA) ditambah Belanja Transfer (di LRA).','nilai'=>'Arus Kas Keluar Dari Aktivitas Operasi = Belanja Operasi + Belanja Tak Terduga (di LRA)  + Belanja Transfer (di LRA)'),
            array('ket' => 'Arus kas keluar aktivitas operasi', 'nilai' => 0),
            array('ket' => 'Belanja operasi', 'nilai' => 0),
            array('ket' => 'Belanja tidak terduga (LRA)', 'nilai' => 0),
            array('ket' => 'Selisih', 'nilai' => 0),
        );

        $c = array(
            array('ket'=>'Arus Kas Masuk dari Aktivitas Investasi (LAK) harus sama dengan Pendapatan Asli Daerah yang berasal dari penjualan Aset Tetap dan Aset Lainnya (di LRA).','nilai'=>'Arus Kas Masuk Dari Aktivitas Investasi (LAK) = Pendapatan Asli Daerah Yang Berasal Dari Penjualan Aset Tetap dan Aset Lainnya (di LRA)'),
            array('ket' => 'Arus kas masuk aktivitas investasi (LAK)', 'nilai' => 0),
            array('ket' => 'PAD penjualan aset tetap dan aset lainnya (LRA)', 'nilai' => 0),
            array('ket' => 'Selisih', 'nilai' => 0)
        );

        $d = array(
            array('ket'=>'Arus Kas Keluar dari aktivitas Investasi (LAK) harus sama dengan Belanja Modal (di LRA).','nilai'=>'Arus Kas Keluar Dari Aktivitas Investasi (LAK) = Belanja Modal (di LRA)'),
            array('ket' => 'Arus kas keluar aktivitas investasi (lak)', 'nilai' => 0),
            array('ket' => 'Belanja modal (lra)', 'nilai' => 0),
            array('ket' => 'Selisih', 'nilai' => 0)
        );

        $e = array(
            array('ket'=>'Arus Kas Masuk dari aktivitas Pendanaan (LAK) harus sama dengan Penerimaan Pendanaan di LRA (selain penggunaan SiLPA).','nilai'=>'Arus Kas Masuk Dari Aktivitas Pendanaan (LAK) = Penerimaan Pendanaan Di LRA (Selain Penggunaan SiLPA)'),
            array('ket' => 'Arus kas masuk aktivitas pendanaan (lak)', 'nilai' => 0),
            array('ket' => 'Penerimaan pendanaan selain penggunaan silpa (lra)', 'nilai' => 0),
            array('ket' => 'Selisih', 'nilai' => 0)
        );

        $f = array(
            array('ket'=>'Arus Kas Keluar dari aktivitas Pendanaan (LAK) harus sama dengan Pengeluaran Pendanaan di LRA','nilai'=>'Arus Kas Keluar Dari Aktivitas Pendanaan (LAK) = Pengeluaran Pendanaan di LRA'),
            array('ket' => 'Arus kas keluar aktivitas pendanaan (lak)', 'nilai' => 0),
            array('ket' => 'Pengeluaran pendanaan (lra)', 'nilai' => 0),
            array('ket' => 'Selisih', 'nilai' => 0)
        );
        return array($a, $b, $c, $d, $e, $f);
    }

    function dataLPE($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {

        // select * FROM OPENQUERY(jbg, 'SET FMTONLY OFF; SET NOCOUNT ON; EXEC jbg_2018.dbo.RptLPE 2018, 4, 4, 7, 1,  ''20181231''')
        $cf = $this->_config();
        $dasar = "OPENQUERY(" . $cf->linked_server . ", 'SET FMTONLY OFF; SET NOCOUNT ON; EXEC " . $cf->nama_db . ".dbo.RptLPE $Tahun,$Kd_Urusan,$Kd_Bidang,$Kd_Unit,$Kd_Sub,''$D''')";

        // $this->db->where('Kd_Grup_1 in (1,2,3)', '', false);
        $this->db->select('sum(Realisasi) nilai');
        $akhir = $this->db->get($dasar)->row()->nilai;

        $this->db->where('Kd_Grup_1', 1);
        $this->db->select('sum(Realisasi) nilai');
        $awal = $this->db->get($dasar)->row()->nilai;

        $this->db->where('Kd_Grup_1', 2);
        $this->db->select('sum(Realisasi) nilai');
        $sp = $this->db->get($dasar)->row()->nilai;

        $this->db->where('Kd_Grup_1', 3);
        $this->db->select('sum(Realisasi) nilai');
        $ko = $this->db->get($dasar)->row()->nilai;

        $data = array(
            array(
                'ket' => 'Ekuitas akhir harus sama dengan ekuitas awal ditambah (dikurangi) surplus/defisit LO ditambah (dikurangi) koreksi berdampak ke ekuitas                ',
                'nilai' => 'Ekuitas akhir = ekuitas awal (+/-) surplus/defisit LO (+/-) koreksi berdampak ke ekuitas'
            ),
            array(
                'ket' => 'Ekuitas Akhir',
                'nilai' => $akhir
            ),
            array(
                'ket' => 'Ekuitas Awal',
                'nilai' => $awal
            ),
            array(
                'ket' => 'Surplus / Defisit LO',
                'nilai' => $sp
            ),
            array(
                'ket' => 'Koreksi',
                'nilai' => $ko
            ),
            array(
                'ket' => '<b>Selisih</b>',
                'nilai' => $akhir - ($ko + $sp + $awal)
            )
        );

        return $data;
    }


    function neracavslak($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {

        $a = array(
            array('ket'=>'Saldo akhir Kas tahun lalu (LAK) harus sama dengan saldo awal Kas tahun berkenaan (LAK), saldo akhir Kas di neraca tahun lalu, dan saldo awal Kas di neraca tahun berjalan.','nilai'=>'Saldo Akhir Kas Tahun Lalu (LAK) = Saldo awal Kas Tahun Berkenaan (LAK) = Saldo Akhir Kas Tahun lalu (Neraca) = Saldo Awal Kas Tahun Berjalan (Neraca) Apabila terdapat selisih harus diungkapkan dalam CaLK','nilai'),
            array('ket' => 'Saldo akhir kas bud tahun lalu (lak)', 'nilai' => 0),
            array('ket' => 'Saldo awal kas bud tahun berkenaan (lak)', 'nilai' => 0),
            array('ket' => 'Saldo akhir kas bud tahun lalu (neraca)', 'nilai' => 0),
            array('ket' => 'Saldo awal kas bud tahun berjalan (neraca)', 'nilai' => 0),
            array('ket' => 'Selisih', 'nilai' => 0)
        );

        $b = array(
            array('ket'=>'Saldo akhir kas di neraca tahun berjalan harus sama dengan saldo akhir Kas di LAK tahun berjalan.','nilai'=>'Saldo Akhir Kas Tahun Berjalan (Neraca) = Saldo Akhir Kas Tahun Berjalan (LAK)'),
            array('ket' => 'Saldo akhir kas thn berjalan (neraca)', 'nilai' => 0),
            array('ket' => 'Saldo akhir kas thn berjalan (lak)', 'nilai' => 0),
            array('ket' => 'Selisih', 'nilai' => 0)
        );

        $c = array(
            array('ket'=>'Utang PFK di neraca harus sama dengan utang PFK di BUD ditambah utang PFK di bendahara pengeluaran.','nilai'=>'Utang PFK (Neraca) = Utang PFK di BUD + Utang PFK pada Bendahara Pengeluaran'),
            array('ket' => 'Utang pfk (neraca)', 'nilai' => 0),
            array('ket' => 'Utang pfk bud', 'nilai' => 0),
            array('ket' => 'Utang pfk bendahara pengeluaran', 'nilai' => 0),
            array('ket' => 'Selisih', 'nilai' => 0),
        );

        $d = array(
            array('ket'=>'Saldo utang PFK di neraca tahun berjalan harus sama dengan saldo utang PFK di neraca tahun sebelumnya ditambah penerimaan PFK tahun berjalan dikurangi pengeluaran PFK tahun berjalan di LAK.','nilai'=>'Saldo Utang PFK tahun berjalan (neraca) = Saldo Utang PFK Tahun sebelumnya (Neraca) + Penerimaan PFK Tahun berjalan – Pengeluaran PFK Tahun Berjalan (LAK)'),
            array('ket' => 'Utang pfk (neraca)', 'nilai' => 0),
            array('ket' => 'Utang pfk tahun x-1 (neraca)', 'nilai' => 0),
            array('ket' => 'Penerimaan pfk (lak)', 'nilai' => 0),
            array('ket' => 'Pengeluaran pfk (lak)', 'nilai' => 0),
            array('ket' => 'Selisih', 'nilai' => 0)
        );

        return array($a, $b, $c, $d);
    }

    function lolraneraca($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {
        $cf = $this->_config();

        $dasar = "OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptLO $Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, ''$D''')";
        $this->db->where("kd_akrual_1=8 and kd_akrual_2=1 and kd_akrual_3=1", '', false);
        $this->db->select("sum(Realisasi) nilai", false);
        $pajaklo = $this->db->get($dasar)->row()->nilai;

        $asd = "OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptLRAAK $Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, ''$D''')";
        $this->db->select("sum(Realisasi) nilai", false);
        $this->db->where("kd_rek_1=4 and kd_rek_2=1 and kd_rek_3=1");
        $pajaklra = $this->db->get($asd)->row()->nilai;

        $kgd = "OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptNeracaAK $Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub,''%'', ''$D''')";
        $this->db->select("sum(SaldoX1) nilai", false);
        $this->db->where("replace(Kd_Rek_Gab3,' ','')='1.1.3'", '', false);
        $pajakakhir = $this->db->get($kgd)->row()->nilai;

        $a = array(
            array('ket'=>'Pendapatan Pajak (LO) harus sama dengan Pendapatan Pajak (LRA) dikurangi Piutang Pajak Awal Tahun ditambah Piutang Pajak Akhir Tahun','nilai'=>'Pendapatan Pajak (LO) = Pendapatan Pajak ( LRA) -  Piutang Pajak Awal Tahun + Piutang Pajak Akhir Tahun'),
            array('ket' => 'Pendapatan pajak (lo) ', 'nilai' => $pajaklo),
            array('ket' => 'Pendapatan pajak (lra)', 'nilai' => $pajaklra),
            array('ket' => 'Piutang pajak akhir tahun (neraca)', 'nilai' => $pajakakhir),
            array('ket' => 'Piutang pajak awal tahun (neraca)', 'nilai' => 0 - $pajakakhir),
            array('ket' => 'Selisih', 'nilai' => $pajaklo - ($pajaklra + $pajakakhir + (0 - $pajakakhir))),
        );

        $this->db->where("kd_akrual_1=8 and kd_akrual_2=1 and kd_akrual_3=2", '', false);
        $this->db->select("sum(Realisasi) nilai", false);
        $retlo = $this->db->get($dasar)->row()->nilai;


        $this->db->select("sum(SaldoX1) nilai", false);
        $this->db->where("replace(Kd_Rek_Gab3,' ','')='1.1.4'", '', false);
        $pua = $this->db->get($kgd)->row()->nilai;

        $b = array(
            array('ket'=>'Pendapatan Retribusi (LO) harus sama dengan Pendapatan Retribusi (LRA) dikurangi Piutang Retribusi Awal Tahun ditambah Piutang Retribusi Akhir Tahun','nilai'=>'Pendapatan Retribusi (LO) = Pendapatan Retribusi (LRA) - Piutang Retribusi Awal Tahun + Piutang Retribusi Akhir Tahun'),
            array('ket' => 'Pendapatan retribusi (lo) ', 'nilai' => $retlo),
            array('ket' => 'Pendapatan retribusi (lra)', 'nilai' => $pajaklra),
            array('ket' => 'Piutang retribusi akhir tahun (neraca)', 'nilai' => $pua),
            array('ket' => 'Piutang retribusi awal tahun (neraca)', 'nilai' => -$pua),
            array('ket' => 'Selisih', 'nilai' => $retlo - ($pajaklra + $pua + (-$pua))),
        );


        $this->db->where("kd_akrual_1=8 and kd_akrual_2=2 and kd_akrual_3=1", '', false);
        $this->db->select("sum(Realisasi) nilai", false);
        $tptlo = $this->db->get($dasar)->row()->nilai;
        // belum
        $c = array(
            array('ket'=>'Pendapatan Bagi Hasil Pajak Provinsi (LO) harus sama dengan Pendapatan Bagi Hasil Pajak Provinsi (LRA) dikurangi Piutang Bagi Hasil Pajak Provinsi Awal Tahun ditambah Piutang Bagi Hasil Pajak Provinsi Akhir Tahun','nilai'=>'Pendapatan Bagi Hasil Pajak Provinsi (LO) = Pendapatan Bagi Hasil Pajak Provinsi (LRA) – Piutang Bagi Hasil Pajak Provinsi Awal Tahun + Piutang Bagi Hasil Pajak Provinsi Akhir Tahun'),
            array('ket' => 'Pendapatan bagi hasil pajak provinsi (lo)', 'nilai' => $tptlo),
            array('ket' => 'Pendapatan bagi hasil pajak provinsi (lra)', 'nilai' => 0),
            array('ket' => 'Piutang bagi hasil pajak provinsi akhir tahun (neraca)', 'nilai' => 0),
            array('ket' => 'Piutang bagi hasil pajak provinsi awal tahun (neraca)', 'nilai' => 0),
        );

        // 
        $this->db->where("kd_akrual_1=9 and kd_akrual_2=1 and kd_akrual_3=2", '', false);
        $this->db->select("sum(Realisasi) nilai", false);
        $bblo = $this->db->get($dasar)->row()->nilai;

        $this->db->select("sum(Realisasi) nilai", false);
        $this->db->where("kd_rek_1=5 and kd_rek_2=1 and kd_rek_3=2");
        $bblra = $this->db->get($asd)->row()->nilai;

        $this->db->select("sum(SaldoX0) nilai", false);
        $this->db->where("replace(Kd_Rek_Gab3,' ','')='1.1.7'", '', false);
        $ppat = $this->db->get($kgd)->row()->nilai;

        $this->db->select("sum(SaldoX1) nilai", false);
        $this->db->where("replace(Kd_Rek_Gab3,' ','')='1.1.7'", '', false);
        $ppatb = $this->db->get($kgd)->row()->nilai;

        $d = array(
            array('ket'=>'Beban Persediaan (LO) harus sama dengan Belanja Barang dan Jasa Persediaan (LRA) ditambah Persediaan Awal Tahun dikurangi Persediaan Akhir Tahun','nilai'=>'Beban Persediaan (LO) = Belanja Barang dan Jasa Persediaan (LRA) + Persediaan Awal Tahun - Persediaan Akhir Tahun. Perhatikan cara penilaian persediaan: FIFO atau weighted average'),
            array('ket' => 'Beban persediaan (lo) ', 'nilai' => $bblo),
            array('ket' => 'Belanja barang dan jasa - persediaan (lra)', 'nilai' => $bblra),
            array('ket' => 'Persediaan awal tahun', 'nilai' => $ppat),
            array('ket' => 'Persediaan akhir tahun', 'nilai' => -$ppatb),
            array('ket' => 'Selisih', 'nilai' => $bblo - ($bblra + $ppat + (-$ppatb))),
        );


        $this->db->where("kd_akrual_1=9 and kd_akrual_2=1 and kd_akrual_3=7", '', false);
        $this->db->select("sum(Realisasi) nilai", false);
        $bplo = $this->db->get($dasar)->row()->nilai;

        $this->db->select("sum(SaldoX1) nilai", false);
        $this->db->where("replace(Kd_Rek_Gab3,' ','')='1.1.7'", '', false);
        $apptb = $this->db->get($kgd)->row()->nilai;


        $this->db->select("sum(SaldoX0) nilai", false);
        $this->db->where("replace(Kd_Rek_Gab3,' ','')='1.1.7'", '', false);
        $apptc = $this->db->get($kgd)->row()->nilai;

        $e = array(
            array('ket'=>'Beban Penyusutan (LO) harus sama dengan Akumulasi Penyusutan Akhir Tahun dikurangi Akumulasi Penyusutan Awal Tahun ','nilai'=>'Beban Penyusutan (LO) = Akumulasi Penyusutan Akhir Tahun – Akumulasi Penyusutan Awal Tahun'),
            array('ket' => 'Beban penyusutan (lo)', 'nilai' => $bplo),
            array('ket' => 'Akumulasi penyusutan akhir tahun', 'nilai' => $apptb),
            array('ket' => 'Akumulasi penyusutan awal tahun', 'nilai' => $apptc),
            array('ket' => 'Selisih', 'nilai' => $bplo - ($apptb - $apptc)),
        );

        return array($a, $b, $c, $d, $e);
    }

    function lolpeneraca($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {

        /* Ekuitas awal (laporan perubahan ekuitas) */
        $cf = $this->_config();
        $lpe = $this->db->query("select  kd_grup_1,nm_grup_1,sum(Realisasi) nilai,sum(Realisasix0) nilai0
        from  OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON; EXEC " . $cf->nama_db . ".dbo.RptLPE $Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub,''$D''') 
        group by  nm_grup_1,kd_grup_1
        order by kd_grup_1 asc")->result_array();


        $kgd = "OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptNeracaAK $Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub,''%'', ''$D''')";
        $this->db->select("sum(SaldoX0) nilai", false);
        $this->db->where("replace(Kd_Rek_Gab3,' ','')='3.1.1'", '', false);
        $ean = $this->db->get($kgd)->row()->nilai;

        $a = array(
            array('ket'=>'Ekuitas Awal pada Laporan Perubahan Ekuitas harus sama dengan Ekuitas Akhir pada Neraca Tahun Sebelumnya','nilai'=>'Ekuitas Awal pada Laporan Perubahan Ekuitas = Ekuitas Akhir pada Neraca Tahun Sebelumnya'),
            array('ket' => 'Ekuitas awal (laporan perubahan ekuitas)', 'nilai' => $lpe[0]['nilai']),
            array('ket' => 'Ekuitas akhir tahun sebelumnya (neraca)', 'nilai' => $ean),
            array('ket' => 'Selisih', 'nilai' => $lpe[0]['nilai'] - $ean)
        );

        $dasar = "OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptLO $Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, ''$D''')";
        $this->db->where("kd_akrual_1=9 and kd_akrual_2=99 and kd_akrual_3=0", '', false);
        $this->db->select("sum(Realisasi) nilai", false);
        $sdlo = $this->db->get($dasar)->row()->nilai;

        $b = array(
            array('ket'=>'Surplus/Defisit pada Laporan Operasional harus sama dengan Surplus/Defisit pada Laporan Perubahan Ekuitas','nilai'=>'Surplus/Defisit pada Laporan Operasional = Surplus/Defisit pada Laporan Perubahan Ekuitas'),
            array('ket' => 'Surplus/defisit (laporan operasional)', 'nilai' => $sdlo),
            array('ket' => 'Surplus/defisit (laporan perubahan ekuitas)', 'nilai' => $lpe[1]['nilai']),
            array('ket' => 'Selisih', 'nilai' => $sdlo - $lpe[1]['nilai']),
        );


        $this->db->select("sum(SaldoX1) nilai", false);
        $this->db->where("replace(Kd_Rek_Gab3,' ','')='3.1.1'", '', false);
        $eanb = $this->db->get($kgd)->row()->nilai;
        $c = array(
            array('ket'=>'Ekuitas akhir pada Laporan Perubahan Ekuitas harus sama dengan Ekuitas pada Neraca','nilai'=>'Ekuitas akhir pada Laporan Perubahan Ekuitas = Ekuitas pada Neraca'),
            array('ket' => 'Ekuitas akhir (laporan perubahan ekuitas)', 'nilai' =>  $lpe[3]['nilai']),
            array('ket' => 'Ekuitas (neraca)', 'nilai' => $eanb),
            array('ket' => 'Selisih', 'nilai' => $lpe[3]['nilai'] - $eanb),
        );

        return array($a, $b, $c);
    }

    function lravssal($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D)
    {

        $res = $this->dasarSAL($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $D);
        $aa=!empty($res[0]['Realisasi'])?$res[0]['Realisasi']:0;
        $bb=!empty($res[1]['Realisasi'])?$res[1]['Realisasi']:0;
        $cc=!empty($res[2]['Realisasi'])?$res[2]['Realisasi']:0;
        $dd=!empty($res[3]['Realisasi'])?$res[3]['Realisasi']:0;
        $ee=!empty($res[4]['Realisasi'])?$res[4]['Realisasi']:0;
        $ak = ( $aa - $bb) +  $cc + $dd + $ee;

        $cf = $this->_config();
        $lrasilpa = $this->db->query("SELECT sum(realisasi) realisasi
        FROM   OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptLraAK $Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, ''$D''')		where Kd_Rek_1='7'")->row()->realisasi;

        $a = array(
            array('ket'=>'Silpa di LRA harus sama dengan Saldo Anggaran Lebih (SAL) akhir pada Laporan Perubahan SAL','nilai'=>'SiLPA pada LRA = Saldo Anggaran Lebih (SAL) Akhir pada Laporan Perubahan SAL'),
            array('ket' => 'Silpa (lra)', 'nilai' => $lrasilpa),
            array('ket' => 'Sal akhir (laporan perubahan sal)', 'nilai' => $ak),
            array('ket' => 'Selisih', 'nilai' => $lrasilpa - $ak),
        );

        $lrasilpaxo = $this->db->query("SELECT sum(realisasi) realisasi
        FROM   OPENQUERY(" . $cf->linked_server . ",'SET FMTONLY OFF; SET NOCOUNT ON;  exec " . $cf->nama_db . ".dbo.RptLraAK $Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, ''$D''')	where
        	kd_rek_1=7 and kd_rek_2=1 and kd_rek_3=1 and kd_rek_4=1")->row()->realisasi;

        $b = array(
            array('ket'=>'Silpa pada LRA Tahun Sebelumnya harus sama dengan Penggunaan Silpa pada Laporan Perubahan SAL harus sama dengan Penerimaan Pembiayaan Silpa pada LRA harus sama dengan Saldo Anggaran Lebih (SAL) Awal pada Laporan Perubahan SAL','nilai'=>'Silpa pada LRA Tahun Sebelumnya = Penggunaan Silpa pada Laporan Perubahan SAL = Penerimaan Pembiayaan Silpa pada LRA = Saldo Anggaran Lebih (SAL) Awal pada Laporan Perubahan SAL'),
            array('ket' => 'Silpa tahun sebelumnya (lra)', 'nilai' => $lrasilpaxo),
            array('ket' => 'Penggunaan silpa (laporan perubahan sal)', 'nilai' => !empty($res[1]['Realisasi'])?$res[1]['Realisasi']:0),
            // kode akun ?
            array('ket' => 'Penerimaan pembiayaan - penggunaan silpa (lra)', 'nilai' => 0),

            array('ket' => 'Sal awal (laporan perubahan sal)', 'nilai' => !empty($res[1]['Realisasi'])?$res[1]['Realisasi']:0),
            array('ket' => 'Selisih', 'nilai' => 0),
        );

        return array($a, $b);
    }
}

/* End of file Makrual.php */
/* Location: ./application/models/Makrual.php */

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mlodetail extends CI_Model
{

    public $table = 'tb_spe_detail_lo';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($kd_skpd, $nm_unit, $q = NULL)
    {
        $this->db->where("kd_skpd like '$kd_skpd%'", null, false);
        $this->db->where('nm_unit', $nm_unit);
        $this->db->where("(Tahun like '%$q%' or Kd_Jurnal like '%$q%' or Nm_Jurnal like '%$q%' or Tgl_Bukti like '%$q%' or No_Bukti like '%$q%' or No_BKU like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Keterangan like '%$q%' or Akun_Akrual_1 like '%$q%' or Akun_Akrual_2 like '%$q%' or Akun_Akrual_3 like '%$q%' or Akun_Akrual_4 like '%$q%' or Akun_Akrual_5 like '%$q%' or Nm_Akrual_1 like '%$q%' or Nm_Akrual_2 like '%$q%' or Nm_Akrual_3 like '%$q%' or Nm_Akrual_4 like '%$q%' or Nm_Akrual_5 like '%$q%' or Debet like '%$q%' or Kredit like '%$q%')", NULL, false);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($kd_skpd, $nm_unit, $limit, $start = 0, $q = NULL)
    {
        $this->db->where("kd_skpd like '$kd_skpd%'", null, false);
        $this->db->where('nm_unit', $nm_unit);
        $this->db->where("(Tahun like '%$q%' or Kd_Jurnal like '%$q%' or Nm_Jurnal like '%$q%' or Tgl_Bukti like '%$q%' or No_Bukti like '%$q%' or No_BKU like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Keterangan like '%$q%' or Akun_Akrual_1 like '%$q%' or Akun_Akrual_2 like '%$q%' or Akun_Akrual_3 like '%$q%' or Akun_Akrual_4 like '%$q%' or Akun_Akrual_5 like '%$q%' or Nm_Akrual_1 like '%$q%' or Nm_Akrual_2 like '%$q%' or Nm_Akrual_3 like '%$q%' or Nm_Akrual_4 like '%$q%' or Nm_Akrual_5 like '%$q%' or Debet like '%$q%' or Kredit like '%$q%')", NULL, false);

        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
    function get_all_data($kd_skpd, $nm_sub_unit, $q = NULL)
    {
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where("(Tahun like '%$q%' or Kd_Jurnal like '%$q%' or Nm_Jurnal like '%$q%' or Tgl_Bukti like '%$q%' or No_Bukti like '%$q%' or No_BKU like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Keterangan like '%$q%' or Akun_Akrual_1 like '%$q%' or Akun_Akrual_2 like '%$q%' or Akun_Akrual_3 like '%$q%' or Akun_Akrual_4 like '%$q%' or Akun_Akrual_5 like '%$q%' or Nm_Akrual_1 like '%$q%' or Nm_Akrual_2 like '%$q%' or Nm_Akrual_3 like '%$q%' or Nm_Akrual_4 like '%$q%' or Nm_Akrual_5 like '%$q%' or Debet like '%$q%' or Kredit like '%$q%')", NULL, false);
        return $this->db->get($this->table)->result();
    }
    function getData($limit, $start = 0, $q = null)
    {
        $this->db->select('kd_skpd,nm_sub_unit,sum(debet) debet ,sum(kredit) kredit', false);
        $this->db->group_by('kd_skpd', false);
        $this->db->group_by('nm_sub_unit', false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')", null, false);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
    function getAllData($q = null)
    {
        return $this->db->query("SELECT tb_spe_ref_unit.kd_skpd,lv1.nm_unit
                            , sum(nilai81) sumnilai81, sum(nilai82) sumnilai82, sum(nilai83) sumnilai83, sum(nilai84) sumnilai84, sum(nilai85) sumnilai85
                            , sum(nilai91) sumnilai91, sum(nilai92) sumnilai92, sum(nilai93) sumnilai93, sum(nilai94) sumnilai94
                            from (
                            select *
                            ,case when akun_akrual_2 ='8.1' then nilai end nilai81
                            ,case when akun_akrual_2 ='8.2' then nilai end nilai82
                            ,case when akun_akrual_2 ='8.3' then nilai end nilai83
                            ,case when akun_akrual_2 ='8.4' then nilai end nilai84
                            ,case when akun_akrual_2 ='8.5' then nilai end nilai85
                            ,case when akun_akrual_2 ='9.1' then nilai end nilai91
                            ,case when akun_akrual_2 ='9.2' then nilai end nilai92
                            ,case when akun_akrual_2 ='9.3' then nilai end nilai93
                            ,case when akun_akrual_2 ='9.4' then nilai end nilai94
                             from 
                            (
                                select  nm_unit
                                      ,akun_akrual_2
                                      ,nm_akrual_2 
                                      ,sum(debet)-sum(kredit) nilai     
                                  from tb_spe_detail_lo
                                  group by  nm_unit
                                      ,akun_akrual_2
                                      ,nm_akrual_2
                                ) lv0
                            ) lv1 left join tb_spe_ref_unit on lv1.nm_unit=tb_spe_ref_unit.nm_unit
                            group by tb_spe_ref_unit.kd_skpd,lv1.nm_unit
                                  order by kd_skpd,lv1.nm_unit")->result();

        /*$this->db->select('kd_skpd,nm_sub_unit,sum(debet) debet ,sum(kredit) kredit',false);
    $this->db->group_by('kd_skpd',false);
    $this->db->group_by('nm_sub_unit',false);
    $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')",null,false);
    $this->db->order_by("kd_skpd", "asc");
   return $this->db->get($this->table)->result();*/
    }
    function getRow($q = null)
    {
        $this->db->select('kd_skpd,nm_sub_unit,sum(debet) debet ,sum(kredit) kredit', false);
        $this->db->group_by('kd_skpd', false);
        $this->db->group_by('nm_sub_unit', false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')", null, false);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    // insert data
    function insert($data)
    {
        return  $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        return   $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->delete($this->table);
    }
    function getSumNilai($q = null)
    {
        $this->db->select("sum(debet) jum_debet,sum(kredit) jum_kredit", false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')", null, false);
        return $this->db->get($this->table)->row();
    }

    function getSUbunit()
    {
        return $this->db->query("select distinct   nm_sub_unit from spe_detail_lo order by  nm_sub_unit asc")->result();
    }
    function getSumNilaidetail($kd_skpd, $nm_sub_unit, $q = NULL)
    {
        $this->db->select("sum(debet) jum_debet,sum(kredit) jum_kredit", false);
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where("(Tahun like '%$q%' or Kd_Jurnal like '%$q%' or Nm_Jurnal like '%$q%' or Tgl_Bukti like '%$q%' or No_Bukti like '%$q%' or No_BKU like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Keterangan like '%$q%' or Akun_Akrual_1 like '%$q%' or Akun_Akrual_2 like '%$q%' or Akun_Akrual_3 like '%$q%' or Akun_Akrual_4 like '%$q%' or Akun_Akrual_5 like '%$q%' or Nm_Akrual_1 like '%$q%' or Nm_Akrual_2 like '%$q%' or Nm_Akrual_3 like '%$q%' or Nm_Akrual_4 like '%$q%' or Nm_Akrual_5 like '%$q%' or Debet like '%$q%' or Kredit like '%$q%')", NULL, false);
        return $this->db->get($this->table)->row();
    }


    function getLevel3($unit, $akun)
    {
        return $this->db->query("SELECT  nm_unit
                                  ,akun_akrual_3
                                  ,nm_akrual_3 
                                  ,sum(debet)-sum(kredit) nilai     
                              from tb_spe_detail_lo
                              where nm_unit='$unit' 
                              and akun_akrual_2='$akun'
                              group by  nm_unit
                                  ,akun_akrual_3
                                  ,nm_akrual_3
                            order by akun_akrual_3 asc
                            ")->result();
    }

    function getLevel4($unit, $akun)
    {
        return $this->db->query("SELECT  nm_unit
                                  ,akun_akrual_4
                                  ,nm_akrual_4 
                                  ,sum(debet)-sum(kredit) nilai     
                              from tb_spe_detail_lo
                              where nm_unit='$unit' 
                              and akun_akrual_3='$akun'
                              group by  nm_unit
                                  ,akun_akrual_4
                                  ,nm_akrual_4
                            order by akun_akrual_4 asc
                            ")->result();
    }


    function getLevel5($unit, $akun)
    {
        return $this->db->query("SELECT  nm_unit
                                  ,akun_akrual_5
                                  ,nm_akrual_5 
                                  ,sum(debet)-sum(kredit) nilai     
                              from tb_spe_detail_lo
                              where nm_unit='$unit' 
                              and akun_akrual_4='$akun'
                              group by  nm_unit
                                  ,akun_akrual_5
                                  ,nm_akrual_5
                            order by akun_akrual_5 asc
                            ")->result();
    }
}

/* End of file Mlodetail.php */
/* Location: ./application/models/Mlodetail.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-29 13:56:39 */
/* http://harviacode.com */

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mlra extends CI_Model
{

    public $table = 'tb_spe_detail_lra';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($kd_skpd, $nm_unit, $q = NULL)
    {
        $this->db->where("kd_skpd like '$kd_skpd%'", null, false);
        $this->db->where('nm_unit', $nm_unit);
        $this->db->where("(Tahun like '%$q%' or Kd_Jurnal like '%$q%' or Nm_Jurnal like '%$q%' or Tgl_Bukti like '%$q%' or No_Bukti like '%$q%' or No_BKU like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Keterangan like '%$q%' or Akun_Akrual_1 like '%$q%' or Akun_Akrual_2 like '%$q%' or Akun_Akrual_3 like '%$q%' or Akun_Akrual_4 like '%$q%' or Akun_Akrual_5 like '%$q%' or Nm_Akrual_1 like '%$q%' or Nm_Akrual_2 like '%$q%' or Nm_Akrual_3 like '%$q%' or Nm_Akrual_4 like '%$q%' or Nm_Akrual_5 like '%$q%' or Debet like '%$q%' or Kredit like '%$q%')", NULL, false);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($kd_skpd, $nm_unit, $limit, $start = 0, $q = NULL)
    {
        $this->db->where("kd_skpd like '$kd_skpd%'", null, false);
        $this->db->where('nm_unit', $nm_unit);
        $this->db->where("(Tahun like '%$q%' or Kd_Jurnal like '%$q%' or Nm_Jurnal like '%$q%' or Tgl_Bukti like '%$q%' or No_Bukti like '%$q%' or No_BKU like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Keterangan like '%$q%' or Akun_Akrual_1 like '%$q%' or Akun_Akrual_2 like '%$q%' or Akun_Akrual_3 like '%$q%' or Akun_Akrual_4 like '%$q%' or Akun_Akrual_5 like '%$q%' or Nm_Akrual_1 like '%$q%' or Nm_Akrual_2 like '%$q%' or Nm_Akrual_3 like '%$q%' or Nm_Akrual_4 like '%$q%' or Nm_Akrual_5 like '%$q%' or Debet like '%$q%' or Kredit like '%$q%')", NULL, false);

        $this->db->limit($limit, $start);
        return $this->db->get($this->table, false)->result();
    }

    function get_all_data($kd_skpd, $nm_sub_unit, $q = NULL)
    {
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where("(Tahun like '%$q%' or Kd_Jurnal like '%$q%' or Nm_Jurnal like '%$q%' or Tgl_Bukti like '%$q%' or No_Bukti like '%$q%' or No_BKU like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Keterangan like '%$q%' or Akun_Akrual_1 like '%$q%' or Akun_Akrual_2 like '%$q%' or Akun_Akrual_3 like '%$q%' or Akun_Akrual_4 like '%$q%' or Akun_Akrual_5 like '%$q%' or Nm_Akrual_1 like '%$q%' or Nm_Akrual_2 like '%$q%' or Nm_Akrual_3 like '%$q%' or Nm_Akrual_4 like '%$q%' or Nm_Akrual_5 like '%$q%' or Debet like '%$q%' or Kredit like '%$q%')", NULL, false);
        return $this->db->get($this->table)->result();
    }
    function getData($limit, $start = 0, $q = null)
    {
        $this->db->select('kd_skpd,nm_sub_unit,sum(debet) debet ,sum(kredit) kredit', false);
        $this->db->group_by('kd_skpd', false);
        $this->db->group_by('nm_sub_unit', false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')", null, false);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
    function getAllData($q = null)
    {

        $wh=" where NOT (Kd_Jurnal IN (8, 10) and akun_Akrual_1<>3)
        and Not (akun_akrual_1='1' and akun_akrual_2=1 and akun_akrual_3=1 and akun_akrual_4=1)
        AND (NOT (akun_akrual_1 = 1 AND akun_akrual_2 = 1 AND akun_akrual_3 = 8 AND akun_akrual_4 = 1))
    AND (NOT (akun_akrual_1 = 2 AND akun_akrual_2 = 1 AND akun_akrual_3 = 7 AND akun_akrual_4 = 1))
    and (not (akun_akrual_1=7 and SUBSTRING(Akun_Akrual_2, 3, 1)>=3))";
        if ($q != null) {
            $wh .= "and nm_unit='$q'";
        } 

        return $this->db->query("SELECT tb_spe_ref_unit.kd_skpd,lv1.nm_unit
                                , sum(nilai41) sumnilai41, sum(nilai42) sumnilai42, sum(nilai43) sumnilai43
                                , sum(nilai51) sumnilai51, sum(nilai52) sumnilai52, sum(nilai53) sumnilai53
                                from (
                                select *
                                ,case when akun_akrual_2 ='4.1' then nilai end nilai41
                                ,case when akun_akrual_2 ='4.2' then nilai end nilai42
                                ,case when akun_akrual_2 ='4.3' then nilai end nilai43
                                ,case when akun_akrual_2 ='5.1' then nilai end nilai51
                                ,case when akun_akrual_2 ='5.2' then nilai end nilai52
                                ,case when akun_akrual_2 ='5.3' then nilai end nilai53
                                 from 
                                (
                                select  nm_unit
                                      ,akun_akrual_2
                                      ,nm_akrual_2 
                                      ,sum(debet)-sum(kredit) nilai     
                                  from tb_spe_detail_lra
                                  $wh
                                  group by  nm_unit
                                      ,akun_akrual_2
                                      ,nm_akrual_2
                                ) lv0
                                ) lv1 left join tb_spe_ref_unit on lv1.nm_unit=tb_spe_ref_unit.nm_unit
                                group by tb_spe_ref_unit.kd_skpd,lv1.nm_unit
                                order by kd_skpd,lv1.nm_unit")->result();
    }

    function getRow($q = null)
    {
        $this->db->select('kd_skpd,nm_sub_unit,sum(debet) debet ,sum(kredit) kredit', false);
        $this->db->group_by('kd_skpd', false);
        $this->db->group_by('nm_sub_unit', false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')", null, false);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function getSumNilai($q = null)
    {
        $this->db->select("sum(debet) jum_debet,sum(kredit) jum_kredit", false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')", null, false);
        return $this->db->get($this->table)->row();
    }

    function getSUbunit()
    {
        return $this->db->query("select distinct   nm_sub_unit from spe_detail_lra order by  nm_sub_unit asc")->result();
    }
    function getSumNilaidetail($kd_skpd, $nm_sub_unit, $q = NULL)
    {
        $this->db->select("sum(debet) jum_debet,sum(kredit) jum_kredit", false);
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where("(Tahun like '%$q%' or Kd_Jurnal like '%$q%' or Nm_Jurnal like '%$q%' or Tgl_Bukti like '%$q%' or No_Bukti like '%$q%' or No_BKU like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Keterangan like '%$q%' or Akun_Akrual_1 like '%$q%' or Akun_Akrual_2 like '%$q%' or Akun_Akrual_3 like '%$q%' or Akun_Akrual_4 like '%$q%' or Akun_Akrual_5 like '%$q%' or Nm_Akrual_1 like '%$q%' or Nm_Akrual_2 like '%$q%' or Nm_Akrual_3 like '%$q%' or Nm_Akrual_4 like '%$q%' or Nm_Akrual_5 like '%$q%' or Debet like '%$q%' or Kredit like '%$q%')", NULL, false);
        // $this->db->group_by('Kd_SKPD',false);
        // $this->db->group_by('Nm_Sub_Unit',false);
        return $this->db->get($this->table)->row();
    }


    function getLevel3($unit, $akun)
    {
        return $this->db->query("SELECT  nm_unit
                                  ,akun_akrual_3
                                  ,nm_akrual_3 
                                  ,sum(debet)-sum(kredit) nilai     
                              from tb_spe_detail_lra
                              where nm_unit='$unit' 
                              and akun_akrual_2='$akun'
                              group by  nm_unit
                                  ,akun_akrual_3
                                  ,nm_akrual_3
                            order by akun_akrual_3 asc
                            ")->result();
    }

    function getLevel4($unit, $akun)
    {
        return $this->db->query("SELECT  nm_unit
                                  ,akun_akrual_4
                                  ,nm_akrual_4 
                                  ,sum(debet)-sum(kredit) nilai     
                              from tb_spe_detail_lra
                              where nm_unit='$unit' 
                              and akun_akrual_3='$akun'
                              group by  nm_unit
                                  ,akun_akrual_4
                                  ,nm_akrual_4
                            order by akun_akrual_4 asc
                            ")->result();
    }


    function getLevel5($unit, $akun)
    {

        return $this->db->query("SELECT  nm_unit
                                  ,akun_akrual_5
                                  ,nm_akrual_5 
                                  ,sum(debet)-sum(kredit) nilai     
                              from tb_spe_detail_lra
                              where nm_unit='$unit' 
                              and akun_akrual_4='$akun'
                              group by  nm_unit
                                  ,akun_akrual_5
                                  ,nm_akrual_5
                            order by akun_akrual_5 asc
                            ")->result();
    }
}

/* End of file Mlra.php */
/* Location: ./application/models/Mlra.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-29 13:55:42 */
/* http://harviacode.com */

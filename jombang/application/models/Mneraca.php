<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mneraca extends CI_Model
{

    public $table = 'tb_spe_detail_neraca';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }


    function getall()
    {
        return $this->db->query("SELECT vneraca_lv0.kd_skpd
                                      ,vneraca_lv0.nm_unit
                                      ,vsaldoawal_lv0.sumnilai_1 saldoawalaset
                                      ,vsaldoawal_lv0.sumnilai_2 saldoawalkewajiban
                                      ,vneraca_lv0.sumnilai_1 mutasiaset
                                      ,vneraca_lv0.sumnilai_2 mutasikewajiban
                                      ,vneraca_lv0.sumnilai_3 mutasiekuitas
                                  from vneraca_lv0
                                  left join vsaldoawal_lv0 on vneraca_lv0.nm_unit=vsaldoawal_lv0.nm_unit
                                  order by vneraca_lv0.kd_skpd")->result();
    }


    function saldoAwalAset($unit)
    {
        return $this->db->query("SELECT  nm_unit
                                      ,akun_akrual_2
                                      ,nm_akrual_2
                                      ,sum(saldo_awal_debet)-sum(saldo_awal_kredit) nilai
                                  from tb_spe_saldo_awal
                                 where akun_akrual_1='1'
                                  and nm_unit='$unit'
                                  group by  nm_unit
                                      ,akun_akrual_2
                                      ,nm_akrual_2
                                order by akun_akrual_2")->result();
    }

    function saldoAwalAsetTiga($unit, $akun)
    {
        return $this->db->query("SELECT  nm_unit
                                      ,akun_akrual_3
                                      ,nm_akrual_3
                                      ,sum(saldo_awal_debet)-sum(saldo_awal_kredit) nilai
                                  from tb_spe_saldo_awal
                                 where akun_akrual_2='$akun'
                                  and nm_unit='$unit'
                                  group by  nm_unit
                                      ,akun_akrual_3
                                      ,nm_akrual_3
                                order by akun_akrual_3")->result();
    }

    function saldoAwalAsetEmpat($unit, $akun)
    {
        return $this->db->query("SELECT  nm_unit
                                      ,akun_akrual_4
                                      ,nm_akrual_4
                                      ,sum(saldo_awal_debet)-sum(saldo_awal_kredit) nilai
                                  from tb_spe_saldo_awal
                                 where akun_akrual_3='$akun'
                                  and nm_unit='$unit'
                                  group by  nm_unit
                                      ,akun_akrual_4
                                      ,nm_akrual_4
                                order by akun_akrual_4")->result();
    }

    function saldoAwalKewajiban($unit)
    {
        return $this->db->query("SELECT  nm_unit
                                      ,akun_akrual_2
                                      ,nm_akrual_2
                                      ,sum(saldo_awal_debet)-sum(saldo_awal_kredit) nilai
                                  FROM TB_SPE_SALDO_AWAL
                                 where akun_akrual_1='2'
                                  and nm_unit='$unit'
                                  group by  nm_unit
                                      ,akun_akrual_2
                                      ,nm_akrual_2")->result();
    }


    function saldoAwalKewajibanTiga($unit, $akun)
    {
        return $this->db->query("SELECT  nm_unit
                                      ,akun_akrual_3
                                      ,nm_akrual_3
                                      ,sum(saldo_awal_debet)-sum(saldo_awal_kredit) nilai
                                  from tb_spe_saldo_awal
                                 where akun_akrual_2='$akun'
                                  and nm_unit='$unit'
                                  group by  nm_unit
                                      ,akun_akrual_3
                                      ,nm_akrual_3
                                order by akun_akrual_3")->result();
    }

    function saldoAwalKewajibanEmpat($unit, $akun)
    {
        return $this->db->query("SELECT  nm_unit
                                      ,akun_akrual_4
                                      ,nm_akrual_4
                                      ,sum(saldo_awal_debet)-sum(saldo_awal_kredit) nilai
                                  from tb_spe_saldo_awal
                                 where akun_akrual_3='$akun'
                                  and nm_unit='$unit'
                                  group by  nm_unit
                                      ,akun_akrual_4
                                      ,nm_akrual_4
                                order by akun_akrual_4")->result();
    }

    function mutasiAset($unit)
    {
        return $this->db->query("SELECT  nm_unit
                                  ,akun_akrual_2
                                  ,nm_akrual_2
                                  ,sum(mutasi_debet)-sum(mutasi_kredit) nilai
                              FROM TB_SPE_DETAIL_NERACA
                                  where akun_akrual_1='1'
                              and nm_unit='$unit'
                              group by  nm_unit
                                  ,akun_akrual_2
                                  ,nm_akrual_2 order by akun_akrual_2 ")->result();
    }


    function mutasiAsetTiga($unit, $akun)
    {
        return $this->db->query("SELECT  nm_unit
                                  ,akun_akrual_3
                                  ,nm_akrual_3
                                  ,sum(mutasi_debet)-sum(mutasi_kredit) nilai
                              FROM TB_SPE_DETAIL_NERACA
                                  where akun_akrual_2='$akun'
                              and nm_unit='$unit'
                              group by  nm_unit
                                  ,akun_akrual_3
                                  ,nm_akrual_3")->result();
    }

    function mutasiAsetEmpat($unit, $akun)
    {
        return $this->db->query("SELECT  nm_unit
                                  ,akun_akrual_4
                                  ,nm_akrual_4
                                  ,sum(mutasi_debet)-sum(mutasi_kredit) nilai
                              FROM TB_SPE_DETAIL_NERACA
                                  where akun_akrual_3='$akun'
                              and nm_unit='$unit'
                              group by  nm_unit
                                  ,akun_akrual_4
                                  ,nm_akrual_4")->result();
    }

    function mutasiKewajiban($unit)
    {
        return $this->db->query("SELECT  nm_unit
                                      ,akun_akrual_2
                                      ,nm_akrual_2
                                      ,sum(mutasi_debet)-sum(mutasi_kredit) nilai
                                  FROM TB_SPE_DETAIL_NERACA
                                      where akun_akrual_1='2'
                                  and nm_unit='$unit'
                                  group by  nm_unit
                                      ,akun_akrual_2
                                      ,nm_akrual_2")->result();
    }

    function mutasiEkuitas($unit)
    {
        return $this->db->query("SELECT  nm_unit
                                  ,akun_akrual_2
                                  ,nm_akrual_2
                                  ,sum(mutasi_debet)-sum(mutasi_kredit) nilai
                              FROM TB_SPE_DETAIL_NERACA
                                  where akun_akrual_1='3'
                              and nm_unit='$unit'
                              group by  nm_unit
                                  ,akun_akrual_2
                                  ,nm_akrual_2")->result();
    }

    /*
    -- mutasi ekuitas


    */
}

/* End of file Mneraca.php */
/* Location: ./application/models/Mneraca.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-29 13:54:54 */
/* http://harviacode.com */

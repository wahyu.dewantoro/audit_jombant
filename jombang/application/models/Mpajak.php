<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mpajak extends CI_Model
{

    public $table = 'tb_spe_pajak';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($kd_skpd, $nm_sub_unit, $q = NULL)
    {
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or Tgl_Bukti like '%$q%' or No_Bukti like '%$q%' or Keterangan like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or D_K like '%$q%' or Id_Bukti like '%$q%' )", null, false);

        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($kd_skpd, $nm_sub_unit, $limit, $start = 0, $q = NULL)
    {
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or Tgl_Bukti like '%$q%' or No_Bukti like '%$q%' or Keterangan like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or D_K like '%$q%' or Id_Bukti like '%$q%' )", null, false);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
    function get_all_data($kd_skpd, $nm_sub_unit, $q = NULL)
    {
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or Tgl_Bukti like '%$q%' or No_Bukti like '%$q%' or Keterangan like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or D_K like '%$q%' or Id_Bukti like '%$q%' )", null, false);
        return $this->db->get($this->table)->result();
    }
    /*select
from spe_pajak
group by */

    function getData($limit, $start = 0, $q = NULL)
    {
        $this->db->select("kd_skpd,nm_sub_unit,sum(nilai) nilai", false);
        $this->db->group_by('kd_skpd', false);
        $this->db->group_by('nm_sub_unit', false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')", null, false);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
    function getAllData($q = NULL)
    {
        $this->db->select("kd_skpd,nm_sub_unit,sum(nilai) nilai", false);
        $this->db->order_by("kd_skpd", "asc");
        $this->db->group_by('kd_skpd', false);
        $this->db->group_by('nm_sub_unit', false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')", null, false);
        return $this->db->get($this->table)->result();
    }
    function getRow($q = NULL)
    {
        $this->db->from($this->table);
        $this->db->select("kd_skpd,nm_sub_unit,sum(nilai) nilai", false);
        $this->db->group_by('kd_skpd', false);
        $this->db->group_by('nm_sub_unit', false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')", null, false);
        return $this->db->count_all_results();
    }

    function getSubunit()
    {
        return $this->db->query("SELECT kd_skpd,nm_unit from tb_spe_ref_unit order by kd_skpd")->result();
    }

    function getSum($q = null)
    {
        $this->db->select("sum(nilai) nilai", false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%' )", null, false);
        return $this->db->get($this->table)->row();
    }

    function getSumdetail($kd_skpd, $nm_sub_unit, $q = NULL)
    {
        $this->db->select("sum(nilai) nilai", false);
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or Tgl_Bukti like '%$q%' or No_Bukti like '%$q%' or Keterangan like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or D_K like '%$q%' or Id_Bukti like '%$q%' )", null, false);
        return $this->db->get($this->table)->row();
    }
}

/* End of file Mpajak.php */
/* Location: ./application/models/Mpajak.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-29 13:53:14 */
/* http://harviacode.com */

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mpengguna extends CI_Model
{

    public $table = 'ms_pengguna';
    public $id = 'id_inc';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
        $this->menu_db = $this->load->database('menu', true);
    }

    // get all
    function get_all()
    {
        $this->menu_db->order_by($this->id, $this->order);
        return $this->menu_db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->menu_db->where($this->id, $id);
        return $this->menu_db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL)
    {
        $this->menu_db->like('id_inc', $q);
        $this->menu_db->or_like('nama', $q);
        $this->menu_db->or_like('username', $q);
        $this->menu_db->or_like('password', $q);
        $this->menu_db->from($this->table);
        return $this->menu_db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $this->menu_db->order_by($this->id, $this->order);
        $this->menu_db->like('id_inc', $q);
        $this->menu_db->or_like('nama', $q);
        $this->menu_db->or_like('username', $q);
        $this->menu_db->or_like('password', $q);
        $this->menu_db->limit($limit, $start);
        return $this->menu_db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        return $this->menu_db->insert($this->table, $data);
        // return $this->db->insert_id();
    }

    // update data
    function update($id, $data)
    {
        $this->menu_db->where($this->id, $id);
        return   $this->menu_db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->menu_db->where($this->id, $id);
        return $this->menu_db->delete($this->table);
    }

    function getRole(){
        return $this->menu_db->get('ms_role')->result();
    }

    function assignRole($data){
        return $this->menu_db->insert('ms_assign_role', $data);
    }

    function lastid(){
        return $this->menu_db->query("select max(id_inc) id  from ms_pengguna")->row()->id;
    }

    function userRole($id){
        $es=$this->menu_db->query("select ms_role_id from ms_assign_role where ms_pengguna_id=$id")->result();
        $role=[];
        foreach($es as $es){
            array_push($role,$es->ms_role_id);
        }
        return $role;
    }

    function deleteRole($id){ 
        $this->menu_db->where('ms_pengguna_id',$id);
        return $this->menu_db->delete('ms_assign_role');

    }
    
}
 
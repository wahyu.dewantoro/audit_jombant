<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mreport extends CI_Model
{


    /*public $id = 'id_inc';
    public $order = 'DESC';*/

    function __construct()
    {
        parent::__construct();
    }

    function getList()
    {
        return $this->db->query("SELECT nama_laporan,url_laporan FROM MS_SHORCTCUT_REPORT")->result();
    }

    function getLimaDuaDua()
    {

        $this->db->select("kd_skpd,nm_sub_unit,convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3) kode_rek_3,tgl_sp2d, no_sp2d,kd_rek_gabung, nilai, nm_rek_5,keterangan",false);
        $this->db->where("nilai < 1000000 and kd_rek_1=5 and kd_rek_2=2 and kd_rek_3=3");
        return $this->db->get("tb_spe_sp2d")->result();

       
    }

    function getLimaDuaTiga()
    {
        
        $this->db->select("kd_skpd,nm_sub_unit,convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3) kode_rek_3,tgl_sp2d, no_sp2d,kd_rek_gabung, nilai, nm_rek_5,keterangan",false);
        $this->db->where("nilai > 300000000 and kd_rek_1=5 and kd_rek_2=2 and kd_rek_3=2 and kd_rek_4=2 and kd_rek_5 in (1,2)");
        return $this->db->get('tb_spe_sp2d')->result();

    }
    function getsp2dyangbelumspj()
    {
        return $this->db->query("SELECT kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan,sum(nilai) nilai
                                FROM tb_spe_sp2d
                                WHERE  (Jenis_SP2D = 'TU' or Jenis_SP2D = 'UP') 
                                group by kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan")->result();
        // and no_sp2d not in (SELECT  SP2D_No_SP2D from SP2D_VS_SPJ)
    }
    function getDetailDuaTiga($kd_skpd, $akun)
    {
        return $this->db->query("SELECT kd_skpd,nm_sub_unit, tgl_sp2d,no_sp2d,jenis_sp2d,kd_rek_gabung,nm_rek_5,nilai
                                FROM tb_spe_sp2d
                                where convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3)='$akun'
                                and kd_skpd='$kd_skpd'")->result();
    }


    function sp2dAkrual()
    {
        return $this->db->query("SELECT kd_rek_gabung,nm_rek_5,sum(nilai) nilai
                                FROM tb_spe_sp2d
                                GROUP BY Kd_Rek_Gabung,nm_rek_5
                                ORDER BY kd_rek_gabung")->result();
    }

    function sp2dAkrualdetail($q, $nm_unit = null)
    {
        if ($nm_unit) {
            $and = " and nm_unit ='$nm_unit'";
        } else {
            $and = "";
        }

        return $this->db->query("select kode_unit,nm_unit,tgl_sp2d,no_sp2d,keterangan,npwp,nm_penerima,sum(nilai) nilai
                                from tb_spe_sp2d where kd_rek_gabung='$q' $and
                                group by kode_unit,nm_unit,tgl_sp2d,no_sp2d,keterangan,npwp,nm_penerima")->result();
    }
}

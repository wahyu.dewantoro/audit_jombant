<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mrole extends CI_Model
{

    public $table = 'ms_role';
    public $id = 'id_inc';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
        $this->menu_db = $this->load->database('menu', true);
    }

    // get all
    function get_all()
    {
        $this->menu_db->order_by($this->id, $this->order);
        return $this->menu_db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->menu_db->where($this->id, $id);
        return $this->menu_db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL)
    {
        $this->menu_db->like('id_inc', $q);
        $this->menu_db->or_like('nama_role', $q);
        $this->menu_db->from($this->table);
        return $this->menu_db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $this->menu_db->order_by($this->id, $this->order);
        $this->menu_db->like('id_inc', $q);
        $this->menu_db->or_like('nama_role', $q);
        $this->menu_db->limit($limit, $start);
        return $this->menu_db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        return  $this->menu_db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->menu_db->where($this->id, $id);
        return   $this->menu_db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->menu_db->where($this->id, $id);
        return $this->menu_db->delete($this->table);
    }

    function getMenu($id)
    {
        $cm = $this->menu_db->query('SELECT id_inc FROM ms_menu')->result_array();
        foreach ($cm as $rm) {
            $kode_menu = $rm['id_inc'];
            $sql = "SELECT ms_menu_id FROM ms_privilege WHERE ms_role_id='$id' AND ms_menu_id='$kode_menu'";
            $qcm = $this->menu_db->query($sql)->num_rows();
            if ($qcm == null) {
                $data = array('ms_menu_id' => $kode_menu, 'ms_role_id' => $id);
                $this->menu_db->insert('ms_privilege', $data);
            }
        }

        $qr = $this->menu_db->query("SELECT * FROM (
                                SELECT b.id_inc kode_role,nama_menu,CASE parent WHEN 0 THEN '#'  ELSE NULL END AS parent,STATUS,a.sort sort,b.created,b.updated,b.deleted
                                FROM ms_menu a , ms_privilege b WHERE a.id_inc=b.ms_menu_id 
                                AND parent=0 AND ms_role_id='$id' 
                                UNION 
                                    SELECT c.id_inc kode_role,a.nama_menu,b.nama_menu parent,STATUS,a.sort ,c.created,c.updated,c.deleted
                                    FROM ms_menu a,(SELECT id_inc,nama_menu FROM ms_menu) b, ms_privilege c WHERE a.parent=b.id_inc AND a.id_inc=c.ms_menu_id AND ms_role_id='$id' ) cb ORDER BY parent ASC,cb.sort ASC")->result_array();
        return $qr;
    }

    function do_role($kode_group, $role)
    {
        $ua = $this->menu_db->query("UPDATE ms_privilege SET status='0' WHERE ms_role_id ='$kode_group'");
        if ($ua) {
            $jumlah = count($role);
            for ($i = 0; $i < $jumlah; $i++) {
                $kode_role = $role[$i];
                $ur = $this->menu_db->query("UPDATE ms_privilege SET status='1' WHERE id_inc='$kode_role'");
                // echo "UPDATE ms_privilege SET status='1' WHERE id_inc='$kode_role'";
                // echo "<br>";
            }
        }

        return $ur;
    }

    function prosesrole($data = array())
    {
        // print_r($data);
        $this->menu_db->trans_start();
        // set null all

        $this->menu_db->set('status', null);
        $this->menu_db->set('created', null);
        $this->menu_db->set('updated', null);
        $this->menu_db->set('deleted', null);
        $this->menu_db->where('ms_role_id', $data['kode_role']);
        $this->menu_db->update('ms_privilege');

        // echo $data['kode_role'];
        // echo "<br>";

        if (!empty($data['read'])) {
            $idrr = explode(',', $data['read']);
            $this->menu_db->set('status', 1);
            $this->menu_db->where_in('id_inc', $idrr);
            $this->menu_db->update('ms_privilege');
            // echo $this->menu_db->last_query().'<br>';
        }


        if (!empty($data['create'])) {
            $idr = explode(',', $data['create']);
            $this->menu_db->set('created', 1);
            $this->menu_db->where_in('id_inc', $idr);
            $this->menu_db->update('ms_privilege');
        }

        if (!empty($data['update'])) {
            $idu = explode(',', $data['update']);
            $this->menu_db->set('updated', 1);
            $this->menu_db->where_in('id_inc', $idu);
            $this->menu_db->update('ms_privilege');
        }

        if (!empty($data['delete'])) {
            $idd = explode(',', $data['delete']);
            $this->menu_db->set('deleted', 1);
            $this->menu_db->where_in('id_inc', $idd);
            $this->menu_db->update('ms_privilege');
        }


        $this->menu_db->trans_complete();

        return $this->menu_db->trans_status();
    }
}

/* End of file Mrole.php */
/* Location: ./application/models/Mrole.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-07-22 15:57:05 */
/* http://harviacode.com */

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mspd extends CI_Model
{

    public $table = 'tb_spe_spd';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    function getLevelDua($kd_skpd, $nm_sub_unit, $nm_unit, $no_spd)
    {
        // select  from tb_spe_spd
        $this->db->select("tgl_spd,no_spd,ket_program,ket_kegiatan,nilai,kd_rek_gabung,nm_rek_5,uraian");
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where('nm_unit', $nm_unit);
        $this->db->where('no_spd', $no_spd);
        return $this->db->get('TB_SPE_SPD')->result();
    }

    // get total rows
    function total_rows($kd_skpd, $nm_sub_unit, $nm_unit, $q = NULL)
    {
        $this->db->select("kd_skpd ,nm_unit ,nm_sub_unit ,tgl_spd ,no_spd ,uraian ,count(*) jumlah,sum(nilai) nilai ", false);
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where('nm_unit', $nm_unit);
        $this->db->where("(kd_skpd like '%$q%' or  nm_unit like '%$q%' or  nm_sub_unit like '%$q%' or  tgl_spd like '%$q%' or  no_spd like '%$q%' or  uraian like '%$q%')", null, false);
        $this->db->group_by("kd_skpd ,nm_unit ,nm_sub_unit ,tgl_spd ,no_spd ,uraian", false);

        $this->db->from('TB_SPE_SPD');
        return $this->db->count_all_results();
    }



    // get data with limit and search
    function get_limit_data($limit, $start = 0, $kd_skpd, $nm_sub_unit, $nm_unit, $q = NULL)
    {

        $this->db->select("kd_skpd ,nm_unit ,nm_sub_unit ,tgl_spd ,no_spd ,uraian ,count(*) jumlah,sum(nilai) nilai ", false);
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where('nm_unit', $nm_unit);
        $this->db->where("(kd_skpd like '%$q%' or  nm_unit like '%$q%' or  nm_sub_unit like '%$q%' or  tgl_spd like '%$q%' or  no_spd like '%$q%' or  uraian like '%$q%')", null, false);
        $this->db->group_by("kd_skpd ,nm_unit ,nm_sub_unit ,tgl_spd ,no_spd ,uraian", false);
        $this->db->limit($limit, $start);
        return $this->db->get('TB_SPE_SPD')->result();
    }



    function get_all_data($kd_skpd, $nm_sub_unit, $nm_unit, $q = NULL)
    {
        $this->db->select("kd_skpd ,nm_unit ,nm_sub_unit ,tgl_spd ,no_spd ,uraian ,count(*) jumlah,sum(nilai) nilai ", false);
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where('nm_unit', $nm_unit);
        $this->db->where("(kd_skpd like '%$q%' or  nm_unit like '%$q%' or  nm_sub_unit like '%$q%' or  tgl_spd like '%$q%' or  no_spd like '%$q%' or  uraian like '%$q%')", null, false);
        $this->db->group_by("kd_skpd ,nm_unit ,nm_sub_unit ,tgl_spd ,no_spd ,uraian", false);
        return $this->db->get('TB_SPE_SPD')->result();
    }

    function getdata($limit, $start = 0, $q = NULL)
    {
        $this->db->select('Kd_SKPD, Nm_Sub_Unit, sum(nilai) nilai', false);
        $this->db->where("(Kd_SKPD like '%$q%' or Nm_Sub_Unit like '%$q%')", null, false);
        $this->db->group_by('Kd_SKPD', false);
        $this->db->group_by('Nm_Sub_Unit', false);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    function getAlldata($q = NULL)
    {
        $table = "(SELECT  kd_skpd ,nm_unit ,nm_sub_unit ,tgl_spd ,no_spd ,uraian ,sum(nilai) nilai FROM TB_SPE_SPD   group by  kd_skpd ,nm_unit ,nm_sub_unit ,tgl_spd ,no_spd ,uraian ) temp";
        $this->db->select("kd_skpd ,nm_unit ,nm_sub_unit ,count(*) jum ,sum(nilai) nilai ", false);
        $this->db->group_by("kd_skpd ,nm_unit ,nm_sub_unit", false);
        $this->db->where("(kd_skpd like '%$q%' or  nm_unit like '%$q%' or nm_sub_unit like '%$q%' )", null, false);
        return $this->db->get($table)->result();
    }

    function gettotal($q = NULL)
    {
        $tmp = "select count(*) jum from (SELECT kd_skpd, nm_unit, nm_sub_unit, count(*) jum, sum(nilai) nilai FROM (
        SELECT kd_skpd, nm_unit, nm_sub_unit, tgl_spd, no_spd, uraian, sum(nilai) nilai FROM TB_SPE_SPD group by kd_skpd, nm_unit, nm_sub_unit, tgl_spd, no_spd, uraian
      ) temp
      WHERE (kd_skpd like '%%' or nm_unit like '%%' or nm_sub_unit like '%%' )
      GROUP BY kd_skpd ,nm_unit ,nm_sub_unit) qwerty";

        $rr = $this->db->query($tmp)->row();
        return $rr->jum;
    }

    function getSumNilai($q = null)
    {
        $this->db->select("sum(nilai) jum_nilai", false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')", null, false);
        return $this->db->get($this->table)->row();
    }

    function getSubunit()
    {
        return $this->db->query("SELECT kd_skpd,nm_unit from tb_spe_ref_unit order by kd_skpd")->result();
    }

    function getSumdetail($kd_skpd, $nm_sub_unit, $q = NULL)
    {
        $this->db->select("sum(nilai) nilai", false);
        $this->db->where("(Kd_SKPD like '%$kd_skpd%' and Nm_Sub_Unit like '%$nm_sub_unit%')", null, false);
        $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SPD like '%$q%' or No_SPD like '%$q%' or Uraian like '%$q%' or  Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or No_SPD like '%$q%' )", NULL, false);
        // $this->db->group_by('Kd_SKPD',false);
        // $this->db->group_by('Nm_Sub_Unit',false);
        return $this->db->get($this->table)->row();
    }
}

/* End of file Mspd.php */
/* Location: ./application/models/Mspd.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-29 12:12:03 */
/* http://harviacode.com */

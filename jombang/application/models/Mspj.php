<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mspj extends CI_Model
{

    public $table = 'tb_spe_spj';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($kd_skpd, $nm_sub_unit, $jenis_spj, $q = NULL)
    {
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where('jenis_spj', $jenis_spj);
        $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SPJ like '%$q%' or No_SPJ like '%$q%' or Tgl_Bukti like '%$q%' or No_Bukti like '%$q%' or Tgl_Pengesahan like '%$q%' or No_Pengesahan like '%$q%' or Keterangan like '%$q%' or Jenis_SPJ like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or Uraian like '%$q%' or Id_Bukti like '%$q%')", NULL, false);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($nm_unit, $nm_sub_unit, $kd_skpd)
    {
        $this->db->select("kd_skpd,nm_unit,nm_sub_unit,tgl_spj,no_spj,keterangan,jenis_spj,sum(nilai) nilai,count(*) jum", false);
        $this->db->group_by("kd_skpd,nm_unit,nm_sub_unit,tgl_spj,no_spj,keterangan,jenis_spj", false);
        $this->db->where('nm_unit', $nm_unit);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where('kd_skpd', $kd_skpd);
        return $this->db->get("tb_spe_spj", false)->result();
    }

    function get_all_data($kd_skpd, $nm_sub_unit, $jenis_spj, $q = NULL)
    {
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where('jenis_spj', $jenis_spj);
        $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SPJ like '%$q%' or No_SPJ like '%$q%' or Tgl_Bukti like '%$q%' or No_Bukti like '%$q%' or Tgl_Pengesahan like '%$q%' or No_Pengesahan like '%$q%' or Keterangan like '%$q%' or Jenis_SPJ like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or Uraian like '%$q%' or Id_Bukti like '%$q%')", NULL, false);
        return $this->db->get($this->table)->result();
    }
    function getData($limit, $start = 0, $q = NULL)
    {
        $this->db->select("kd_skpd,nm_sub_unit,jenis_spj,count(jenis_spj) jumlah,sum(nilai) nilai", false);
        $this->db->group_by('kd_skpd', false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%' or jenis_spj like '%$q%')", NULL, false);
        $this->db->group_by('nm_sub_unit', false);
        $this->db->group_by('jenis_spj', false);
        $this->db->group_by('kd_skpd', false);

        $this->db->limit($limit, $start);
        return $this->db->get($this->table, false)->result();
    }

    function getAllData($q = NULL)
    {
        $table = "(select kd_skpd,nm_unit,nm_sub_unit,no_spj,keterangan,jenis_spj,tgl_spj,sum(nilai) nilai
                from tb_spe_spj
                group by kd_skpd,nm_unit,nm_sub_unit,no_spj,keterangan,jenis_spj,tgl_spj
                ) temp";
        $this->db->select("kd_skpd,nm_unit,nm_sub_unit,count(*) jum,sum(case when jenis_spj ='up' then 1 end) jnsup ,sum(case when jenis_spj ='up' then nilai end) nilaiup ,sum(case when jenis_spj ='gu' then 1 end) jnsgu ,sum(case when jenis_spj ='gu' then nilai end) nilaigu ,sum(case when jenis_spj ='tu' then 1 end) jnstu ,sum(case when jenis_spj ='tu' then nilai end) nilaitu ,sum(case when jenis_spj ='ls' then 1 end) jnsls ,sum(case when jenis_spj ='ls' then nilai end) nilails ,sum(case when jenis_spj ='nihil' then 1 end) jnsnihil ,sum(case when jenis_spj ='nihil' then nilai end) nilainihil", false);
        $this->db->where("nm_unit like '%$q%' ", NULL, false);
        $this->db->group_by('kd_skpd,nm_unit,nm_sub_unit', false);
        return $this->db->get($table, false)->result();
    }

    function getRow($q = NULL)
    {
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%' or jenis_spj like '%$q%')", NULL, false);
        $this->db->from($this->table, false);
        $this->db->group_by('nm_sub_unit', false);
        $this->db->group_by('jenis_spj', false);
        $this->db->group_by('kd_skpd', false);
        $this->db->select("kd_skpd,nm_sub_unit,jenis_spj,count(jenis_spj) jumlah,sum(nilai) nilai", false);
        return $this->db->count_all_results();
    }
    function getSumNilai($q = null)
    {
        $this->db->select("sum(nilai) jum_nilai", false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')", null, false);
        return $this->db->get($this->table)->row();
    }

    function getSubunit()
    {
        return $this->db->query("SELECT kd_skpd,nm_unit from tb_spe_ref_unit order by kd_skpd")->result();
    }

    function getSumdetail($kd_skpd, $nm_sub_unit, $q = NULL, $jenis_spj)
    {
        $this->db->select("sum(nilai) nilai", false);
        $this->db->where("(Kd_SKPD like '%$kd_skpd%' and Nm_Sub_Unit like '%$nm_sub_unit%' and jenis_spj like '%$jenis_spj%')", null, false);
        $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SPJ like '%$q%' or No_SPJ like '%$q%' or Uraian like '%$q%' or Jenis_SPJ like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or No_SPJ like '%$q%' )", NULL, false);

        return $this->db->get($this->table)->row();
    }


    function getdetailspj($kd_skpd, $nm_unit, $nm_sub_unit, $no_spj)
    {
        return $this->db->query("SELECT  ket_program,ket_kegiatan,tgl_spj,no_spj,tgl_bukti,no_bukti,tgl_pengesahan,no_pengesahan,keterangan,jenis_spj,nilai,kd_rek_gabung,nm_rek_5,uraian
                                from tb_spe_spj
                                 where kd_skpd ='$kd_skpd'
                                 and nm_unit ='$nm_unit'
                                 and nm_sub_unit ='$nm_sub_unit'
                                 and no_spj ='$no_spj'
                                ")->result();
    }
}

/* End of file Mspj.php */
/* Location: ./application/models/Mspj.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-29 12:10:04 */
/* http://harviacode.com */

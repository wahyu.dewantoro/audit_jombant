<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mspm extends CI_Model
{

    public $table = 'tb_spe_spm';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($kd_skpd, $nm_sub_unit, $jenis_spm, $q = NULL)
    {

        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where('jenis_spm', $jenis_spm);
        $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SPM like '%$q%' or No_SPM like '%$q%' or Uraian like '%$q%' or Jenis_SPM like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or Nm_Penerima like '%$q%' or Rek_Penerima like '%$q%' or Bank_Penerima like '%$q%' or NPWP like '%$q%' or No_SPP like '%$q%')", NULL, false);

        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($nm_unit, $nm_sub_unit, $kd_skpd)
    {










        $this->db->select("kd_skpd,nm_unit,nm_sub_unit,tgl_spm,no_spm,uraian,jenis_spm,count(*) jum,sum(nilai) nilai", false);
        $this->db->where('nm_unit', $nm_unit);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->group_by('kd_skpd,nm_unit,nm_sub_unit,tgl_spm,no_spm,uraian,jenis_spm', false);
        return $this->db->get('tb_spe_spm', false)->result();
        /*$this->db->select("kd_skpd,nm_sub_unit,jenis_spm,convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3) rekening,count(jenis_spm) jumlah,sum(nilai) nilai",false);
        $this->db->where("nm_unit like '%$nm_unit%'",null,false);
        $this->db->group_by("kd_skpd",false);
        $this->db->group_by("nm_sub_unit",false);
        $this->db->group_by("jenis_spm",false);
        $this->db->group_by("convert(varchar,kd_rek_1)+'.'+convert(varchar,kd_rek_2)+'.'+convert(varchar,kd_rek_3)",false);
        return $this->db->get('tb_spe_spm')->result();*/
    }

    function get_all_data($kd_skpd, $nm_sub_unit, $jenis_spm, $q = NULL)
    {
        $this->db->where('kd_skpd', $kd_skpd);
        $this->db->where('nm_sub_unit', $nm_sub_unit);
        $this->db->where('jenis_spm', $jenis_spm);
        $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SPM like '%$q%' or No_SPM like '%$q%' or Uraian like '%$q%' or Jenis_SPM like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or Nm_Penerima like '%$q%' or Rek_Penerima like '%$q%' or Bank_Penerima like '%$q%' or NPWP like '%$q%' or No_SPP like '%$q%')", NULL, false);
        return $this->db->get($this->table)->result();
    }

    function getData($limit, $start = 0, $q = NULL)
    {
        $this->db->select("kd_skpd,nm_sub_unit,jenis_spm,count(jenis_spm) jumlah,sum(nilai) nilai", false);
        $this->db->group_by('kd_skpd', false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%' or jenis_spm like '%$q%')", NULL, false);
        $this->db->group_by('nm_sub_unit', false);
        $this->db->group_by('jenis_spm', false);
        $this->db->group_by('kd_skpd', false);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table, false)->result();
    }

    function getAllData($q = NULL)
    {
        // getAllData
        /*select  from (
   select kd_skpd,nm_unit,nm_sub_unit,no_spm,uraian,jenis_spm,sum(nilai) nilai
   from tb_spe_spm
   group by kd_skpd,nm_unit,nm_sub_unit,no_spm,uraian,jenis_spm
) temp
group by kd_skpd,nm_unit,nm_sub_unit*/


        $this->db->select("kd_skpd,nm_unit,nm_sub_unit ,sum(case when jenis_spm ='up' then 1 end) jnsup ,sum(case when jenis_spm ='up' then nilai end) nilaiup ,sum(case when jenis_spm ='gu' then 1 end) jnsgu ,sum(case when jenis_spm ='gu' then nilai end) nilaigu ,sum(case when jenis_spm ='tu' then 1 end) jnstu ,sum(case when jenis_spm ='tu' then nilai end) nilaitu ,sum(case when jenis_spm ='ls' then 1 end) jnsls ,sum(case when jenis_spm ='ls' then nilai end) nilails ,sum(case when jenis_spm ='nihil' then 1 end) jnsnihil ,sum(case when jenis_spm ='nihil' then nilai end) nilainihil", false);
        $this->db->where("nm_unit like '%$q%' ", NULL, false);
        $this->db->group_by('kd_skpd,nm_unit,nm_sub_unit', false);
        return $this->db->get("(select kd_skpd,nm_unit,nm_sub_unit,no_spm,uraian,jenis_spm,tgl_spm,sum(nilai) nilai
                               from tb_spe_spm
                               group by kd_skpd,nm_unit,nm_sub_unit,no_spm,uraian,jenis_spm,tgl_spm) temp", false)->result();
    }
    function getRow($q = NULL)
    {
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%' or jenis_spm like '%$q%')", NULL, false);
        $this->db->select("kd_skpd,nm_sub_unit,jenis_spm,count(jenis_spm) jumlah,sum(nilai) nilai", false);
        $this->db->group_by('nm_sub_unit', false);
        $this->db->group_by('jenis_spm', false);
        $this->db->group_by('kd_skpd', false);
        $this->db->from($this->table, false);
        return $this->db->count_all_results();
    }
    function getSumNilai($q = null)
    {
        $this->db->select("sum(nilai) jum_nilai", false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')", null, false);
        return $this->db->get($this->table)->row();
    }

    function getSubunit()
    {
        return $this->db->query("SELECT kd_skpd,nm_unit from tb_spe_ref_unit order by kd_skpd")->result();
    }

    function getSumdetail($kd_skpd, $nm_sub_unit, $q = NULL, $jenis_spm)
    {
        $this->db->select("sum(nilai) nilai", false);
        $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or ID_Prog like '%$q%' or Ket_Program like '%$q%' or Kd_Keg like '%$q%' or Ket_Kegiatan like '%$q%' or Tgl_SPM like '%$q%' or No_SPP like '%$q%' or Uraian like '%$q%' or Jenis_SPM like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or Nm_Penerima like '%$q%' or Rek_Penerima like '%$q%' or Bank_Penerima like '%$q%' or NPWP like '%$q%' or No_SPM like '%$q%' )", NULL, false);
        $this->db->where("(Kd_SKPD like '%$kd_skpd%' and Nm_Sub_Unit like '%$nm_sub_unit%' and jenis_spm like '%$jenis_spm%')", null, false);
        $this->db->group_by('Kd_SKPD', false);
        $this->db->group_by('Nm_Sub_Unit', false);
        $this->db->group_by('jenis_spm', false);
        return $this->db->get($this->table)->row();
    }

    function getdetailspm($kd_skpd, $nm_unit, $nm_sub_unit, $no_spm)
    {
        return $this->db->query("SELECT ket_program, ket_kegiatan, tgl_spm, no_spm,  nilai, kd_rek_gabung, nm_rek_5
                                    FROM tb_spe_spm
                                    WHERE kd_skpd='$kd_skpd'
                                    and no_spm='$no_spm'
                                    and nm_unit='$nm_unit'
                                    and nm_sub_unit='$nm_sub_unit'
                                    and no_spm='$no_spm'")->result();
    }
}

/* End of file Mspm.php */
/* Location: ./application/models/Mspm.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-29 12:08:22 */
/* http://harviacode.com */

<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Msppdpot extends CI_Model
{

    public $table = 'tb_spe_sp2d_potongan';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($Kd_SKPD, $Nm_Sub_Unit, $q = NULL)
    {
        $this->db->where('Kd_SKPD', $Kd_SKPD);
        $this->db->where('Nm_Sub_Unit', $Nm_Sub_Unit);
        $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or  Tgl_SP2D like '%$q%' or No_SP2D like '%$q%' or Keterangan like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or Id_Bukti like '%$q%')", null, false);

        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($kd_skpd, $nm_sub_unit, $nm_unit)
    {
        /*  select kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan,sum(nilai) nilai
                   from tb_spe_sp2d_potongan
                   group by kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan*/










        $this->db->select("kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan,count(*) jum,sum(nilai) nilai", false);
        $this->db->group_by("kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan", false);
        $this->db->where("kd_skpd", $kd_skpd);
        $this->db->where("nm_sub_unit", $nm_sub_unit);
        $this->db->where("nm_unit", $nm_unit);
        return $this->db->get("tb_spe_sp2d_potongan", false)->result();
    }
    function get_all_data($Kd_SKPD, $Nm_Sub_Unit, $limit, $start = 0, $q = NULL)
    {
        $this->db->where('Kd_SKPD', $Kd_SKPD);
        $this->db->where('Nm_Sub_Unit', $Nm_Sub_Unit);
        $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or  Tgl_SP2D like '%$q%' or No_SP2D like '%$q%' or Keterangan like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or Id_Bukti like '%$q%')", null, false);
        return $this->db->get($this->table)->result();
    }
    function getdata($limit, $start = 0, $q = NULL)
    {
        $this->db->select('Kd_SKPD, Nm_Sub_Unit, sum(nilai) nilai', false);
        $this->db->where("(Kd_SKPD like '%$q%' or Nm_Sub_Unit like '%$q%')", null, false);
        $this->db->group_by('Kd_SKPD', false);
        $this->db->group_by('Nm_Sub_Unit', false);
        $this->db->group_by('Kd_SKPD', false);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    function getalldata($q = NULL)
    {


        $table = "(
                   select kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan,sum(nilai) nilai
                   from tb_spe_sp2d_potongan
                   group by kd_skpd,nm_unit,nm_sub_unit,tgl_sp2d,no_sp2d,keterangan
                ) temp";
        $this->db->select("kd_skpd,nm_unit,nm_sub_unit,count(*) jum,sum(nilai) nilai", false);
        $this->db->where("(nm_unit like '%$q%' or kd_skpd like '%$q%' or nm_sub_unit like '%$q%')", null, false);
        $this->db->group_by("kd_skpd,nm_unit,nm_sub_unit", false);
        return $this->db->get($table, false)->result();
    }

    function gettotal($q = NULL)
    {
        /*//*/
        $this->db->where("(Kd_SKPD like '%$q%' or Nm_Sub_Unit like '%$q%')", null, false);
        $this->db->from($this->table);
        $this->db->group_by('Kd_SKPD', false);
        $this->db->group_by('Nm_Sub_Unit', false);
        $this->db->group_by('Kd_SKPD', false);
        $this->db->select('Kd_SKPD, Nm_Sub_Unit, sum(nilai) nilai', false);
        return $this->db->count_all_results();
    }
    function getSumNilai($q = null)
    {
        $this->db->select("sum(nilai) jum_nilai", false);
        $this->db->where("(kd_skpd like '%$q%' or nm_sub_unit like '%$q%')", null, false);
        return $this->db->get($this->table)->row();
    }

    function getSubunit()
    {
        return $this->db->query("SELECT kd_skpd,nm_unit from tb_spe_ref_unit order by kd_skpd")->result();
    }

    function getSumdetail($kd_skpd, $nm_sub_unit, $q = NULL)
    {
        $this->db->select("sum(nilai) nilai", false);
        $this->db->where("(Kd_SKPD like '%$kd_skpd%' and Nm_Sub_Unit like '%$nm_sub_unit%')", null, false);
        $this->db->where("(Tahun like '%$q%' or Kd_SKPD like '%$q%' or Nm_Unit like '%$q%' or Nm_Sub_Unit like '%$q%' or  Tgl_SP2D like '%$q%' or No_SP2D like '%$q%' or Keterangan like '%$q%' or Nilai like '%$q%' or Kd_Rek_1 like '%$q%' or Kd_Rek_2 like '%$q%' or Kd_Rek_3 like '%$q%' or Kd_Rek_4 like '%$q%' or Kd_Rek_5 like '%$q%' or Kd_Rek_Gabung like '%$q%' or Nm_Rek_5 like '%$q%' or Id_Bukti like '%$q%')", null, false);
        return $this->db->get($this->table)->row();
    }

    function getDetail($kd_skpd, $nm_sub_unit, $nm_unit, $no_sp2d)
    {
        return $this->db->query("SELECT tgl_sp2d,no_sp2d,keterangan,nilai,nm_pot,kd_rek_gabung,nm_rek_5,id_bukti
                                FROM tb_spe_sp2d_potongan
                                WHERE 
                                kd_skpd ='$kd_skpd'
                                AND nm_unit='$nm_unit'
                                AND nm_sub_unit='$nm_sub_unit'
                                AND no_sp2d='$no_sp2d'")->result();
    }
}

/* End of file Msppdpot.php */
/* Location: ./application/models/Msppdpot.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2018-10-29 13:50:01 */
/* http://harviacode.com */

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">                       
        <div class="card mb-3">
            <div class="card-body">
                <div class="tools float-right">
                    <form action="<?php echo site_url('akrualrek/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control form-control-xs" name="q" value="<?php echo $q; ?>" placeholder="Pencarian">
                            <span class="input-group-btn">
                            <div class="btn-group">
                            
                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                                <?php if ($q <> '')  { ?>
                                    <a href="<?php echo site_url('akrualrek'); ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
                              <?php } 
                              
                                    if($akses['create']==1){
                                     echo anchor(site_url('akrualrek/create'),'<i class="mdi mdi-plus"></i> Tambah', 'class="btn btn-success btn-sm"');
                                    }
                                 ?>
                            </div>
                        </span>
                    </div>
                </form>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								
								<th> Akrual 1</th>
								<th> Akrual 2</th>
								<th> Akrual 3</th>
								<th> Akrual 4</th>
								<th> Akrual 5</th>
								<th>D / K</th>
								 
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($akrualrek_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo ++$start ?></td>
								
								<td class="cell-detail">
									<span><?php echo $rk->Akun_Akrual_1 ?></span>
									<span class="cell-detail-description"><?php echo $rk->Nm_Akrual_1 ?></span>
								</td>

								<td class="cell-detail">
									<span><?php echo $rk->Akun_Akrual_2 ?></span>
									<span class="cell-detail-description"><?php echo $rk->Nm_Akrual_2 ?></span>
								</td>

								<td class="cell-detail">
									<span><?php echo $rk->Akun_Akrual_3 ?></span>
									<span class="cell-detail-description"><?php echo $rk->Nm_Akrual_3 ?></span>
								</td>

								<td class="cell-detail">
									<span><?php echo $rk->Akun_Akrual_4 ?></span>
									<span class="cell-detail-description"><?php echo $rk->Nm_Akrual_4 ?></span>
								</td>

								<td class="cell-detail">
									<span><?php echo $rk->Akun_Akrual_5 ?></span>
									<span class="cell-detail-description"><?php echo $rk->Nm_Akrual_5 ?></span>
								</td> 
								<td align="center"><?php echo $rk->D_K ?></td>
								 
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					</div>
					<button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
					<div class="float-right">
						<?php echo $pagination ?>
					</div>
            </div>                                                      
        </div><!-- end card-->                  
    </div>
</div>
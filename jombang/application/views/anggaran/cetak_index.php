<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Anggaran.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Anggaran</h3>
<table class="tablee" border="1">
  <thead>
    <tr>
        
        <th rowspan="2">OPD</th>
        <th rowspan="2">Unit</th>
        <th colspan="2">Anggaran Awal</th>
        <th colspan="2">Anggaran Perubahan</th>
        
    </tr>
    <tr>
        <th>Pendapatan</th>
        <th>Belanja</th>
        <th>Pendapatan</th>
        <th>Belanja</th>
    </tr>
</thead>
<tbody>
    <?php foreach($anggaran_data as $rk){?>
        <tr>
            
            <td><?= $rk->kd_skpd ?></td>
            <td><?= $rk->nm_unit ?></td>
            <td align="right"><?php echo number_format($rk->anggaran_awal_4,0,'','.') ?></td>
            <td align="right"><?php echo number_format($rk->anggaran_awal_5,0,'','.') ?></td>
            <td align="right"><?= number_format($rk->anggaran_perubahan_1_4,0,'','.') ?></td>
            <td align="right"><?= number_format($rk->anggaran_perubahan_1_5,0,'','.') ?></td>
        </tr>
    <?php } ?>
</tbody>
</table>
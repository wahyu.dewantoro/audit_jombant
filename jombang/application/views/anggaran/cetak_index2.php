<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Anggaran.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3><?= $title ?></h3>
<?= $nm_unit?><br>
<?= $var ?> (<?= $jenis ?>)
  <table class="table table-striped table-bordered" border="1">
                      <thead>
                          <tr>
                              <th colspan="3">Akun</th>
                              <th colspan="3">Rekening</th>
                              <th>Nilai</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php foreach($anggaran_data as $rk){?>
                          <tr>
                            <td colspan="3" align="left"><strong><?= $rk->akun_akrual_2 ?></strong></td>
                            <td colspan="3" align="left"><strong><?= $rk->nm_akrual_2 ?></strong></td>
                            <td align="right"><strong><?php if($jenis=='awal'){echo number_format($rk->nilai,0,'','.');}else{echo number_format($rk->nilaisatu,0,'','.');}?></strong></td>
                          </tr>
                          <!-- GetLeveltiga -->
                          <?php 
                          $tiga=$this->Manggaran->GetLeveltiga($nm_unit,$rk->akun_akrual_2);
                          foreach($tiga as $rn){?>
                            <tr style="color:green">
                              <td></td>
                              <td colspan="2" align="left"><?= $rn->akun_akrual_3?></td>
                              <td></td>
                              <td colspan="2" align="left"><?= $rn->nm_akrual_3?></td>
                              <td align="right"><?php if($jenis=='awal'){echo number_format($rn->nilai,0,'','.');}else{echo number_format($rn->nilaisatu,0,'','.');}?></td> 
                            </tr>
                          <?php
                          $lima=$this->Manggaran->GetLevelempat($nm_unit,$rn->akun_akrual_3);
                          foreach($lima as $rm){  ?>
                            <tr style="color:red">
                              <td></td>
                              <td></td>
                              <td align="left"><i><?= $rm->akun_akrual_4?></i></td>
                              <td></td>
                              <td></td>
                              <td align="left"><i><?= $rm->nm_akrual_4?></i></td>
                              <td align="right"><i><?php if($jenis=='awal'){echo number_format($rm->nilai,0,'','.');}else{echo number_format($rm->nilaisatu,0,'','.');}?></i></td>  
                            </tr>
                        <?php } } } ?>
                      </tbody>
                    </table>
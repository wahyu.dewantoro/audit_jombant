<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=RealisasiAnggaran.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Realisasi Anggaran</h3>
<table class="tablee" border="1">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>SKPD</th>
								<th>Nm Unit</th>
								<th>Anggaran </th>
								<th>Realisasi</th>
                                <th>Presentase</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1; foreach ($anggaran_data as $rk) {?>
                            <tr>
                                <td valign="top" width="10px" class="text-center"><?php echo $no++; ?></td>
                                <td valign="top" ><?php echo $rk->kd_skpd ?></td>
                                <td valign="top" ><?php if($rk->nm_sub_unit==$rk->nm_unit){ echo $rk->nm_sub_unit; }else{echo $rk->nm_unit.' ('.$rk->nm_sub_unit.')'; } ?></td>

                                <td valign="top"  align="right"><?php echo number_format($rk->anggaran,'2',',','.') ?></td>
                                <td valign="top"  align="right"><?php echo number_format($rk->nilai,'2',',','.') ?></td>
                                <td valign="top"  align="center"><?=  number_format(($rk->nilai/$rk->anggaran)*100,'0','','') ?> %</td>
                            </tr>
                        <?php
                        } ?>
						</tbody>
					</table>
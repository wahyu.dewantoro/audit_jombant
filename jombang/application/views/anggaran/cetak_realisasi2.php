<?php

// Panggil class PHPExcel nya
$csv = new PHPExcel();

// Settingan awal fil excel
$csv->getProperties()->setCreator('E-Audit')
             ->setLastModifiedBy('E-Audit')
             ->setTitle("Realisasi Anggaran")
             ->setSubject("Realisasi Anggaran")
             ->setDescription("Realisasi Anggaran")
             ->setKeywords("Realisasi Anggaran");

// Buat header tabel nya pada baris ke 1
$csv->setActiveSheetIndex(0)->setCellValue('A1', "NO;"); // Set kolom A1 dengan tulisan "NO"
$csv->setActiveSheetIndex(0)->setCellValue('B1', "SKPD"); // Set kolom B1 dengan tulisan "NIS"
$csv->setActiveSheetIndex(0)->setCellValue('C1', "Nama Unit"); // Set kolom C1 dengan tulisan "NAMA"
$csv->setActiveSheetIndex(0)->setCellValue('D1', "Anggaran "); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('E1', "Realisasi"); // Set kolom E1 dengan tulisan "TELEPON"
$csv->setActiveSheetIndex(0)->setCellValue('F1', "Presentase"); // Set kolom F1 dengan tulisan "ALAMAT"



$no = 1; // Untuk penomoran tabel, di awal set dengan 1
$numrow = 2; // Set baris pertama untuk isi tabel adalah baris ke 2
$nm_unit="";
foreach ($anggaran_data as $rk) {

    if($rk->nm_sub_unit==$rk->nm_unit){ $nm_unit= $rk->nm_unit; }else{$nm_unit= $rk->nm_unit."(".$rk->nm_sub_unit.")"; }
    $csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
    $csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $rk->kd_skpd);
    $csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $nm_unit);
    $csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, number_format($rk->anggaran,'2',',','.'));
    $csv->setActiveSheetIndex(0)->setCellValueExplicit('E'.$numrow, number_format($rk->nilai,'2',',','.'));
    $csv->setActiveSheetIndex(0)->setCellValue('F'.$numrow, number_format(($rk->nilai/$rk->anggaran)*100,'0','','')." %");
    $no++; // Tambah 1 setiap kali looping
    $numrow++; // Tambah 1 setiap kali looping
}

// Set orientasi kertas jadi LANDSCAPE
$csv->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

// Set judul file excel nya
$csv->getActiveSheet(0)->setTitle("Laporan Data Transaksi");
$csv->setActiveSheetIndex(0);

// Proses file excel
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="Realisasi Anggaran.csv"'); // Set nama file excel nya
header('Cache-Control: max-age=0');

$write = new PHPExcel_Writer_CSV($csv);
$write->save('php://output');
?>
<?php

// Panggil class PHPExcel nya
$csv = new PHPExcel();

// Settingan awal fil excel
// $csv->getProperties()->setCreator('E-Audit')
//              ->setLastModifiedBy('E-Audit')
//              ->setTitle("Anggaran")
//              ->setSubject("Anggaran")
//              ->setDescription("Anggaran")
//              ->setKeywords("Anggaran");

// Buat header tabel nya pada baris ke 1
$csv->setActiveSheetIndex(0)->setCellValue('A1', "NO"); // Set kolom A1 dengan tulisan "NO"
$csv->setActiveSheetIndex(0)->setCellValue('B1', "Kode Rekening"); // Set kolom B1 dengan tulisan "NIS"
$csv->setActiveSheetIndex(0)->setCellValue('C1', "Rekening"); // Set kolom C1 dengan tulisan "NAMA"
$csv->setActiveSheetIndex(0)->setCellValue('D1', "Anggaran Awal"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('E1', "Anggaran Perubahan 1"); // Set kolom E1 dengan tulisan "TELEPON"
$csv->setActiveSheetIndex(0)->setCellValue('F1', "Anggaran Perubahan 2"); // Set kolom F1 dengan tulisan "ALAMAT"



$no = 1; // Untuk penomoran tabel, di awal set dengan 1
$numrow = 2; // Set baris pertama untuk isi tabel adalah baris ke 2

$a=0;
$b=0;
$c=0;
$kd_rek="";
$nm_akrual="";
foreach ($anggaran_data as $rk) {
    $a+=$rk->anggaran_awal;
    $b+=$rk->anggaran_perubahan_1;
    $c+=$rk->anggaran_perubahan_2;
    if($rk->akun_akrual_3<>''){ $kd_rek=$rk->akun_akrual_3; }else{echo $kd_rek="-";}
    if($rk->nm_akrual_3<>''){ $nm_akrual=$rk->nm_akrual_3; }else{echo $nm_akrual="-";}
    $csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
    $csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $kd_rek);
    $csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $nm_akrual);
    $csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, number_format($rk->anggaran_awal,'2',',','.'));

    // Khusus untuk no telepon. kita set type kolom nya jadi STRING
    $csv->setActiveSheetIndex(0)->setCellValueExplicit('E'.$numrow, number_format($rk->anggaran_perubahan_1,'2',',','.'));

    $csv->setActiveSheetIndex(0)->setCellValue('F'.$numrow, number_format($rk->anggaran_perubahan_2,'2',',','.'));

    $no++; // Tambah 1 setiap kali looping
    $numrow++; // Tambah 1 setiap kali looping
}

// Set orientasi kertas jadi LANDSCAPE
$csv->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

// Set judul file excel nya
// $csv->getActiveSheet(0)->setTitle("Anggaran");
// $csv->setActiveSheetIndex(0);

// Proses file excel
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment; filename=Anggaran ".$kd_skpd." ".str_replace('.',' ', str_replace(',', '', $nm_sub_unit)).".csv");
header('Cache-Control: max-age=0');

$write = new PHPExcel_Writer_CSV($csv);
$write->save('php://output');
?>
<?php

// Panggil class PHPExcel nya
$csv = new PHPExcel();

// Settingan awal fil excel

// Buat header tabel nya pada baris ke 1
$csv->setActiveSheetIndex(0)->setCellValue('A1', "NO"); // Set kolom A1 dengan tulisan "NO"
$csv->setActiveSheetIndex(0)->setCellValue('B1', "Kode Rekening"); // Set kolom B1 dengan tulisan "NIS"
$csv->setActiveSheetIndex(0)->setCellValue('C1', "Rekening"); // Set kolom B1 dengan tulisan "NIS"
$csv->setActiveSheetIndex(0)->setCellValue('D1', "Program"); // Set kolom C1 dengan tulisan "NAMA"
$csv->setActiveSheetIndex(0)->setCellValue('E1', "Kegiatan "); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('F1', "Anggaran Awal "); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('G1', "Anggaran Perubahan 1"); // Set kolom E1 dengan tulisan "TELEPON"
$csv->setActiveSheetIndex(0)->setCellValue('H1', "Anggaran Perubahan 2"); // Set kolom E1 dengan tulisan "TELEPON"

$no = 1; // Untuk penomoran tabel, di awal set dengan 1
$numrow = 2; // Set baris pertama untuk isi tabel adalah baris ke 2
$nm_unit="";
foreach ($anggaran_data as $rk) {

    if($rk->akun_akrual_5<>''){ $kd_rek=$rk->akun_akrual_5; }else{echo $kd_rek="-";}
    if($rk->nm_akrual_5<>''){ $nm_akrual=$rk->nm_akrual_5; }else{echo $nm_akrual="-";}
    $csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
    $csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $kd_rek);
    $csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $nm_akrual);
    $csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $rk->ket_program);
    $csv->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $rk->ket_kegiatan);
    $csv->setActiveSheetIndex(0)->setCellValue('F'.$numrow, number_format($rk->anggaran_awal,'2',',','.') );
    $csv->setActiveSheetIndex(0)->setCellValueExplicit('G'.$numrow, number_format($rk->anggaran_perubahan_1,'2',',','.'));
    $csv->setActiveSheetIndex(0)->setCellValueExplicit('H'.$numrow, number_format($rk->anggaran_perubahan_2,'2',',','.'));
    $no++; // Tambah 1 setiap kali looping
    $numrow++; // Tambah 1 setiap kali looping
}

// Set orientasi kertas jadi LANDSCAPE
$csv->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

// Set judul file excel nya
// $csv->getActiveSheet(0)->setTitle("Neraca");
// $csv->setActiveSheetIndex(0);

// Proses file excel
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment; filename=Anggaran ".$kd_skpd." ".str_replace('.',' ', str_replace(',', '', $nm_sub_unit)).".csv");
header('Cache-Control: max-age=0');

$write = new PHPExcel_Writer_CSV($csv);
$write->save('php://output');
?>
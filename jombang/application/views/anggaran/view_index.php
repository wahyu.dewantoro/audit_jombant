<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">                
                <div class="row">
                    <div class="col-md-1">
                        <p class="text-right"><strong>Filter</strong></p>
                    </div>
                    <div class="col-md-2">
                        <select class="form-control form-control-sm" id="filter">
                            <option value="all">Semua Anggaran</option>
                            <option value="awal">Anggaran Awal</option>
                            <option value="perubahan">Anggaran Perubahan</option>
                        </select>
                    </div>
                    
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                
                                <th rowspan="2">OPD</th>
                                <th class='awal' colspan="2">Anggaran Awal</th>
                                <th class="perubahan" colspan="2">Anggaran Perubahan</th>
                            </tr>
                            <tr>
                                <th class="awal">Pendapatan</th>
                                <th class="awal">Belanja</th>
                                <th class="perubahan">Pendapatan</th>
                                <th class="perubahan">Belanja</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($anggaran_data as $rk){?>
                                <tr>
                                    
                                    <td class="cell-detail"><?= $rk->nm_unit ?><span class="cell-detail-description"><?= $rk->kd_skpd ?></span></td>
                                    <td class="awal" align="right"><?php if($rk->anggaran_awal_4 > 0){ echo anchor('anggaran/read?nm_unit='.urlencode($rk->nm_unit).'&jenis='.urlencode('Awal').'&akun='.urlencode('4'),number_format($rk->anggaran_awal_4,0,'','.')) ;}else{echo "-";} ?></td>
                                    <td class="awal" align="right"><?php if($rk->anggaran_awal_5 > 0){ echo anchor('anggaran/read?nm_unit='.urlencode($rk->nm_unit).'&jenis='.urlencode('Awal').'&akun='.urlencode('5'),number_format($rk->anggaran_awal_5,0,'','.')) ;}else{echo "-";} ?></td>
                                    <td class="perubahan" align="right"><?php if($rk->anggaran_perubahan_1_4 > 0){ echo anchor('anggaran/read?nm_unit='.urlencode($rk->nm_unit).'&jenis='.urlencode('Perubahan').'&akun='.urlencode('4'), number_format($rk->anggaran_perubahan_1_4,0,'','.')) ; }else{echo "-";} ?></td>
                                    <td class="perubahan" align="right"><?php if($rk->anggaran_perubahan_1_5 > 0){ echo anchor('anggaran/read?nm_unit='.urlencode($rk->nm_unit).'&jenis='.urlencode('Perubahan').'&akun='.urlencode('5') ,number_format($rk->anggaran_perubahan_1_5,0,'','.')) ; }else{echo "-";} ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                 </div>   
            </div>
        </div><!-- end card-->
    </div>
</div>
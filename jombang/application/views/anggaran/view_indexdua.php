<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            	<div class="row">
            		<div class="col-md-4">
            			<table>
            				<tr>
            					<td><strong>OPD</strong></td>
            					<td>: <?= $nm_unit?></td>
            				</tr>
            				<tr>
            					<td><strong>Anggaran </strong></td>
            					<td>:  <?= $var ?> (<?= $jenis ?>)</td>
            				</tr>
            				
            			</table>
            		</div>
            		
            	</div>
            	<div class="row">
            		<div class="col-md-12">
            			<div class="table-responsive">
            				<table class="table table-striped table-bordered">
            					<thead>
            						<tr>
                                                      <th colspan="3">Akun</th>
                                                      <th colspan="3">Rekening</th>
                                                      <th>Nilai</th>
                                                  </tr>
            					</thead>
            					<tbody>
            						<?php 
                                                $nilai=0;
                                                foreach($anggaran_data as $rk){?>
            							<tr>
            								<td colspan="3"><strong><?= $rk->akun_akrual_2 ?></strong></td>
            								<td colspan="3"><strong><?= $rk->nm_akrual_2 ?></strong></td>
            								<td align="right"><strong><?php if($jenis=='awal'){echo number_format($rk->nilai,0,'','.');}else{echo number_format($rk->nilaisatu,0,'','.');}?></strong></td>
            							</tr>
            							<!-- GetLeveltiga -->
            							<?php 
            							$tiga=$this->Manggaran->GetLeveltiga($nm_unit,$rk->akun_akrual_2);
            							foreach($tiga as $rn){?>
            								<tr style="color: green;">
            									<td></td>
            									<td colspan="2"><?= $rn->akun_akrual_3?></td>
            									<td></td>
            									<td colspan="2"><?= $rn->nm_akrual_3?></td>
            									<td align="right"><?php if($jenis=='awal'){echo number_format($rn->nilai,0,'','.');}else{echo number_format($rn->nilaisatu,0,'','.');}?></td>	
            								</tr>
            							<?php
            							$lima=$this->Manggaran->GetLevelempat($nm_unit,$rn->akun_akrual_3);
            							foreach($lima as $rm){ 	?>
            								<tr style="color: red;">
            									<td></td>
            									<td></td>
            									<td><i><?= $rm->akun_akrual_4?></i></td>
            									<td></td>
            									<td></td>
            									<td><i><?= $rm->nm_akrual_4?></i></td>
            									<td align="right"><i><?php if($jenis=='awal'){echo number_format($rm->nilai,0,'','.'); $nilai+=$rm->nilai;}else{echo number_format($rm->nilaisatu,0,'','.'); $nilai+=$rm->nilaisatu;}?></i></td>	
            								</tr>
            						<?php } } } ?>
            					</tbody>
                                          <tfoot>
                                                <th colspan="6">Jumlah</th>
                                                <td align="right"><strong><?= number_format($nilai,'0','','.');?></strong></td>
                                          </tfoot>
            				</table>
						</div>		
            		</div>
            	</div>
            </div>
        </div><!-- end card-->
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
                
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th rowspan="2">OPD</th>
                                <th colspan="3">Pendapatan</th>
                                <th colspan="3">Belanja</th>
                            </tr>
                            <tr>
                                
                                <th>Anggaran</th>
                                <th>Realisasi</th>
                                <th width="80px">%</th>
                                <th>Anggaran</th>
                                <th>Realisasi</th>
                                <th width="80px">%</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($anggaran_data as $rk){?>
                            <tr>
                                <td class="cell-detail"><?= $rk->Nm_Unit ?><span class="cell-detail-description"><?= $rk->kd_skpd ?></span></td>
                                <td align="right"><?php if($rk->anggaran_4 > 0){ echo anchor('anggaran/readrealisasi?nm_unit='.urlencode($rk->Nm_Unit).'&jenis='.$rk->jenis_4.'&akun=4',number_format($rk->anggaran_4,'0','','.')); } else{ echo "-";}  ?></td>
                                <td align="right"><?php if($rk->realisasi_4 > 0){ echo anchor('anggaran/readrealisasi?nm_unit='.urlencode($rk->Nm_Unit).'&jenis='.$rk->jenis_4.'&akun=4',number_format($rk->realisasi_4,'0','','.')); } else{ echo "-";}  ?></td>
                                <td align="center"><?php if($rk->anggaran_4 > 0){  echo number_format(($rk->realisasi_4/$rk->anggaran_4)*100,'2',',','.'); }else{echo "-";}?> </td>
                                <td align="right"><?php if($rk->anggaran_5 > 0){ echo anchor('anggaran/readrealisasi?nm_unit='.urlencode($rk->Nm_Unit).'&jenis='.$rk->jenis_5.'&akun=5',number_format($rk->anggaran_5,'0','','.')); } else{ echo "-";}  ?></td>
                                <td align="right"><?php if($rk->realisasi_5 >0 ){ echo anchor('anggaran/readrealisasi?nm_unit='.urlencode($rk->Nm_Unit).'&jenis='.$rk->jenis_5.'&akun=5',number_format($rk->realisasi_5,'0','','.')); }else{ echo '-';} ?></td>
                                <td align="center"><?php if($rk->anggaran_5 > 0){  echo number_format(($rk->realisasi_5/$rk->anggaran_5)*100,'2',',','.'); }else{echo "-";}?> </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>    
				</div>
					
                    
            </div>
        </div><!-- end card-->
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <form method="get" action="<?= base_url() . 'realisasi' ?>">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <select class="form-control form-control-sm select2" data-placeholder="Pilih OPD" name="opd" required>
                                        <option value=""> </option>
                                        <?php foreach ($opd as $opd) { ?>
                                            <option <?php if ($kdopd == $opd->Kd_SKPD) {
                                                            echo 'selected';
                                                        } ?> value="<?= $opd->Kd_SKPD ?>"><?= $opd->Nm_Sub_Unit ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="submit">Tampilkan</button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                    <?php if ($kdopd <> '') { ?>
                        <div class="col-12">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>No Urut</th>
                                        <th>Uraian</th>
                                        <th>Anggaran</th>
                                        <th>Realisasi</th>
                                        <th>(%)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $da = 0;
                                        $db = 0;
                                        foreach ($lra as $rka) { ?>
                                        <tr>
                                            <td><?php echo $rka->Akun_Akrual_3 ?></td>
                                            <td><?php echo $rka->Nm_Akrual_3 ?></td>
                                            <td align="right"><?= number_format($rka->anggaran, 0, '', '.') ?></td>
                                            <td align="right"><?= number_format($rka->realisasi, 0, '', '.') ?></td>
                                            <td align="center"><?php
                                                                        echo number_format((100 - ($rka->selisih  / $rka->anggaran * 100)), 2, ',', '.');
                                                                        ?> %</td>
                                        </tr>
                                    <?php $da += $rka->anggaran;
                                            $db += $rka->realisasi;
                                        } ?>
                                    <tr>

                                        <td align="center" colspan='2'>Surplus / Defisit</td>
                                        <td align="right"><?= number_format($da, 0, '', '.') ?></td>
                                        <td align="right"><?= number_format($db, 0, '', '.') ?></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr>

                        </div>
                    <?php } ?>

                </div>
            </div>
        </div><!-- end card-->
    </div>
</div>
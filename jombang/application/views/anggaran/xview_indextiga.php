<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            	<div class="row">
            		<div class="col-md-4">
            			<table>
            				<tr>
            					<td>SKPD</td>
            					<td>: <strong><?= $kd_skpd?></strong></td>
            				</tr>
            				<tr>
            					<td>Sub Unit</td>
            					<td>: <strong><?= $nm_sub_unit ?></strong></td>
            				</tr>
            				<tr>
            					<td>Rekening</td>
            					<td>: <strong><?= $akun_akrual_3.' - '.$nm_akrual_3 ?></strong></td>
            				</tr>
            			</table>
            		</div>
            		<div class="col-md-8">

		                    <form action="<?php echo $action ?>" method="get">
		                    	<div class="row">
		                    		<div class="col-md-6">

		                    		</div>
		                    		<div class="col-md-6">
		                    			<div class="form-group">
		                    				<label><strong>Pencarian</strong></label>
		                    				<div class="input-group">
						                    	<input type='hidden' name='kd_skpd' value='<?= $kd_skpd ?>' >
						                    	<input type='hidden' name='nm_sub_unit' value='<?= $nm_sub_unit ?>' >
						                    	<input type='hidden' name='akun_akrual_3' value='<?= $akun_akrual_3 ?>' >
						                    	<input type='hidden' name='nm_akrual_3' value='<?= $nm_akrual_3 ?>' >
						                        <input type="text" class="form-control form-control-xs" name="q" value="<?php echo $q; ?>" placeholder="Pencarian">
						                            <span class="input-group-btn">
						                            <div class="btn-group">

						                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
						                                <?php if ($q <> '' || $akun_akrual_5<>'')  { ?>
						                                    <a href="<?php echo $action ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
						                              <?php }


						                                 ?>
														 <!-- <?= anchor($cetak,'<i class="mdi mdi-print"></i> Cetak','class="btn btn-success"')?> -->
						                            </div>
						                        </span>
						                    </div>
						                </div>
		                    		</div>
		                    	</div>

		                </form>

            		</div>
            	</div>

                <div class="table-responsive">
                    <table class="table table-hover table-striped table-bordered" id="table2">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
                                <th>Kode Rekening</th>
                                <th>Program</th>
                                <th>Kegiatan</th>
								<th>Anggaran Awal</th>
								<th>Anggaran Perubahan 1</th>
								<th>Anggaran Perubahan 2</th>

                            </tr>
                        </thead>
                        <tbody>
							<?php
							$a=0;
							$b=0;
							$c=0;
							foreach ($anggaran_data as $rk)  {
								$a+=$rk->anggaran_awal;
								$b+=$rk->anggaran_perubahan_1;
								$c+=$rk->anggaran_perubahan_2;

								?>
                            <tr>
								<td  align="center"><?php echo ++$start ?></td>
								<td class="cell-detail"><span><?php echo $rk->akun_akrual_5 ?></span>
										<span class="cell-detail-description"><?php echo $rk->nm_akrual_5 ?></span>
								</td>
								<td><?= $rk->ket_program ?></td>
								<td><?= $rk->ket_kegiatan ?></td>
								<td align ="right"><?php echo number_format($rk->anggaran_awal,'2',',','.'); ?></td>
								<td align ="right"><?php echo number_format($rk->anggaran_perubahan_1,'2',',','.'); ?></td>
								<td align ="right"><?php echo number_format($rk->anggaran_perubahan_2,'2',',','.'); ?></td>

							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					</div>
					<button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>

					<div class="float-right">
						<?php echo $pagination ?>
					</div>
					<div class="float-right">
                    <div class="btn-group">
                        <button  class="btn  page-link btn-space btn-success" disabled>  Awal : <?php echo number_format($a,'0','','.') ?></button>
                    <button  class="btn  page-link btn-space btn-warning" disabled>  Perubahan 1 : <?php echo number_format($b,'0','','.') ?></button>
                    <button  class="btn  page-link btn-space btn-success" disabled>  Perubahan 2 : <?php echo number_format($c,'0','','.') ?></button>
                    </div>
                    </div>
            </div>
        </div><!-- end card-->
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">                       
        <div class="card card-table">
            <div class="card-heading">
                <div class="tools float-right">
                    <form action="<?php echo site_url('AsignRole/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control form-control-xs" name="q" value="<?php echo $q; ?>" placeholder="Pencarian">
                            <span class="input-group-btn">
                            <div class="btn-group">
                            <?php if ($q <> '')  { ?>
                                    <a href="<?php echo site_url('AsignRole'); ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
                              <?php }   ?>
                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Search</button>
                                <?php 
                                if($akses['create']==1){
                                    echo anchor(site_url('AsignRole/create'),'<i class="mdi mdi-plus"></i> Tambah', 'class="btn btn-success btn-sm"'); 
                                }
                                ?>
                            </div>
                        </span>
                    </div>
                </form>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive ">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>Pengguna</th>
								<th>Role</th>
								<th width="20px "> </th>
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($asignrole_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo ++$start ?></td>
								<td><?php echo $rk->nama ?></td>
								<td><?php echo $rk->nama_role ?></td>
								<td style="text-align:center" >
									<div class="btn-group">
										<?php 
                                        if($akses['update']==1){
                                            echo anchor(site_url('AsignRole/update/'.acak($rk->id_inc)),'<i class="mdi mdi-edit"></i>','class="btn btn-xs btn-success"');    
                                        }
										
										if($akses['delete']==1){
                                            echo anchor(site_url('asignrole/delete/'.acak($rk->id_inc)),'<i class="mdi mdi-delete"></i>','class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'apakah anda yakin? ?\')"'); 
                                        }

										?>
									</div>
								</td>
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					<button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
					<div class="float-right">
						<?php echo $pagination ?>
					</div>
                </div>
            </div>                                                      
        </div><!-- end card-->                  
    </div>
</div>
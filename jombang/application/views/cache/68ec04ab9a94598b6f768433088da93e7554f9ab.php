<?php $__env->startSection('judul'); ?>
<h2 class="page-head-title"> BAST
    <div class="float-right">
        <?php echo anchor('kegiatan', "<i class='mdi mdi-flip-to-back'></i> Kembali", 'class="btn btn-primary"');; ?>

    </div>
</h2>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card card-contrast">
            <div class="card-header card-header-contrast card-header-featured ">
                Form
                <div class="tools">
                </div>
            </div>
            <div class="card-body">
                <form action="<?php echo e(base_url().'kegiatan/updateBast'); ?>" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-2 col-lg-2">
                            <label for="">Tanggal BAST</label>
                            <input type="text" name="tanggal_bast" id="tanggal_bast" value="<?= $tanggal_bast?>"
                                class="required form-control form-control-sm datepicker">
                        </div>
                        <div class="col-4 col-lg-4">
                            <label for="">
                                <i class="mdi mdi-attachment-alt"></i> (.pdf/.zip/.rar/.jpg)
                                <button id='addButton' type="button" class="btn btn-xs btn-success"><i
                                        class="mdi mdi-plus"></i></button>
                                <button id='removeButton' type="button" class="btn btn-xs btn-danger"><i
                                        class="mdi mdi-delete"></i></button>
                            </label>
                            <div id='TextBoxesGroup'>
                                <div class="col-md-10" id="TextBoxDiv1">
                                    <input type="file" class="form-control form-control-sm " name="file_progres[]" id="file_progres">
                                </div>
                            </div>
                        <input type="hidden" name="id" value="<?= acak($id)?>">
                        </div>
                        <div class="col-5 col-lg-5">
                            <?php $__currentLoopData = $file; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <a target="_blank" href="<?php echo e(base_url().$file->nama_file); ?>"><i class="mdi mdi-attachment-alt"></i> <?php echo e($file->nama_ori); ?></a> <span data-id="<?php echo e($file->id); ?>" class="hapusdok"><i class="mdi mdi-delete"></i></span> <br>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <div class="col-12">
                            <div class="float-right">
                                <button class="btn btn-sm btn-success"><i class="mdi mdi-cloud-done"></i>
                                    Simpan</button>
                            </div>
                        </div>

                    </div>
                </form>

            </div>
        </div><!-- end card-->
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script type="text/javascript">
    var counter = 2;
    $("#addButton").click(function() {
      if (counter > 10) {
        alert("Only 10 textboxes allow");
        return false;
      }
      var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv' + counter).attr("class", 'col-md-10');
      newTextBoxDiv.after().html('<input type="file" class="form-control" name="file_progres[]" id="file_progres">');
      newTextBoxDiv.appendTo("#TextBoxesGroup");

      counter++;
    });


    $("#removeButton").click(function() {
      if (counter == 2) {
        alert("No more textbox to remove");
        return false;
      }
      counter--;

      $("#TextBoxDiv" + counter).remove();
    });

    $(function() {
		$(document).on('click', '.hapusdok', function(e) {
			e.preventDefault();
			if($('.hapusdok').length==1){
				alert('Tidak dapat Menghapus, Dokumen Pendukung Harus ada');
			} else{

			if (confirm('Apakah anda akan menghapus file?')) {

				$.ajax({
					url: "<?= base_url().'kegiatan/hapus_file'?>",
					method: 'GET',
					data: {
						id: $(this).attr('data-id')
					},
					success: function(data) {
						// alert(data);
						location.reload(true);
					}
				});
			}
			}
		});
	});
  </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp7\htdocs\audit_jombant\jombang\application\views/kegiatan/bast.blade.php ENDPATH**/ ?>
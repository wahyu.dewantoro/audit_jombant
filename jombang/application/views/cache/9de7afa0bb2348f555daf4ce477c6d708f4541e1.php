<?php $__env->startSection('judul'); ?>
<h2 class="page-head-title"> Data Kegiatan
  <div class="float-right">
    <?php echo anchor('kegiatan/cetak/' . acak($id), '<i class="mdi mdi-collection-pdf"></i> Cetak', 'class="btn btn-sm btn-danger"'); ?>

    <?php echo anchor('kegiatan/bast/' . acak($id), '<i class="mdi mdi-collection-text"></i> Bast', 'class="btn btn-sm btn-success"'); ?>

    
    <?php echo anchor('kegiatan','<i class="mdi mdi-flip-to-back"></i> Kembali','class="btn btn-sm btn-primary"'); ?>

  </div>
</h2>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
  <div class="col-12 col-lg-8">
    <div class="card">
      <!-- <div class="card-header">Default Tabs</div> -->
      <div class="tab-container">
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item"><a class="nav-link active" href="#home" data-toggle="tab" role="tab"><i class="mdi mdi-file-text"></i> Kegiatan</a></li>
          <li class="nav-item"><a class="nav-link" href="#messagesa" data-toggle="tab" role="tab"><i class="mdi mdi-receipt"></i> SPK</a></li>
          <li class="nav-item"><a class="nav-link" href="#messagesb" data-toggle="tab" role="tab"><i class="mdi mdi-receipt"></i> Addedum</a></li>
          <li class="nav-item"><a class="nav-link" href="#foto" data-toggle="tab" role="tab"><i class="mdi mdi-collection-image"></i> Foto Pendukung</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="home" role="tabpanel">
            <table class="no-border no-strip skills">
              <tbody class="no-border-x no-border-y">
                <tr>
                  <td class="item">OPD</td>
                  <td class="icon">:</td>
                  <td><?= $kode_skpd ?> <?= $nama_skpd ?></td>
                </tr>
                <tr>
                  <td class="item">Jenis Belanja</td>
                  <td class="icon">:</td>
                  <td><?= $kode_belanja ?> <?= $jenis_belanja ?></td>
                </tr>
                <tr>
                  <td class="item">Kegiatan</td>
                  <td class="icon">:</td>
                  <td><?= $nama_kegiatan ?></td>
                </tr>
                <tr>
                  <td class="item">Sub Kegiatan</td>
                  <td class="icon">:</td>
                  <td><?= $sub_kegiatan ?></td>
                </tr>
                <tr>
                  <td class="item">Nama PPK</td>
                  <td class="icon">:</td>
                  <td><?= $nama_ppk ?></td>
                </tr>
                <tr>

                  <td class="item">Pagu Anggaran</td>
                  <td class="icon">:</td>
                  <td><?= 'Rp.' . angka($pagu_anggaran) ?></td>
                </tr>
                <tr>
                  <td class="item">Harga Perkiraan Sendiri</td>
                  <td class="icon">:</td>
                  <td><?= 'Rp.' . angka($harga_perkiraan) ?></td>
                </tr>
                <tr>
                  <td class="item">pelaksana</td>
                  <td class="icon">:</td>
                  <td><?= $pelaksana ?></td>
                </tr>
                <tr>
                  <td class="item">No SP</td>
                  <td class="icon">:</td>
                  <td><?= $no_spmk ?></td>
                </tr>
                <tr>
                  <td class="item">Tanggal SP</td>
                  <td class="icon">:</td>
                  <td><?= $tanggal_spmk <> '' ? date_indo($tanggal_spmk) : '' ?></td>
                </tr>
                <tr>
                  <td class="item">Tanggal BAST</td>
                  <td class="icon">:</td>
                  <td><?= $tanggal_bast <> '' ? date_indo($tanggal_bast) : '' ?></td>
                </tr>
              </tbody>
            </table>
          </div>

          <div class="tab-pane" id="messagesa" role="tabpanel">
            <table class="no-border no-strip skills">
              <tbody class="no-border-x no-border-y">
                <tr>
                  <td class="item">No Kontrak/No SK</td>
                  <td class="icon">:</td>
                  <td><?= $no_spk ?></td>
                </tr>
                <tr>
                  <td class="item">Tanggal</td>
                  <td class="icon">:</td>
                  <td><?= date_indo($tanggal_spk) ?></td>
                </tr>
                <tr>
                  <td class="item">Mulai Pekerjaan</td>
                  <td class="icon">:</td>
                  <td><?= date_indo($mulai_pekerjaan_spk) ?></td>
                </tr>
                <tr>
                  <td class="item">Selesai Pekerjaan</td>
                  <td class="icon">:</td>
                  <td><?= date_indo($selesai_pekerjaan_spk) ?></td>
                </tr>
                <tr>
                  <td class="item">Nilai</td>
                  <td class="icon">:</td>
                  <td>Rp.<?= angka($nilai_spk) ?></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="tab-pane" id="messagesb" role="tabpanel">
            <table class="no-border no-strip skills">
              <tbody class="no-border-x no-border-y">
                <tr>
                  <td class="item">Nomer</td>
                  <td class="icon">:</td>
                  <td><?= $no_addedum ?></td>
                </tr>
                <tr>
                  <td class="item">Tanggal</td>
                  <td class="icon">:</td>
                  <td><?= $tanggal_addedum <> '' ? date_indo($tanggal_addedum) : '' ?></td>
                </tr>
                <tr>
                  <td class="item">Mulai Pekerjaan</td>
                  <td class="icon">:</td>
                  <td><?= $mulai_pekerjaan_addedum <> '' ? date_indo($mulai_pekerjaan_addedum) : '' ?></td>
                </tr>
                <tr>
                  <td class="item">Nilai</td>
                  <td class="icon">:</td>
                  <td>Rp.<?= angka($nilai_addedum) ?></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="tab-pane" id="foto" role="tabpanel">
              <?php $__currentLoopData = $file; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $file): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

              <a target="_blank" href="<?php echo e(base_url().$file->nama_file); ?>"><i class="mdi mdi-attachment-alt"></i> <?php echo e($file->nama_ori); ?></a> <br>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <hr>

  <div class="col-lg-4 col-12">
    <div class="be-booking-promo be-booking-promo-danger">
      <div class="be-booking-desc">
        <h4 class="be-booking-desc-title">Rekapitulasi</h4>
        <span class="be-booking-desc-details">
          <strong>SPK</strong> : Rp.<?= angka($nilai_spk) ?> <br>
          <strong>Addedum</strong> : Rp.<?= angka($nilai_addedum) ?><br>

        </span>
      </div>
      <div class="be-booking-promo-price">
        <div class="be-booking-promo-amount">
          <span class="currency"><i class="mdi mdi-calendar"></i> </span> <span class="frecuency"><?= date('d') . '/' . medium_bulan(date('m')) . '/' . date('y') ?></span>
        </div>

      </div>
    </div>


  </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp7\htdocs\audit_jombant\jombang\application\views/kegiatan/view_read.blade.php ENDPATH**/ ?>
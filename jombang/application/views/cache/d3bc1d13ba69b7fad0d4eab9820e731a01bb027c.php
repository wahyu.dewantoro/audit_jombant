<?php $__env->startSection('judul'); ?>
<h2 class="page-head-title"> Target Progres Kegiatan
    <div class="float-right">
        <?php echo anchor('kegiatan', "<i class='mdi mdi-flip-to-back'></i> Kembali", 'class="btn btn-primary"');; ?>

    </div>
</h2>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card card-contrast">
            <div class="card-header card-header-contrast card-header-featured ">
                Form
                <div class="tools">
                </div>
            </div>
            <div class="card-body">
                <form action="<?php echo e(base_url().'kegiatan/updatejadwal'); ?>" method="post">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nama Pekerjaan</label>
                                <input type="text" class="form-control" readonly value="<?php echo e($pekerjaan->nama_kegiatan); ?>">
                            </div>
                            <div class="form-group">
                                <label>Mulai</label>
                                <input type="text" class="form-control" readonly value="<?php echo e(date_indo($mulai)); ?>">
                            </div>
                            <div class="form-group">
                                <label>Selesai</label>
                                <input type="text" class="form-control" readonly value="<?php echo e(date_indo($selesai)); ?>">
                                <input type="hidden" name="pekerjaan_sipil_id" value="<?php echo e($pekerjaan->id); ?>">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <?php $__currentLoopData = $lm; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lm): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Progres Minggu Ke <?php echo e($lm->minggu); ?> <br> <?php echo e(gabungTanggal($lm->awal_minggu,$lm->akhir_minggu)); ?></label>
                                        <input type="number" class="form-control" required name="minggu[]" value="<?php echo e($lm->persentase); ?>" placeholder="dalam persen (%)">
                                        <input type="hidden" name="idm[]" value="<?php echo e($lm->id); ?>">
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="float-right">
                                <button class="btn btn-sm btn-success"><i class="mdi mdi-cloud-done"></i> Simpan</button>
                            </div>
                        </div>

                    </div>
                </form>

            </div>
        </div><!-- end card-->
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp7\htdocs\audit_jombant\jombang\application\views/kegiatan/jadwal.blade.php ENDPATH**/ ?>
<aside class="page-aside">
    <div class="be-scroller">
      <div class="aside-content">
        <div class="content">
          <div class="aside-header">
            <button class="navbar-toggle" data-target=".aside-nav" data-toggle="collapse" type="button"><span class="icon mdi mdi-caret-down"></span></button>
            <!-- <span class="title">Pekerjaan</span> -->
            <!-- <p class="description">Service description</p> -->


          </div>
          <div class="aside-body">
            <strong>Data Pekerjaan</strong>  
             <table class="table">
                  <tbody class="">
                    <tr>
                        <td >OPD</td>
                        <td ><i class="mdi mdi-chevron-right"></i></td>
                        <td><?= $kode_skpd ?> <?= $nama_skpd ?></td>
                    </tr>
                    <tr>
                        
                        <td >Jenis Belanja</td>
                        <td ><i class="mdi mdi-chevron-right"></i></td>
                        <td><?= $kode_belanja ?> <?= $jenis_belanja ?></td>
                    </tr>
                    <tr>
                        
                        <td >Kegiatan</td>
                        <td ><i class="mdi mdi-chevron-right"></i></td>
                        <td><?= $nama_kegiatan ?></td>
                    </tr>
                    <tr>
                        
                        <td >Nama PPK</td>
                        <td ><i class="mdi mdi-chevron-right"></i></td>
                        <td><?= $nama_ppk ?></td>
                    </tr>
                    <tr>
                        
                        <td >Pagu Anggaran</td>
                        <td ><i class="mdi mdi-chevron-right"></i></td>
                        <td><?= 'Rp.'.angka($pagu_anggaran) ?></td>
                    </tr>
                    <tr>
                        <td >Harga Perkiraan Sendiri</td>
                        <td ><i class="mdi mdi-chevron-right"></i></td>
                        <td><?= 'Rp.'.angka($harga_perkiraan) ?></td>
                    </tr>
                    <tr>
                        <td >pelaksana</td>
                        <td ><i class="mdi mdi-chevron-right"></i></td>
                        <td><?= $pelaksana ?></td>
                    </tr>
                    <tr>
                        <td >No SPMK</td>
                        <td ><i class="mdi mdi-chevron-right"></i></td>
                        <td><?= $no_spmk ?></td>
                    </tr>
                    <tr>
                        <td >Tanggal SPMK</td>
                        <td ><i class="mdi mdi-chevron-right"></i></td>
                        <td><?= $tanggal_spmk<>''?date_indo($tanggal_spmk):'' ?></td>
                    </tr>
                    <tr>
                        <td >Tanggal BAST</td>
                        <td ><i class="mdi mdi-chevron-right"></i></td>
                        <td><?= $tanggal_bast<>''?date_indo($tanggal_bast):'' ?></td>
                    </tr>
                  </tbody>
              </table>
              <strong>Konsultan</strong>
               <table class="table">
                  <tbody >
                    <tr>
                        <td >Nama</td>
                        <td ><i class="mdi mdi-chevron-right"></i></td>
                        <td><?= $konsultan ?></td>
                    </tr>
                    <tr>
                        
                        <td >Nomer SK</td>
                        <td ><i class="mdi mdi-chevron-right"></i></td>
                        <td><?= $no_sk_konsultan ?></td>
                    </tr>
                    <tr>
                        
                        <td >Nilai</td>
                        <td ><i class="mdi mdi-chevron-right"></i></td>
                        <td>Rp.<?= angka($nilai_konsultan) ?></td>
                    </tr>
                    
                  </tbody>
                </table>
              <strong>Pengawas</strong>
               <table class="table">
                  <tbody>
                    <tr>
                        <td >Nama</td>
                        <td ><i class="mdi mdi-chevron-right"></i></td>
                        <td><?= $pengawas ?></td>
                    </tr>
                    <tr>
                        <td >Nomer SK</td>
                        <td ><i class="mdi mdi-chevron-right"></i></td>
                        <td><?= $no_sk_pengawas ?></td>
                    </tr>
                    <tr>
                        <td >Nilai</td>
                        <td ><i class="mdi mdi-chevron-right"></i></td>
                        <td>Rp.<?= angka($nilai_pengawas) ?></td>
                    </tr>
                    
                  </tbody>
                </table>
              <strong>SPK</strong>
              <table class="table">
                <tbody>
                  <tr>
                      <td >Nomer</td>
                      <td ><i class="mdi mdi-chevron-right"></i></td>
                      <td><?= $no_spk ?></td>
                  </tr>
                    <tr>
                      <td >Tanggal</td>
                      <td ><i class="mdi mdi-chevron-right"></i></td>
                      <td><?= date_indo($tanggal_spk) ?></td>
                  </tr>
                    <tr>
                      <td >Mulai Pekerjaan</td>
                      <td ><i class="mdi mdi-chevron-right"></i></td>
                      <td><?= date_indo($mulai_pekerjaan_spk) ?></td>
                  </tr>
                  <tr>
                      <td >Nilai</td>
                      <td ><i class="mdi mdi-chevron-right"></i></td>
                      <td>Rp.<?= angka($nilai_spk) ?></td>
                  </tr>
                </tbody>
              </table>
              <strong>Addedum</strong>
               <table class="table">
                          <tbody class="no-border-x no-border-y">
                            <tr>
                                <td class="item">Nomer</td>
                                <td class="icon"><i class="mdi mdi-chevron-right"></i></td>
                                <td><?= $no_addedum ?></td>
                            </tr>
                              <tr>
                                <td class="item">Tanggal</td>
                                <td class="icon"><i class="mdi mdi-chevron-right"></i></td>
                                <td><?= $tanggal_addedum<>''? date_indo($tanggal_addedum):'' ?></td>
                            </tr>
                              <tr>
                                <td class="item">Mulai Pekerjaan</td>
                                <td class="icon"><i class="mdi mdi-chevron-right"></i></td>
                                <td><?= $mulai_pekerjaan_addedum<>''? date_indo($mulai_pekerjaan_addedum):'' ?></td>
                            </tr>
                            <tr>
                                <td class="item">Nilai</td>
                                <td class="icon"><i class="mdi mdi-chevron-right"></i></td>
                                <td>Rp.<?= angka($nilai_addedum) ?></td>
                            </tr>
                          </tbody>
                        </table>
          </div>
        </div>
        
      </div>
    </div>
  </aside>
  <div class="main-content container-fluid">
        <form method="post" action="<?= base_url().'progressipil/progres_action' ?>" enctype="multipart/form-data">
            <input type="hidden" name="pekerjaan_sipil_id" value="<?= $pekerjaan_sipil_id ?>">
          <div class="email-head">
            <div class="email-head-title"> Entri progres kegiatan<span class="icon mdi mdi-edit"></span></div>
          </div>
          <div class="email-compose-fields">
            <div class="subject">
              <div class="form-group row pt-2">
                <label class="col-md-2 control-label">Tanggal <sup>*</sup></label>
                <div class="col-md-10">
                  <input class="form-control datepicker" type="text" name="tanggal_progres" id="tanggal_progres" value="<?= $tanggal_progres ?>">
                  <?= form_error('tanggal_progres'); ?>
                </div>
              </div>
            </div>
            <div class="subject">
              <div class="form-group row pt-2">
                <label class="col-md-2 control-label">Nilai Progres <sup>*</sup></label>
                <div class="col-md-10">
                  <input class="form-control" type="text" name="nominal_progres" id="nominal_progres" onkeyup="formatangka(this)" value="<?= $nominal_progres ?>">
                  <?= form_error('nominal_progres'); ?>
                </div>
              </div>
            </div>
            <div class="subject">
              <div class="form-group row pt-2">
                <label class="col-md-2 control-label">
                  <i class="mdi mdi-attachment-alt"></i> (.pdf/.zip/.rar/.jpg)<br>
                  <button id='addButton' type="button" class="btn btn-xs btn-success"><i class="mdi mdi-plus"></i></button>
                  <button id='removeButton' type="button" class="btn btn-xs btn-danger"><i class="mdi mdi-delete"></i></button>

                </label>
                <div id='TextBoxesGroup'>
                  <div class="col-md-10" id="TextBoxDiv1">
                    <input type="file" class="form-control" name="file_progres[]" id="file_progres">
                  </div>
                </div>
              </div>
            </div>
            <div class="subject">
              <div class="form-group row pt-2">
                <label class="col-md-2 control-label">Keterangan </label>
                <div class="col-md-10">
                  <textarea id="email-editor" name="deskripsi_progres" placeholder="Deskripsi"><?= $email_editor ?></textarea>      
                </div>
              </div>
            </div>
          </div>
          <div class="subject">
            
            

            <div class="form-group">
              <button class="btn btn-primary btn-space" type="submit"><i class="mdi mdi-cloud-done"></i> Simpan</button>
              <?= anchor('progressipil','<i class="mdi mdi-close"></i> Batal','class="btn btn-warning btn-space"')?>
              
            </div>
          </div>
        </form>
    </div>
<?php /**PATH E:\xampp\htdocs\audit_jbg\jombang\application\views/progres_sipil/view_form_progres.blade.php ENDPATH**/ ?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">

                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>

                                <th>OPD</th>
                                <th class='awal'>Debet</th>
                                <th class="perubahan">Kredit</th>
                            </tr>

                        </thead>
                        <tbody>
                            <?php foreach ($data as $rk) { ?>
                                <tr>

                                    <td class="cell-detail">
                                        <a href="<?= base_url().'transaksientitas/detail/'.$rk->kd_skpd?>"><?= $rk->nm_sub_unit ?></a>
                                        <span class="cell-detail-description"><?= $rk->kd_skpd ?></span>
                                    </td>
                                    <td align="right"><?= number_format($rk->debet, '0', '', '.');  ?></td>
                                    <td align="right"><?= number_format($rk->kredit, '0', '', '.');  ?></td>

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- end card-->
    </div>
</div>
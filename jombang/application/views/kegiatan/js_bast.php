<script type="text/javascript">
  var counter = 2;
  $("#addButton").click(function() {
    if (counter > 10) {
      alert("Only 10 textboxes allow");
      return false;
    }
    var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv' + counter).attr("class", 'col-md-10');
    newTextBoxDiv.after().html('<input type="file" class="form-control" name="file_progres[]" id="file_progres">');
    newTextBoxDiv.appendTo("#TextBoxesGroup");

    counter++;
  });


  $("#removeButton").click(function() {
    if (counter == 2) {
      alert("No more textbox to remove");
      return false;
    }
    counter--;

    $("#TextBoxDiv" + counter).remove();
  });
</script>
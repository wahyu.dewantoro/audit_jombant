<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <form method="get" action="<?= base_url() . 'keuangan' ?>">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <select class="form-control form-control-sm" name="opd" required>
                                        <option value="">Pilih OPD</option>
                                        <?php foreach ($opd as $opd) { ?>
                                            <option <?php if ($kdopd == $opd->Kd_SKPD) {
                                                            echo 'selected';
                                                        } ?> value="<?= $opd->Kd_SKPD ?>"><?= $opd->Nm_Sub_Unit ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="submit">Go!</button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
                <?php if (!empty($kdopd)) { ?>
                    <div class="row">
                        <div class="col-md-12">
                            <b>A. ANALISIS VERTIKAL</b><br>
                            <b>1. Analisis vertikal dalam LRA </b>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <p>SiLPA tahun berjalan harus sama dengan total pendapatan dikurangi total belanja dan transfer ditambah total penerimaan pembiayaan dikurangi dengan total pengeluaran </p>
                                        </td>
                                        <td>
                                            <p>SiLPA = Total Pendapatan – Total Belanja dan Transfer + Total Penerimaan Pembiayaan – Total Pengeluaran Pembiayaan</p>
                                        </td>

                                    </tr>
                                </tbody>
                            </table>

                            <table width="100%" class="table table-bordered">
                                <tr>
                                    <td wdth="48%">SILPA LRA Tahun Berjalan</td>
                                    <td width="1%">:</td>
                                    <td align="right"><?php
                                                            $vlra = $lra[5]['Realisasi'];
                                                            echo number_format($vlra, 0, '', '.');
                                                            ?></td>
                                </tr>
                                <tr>
                                    <td>TOTAL PENDAPATAN</td>
                                    <td width="1%">:</td>
                                    <td align="right"><?= number_format($pendapatanlra, 0, '', '.') ?></td>
                                </tr>
                                <tr>
                                    <td>TOTAL BELANJA DAN TRANSFER</td>
                                    <td width="1%">:</td>
                                    <td align="right"><?= number_format($belanjalra, 0, '', '.') ?></td>
                                </tr>
                                <tr>
                                    <td>PENERIMAAN PEMBIAYAAN</td>
                                    <td width="1%">:</td>
                                    <td align="right"><?= number_format($penerimaanBiayaLra, 0, '', '.') ?></td>
                                </tr>
                                <tr>
                                    <td>PENGELUARAN PEMBIAYAAN</td>
                                    <td width="1%">:</td>
                                    <td align="right"><?= number_format($pengeluaranBiayaLra, 0, '', '.') ?></td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right"><?= number_format(($pendapatanlra - $belanjalra) - ($pendapatanlra - $belanjalra) + ($penerimaanBiayaLra - $pengeluaranBiayaLra), 0, '', '.') ?></td>
                                </tr>
                            </table>
                            <b>2. Analisis vertikal dalam Neraca</b>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Aset harus sama dengan total kewajiban ditambah dengan total ekuitas.
                                        </td>
                                        <td>
                                            Aset = Kewajiban + Ekuitas
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">Aset</td>
                                    <td width="2%">:</td>
                                    <td align="right"><?php
                                                            // aset neraca
                                                            $asetneraca = 0;
                                                            for ($in = 0; $in < 22; $in++) {
                                                                $asetneraca += $neraca[$in]['SaldoX1'];
                                                            }
                                                            echo number_format($asetneraca, 0, '', '.');
                                                            ?></td>
                                </tr>
                                <tr>
                                    <td>Kewajiban</td>
                                    <td width="1%">:</td>
                                    <td align="right"><?php
                                                            $kewajibanneraca = 0;
                                                            for ($ina = 22; $ina < 31; $ina++) {
                                                                $kewajibanneraca += $neraca[$ina]['SaldoX1'];
                                                            }
                                                            echo number_format($kewajibanneraca, 0, '', '.');

                                                            ?></td>
                                </tr>
                                <tr>
                                    <td>Ekuitas</td>
                                    <td width="1%">:</td>
                                    <td align="right"><?php
                                                            $ekuitasneraca = 0;
                                                            for ($inb = 31; $inb < 32; $inb++) {
                                                                $ekuitasneraca += $neraca[$inb]['SaldoX1'];
                                                            }
                                                            echo number_format($ekuitasneraca, 0, '', '.');

                                                            //  number_format($ekuitasneraca, 0, '', '.') 
                                                            ?></td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right"><?= number_format($asetneraca - ($kewajibanneraca + $ekuitasneraca), 0, '', '.') ?></td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Kas di Bendahara Pengeluaran harus sama dengan sisa uang persediaan yang belum disetor ke kasda ditambah dengan utang pfk di bendahara pengeluaran yang belum disetor ke kas negara.
                                        </td>
                                        <td>
                                            Kas di Bendahara Pengeluaran = sisa uang persediaan yang belum disetor + utang pfk di bendahara pengeluaran
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">Kas di Bendahara Pengeluaran</td>
                                    <td width="2%">:</td>
                                    <td align="right">
                                        <?php
                                            $nk = $neracasap[2]['SaldoX1'];
                                            echo number_format($nk, 0, '', '.');
                                            ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Sisa Uang Persediaan belum Setor</td>
                                    <td width="1%">:</td>
                                    <td align="right">
                                        <?php
                                            $nkb = $neracasap[2]['SaldoX0'];
                                            echo number_format($nkb, 0, '', '.');
                                            ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>utang PFK di Bendahara Pengeluaran</td>
                                    <td width="1%">:</td>
                                    <td align="right">
                                        <?php
                                            $nkc = $neraca[22]['SaldoX0'];
                                            echo number_format($nkc, 0, '', '.');
                                            ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">
                                        <?php
                                            echo number_format($nk - ($nkb + $nkc), 0, '', '.');
                                            ?>
                                    </td>
                                </tr>
                            </table>
                            <b>3. Analisis vertikal dalam LAK</b>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Arus kas bersih dari aktivitas operasi harus sama dengan arus masuk kas dari aktivitas operasi dikurangi arus keluar kas dari aktivitas operasi.
                                        </td>
                                        <td>
                                            Arus Kas Bersih dari Aktivitas Operasi = Arus Masuk Kas dari Aktivitas Operasi -Arus Keluar Kas dari Aktivitas Operasi
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">ARUS KAS BERSIH AKTIVITAS OPERASI</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>ARUS KAS MASUK AKTIVITAS OPERASI</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>ARUS KAS KELUAR AKTIVITAS OPERASI</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>

                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Arus Kas Bersih dari Aktivitas Pendanaan harus sama dengan Arus Masuk Kas dari Aktivitas Pendanaan dikurangi arus keluar kas dari aktivitas pendanaan
                                        </td>
                                        <td>
                                            Arus Kas Bersih dari Aktivitas Pendanaan = Arus Masuk Kas dari Aktivitas Pendanaan - Arus Keluar Kas dari Aktivitas Pendanaan
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">ARUS KAS BERSIH AKTIVITAS PENDANAAN</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>ARUS KAS MASUK AKTIVITAS PENDANAAN</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>ARUS KAS KELUAR AKTIVITAS PENDANAAN</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Arus Kas Bersih dari Aktivitas Transitoris harus sama dengan Arus Masuk Kas dari Aktivitas Transitoris ditambah arus keluar kas dari aktivitas transitoris
                                        </td>
                                        <td>
                                            Arus Kas Bersih dari Aktivitas Transitoris = Arus Masuk Kas dari Aktivitas Transitoris + Arus Keluar Kas dari Aktivitas Transitoris
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">ARUS KAS BERSIH AKTIVITAS TRANSITORIS</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>ARUS KAS MASUK AKTIVITAS TRANSITORIS</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>ARUS KAS KELUAR AKTIVITAS TRANSITORIS</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>

                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Kenaikan/Penurunan Kas harus sama dengan Arus Kas Bersih dari Aktivitas Operasi ditambah arus kas bersih dari aktivitas investasi ditambah arus kas bersih dari aktivitas pendanaan ditambah arus kas bersih dari aktivitas transitoris.
                                        </td>
                                        <td>
                                            Kenaikan/Penurunan Kas = Arus Kas Bersih dari Aktivitas Operasi + Arus Kas Bersih dari Aktivitas Investasi + Arus Kas Bersih dari Aktivitas Pendanaan + Arus Kas Bersih dari Aktivitas Transitoris
                                        </td>

                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">KENAIKAN/PENURUNAN KAS</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>ARUS KAS BERSIH AKTIVITAS OPERASI</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>ARUS KAS BERSIH AKTIVITAS INVESTASI </td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>ARUS KAS BERSIH AKTIVITAS PENDANAAN </td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>ARUS KAS BERSIH AKTIVITAS TRANSITORIS</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>

                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Saldo Akhir Kas di Bud harus sama dengan saldo awal kas di bud ditambah kenaikan/ penurunan Kas
                                        </td>
                                        <td>
                                            Saldo Akhir Kas di Bud = saldo awal kas di bud + kenaikan/penurunan Kas
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">SALDO AKHIR KAS BUD</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>SALDO AWAL KAS BUD</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>KENAIKAN/PENURUNAN KAS</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Saldo Akhir Kas harus sama dengan Saldo Akhir di Bud ditambah saldo akhir kas di bendahara pengeluaran ditambah saldo akhir kas di bendahara penerimaan + saldo akhir kas di Blud
                                        </td>
                                        <td>
                                            Saldo Akhir Kas = Saldo Akhir di Bud + saldo akhir kas di bendahara pengeluaran + saldoakhir kas di bendahara penerimaan + saldo akhir kas di Blud
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">SALDO AKHIR KAS</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>SALDO AKHIR KAS di Bud</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>SALDO AKHIR KAS di Bendahara Pengeluaran</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>SALDO AKHIR KAS di Bendahara Penerimaan</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>SALDO AKHIR KAS di Blud</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>SALDO AKHIR KAS di Bendahara Dana bos</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <b>4. Analisis vertikal dalam Laporan Operasional (LO)</b>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Surplus/Defisit LO harus sama dengan total Pendapatan (LO) dikurangi total beban (lo) ditambah (dikurangi) total surplus (defisit) kegiatan non operasional (lo) ditambah (dikurangi) pos luar biasa (LO)
                                        </td>
                                        <td>
                                            Surplus/Defisit LO= Total Pendapatan (LO) - Total Beban (LO) +/- Total Surplus/Defisit Kegiatan Non Operasional (LO) +/- Pos Luar Biasa (LO)
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">SURPLUS (DEFISIT) LO</td>
                                    <td width="2%">:</td>
                                    <td align="right"><?php $aab = $lo[38]['Realisasi'];
                                                            echo number_format($aab, 0, '', '.') ?></td>
                                </tr>
                                <tr>
                                    <td>TOTAL PENDAPATAN (LO)</td>
                                    <td width="1%">:</td>
                                    <?php
                                        // total pendapatan LO
                                        $tlo = 0;
                                        for ($llo = 0; $llo < 15; $llo++) {
                                            $tlo += $lo[$llo]['Realisasi'];
                                        }
                                        ?>
                                    <td align="right"><?= number_format($tlo, 0, '', '.') ?></td>
                                </tr>
                                <tr>
                                    <td>TOTAL BEBAN (LO)</td>
                                    <td width="1%">:</td>
                                    <?php
                                        // total beban LO
                                        $tblo = 0;
                                        for ($blo = 15; $blo < 34; $blo++) {
                                            $tblo += $lo[$blo]['Realisasi'];
                                        }
                                        ?>
                                    <td align="right"><?= number_format($tblo, 0, '', '.') ?></td>
                                </tr>
                                <tr>
                                    <td>TOTAL SURPLUS (DEFISIT) KEGIATAN NON OPERASIONAL</td>
                                    <td width="1%">:</td>
                                    <td align="right"><?php $aac = $lo[35]['Realisasi'];
                                                            echo number_format($aac, 0, '', '.') ?></td>
                                </tr>
                                <tr>
                                    <td>TOTAL POS LUAR BIASA </td>
                                    <td width="1%">:</td>
                                    <td align="right"><?php $aad = $lo[36]['Realisasi'];
                                                            number_format($aad, 0, '', '.') ?></td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right"><?php $aae = $aab - ($tlo + $tblo + $aac + $aad);
                                                            echo number_format($aae, 0, '', '.'); ?></td>
                                </tr>
                            </table>
                            <b>5. Analisis Vertikal dalam Laporan Perubahan SAL</b>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            SAL Akhir harus sama dengan SAL Awal dikurangi penggunaan sal sebagai penerimaan pembiayaan tahun berkenaan ditambah sisa lebih/kurang pembiayaan anggaran tahun berkenaan ditambah koreksi kurang/lebih kesalahan pembukuan tahun sebelumnya
                                        </td>
                                        <td>
                                            SAL Akhir = SAL Awal – Penggunaan SAL sebagai penerimaan pembiayaan tahun berkenaan + Sisa Lebih/Kurang Pembiayaan Anggaran Tahun Berkenaan + Koreksi Kurang/Lebih Kesalahan Pembukuan Tahun Sebelumnya
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">SAL Akhir </td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>SAL Awal</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>Penggunaan SAL</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>SAL Tahun Berjalan</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>Koreksi Kurang/Lebih Kesalahan Pembukuan Tahun Sebelumnya </td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <b>6. Analisis Vertikal dalam Laporan Perubahan Ekuitas</b>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Ekuitas akhir harus sama dengan ekuitas awal ditambah (dikurangi) surplus/defisit lo ditambah (dikurangi) koreksi berdampak ke ekuitas
                                        </td>
                                        <td>
                                            Ekuitas akhir = ekuitas awal (+/-) surplus/defisit LO (+/-) koreksi berdampak ke ekuitas
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">Ekuitas Akhir</td>
                                    <td width="2%">:</td>
                                    <td align="right">
                                        <?php
                                            $tea = 0;
                                            foreach ($lpe as $rlpe) {
                                                $tea += $rlpe['Realisasi'];
                                            }
                                            echo number_format($tea, 0, '', '.');
                                            ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Ekuitas Awal</td>
                                    <td width="1%">:</td>
                                    <td align="right">
                                        <?php
                                            $teab = $lpe[0]['Realisasi'];
                                            echo number_format($teab, 0, '', '.');
                                            ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Surplus/defisit LO</td>
                                    <td width="1%">:</td>
                                    <td align="right">
                                        <?php
                                            $teac = $lpe[1]['Realisasi'];
                                            echo number_format($teac, 0, '', '.');
                                            ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Koreksi </td>
                                    <td width="1%">:</td>
                                    <td align="right">
                                        <?php
                                            $tead = $lpe[2]['Realisasi'] + $lpe[3]['Realisasi'] + $lpe[4]['Realisasi'];
                                            echo number_format($tead, 0, '', '.');
                                            ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">
                                        <?= number_format(($tea - ($teab + $teac + $tead))) ?>
                                    </td>
                                </tr>
                            </table>
                            <b>b. ANALISIS HORIZONTAL</b> <br>
                            <b>1. Analisis horizontal antara LRA dan Neraca</b>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            SiLPA di Lra harus sama dengan kas di kas daerah ditambah kas di bendahara pengeluaran ditambah kas di blud ditambah setara kas dikurangi dengan utang pfk di neraca.
                                        </td>
                                        <td>
                                            SiLPA (LRA) = Kas di Kas daerah + kas di bendahara pengeluaran + kas di blud + setara kas – utang pfk (neraca)
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">SILPA di Lra</td>
                                    <td width="2%">:</td>
                                    <td align="right">
                                        <?php echo number_format($vlra, 0, '', '.'); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>KAS di Kas daerah</td>
                                    <td width="1%">:</td>
                                    <td align="right">
                                        <?php
                                            $kk = $neraca[0]['SaldoX1'];
                                            echo number_format($kk, 0, '', '.');
                                            ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>KAS di Bendahara Pengeluaran</td>
                                    <td width="1%">:</td>
                                    <td align="right">
                                        <?php
                                            echo number_format($nk, 0, '', '.');
                                            ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>KAS di BLUD </td>
                                    <td width="1%">:</td>
                                    <td align="right">
                                        <?php
                                            $nkaa = $neracasap[3]['SaldoX1'];
                                            echo number_format($nkaa, 0, '', '.');
                                            ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Setara KAS</td>
                                    <td width="1%">:</td>
                                    <td align="right">
                                        <?php
                                            $nkbb = $neracasap[7]['SaldoX1'];
                                            echo number_format($nkbb, 0, '', '.');
                                            ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>KAS di Bendahara Dana BOS</td>
                                    <td width="1%">:</td>
                                    <td align="right">
                                        <?php
                                            $nkcc = $neracasap[5]['SaldoX1'];
                                            echo number_format($nkcc, 0, '', '.');
                                            ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Utang PFK Neraca</td>
                                    <td width="1%">:</td>
                                    <td align="right">
                                        <?php
                                            $nkdd = $neraca[22]['SaldoX0'];
                                            echo number_format($nkdd, 0, '', '.');
                                            ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">
                                        <?= 
                                            number_format( $vlra - ($nkdd+$nkcc+$nkbb+$nkaa+$nk+$kk) ,0,'','.');
                                        ?>
                                    </td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Untuk metode harga perolehan, Pengeluaran Pembiayaan untuk Penyertaan Modal Daerah (LRA) harus tercermin dalam
                                        </td>
                                        <td>
                                            Untuk metode harga perolehan, Pengeluaran pembiayaan untuk Penyertaan Modal Daerah (LRA) = penambahan nilai penyertaan modal pemerintah daerah (Neraca).
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">PENGELUARAN PEMBIAYAAN (PENYERTAAN MODAL)</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>SALDO PENYERTAAN MODAL TAHUN X-1</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>SALDO PENYERTAAN MODAL TAHUN X</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>

                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Penerimaan/Pengeluaran Pembiayaan Pinjaman Jangka Panjang (LRA) = Utang Jangka Panjang + Bagian Lancar Utang Jangka Panjang Tahun berkenaan – Utang Jangka Panjang Tahun sebelumnya - Bagian Lancar Utang Jangka Panjang tahun sebelumnya.
                                        </td>
                                        <td>
                                            Penerimaan/Pengeluaran Pembiayaan Pinjaman Jangka Panjang (LRA) = Utang Jangka Panjang + Bagian Lancar Utang Jangka Panjang Tahun berkenaan – Utang Jangka Panjang Tahun sebelumnya (Neraca)
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">PENERIMAAN/PENGELUARAN PEMBIAYAAN PINJAMAN JK. PANJANG (LRA)</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>UTANG JANGKA PANJANG (PINJAMAN)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>BAG. LANCAR UTANG JANGKA PANJANG THN X</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>UTANG JANGKA PANJANG TAHUN X-1 (PINJAMAN) </td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>BAG. LANCAR UTANG JANGKA PANJANG THN X-1</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>

                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Realisasi belanja modal harus sama dengan penambahan aset tetap (dan aset lainnya), jika selisih harus dijelaskan di Calk
                                        </td>
                                        <td>
                                            Teliti apakah pengungkapan selisih dalam CaLK sudah cukup memadai. Mungkin ada penerimaan hibah berupa aset dan kapitalisasi biaya. Atau ada kesalahan berupa: salah anggaran selain BM ternyata menghasilkan aset atau aset daerah yg baru ditemukan
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">REALISASI BELANJA MODAL TANAH</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>PENAMBAHAN (PENURUNAN)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>- ASET TANAH 2016</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>- ASET TANAH 2015</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <b>2. Analisis horizontal antara LRA dan LAK</b>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Arus Kas Masuk dari Aktivitas Operasi (LAK) harus sama dengan Total Pendapatan Daerah (LRA) dikurangi pendapatan asli daerah lainnya yang berasal dari penjualan aset tetap dan aset lainnya.
                                        </td>
                                        <td>
                                            Arus Kas Masuk Dari Aktivitas Operasi (LAK) = Total Pendapatan Daerah (LRA) – Pendapatan Asli Daerah Lainnya yang Berasal dari Penjualan Aset Tetap dan Aset Lainnya (LRA) *)
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">ARUS KAS MASUK AKTIVITAS OPERASI (LAK)</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>PENDAPATAN DAERAH (LRA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>PAD PENJUALAN ASET TETAP DAN ASET LAINNYA</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>

                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Arus Kas Keluar dari Aktivitas Operasi harus sama dengan Belanja Operasi ditambah belanja tak terduga (di lra) ditambah belanja transfer (di Lra).
                                        </td>
                                        <td>
                                            Arus Kas Keluar Dari Aktivitas Operasi = Belanja Operasi + Belanja Tak Terduga (di Lra) + belanja transfer (di lra) *)
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">ARUS KAS KELUAR AKTIVITAS OPERASI</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>BELANJA OPERASI</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>BELANJA TIDAK TERDUGA (LRA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>

                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Arus Kas Masuk dari Aktivitas Investasi (LAK) harus sama dengan Pendapatan Asli Daerah yang berasal dari penjualan Aset Tetap dan Aset Lainnya (di Lra).
                                        </td>
                                        <td>
                                            Arus Kas Masuk Dari Aktivitas Investasi (LAK) = Pendapatan Asli Daerah Yang Berasal Dari Penjualan Aset Tetap dan Aset Lainnya (di Lra)
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">ARUS KAS MASUK AKTIVITAS INVESTASI (LAK)</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>PAD PENJUALAN ASET TETAP DAN ASET LAINNYA (LRA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Arus Kas Masuk dari aktivitas Pendanaan (LAK) harus sama dengan Penerimaan Pendanaan di Lra (selain penggunaan siLPA).
                                        </td>
                                        <td>
                                            Arus Kas Masuk Dari Aktivitas Pendanaan (LAK) = Penerimaan Pendanaan di Lra (selain penggunaan siLPA)
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">ARUS KAS MASUK AKTIVITAS PENDANAAN (LAK)</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>PENERIMAAN PENDANAAN SELAIN PENGGUNAAN SILPA (LRA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Arus Kas Keluar dari aktivitas Pendanaan (LAK) harus sama dengan Pengeluaran Pendanaan di Lra
                                        </td>
                                        <td>
                                            Arus Kas Keluar Dari Aktivitas Pendanaan (LAK) = Pengeluaran Pendanaan di Lra
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">ARUS KAS KELUAR AKTIVITAS PENDANAAN (LAK)</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>PENGELUARAN PENDANAAN (LRA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <b>3. Analisis horizontal antara Neraca dan LAK</b>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Saldo akhir Kas tahun lalu (LAK) harus sama dengan saldo awal Kas tahun berkenaan (LAK), saldo akhir Kas di neraca tahun lalu, dan saldo awal kas di neraca tahun berjalan.
                                        </td>
                                        <td>
                                            Saldo Akhir Kas Tahun Lalu (LAK) = Saldo awal Kas Tahun Berkenaan (LAK) = Saldo Akhir Kas Tahun lalu (Neraca) = Saldo Awal Kas Tahun Berjalan (Neraca)
                                            Apabila terdapat selisih harus diungkapkan dalam CaLK

                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">SALDO AKHIR KAS BUD TAHUN LALU (LAK)</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>SALDO AWAL KAS BUD TAHUN BERKENAAN (LAK)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>SALDO AKHIR KAS BUD TAHUN LALU (NERACA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>SALDO AWAL KAS BUD TAHUN BERJALAN (NERACA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Saldo akhir kas di neraca tahun berjalan harus sama dengan saldo akhir kas di lak tahun berjalan.
                                        </td>
                                        <td>
                                            Saldo Akhir Kas Tahun Berjalan (Neraca) = Saldo Akhir Kas Tahun Berjalan (LAK)
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">SALDO AKHIR KAS THN BERJALAN (NERACA)</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>SALDO AKHIR KAS THN BERJALAN (LAK)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>

                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Utang PFK di neraca harus sama dengan utang pfk di bud ditambah utang pfk di bendahara pengeluaran.
                                        </td>
                                        <td>
                                            Utang PFK (Neraca) = Utang PFK di Bud + utang pfk pada bendahara pengeluaran
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">UTANG PFK (NERACA)</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>UTANG PFK BUD</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>UTANG PFK BENDAHARA PENGELUARAN</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Saldo utang PFK di neraca tahun berjalan harus sama dengan saldo utang pfk di neraca tahun sebelumnya ditambah penerimaan pfk tahun berjalan dikurangi pengeluaran pfk tahun berjalan di Lak.
                                        </td>
                                        <td>
                                            Saldo Utang PFK tahun berjalan (neraca) = Saldo Utang PFK Tahun sebelumnya (Neraca) + Penerimaan PFK Tahun berjalan – Pengeluaran PFK Tahun Berjalan (LAK)
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">UTANG PFK (NERACA)</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>UTANG PFK TAHUN X-1 (NERACA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>PENERIMAAN PFK (LAK)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>PENGELUARAN PFK (LAK)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>

                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <b>4. Analisis horizontal antara LRA dan Laporan Perubahan SAL</b>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Silpa di Lra harus sama dengan saldo anggaran lebih (sal) akhir pada laporan perubahan SAL
                                        </td>
                                        <td>
                                            SiLPA pada LRA = Saldo Anggaran Lebih (SAL) Akhir pada Laporan Perubahan SAL
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">SILPA (LRA)</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>SAL AKHIR (LAPORAN PERUBAHAN SAL)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Silpa pada LRA Tahun Sebelumnya harus sama dengan Penggunaan Silpa pada Laporan Perubahan SAL harus sama dengan Penerimaan Pembiayaan Silpa pada LRA harus sama dengan Saldo Anggaran Lebih (SAL) Awal pada Laporan Perubahan SAL
                                        </td>
                                        <td>
                                            Silpa pada LRA Tahun Sebelumnya = Penggunaan Silpa pada Laporan Perubahan SAL = Penerimaan Pembiayaan Silpa pada LRA = Saldo Anggaran Lebih (SAL) Awal pada Laporan Perubahan SAL
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">SILPA TAHUN SEBELUMNYA (LRA)</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>PENGGUNAAN SILPA (LAPORAN PERUBAHAN SAL)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>PENERIMAAN PEMBIAYAAN - PENGGUNAAN SILPA (LRA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>SAL AWAL (LAPORAN PERUBAHAN SAL)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <b>5. Analisis horizontal antara LO, Laporan Perubahan Ekuitas dan Neraca</b>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Ekuitas Awal pada Laporan Perubahan Ekuitas harus sama dengan Ekuitas Akhir pada Neraca Tahun Sebelumnya
                                        </td>
                                        <td>
                                            Ekuitas Awal pada Laporan Perubahan Ekuitas = Ekuitas Akhir pada Neraca Tahun Sebelumnya
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">EKUITAS AWAL (LAPORAN PERUBAHAN EKUITAS)</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>EKUITAS AKHIR TAHUN SEBELUMNYA (NERACA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Surplus/Defisit pada Laporan Operasional harus sama dengan Surplus/Defisit pada Laporan Perubahan Ekuitas
                                        </td>
                                        <td>
                                            Surplus/Defisit pada Laporan Operasional = Surplus/Defisit pada Laporan Perubahan Ekuitas
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">SURPLUS/DEFISIT (LAPORAN OPERASIONAL)</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>SURPLUS/DEFISIT (LAPORAN PERUBAHAN EKUITAS)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Ekuitas akhir pada Laporan Perubahan Ekuitas harus sama dengan Ekuitas pada Neraca
                                        </td>
                                        <td>
                                            Ekuitas akhir pada Laporan Perubahan Ekuitas = Ekuitas pada Neraca
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">EKUITAS AKHIR (LAPORAN PERUBAHAN EKUITAS)</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>EKUITAS (NERACA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <b>6. Analisis horizontal antara LO, LRA dan Neraca</b>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Pendapatan Pajak (LO) harus sama dengan Pendapatan Pajak (LRA) dikurangi piutang pajak awal tahun ditambah piutang pajak akhir Tahun
                                        </td>
                                        <td>
                                            Pendapatan Pajak (LO) = Pendapatan Pajak ( LRA) - Piutang Pajak Awal Tahun + Piutang Pajak Akhir Tahun
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">PENDAPATAN PAJAK (LO) </td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>PENDAPATAN PAJAK (LRA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>PIUTANG PAJAK AKHIR TAHUN (NERACA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>PIUTANG PAJAK AWAL TAHUN (NERACA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Pendapatan Retribusi (LO) harus sama dengan Pendapatan Retribusi (LRA) dikurangi piutang retribusi awal tahun ditambah piutang retribusi akhir Tahun
                                        </td>
                                        <td>
                                            Pendapatan Retribusi (LO) = Pendapatan Retribusi (LRA) - Piutang Retribusi Awal Tahun + Piutang Retribusi Akhir Tahun
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">PENDAPATAN RETRIBUSI (LO) </td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>PENDAPATAN RETRIBUSI (LRA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>PENDAPATAN RETRIBUSI (LRA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>PIUTANG RETRIBUSI AWAL TAHUN (NERACA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Pendapatan Bagi Hasil Pajak Provinsi (LO) harus sama dengan Pendapatan Bagi Hasil Pajak Provinsi (LRA) dikurangi piutang bagi hasil pajak provinsi awal tahun ditambah piutang bagi hasil pajak provinsi akhir Tahun
                                        </td>
                                        <td>
                                            Pendapatan Bagi Hasil Pajak Provinsi (LO) = Pendapatan Bagi Hasil Pajak Provinsi (LRA) – Piutang Bagi Hasil Pajak Provinsi Awal Tahun + Piutang Bagi Hasil Pajak Provinsi Akhir Tahun
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">PENDAPATAN BAGI HASIL PAJAK PROVINSI (LO) </td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>PENDAPATAN BAGI HASIL PAJAK PROVINSI (LRA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>PIUTANG BAGI HASIL PAJAK PROVINSI AKHIR TAHUN (NERACA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>PIUTANG BAGI HASIL PAJAK PROVINSI AWAL TAHUN (NERACA)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Beban Persediaan (lo) harus sama dengan belanja barang dan jasa persediaan (lra) ditambah persediaan awal tahun dikurangi persediaan akhir Tahun
                                        </td>
                                        <td>
                                            Beban Persediaan (lo) = Belanja barang dan jasa persediaan (lra) + persediaan awal tahun - persediaan akhir tahun. perhatikan cara penilaian persediaan: fifo atau weighted average
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">BEBAN PERSEdiAAn (lo) </td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>BELANJA BARANG DAN JASA - PERSEdiAAn (lra)</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>PERSEdiAAn awal tAHun</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>PERSEdiAAn akhir TAhun</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                            <table class="table table-bordered" width="100%">
                                <thead>
                                    <tr>
                                        <th width="50%">Uraian</th>
                                        <th>Persamaan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            Beban Penyusutan (LO) harus sama dengan Akumulasi Penyusutan Akhir Tahun dikurangi akumulasi penyusutan awal Tahun
                                        </td>
                                        <td>
                                            Beban Penyusutan (LO) = Akumulasi Penyusutan Akhir Tahun – Akumulasi Penyusutan Awal Tahun
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td wdth="200px">BEBAN PENYUSUTAN (LO)</td>
                                    <td width="2%">:</td>
                                    <td align="right"></td>
                                </tr>
                                <tr>
                                    <td>AKUMULASI PENYUSUTAN AKHIR TAHUN</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                                <tr>
                                    <td>AKUMULASI PENYUSUTAN AWAL TAHUN</td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>

                                <tr>
                                    <td><b>Selisih</b></td>
                                    <td width="1%">:</td>
                                    <td align="right">0</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div><!-- end card-->
    </div>
</div>
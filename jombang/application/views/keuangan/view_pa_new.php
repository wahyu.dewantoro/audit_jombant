<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <form method="get" action="<?= base_url() . 'keuangan' ?>">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <select class="form-control form-control-sm select2" name="opd" required data-placeholder="Pilih OPD">
                                        <option value=""> </option>
                                        <?php foreach ($opd as $opd) { ?>
                                            <option <?php if ($kdopd == $opd->Kd_SKPD) {
                                                            echo 'selected';
                                                        } ?> value="<?= $opd->Kd_SKPD ?>"><?= $opd->Nm_Sub_Unit ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="submit">Tampilkan</button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>

                </div>
            </div>


        </div><!-- end card-->
    </div>
</div>

<?php if ($kdopd <> '') { ?>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3>Analisis Vertikal</h3>
                </div>
                <div class="tab-container">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#icon1" data-toggle="tab" role="tab">
                                <i class="mdi mdi-file-text"></i> <b>LRA </b>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#icon2" data-toggle="tab" role="tab">
                                <i class="mdi mdi-chart-donut"></i> <b>Neraca </b>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#icon3" data-toggle="tab" role="tab">
                                <i class="mdi mdi-refresh-alt"></i> <b>LAK </b>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#icon4" data-toggle="tab" role="tab">
                                <i class="mdi mdi-directions-subway"></i> <b>LO</b>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#icon5" data-toggle="tab" role="tab">
                                <i class="mdi mdi-file-text"></i> <b>SAL</b>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#icon6" data-toggle="tab" role="tab">
                                <i class="mdi mdi-swap"></i> <b>LPE</b>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="icon1" role="tabpanel">
                            <table class="table table-bordered tabled-striped">
                                <thead>
                                    <tr>
                                        <th>Uraian</th>
                                        <th>Nilai</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $az = 0;
                                        foreach ($lravertikal as $lravertikal) { ?>
                                        <tr>
                                            <td><?php echo $lravertikal['ket'] ?></td>
                                            <td align="right"><?php if ($az == 0) {
                                                                            echo $lravertikal['nilai'];
                                                                        } else {
                                                                            echo number_format($lravertikal['nilai']);
                                                                        } ?></td>
                                        </tr>
                                    <?php $az++;
                                        } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="icon2" role="tabpanel">

                            <table class="table table-bordered tabled-striped">
                                <thead>
                                    <tr>
                                        <th width="70%">Uraian</th>
                                        <th>Nilai</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $z = 0;
                                        foreach ($neracavertikal[0] as $neracavertikala) { ?>
                                        <tr>
                                            <td><?php echo $neracavertikala['ket'] ?></td>
                                            <td align="right"><?php if ($z == 0) {
                                                                            echo $neracavertikala['nilai'];
                                                                        } else {
                                                                            echo number_format($neracavertikala['nilai']);
                                                                        } ?></td>
                                        </tr>
                                    <?php $z++;
                                        } ?>
                                </tbody>
                            </table>
                            <hr>


                            <table class="table table-bordered tabled-striped">
                                <thead>
                                    <tr>
                                        <th width="70%">Uraian</th>
                                        <th>Nilai</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $za = 0;
                                        foreach ($neracavertikal[1] as $neracavertikalb) { ?>
                                        <tr>
                                            <td><?php echo $neracavertikalb['ket'] ?></td>
                                            <td align="right"><?php if ($za == 0) {
                                                                            echo $neracavertikalb['nilai'];
                                                                        } else {
                                                                            echo number_format($neracavertikalb['nilai']);
                                                                        } ?></td>
                                        </tr>
                                    <?php $za++;
                                        } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="icon3" role="tabpanel">
                            <?php foreach ($dataLAK as $dataLAKa) { ?>
                                <table class="table table-bordered tabled-striped">
                                    <thead>
                                        <tr>
                                            <th width="70%">Uraian</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $zb = 0;
                                                foreach ($dataLAKa as $dataLAKaa) { ?>
                                            <tr>
                                                <td><?php echo $dataLAKaa['ket'] ?></td>
                                                <td align="right"><?php if ($zb == 0) {
                                                                                    echo $dataLAKaa['nilai'];
                                                                                } else {
                                                                                    echo number_format($dataLAKaa['nilai']);
                                                                                } ?></td>
                                            </tr>
                                        <?php $zb++;
                                                } ?>
                                    </tbody>
                                </table>
                                <br>
                            <?php } ?>
                        </div>
                        <div class="tab-pane" id="icon4" role="tabpanel">
                            <table class="table table-bordered tabled-striped">
                                <thead>
                                    <tr>
                                        <th width="70%">Uraian</th>
                                        <th>Nilai</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $zc = 0;
                                        foreach ($lo[0] as $loa) { ?>
                                        <tr>
                                            <td><?php echo $loa['ket'] ?></td>
                                            <td align="right"><?php if ($zc == 0) {
                                                                            echo $loa['nilai'];
                                                                        } else {
                                                                            echo number_format($loa['nilai']);
                                                                        } ?></td>
                                        </tr>
                                    <?php $zc++;
                                        } ?>
                                </tbody>
                            </table>

                        </div>
                        <div class="tab-pane" id="icon5" role="tabpanel">
                            <?php $zd = 0;
                                foreach ($dataSAL as $dataSALa) { ?>
                                <table class="table table-bordered tabled-striped">
                                    <thead>
                                        <tr>
                                            <th width="70%">Uraian</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $zd = 0;
                                                foreach ($dataSALa as $dataSALaa) { ?>
                                            <tr>
                                                <td><?php echo $dataSALaa['ket'] ?></td>
                                                <td align="right"><?php if ($zd == 0) {
                                                                                    echo $dataSALaa['nilai'];
                                                                                } else {
                                                                                    echo number_format($dataSALaa['nilai']);
                                                                                } ?></td>
                                            </tr>
                                        <?php $zd++;
                                                } ?>
                                    </tbody>
                                </table>
                                <br>
                            <?php } ?>
                        </div>
                        <div class="tab-pane" id="icon6" role="tabpanel">
                            <table class="table table-bordered tabled-striped">
                                <thead>
                                    <tr>
                                        <th width="70%">Uraian</th>
                                        <th>Nilai</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $ze = 0;
                                        foreach ($lpe as $lpea) { ?>
                                        <tr>
                                            <td><?php echo $lpea['ket'] ?></td>
                                            <td align="right"><?php if ($ze == 0) {
                                                                            echo $lpea['nilai'];
                                                                        } else {
                                                                            echo number_format($lpea['nilai']);
                                                                        } ?></td>
                                        </tr>
                                    <?php $ze++;
                                        } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3>Analisis Horizontal</h3>
                </div>
                <div class="tab-container">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#icon1a" data-toggle="tab" role="tab">
                                <i class="mdi mdi-file-text"></i> <b>LRA & Neraca</b>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#icon1b" data-toggle="tab" role="tab">
                                <i class="mdi mdi-file-text"></i> <b>LRA & LAK</b>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#icon1c" data-toggle="tab" role="tab">
                                <i class="mdi mdi-file-text"></i> <b> Neraca & LAK</b>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#icon1d" data-toggle="tab" role="tab">
                                <i class="mdi mdi-file-text"></i> <b> LRA & LP SAL</b>
                            </a>
                        </li>
                        <!-- LO, Laporan Perubahan Ekuitas dan Neraca -->
                        <li class="nav-item">
                            <a class="nav-link " href="#icon1e" data-toggle="tab" role="tab">
                                <i class="mdi mdi-file-text"></i> <b> LO, LPE & Neraca</b>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#icon1f" data-toggle="tab" role="tab">
                                <i class="mdi mdi-file-text"></i> <b> LO, LRA & Neraca</b>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="icon1a" role="tabpanel">
                            <?php foreach ($lrahorizontal as $lrahorizontalb) { ?>
                                <table class="table table-bordered tabled-striped">
                                    <thead>
                                        <tr>
                                            <th width="70%">Uraian</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $sdf = 0;
                                                foreach ($lrahorizontalb as $lraha) { ?>
                                            <tr>
                                                <td><?php echo $lraha['ket'] ?></td>
                                                <td align="right"><?php if ($sdf == 0) {
                                                                                    echo $lraha['nilai'];
                                                                                } else {
                                                                                    echo number_format($lraha['nilai']);
                                                                                } ?></td>
                                            </tr>
                                        <?php $sdf++;
                                                } ?>
                                    </tbody>
                                </table>
                                <hr>
                            <?php } ?>
                            <!-- <table class="table table-bordered tabled-striped">
                                <thead>
                                    <tr>
                                        <th width="70%">Uraian</th>
                                        <th>Nilai</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($lrahorizontal[1] as $lrahb) { ?>
                                        <tr>
                                            <td><?php echo $lrahb['ket'] ?></td>
                                            <td align="right"><?php echo number_format($lrahb['nilai']) ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <hr>
                            <table class="table table-bordered tabled-striped">
                                <thead>
                                    <tr>
                                        <th width="70%">Uraian</th>
                                        <th>Nilai</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($lrahorizontal[2] as $lrahc) { ?>
                                        <tr>
                                            <td><?php echo $lrahc['ket'] ?></td>
                                            <td align="right"><?php echo number_format($lrahc['nilai']) ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <hr>
                            <table class="table table-bordered tabled-striped">
                                <thead>
                                    <tr>
                                        <th width="70%">Uraian</th>
                                        <th>Nilai</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($lrahorizontal[3] as $lrahd) { ?>
                                        <tr>
                                            <td><?php echo $lrahd['ket'] ?></td>
                                            <td align="right"><?php echo number_format($lrahd['nilai']) ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table> -->
                        </div>
                        <div class="tab-pane  " id="icon1b" role="tabpanel">
                            <?php foreach ($lralak as $lralaka) { ?>
                                <table class="table table-bordered tabled-striped">
                                    <thead>
                                        <tr>
                                            <th width="70%">Uraian</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $dac = 0;
                                                foreach ($lralaka as $lralakc) { ?>
                                            <tr>
                                                <td><?php echo $lralakc['ket'] ?></td>
                                                <td align="right"><?php if ($dac == 0) {
                                                                                    echo $lralakc['nilai'];
                                                                                } else {
                                                                                    echo number_format($lralakc['nilai']);
                                                                                } ?></td>
                                            </tr>
                                        <?php $dac++;
                                                } ?>
                                    </tbody>
                                </table>
                                <hr>
                            <?php } ?>
                        </div>
                        <div class="tab-pane  " id="icon1c" role="tabpanel">
                            <!-- neracavslak -->
                            <?php foreach ($neracavslak as $neracavslaka) { ?>
                                <table class="table table-bordered tabled-striped">
                                    <thead>
                                        <tr>
                                            <th width="70%">Uraian</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $dad = 0;
                                                foreach ($neracavslaka as $neracavslakc) { ?>
                                            <tr>
                                                <td><?php echo $neracavslakc['ket'] ?></td>
                                                <td align="right"><?php if ($dad == 0) {
                                                                                    echo $neracavslakc['nilai'];
                                                                                } else {
                                                                                    echo number_format($neracavslakc['nilai']);
                                                                                } ?></td>
                                            </tr>
                                        <?php $dad++;
                                                } ?>
                                    </tbody>
                                </table>
                                <hr>
                            <?php } ?>
                        </div>

                        <div class="tab-pane  " id="icon1d" role="tabpanel">
                            <!-- LRA dan Laporan Perubahan SAL -->
                            <?php foreach ($lravssal as $lravssala) { ?>
                                <table class="table table-bordered tabled-striped">
                                    <thead>
                                        <tr>
                                            <th width="70%">Uraian</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $dae = 0;
                                                foreach ($lravssala as $lravssalc) { ?>
                                            <tr>
                                                <td><?php echo $lravssalc['ket'] ?></td>
                                                <td align="right"><?php if ($dae == 0) {
                                                                                    echo $lravssalc['nilai'];
                                                                                } else {
                                                                                    echo number_format($lravssalc['nilai']);
                                                                                } ?></td>
                                            </tr>
                                        <?php $dae++;
                                                } ?>
                                    </tbody>
                                </table>
                                <hr>
                            <?php } ?>
                        </div>
                        <!-- icon1e -->
                        <div class="tab-pane  " id="icon1e" role="tabpanel">
                            <?php foreach ($lolpeneraca as $lolpeneracaa) { ?>
                                <table class="table table-bordered tabled-striped">
                                    <thead>
                                        <tr>
                                            <th width="70%">Uraian</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $dag = 0;
                                                foreach ($lolpeneracaa as $lolpeneracac) { ?>
                                            <tr>
                                                <td><?php echo $lolpeneracac['ket'] ?></td>
                                                <td align="right"><?php if ($dag == 0) {
                                                                                    echo $lolpeneracac['nilai'];
                                                                                } else {
                                                                                    echo number_format($lolpeneracac['nilai']);
                                                                                } ?></td>
                                            </tr>
                                        <?php $dag++;
                                                } ?>
                                    </tbody>
                                </table>
                                <hr>
                            <?php } ?>
                        </div>

                        <div class="tab-pane  " id="icon1f" role="tabpanel">
                            <?php foreach ($lolraneraca as $lolraneracaa) { ?>
                                <table class="table table-bordered tabled-striped">
                                    <thead>
                                        <tr>
                                            <th width="70%">Uraian</th>
                                            <th>Nilai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $daf = 0;
                                                foreach ($lolraneracaa as $lolraneracac) { ?>
                                            <tr>
                                                <td><?php echo $lolraneracac['ket'] ?></td>
                                                <td align="right"><?php if ($daf == 0) {
                                                                                    echo $lolraneracac['nilai'];
                                                                                } else {
                                                                                    echo number_format($lolraneracac['nilai']);
                                                                                } ?></td>
                                            </tr>
                                        <?php $daf++;
                                                } ?>
                                    </tbody>
                                </table>
                                <hr>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
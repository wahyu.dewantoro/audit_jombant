<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Tanggal_BAST_lebih_dari_Tanggal_SPK.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style>
    .str {
        mso-number-format: \@;
    }

</style>
<h2>Tanggal BAST lebih dari Tanggal SPK</h2>
<table class="table table-bordered table-striped" border="1" cellspacing="0" cellpadding="0">
    <thead>
        <tr>
            <th>No</th>
            <th>OPD</th>
            <th>Jenis Belanja</th>
            <th>Kegiatan</th>
            <th>Sub Kegiatan</th>
            <th>Pagu</th>
            <th>Tanggal Pekerjaan SPK</th>
            <th>Tanggal BAST</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $sipil) { $persentase=number_format($sipil->nilai_spk/$sipil->harga_perkiraan*100,2,",",".");  ?>
        <tr>
            <td valign="top" width="10px" align="center"><?= ++$start ?></td>
            <td valign="top" class="cell-detail"><?= $sipil->kode_skpd; ?>
                <span class="cell-detail-description"><?= $sipil->nama_skpd; ?></span></td>
            <td valign="top" class="cell-detail"><?= $sipil->kode_belanja; ?> <span
                    class="cell-detail-description"><?= $sipil->jenis_belanja; ?></span></td>
            <td valign="top"><?= $sipil->nama_kegiatan; ?></td>
            <td valign="top"><?= $sipil->sub_kegiatan; ?></td>
            <td valign="top" align="right"><?= $sipil->pagu_anggaran; ?></td>
            <td valign="top" align="right" class="str"><?= date_indo($sipil->mulai_pekerjaan_spk)." s/d " .date_indo($sipil->selesai_pekerjaan_spk); ?></td>
            <td valign="top" align="right" class="str"><?= date_indo($sipil->tanggal_bast); ?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  include APPPATH . 'views\layouts\head.blade.php';
  ?>


</head>

<body>
  <div class="loading">
    <div class="loading-bar"></div>
    <div class="loading-bar"></div>
    <div class="loading-bar"></div>
    <div class="loading-bar"></div>
  </div>
  <div class="be-wrapper  be-fixed-sidebar">
    <?php
    include APPPATH . 'views\layouts\header.blade.php';
    include APPPATH . 'views\layouts\sidebar.blade.php';
    ?>
    <div class="be-content">
      <div class="page-head">
        <h2 class="page-head-title page-title"><?= $title ?>
          <div style="float:right">
            <?php
            if (isset($cetak)) {
              echo  anchor($cetak, '<i class="mdi mdi-print"></i> Excel', 'class="btn btn-sm btn-success"');
            }
            if (isset($cetak2)) {
              echo  anchor($cetak2, '<i class="mdi mdi-print"></i> CSV', 'class="btn btn-sm btn-secondary"');
            }
            if (isset($kembali)) {
              echo  anchor($kembali, '<i class="mdi mdi-flip-to-back"></i> Kembali', 'class="btn btn-sm btn-primary"');
            }
            ?>
          </div>
        </h2>
      </div>
      <div class="main-content container-fluid">
        <div class="pesan"><?= $this->session->flashdata('message') ?></div>
        <?= $contents ?>
      </div>
    </div>
  </div>

  <?php
  include APPPATH . 'views\layouts\footer.blade.php';
  if (isset($script)) {
    $this->load->view($script);
  } ?>

  <?php
  if (isset($script2)) {
    $this->load->view($script2);
  } ?>
</body>

</html>
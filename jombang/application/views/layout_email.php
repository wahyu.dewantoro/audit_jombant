
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from foxythemes.net/preview/products/beagle/layouts-offcanvas-menu.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 13 Jul 2018 11:57:42 GMT -->
<head>
     <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?= base_url() ?>assets/img/logo-fav.png">
    <title><?= $title ?></title>
    

     <link rel="shortcut icon" href="<?= base_url() ?>assets/img/logo-fav.png">
    <title>Beagle</title>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/><link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/select2/css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/summernote/summernote-bs4.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/app.css" type="text/css"/>
    <style>
        .pendek { width: 5em; background-color: #1E90FF; }
    </style>
   
  </head>
  <body>
    <div class="be-wrapper be-aside be-fixed-sidebar">
    <?php 
      include APPPATH.'views\layouts\header.blade.php';
      include APPPATH.'views\layouts\sidebar.blade.php';
    ?>
      
      <div class="be-content be-no-padding">
          <!-- <div class="pesan"><?= $this->session->flashdata('message')?></div> -->
            <?= $contents ?>
        
      </div>

    </div>
   <script src="<?= base_url() ?>assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/app.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/summernote/summernote-bs4.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/summernote/summernote-ext-beagle.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>     
    <script src="<?= base_url() ?>assets/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        //-initialize the javascript
        App.init();
        App.mailCompose();
      });
    </script>

    <!-- <script src="<?= base_url() ?>assets/js/app.js" type="text/javascript"></script>   -->

    <script type="text/javascript">
      // menu aktif
      $(function() {
          var base='<?= base_url() ?>'
          var seg='<?= strtolower($this->uri->segment(1)); ?>';
        $('.sidebar-elements a[href~="' + base+seg + '"]').parents('li').addClass('active');
      });


      
      $(document).ready(function() {
        // $('.select2').select2({ width: '100%' });
        $('.datepicker').datetimepicker({
            minView: 2,
            format: 'dd/mm/yyyy',
            autoclose: true
        });
    });

        function formatangka(objek) {
         a = objek.value;
         b = a.replace(/[^\d]/g,"");
         c = "";
         panjang = b.length;
         j = 0;
         for (i = panjang; i > 0; i--) {
           j = j + 1;
           if (((j % 3) == 1) && (j != 1)) {
             c = b.substr(i-1,1) + "." + c;
           } else {
             c = b.substr(i-1,1) + c;
           }
         }
         objek.value = c;
      };





//            angka 500 dibawah ini artinya pesan akan muncul dalam 0,5 detik setelah document ready
            $(document).ready(function(){setTimeout(function(){$(".pesan").fadeIn('slow');}, 500);});
//            angka 3000 dibawah ini artinya pesan akan hilang dalam 3 detik setelah muncul
            setTimeout(function(){$(".pesan").fadeOut('slow');}, 3000);




    </script>
    <?php if (isset($script)) { $this->load->view($script); } ?>
  </body>
</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php
    include APPPATH . 'views\layouts\head.blade.php';
    ?>
    <style>
        /* Chrome, Safari and Opera syntax */
        :-webkit-full-screen {
            background-color: yellow;
        }

        /* Firefox syntax */
        :-moz-full-screen {
            background-color: yellow;
        }

        /* IE/Edge syntax */
        :-ms-fullscreen {
            background-color: yellow;
        }

        /* Standard syntax */
        :fullscreen {
            background-color: yellow;
        }

        /* Style the button */
        button {
            padding: 20px;
            font-size: 20px;
        }
    </style>

</head>

<body id='fullscreen'>
    <div class="loading">
        <div class="loading-bar"></div>
        <div class="loading-bar"></div>
        <div class="loading-bar"></div>
        <div class="loading-bar"></div>
    </div>
    <div class="be-wrapper   be-nosidebar-left">
        <?php
        include APPPATH . 'views\layouts\header.blade.php';
        // include APPPATH . 'views\layouts\sidebar.blade.php';
        ?>
        <div class="be-content">
            <div class="page-head">
                <h2 class="page-head-title page-title"><?= $title ?>
                    <div style="float:right">
                        <div id="full">
                            <button class="btn btn-xs btn-success" onclick="openFullscreen();">Open Fullscreen</button>
                        </div>
                        <?php
                        if (isset($cetak)) {
                            echo  anchor($cetak, '<i class="mdi mdi-print"></i> Excel', 'class="btn btn-sm btn-success"');
                        }
                        if (isset($cetak2)) {
                            echo  anchor($cetak2, '<i class="mdi mdi-print"></i> CSV', 'class="btn btn-sm btn-secondary"');
                        }
                        if (isset($kembali)) {
                            echo  anchor($kembali, '<i class="mdi mdi-flip-to-back"></i> Kembali', 'class="btn btn-sm btn-primary"');
                        }
                        ?>
                    </div>
                </h2>
            </div>
            <div class="main-content container-fluid">
                <div class="pesan"><?= $this->session->flashdata('message') ?></div>
                <?= $contents ?>
            </div>
        </div>
    </div>

    <!-- <button onclick="closeFullscreen();">Close Fullscreen</button> -->
    <?php
    include APPPATH . 'views\layouts\footer.blade.php';
    if (isset($script)) {
        $this->load->view($script);
    } ?>

    <?php
    if (isset($script2)) {
        $this->load->view($script2);
    } ?>



    <script>
        setTimeout(function() {
            location = ''
        }, 60000)

        var elem = document.documentElement;

        function openFullscreen() {
            $('#full').html('');
            if (elem.requestFullscreen) {
                elem.requestFullscreen();
            } else if (elem.mozRequestFullScreen) {
                /* Firefox */
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullscreen) {
                /* Chrome, Safari & Opera */
                elem.webkitRequestFullscreen();
            } else if (elem.msRequestFullscreen) {
                /* IE/Edge */
                elem.msRequestFullscreen();
            }


        }

        function closeFullscreen() {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
        }
    </script>

</body>

</html>
<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Detail LO.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Detail LO</h3>
<table class="tablee" border="1">
                        <thead>
                            <tr>
                                <th rowspan="2">OPD</th>
                                <th rowspan="2">Unit</th>
                                <th colspan="9">Akun</th>
                            </tr>
                            <tr>
                                <th>Pendapatan Asli Daerah (PAD)</th>
                                <th>Pendapatan Transfer</th>
                                <th>Lain-Lain Pendapatan Daerah yang Sah</th>
                                <th>Surplus Non Operasional</th>
                                <th>Pendapatan Luar Biasa</th>
                                <th>Beban Operasi</th>
                                <th>Beban Transfer</th>
                                <th>Defisit Non Operasional</th>
                                <th>Beban Luar Biasa</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                         foreach ($lodetail_data as $rk) {?>
                            <tr>
								<td valign="top" ><?php echo $rk->kd_skpd ?></td>
								<td valign="top" ><?php echo $rk->nm_unit ?></td>
                                <td align="right"><?= abs(number_format($rk->sumnilai81,0,'','.')) ?></td>
                                <td align="right"><?= abs(number_format($rk->sumnilai82,0,'','.')) ?></td>
                                <td align="right"><?= abs(number_format($rk->sumnilai83,0,'','.')) ?></td>
                                <td align="right"><?= abs(number_format($rk->sumnilai84,0,'','.')) ?></td>
                                <td align="right"><?= abs(number_format($rk->sumnilai85,0,'','.')) ?></td>
                                <td align="right"><?= abs(number_format($rk->sumnilai91,0,'','.')) ?></td>
                                <td align="right"><?= abs(number_format($rk->sumnilai92,0,'','.')) ?></td>
                                <td align="right"><?= abs(number_format($rk->sumnilai93,0,'','.')) ?></td>
                                <td align="right"><?= abs(number_format($rk->sumnilai94,0,'','.')) ?></td>
                            </tr>
                        <?php
                        } ?>
						</tbody>
					</table>
<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Detail LO.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3><?= $title ?></h3>
<?= $nm_unit ?><br>
<?= $akun ?>
<table class="table table-bordered" border='1'>
                            <thead>
                              <tr>
                                  <th colspan="3">Akun</th>
                                  <th colspan="3">Rekening</th>
                                  <th>Nilai</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach($lodetail_data as $rk){?>
                                <tr>
                                  <td align="left" colspan="3"><strong><?php echo $rk->akun_akrual_3?></strong></td>
                                  <td colspan="3"><strong><?php echo $rk->nm_akrual_3?></strong></td>
                                  <td align="right"><strong><?php echo number_format(abs($rk->nilai),'0','','.')?></strong></td>
                                </tr>
                                <?php 
                                  $level4=$this->Mlodetail->getLevel4($nm_unit,$rk->akun_akrual_3);
                                  foreach($level4 as $rn ){  ?>
                                    <tr style="color:green">
                                      <td></td>
                                      <td align="left" colspan="2"><?php echo $rn->akun_akrual_4 ?></td>
                                      <td></td>
                                      <td colspan="2"><?php echo $rn->nm_akrual_4 ?></td>
                                      <td align="right"><?php echo number_format(abs($rn->nilai),'0','','.')?></td>
                                    </tr>
                                    <?php 
                                    $level5=$this->Mlodetail->getLevel5($nm_unit,$rn->akun_akrual_4);
                                    foreach($level5 as $rm){?>
                                      <tr style="color:red">
                                        <td></td>
                                        <td></td>
                                        <td><i><?php echo $rm->akun_akrual_5 ?></i></td>
                                        <td></td>
                                        <td></td>
                                        <td ><i><?php echo $rm->nm_akrual_5 ?></i></td>
                                        <td align="right"><i><?php echo number_format(abs($rm->nilai),'0','','.')?></i></td>
                                      </tr>
                              <?php } } } ?>
                            </tbody>
                        </table>
<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Detail LRA.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Detail LRA</h3>
<table class="tablee" border="1">
<thead>
                            <tr>
                                <th width="10px" rowspan="2">No</th>
								<th rowspan="2">OPD</th>
								<th rowspan="2">Nama Unit</th>
                                <th colspan="6">Akun</th>
                            </tr>
                            <tr>
                                <th>Pendapatan Asli Daerah</th>
                                <th>Pendapatan Transfer</th>
                                <th>Lain - Lain Pendapatan Daerah Yang Sah</th>
                                <th>Belanja Operasi </th>
                                <th>Belanja Modal </th>
                                <th>Belanja Tak Terduga</th>

                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1; foreach ($lradetail_data as $rk) {?>
                            <tr>
                                <td valign="top" width="10px" class="text-center"><?php echo $no++; ?></td>
								<td valign="top" ><?php echo $rk->kd_skpd ?></td>
								<td valign="top" ><?php echo $rk->nm_unit ?></td>
                                <td align="right"><?= number_format(abs($rk->sumnilai41),0,'','.') ?></td>
                                <td align="right"><?= number_format(abs($rk->sumnilai42),0,'','.') ?></td>
                                <td align="right"><?= number_format(abs($rk->sumnilai43),0,'','.') ?></td>
                                <td align="right"><?= number_format(abs($rk->sumnilai52),0,'','.') ?></td>
                                <td align="right"><?= number_format(abs($rk->sumnilai52),0,'','.') ?></td>
                                <td align="right"><?= number_format(abs($rk->sumnilai53),0,'','.') ?></td>
                            </tr>
                        <?php
                        } ?>
						</tbody>
					</table>
<?php

// Panggil class PHPExcel nya
$csv = new PHPExcel();

// Settingan awal fil excel

// Buat header tabel nya pada baris ke 1
$csv->setActiveSheetIndex(0)->setCellValue('A1', "NO"); // Set kolom A1 dengan tulisan "NO"
$csv->setActiveSheetIndex(0)->setCellValue('B1', "Kode SKPD"); // Set kolom B1 dengan tulisan "NIS"
$csv->setActiveSheetIndex(0)->setCellValue('C1', "Sub Unit"); // Set kolom C1 dengan tulisan "NAMA"
$csv->setActiveSheetIndex(0)->setCellValue('D1', "Akun 4.1"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('E1', "Akun 4.2"); // Set kolom E1 dengan tulisan "TELEPON"
$csv->setActiveSheetIndex(0)->setCellValue('F1', "Akun 4.3"); // Set kolom E1 dengan tulisan "TELEPON"
$csv->setActiveSheetIndex(0)->setCellValue('G1', "Akun 5.2"); // Set kolom E1 dengan tulisan "TELEPON"
$csv->setActiveSheetIndex(0)->setCellValue('H1', "Akun 5.2"); // Set kolom E1 dengan tulisan "TELEPON"
$csv->setActiveSheetIndex(0)->setCellValue('I1', "Akun 5.3"); // Set kolom E1 dengan tulisan "TELEPON"

$no = 1; // Untuk penomoran tabel, di awal set dengan 1
$numrow = 2; // Set baris pertama untuk isi tabel adalah baris ke 2
$nm_unit="";
foreach ($lradetail_data as $rk) {

    $csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
    $csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $rk->kd_skpd);
    $csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $rk->nm_unit);
    $csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, number_format($rk->sumnilai41,'2',',','.') );
    $csv->setActiveSheetIndex(0)->setCellValueExplicit('E'.$numrow, number_format($rk->sumnilai42,'2',',','.'));
    $csv->setActiveSheetIndex(0)->setCellValueExplicit('F'.$numrow, number_format($rk->sumnilai43,'2',',','.'));
    $csv->setActiveSheetIndex(0)->setCellValue('G'.$numrow, number_format($rk->sumnilai52,'2',',','.') );
    $csv->setActiveSheetIndex(0)->setCellValueExplicit('H'.$numrow, number_format($rk->sumnilai52,'2',',','.'));
    $csv->setActiveSheetIndex(0)->setCellValueExplicit('I'.$numrow, number_format($rk->sumnilai53,'2',',','.'));
    $no++; // Tambah 1 setiap kali looping
    $numrow++; // Tambah 1 setiap kali looping
}

// Set orientasi kertas jadi LANDSCAPE
$csv->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

// Set judul file excel nya
// $csv->getActiveSheet(0)->setTitle("Neraca");
// $csv->setActiveSheetIndex(0);

// Proses file excel
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="Detail LRA.csv"'); // Set nama file excel nya
header('Cache-Control: max-age=0');

$write = new PHPExcel_Writer_CSV($csv);
$write->save('php://output');
?>
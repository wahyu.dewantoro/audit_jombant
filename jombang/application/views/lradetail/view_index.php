<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">

                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered" id="">
                        <thead>
                            <tr>
                                <th rowspan="2">OPD</th>
                                <th colspan="6">Akun</th>
                            </tr>
                            <tr>

                                <th>Pendapatan Asli Daerah (4.1)</th>
                                <th>Pendapatan Transfer (4.2)</th>
                                <th>Lain - Lain Pendapatan Daerah Yang Sah (4.3)</th>
                                <th>Belanja Operasi (5.1)</th>
                                <th>Belanja Modal (5.2)</th>
                                <th>Belanja Tak Terduga (5.3)</th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $a = 0;
                            $b = 0;
                            $c = 0;
                            $d = 0;
                            $e = 0;
                            $f = 0;

                            foreach ($lradetail_data as $rk) { ?>
                                <tr>
                                    <td class="cell-detail"><?= $rk->nm_unit ?> <span class="cell-detail-description"><?= $rk->kd_skpd ?></span></td>
                                    <td align="right">
                                        <?php if (abs($rk->sumnilai41) > 0) {
                                                echo  anchor('lradetail/read?kd_skpd=' . urlencode($rk->kd_skpd) . '&nm_unit=' . urlencode($rk->nm_unit) . '&akun=' . urlencode('4.1'), number_format(abs($rk->sumnilai41), 0, '', '.'));
                                            } else {
                                                echo "-";
                                            } ?>
                                    </td>
                                    <td align="right">
                                        <?php if (abs($rk->sumnilai42) > 0) {
                                                echo  anchor('lradetail/read?kd_skpd=' . urlencode($rk->kd_skpd) . '&nm_unit=' . urlencode($rk->nm_unit) . '&akun=' . urlencode('4.2'), number_format(abs($rk->sumnilai42), 0, '', '.'));
                                            } else {
                                                echo "-";
                                            } ?>
                                    </td>
                                    <td align="right">
                                        <?php if (abs($rk->sumnilai43) > 0) {
                                                echo  anchor('lradetail/read?kd_skpd=' . urlencode($rk->kd_skpd) . '&nm_unit=' . urlencode($rk->nm_unit) . '&akun=' . urlencode('4.3'), number_format(abs($rk->sumnilai43), 0, '', '.'));
                                            } else {
                                                echo "-";
                                            } ?>
                                    </td>
                                    <td align="right">
                                        <?php if (abs($rk->sumnilai51) > 0) {
                                                echo  anchor('lradetail/read?kd_skpd=' . urlencode($rk->kd_skpd) . '&nm_unit=' . urlencode($rk->nm_unit) . '&akun=' . urlencode('5.1'), number_format(abs($rk->sumnilai51), 0, '', '.'));
                                            } else {
                                                echo "-";
                                            } ?>
                                    </td>
                                    <td align="right">
                                        <?php if (abs($rk->sumnilai52) > 0) {
                                                echo  anchor('lradetail/read?kd_skpd=' . urlencode($rk->kd_skpd) . '&nm_unit=' . urlencode($rk->nm_unit) . '&akun=' . urlencode('5.2'), number_format(abs($rk->sumnilai52), 0, '', '.'));
                                            } else {
                                                echo "-";
                                            } ?>
                                    </td>
                                    <td align="right">
                                        <?php if (abs($rk->sumnilai53) > 0) {
                                                echo  anchor('lradetail/read?kd_skpd=' . urlencode($rk->kd_skpd) . '&nm_unit=' . urlencode($rk->nm_unit) . '&akun=' . urlencode('5.3'), number_format(abs($rk->sumnilai53), 0, '', '.'));
                                            } else {
                                                echo "-";
                                            } ?>
                                    </td>
                                </tr>
                            <?php
                                $a += abs($rk->sumnilai41);
                                $b += abs($rk->sumnilai42);
                                $c += abs($rk->sumnilai43);
                                $d += abs($rk->sumnilai51);
                                $e += abs($rk->sumnilai52);
                                $f += abs($rk->sumnilai53);
                            }   ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><strong>Jumlah</strong></td>
                                <td align="right"><?= number_format($a, 0, '', '.'); ?></td>
                                <td align="right"><?= number_format($b, 0, '', '.'); ?></td>
                                <td align="right"><?= number_format($c, 0, '', '.'); ?></td>
                                <td align="right"><?= number_format($d, 0, '', '.'); ?></td>
                                <td align="right"><?= number_format($e, 0, '', '.'); ?></td>
                                <td align="right"><?= number_format($f, 0, '', '.'); ?></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div><!-- end card-->
    </div>
</div>
<script>
    $('#excel').hide();
    $('#pdf').hide();
    $('#kec').on('change', function() {
        $('.loading').show();
        var _url = "<?php echo base_url() ?>" + "maskeudes/getdesa";
        $.ajax({
            url: _url,
            data: {
                'kd_kec': $(this).val()
            },
            dataType: 'json',
            type: 'get',
            success: function(data) {
                $('.loading').hide();
                $("#desa").html("<option value=''></option>");
                $.each(data, function(key, item) {
                    $('#desa')
                        .append($("<option></option>")
                            .attr("value", item.id)
                            .text(item.text));
                });
            },
            error: function(res) {
                $('.loading').hide();
                alert('sistem error');
            }
        });
    });

    // $("#excel").attr("href","<?php echo base_url() ?>" + "maskeudes/excel?kd_desa=");
    $('#desa').on('change', function() {
        var _url = "<?php echo base_url() ?>" + "maskeudes/cetak";
        $("#excel").attr("href","<?php echo base_url() ?>" + "maskeudes/excel?kd_desa="+$(this).val());
        $("#pdf").attr("href","<?php echo base_url() ?>" + "maskeudes/pdf?kd_desa="+$(this).val());
        $('.loading').show();
        // if ($(this).val() != null) {
        // $('.loading').hide();

        $.ajax({
            url: _url,
            data: {
                'kd_desa': $(this).val()
            },
            type: 'post',
            success: function(data) {
                $('.loading').hide();
                $('#hasil').html("<hr>"+data);
                $('#excel').show();
                $('#pdf').show();

            },
            error: function(res) {
                $('.loading').hide();
                alert('sistem error');
                $('#hasil').html("");
            }
        });



    });
</script>
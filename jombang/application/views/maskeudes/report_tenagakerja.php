<div class="col-md-12">
    <table class="table table-bordered" width="100%">
        <thead>
            <tr>
                <th>No </th>
                <th>Desa</th>
                <th>Pagu</th>
                <th>Anggaran</th>
                <th>%</th>
                <th>Pagu PAK</th>
                <th>Anggaran PAK</th>
                <th>%</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 1;
            foreach ($res as $res) { ?>
                <tr>
                    <td align="center"><?= $no++ ?> </td>
                    <td class="cell-detail">
                        <span><?= $res->Nama_Desa ?></span>
                    </td>
                    
                    <td align="right"><?= number_format($res->pagu, 0, '', '.')  ?></td>
                    <td align="right"><?= number_format($res->anggaran, 0, '', '.')  ?></td>
                    <td align="right"><?= number_format($res->persen, 2, ',', '.')  ?></td>
                    <td align="right"><?= number_format($res->pagu_pak, 0, '', '.')  ?></td>
                    <td align="right"><?= number_format($res->anggaran_pak, 0, '', '.')  ?></td>
                    <td align="right"><?= number_format($res->persen_pak, 2, ',', '.')  ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
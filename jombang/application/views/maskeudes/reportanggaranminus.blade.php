@extends('layouts.master')
@section('judul')
<h2 class="page-head-title"> Report Anggaran Minus </h2> @endsection
@section('content') <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-12">
            <div class="card-body">
                <div class="table-rsponsive">
                    <table class="table table-striped table-hover table-bordered" id="table2">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Desa</th>
                                <th>Uraian</th>
                                <th>Anggaran</th>
                                <th>Relaisasi</th>
                                <th>Surplus / Defisit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                            foreach ($res as $res) { ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td class="cell-detail"><?= $res->kd_desa ?>
                                        <span class="cell-detail-description"><?= $res->Nama_Desa ?></span>
                                    </td>
                                    
                                    <td class="cell-detail"><?= $res->akun_3 ?>
                                        <span class="cell-detail-description"><?= $res->Nama_Jenis ?> </span>
                                    </td>
                                    <td align="right"><?= number_format($res->anggaran,0,'','.') ?></td>
                                    <td align="right"><?= number_format($res->realisasi,0,'','.') ?></td>
                                    <td align="right"><?= number_format($res->anggaran-$res->realisasi,0,'','.') ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div><!-- end card-->
    </div>
</div>
@endsection
@section('script')
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        //-initialize the javascript
        // App.init();
        App.dataTables();
    });
</script>
@endsection
@extends('layouts.master')
@section('judul')
<h2 class="page-head-title"> Laporan Panjar</h2> @endsection @section('content') <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-12">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-12">
                        <table class="table  table-hover table-bordered" id="table1">
                            <thead>
                                <tr>
                                    <th>No </th>
                                    <th>Desa</th>
                                    <th>Keterangan</th>
                                    <th>Pagu</th>
                                    <th>NO SPP</th>
                                    <th>Nilai Panjar</th>
                                    <th>NO SPJ</th>
                                    <th>Nilai SPJ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                foreach ($res as $res) {
                                  $a=  date_create($res->Tgl_SPP);$b= date_create($res->tgl_spj);$diff  = date_diff($a, $b); ?>
                                <tr <?= $diff->d>10?'style="background-color: #ffb8b8"':"" ?>>
                                    <td align="center"><?= $no++?> </td>
                                    <td class="cell-detail">
                                        <span><?= ucwords(strtolower($res->nama_desa)) ?></span>
                                        <span class="cell-detail-description"><?= str_replace('Kecamatan ','',ucwords(strtolower($res->Nama_Kecamatan))) ?></span>
                                    </td>
                                    
                                    <td class="cell-detail"><?= $res->Keterangan ?>
                                        <span class="cell-detail-description" ><?= $res->Kd_Keg ?></span>
                                    <td align="right"><?= number_format($res->pagu, 0, '', '.')  ?></td>
                                    <td class="cell-detail"><?= $res->No_SPP ?>
                                        <span
                                            class="cell-detail-description" ><?= date_indo(date('Y-m-d',strtotime($res->Tgl_SPP))) ?></span>
                                    <td align="right"><?= number_format($res->panjar, 0, '', '.')  ?></td>
                                    <td class="cell-detail"><?= $res->no_spj ?>
                                        <span
                                            class="cell-detail-description" ><?= date_indo(date('Y-m-d',strtotime($res->tgl_spj))) ?></span>
                                    <td align="right"><?= number_format($res->spj, 0, '', '.')  ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <p><b>* warna merah menunjukan tgl spj > 10 hari dari tgl spp *</b></p>
                    </div>
                </div>
            </div>
        </div><!-- end card-->
    </div>
</div>
@endsection
@section('script')
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript">
</script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js"
    type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js"
    type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js"
    type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js"
    type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js"
    type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js"
    type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"
    type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        //-initialize the javascript
        // App.init();
        App.dataTables();
    });
</script>
@endsection
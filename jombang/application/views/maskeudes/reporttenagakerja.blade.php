@extends('layouts.master')
@section('judul')
<h2 class="page-head-title"> Report Honor Tenaga Kerja < 30% </h2> @endsection @section('content') <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-12">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-12">
                    <table class="table  table-hover table-bordered" id="table1">
                            <thead>
                                <tr>
                                    <th>No </th>
                                    <th>Desa</th>
                                    <th>Kecamatan</th>
                                    <th>Pagu</th>
                                    <th>Anggaran</th>
                                    <th>%</th>
                                    <th>Pagu PAK</th>
                                    <th>Anggaran PAK</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1;
                                foreach ($res as $res) { ?>
                                    <tr>
                                        <td align="center"><?= $no++ ?> </td>
                                        <td class="cell-detail">
                                            <span><?= ucwords(strtolower($res->Nama_Desa)) ?></span>
                                        </td>
                                        <td class="cell-detail">
                                            <span><?= str_replace('Kecamatan ','',ucwords(strtolower($res->Nama_Kecamatan))) ?></span>
                                        </td>
                                        <td align="right"><?= number_format($res->pagu, 0, '', '.')  ?></td>
                                        <td align="right"><?= number_format($res->anggaran, 0, '', '.')  ?></td>
                                        <td <?php if($res->persen < 30 ){ echo  "style='background-color: red;'";}  ?> align="right"><?= number_format($res->persen, 2, ',', '.')  ?>%</td>
                                        <td align="right"><?= number_format($res->pagu_pak, 0, '', '.')  ?></td>
                                        <td align="right"><?= number_format($res->anggaran_pak, 0, '', '.')  ?></td>
                                        <td <?php if($res->persen_pak < 30 ){ echo  "style='background-color: red;'";}  ?> align="right"><?= number_format($res->persen_pak, 2, ',', '.')  ?>%</td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- end card-->
    </div>
</div>
@endsection
@section('script')
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        //-initialize the javascript
        // App.init();
        App.dataTables();
    });
</script>
@endsection
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-12">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group row ilang">
                                    <div class="col-12">
                                        <select name="kec" class="form-control form-control-sm select2 required" id="kec" data-placeholder="Pilih Kecamatan">
                                            <option value=""></option>
                                            <?php foreach ($kec as $rk) { ?>
                                                <option value="<?= $rk->Kd_Kec ?>"><?= str_replace('KECAMATAN ','',$rk->Nama_Kecamatan) ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <div class="col-12">
                                        <select name="desa" class="form-control select2 form-control-sm required" id="desa" data-placeholder="Pilih Desa">
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group row">
                                    <div class="col-12">
                                        <a id="excel" href="#" target="_blank" class="btn btn-success"><i class="mdi mdi-print"></i> Excel</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group row">
                                    <div class="col-12">
                                        <a id="pdf" href="#" target="_blank" class="btn btn-danger"><i class="mdi mdi-print"></i> PDF</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="hasil"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
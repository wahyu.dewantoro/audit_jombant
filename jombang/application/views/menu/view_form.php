<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">                       
        <div class="card card-border-color card-border-color-primary">
            <div class="card-body">
                <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Nama Menu <?php echo form_error('nama_menu') ?></label>
            <input type="text" class="form-control form-control-xs" name="nama_menu" id="nama_menu" placeholder="Nama Menu" value="<?php echo $nama_menu; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Link Menu <?php echo form_error('link_menu') ?></label>
            <input type="text" class="form-control form-control-xs" name="link_menu" id="link_menu" placeholder="Link Menu" value="<?php echo $link_menu; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Parent <?php echo form_error('parent') ?></label>
            <!-- <input type="text"  placeholder="Parent" value="<?php echo $parent; ?>" /> -->
            <select class="select2 form-control form-control-xs" name="parent" id="parent">
                <option value="0">Parent</option>
                <?php foreach($lp as $lp){?>
                <option value="<?= $lp->id_inc?>" <?php if($lp->id_inc==$parent){echo "selected";}?>><?= $lp->nama_menu ?></option>
                <?php } ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="int">Sort <?php echo form_error('sort') ?></label>
            <input type="text" class="form-control form-control-xs" name="sort" id="sort" placeholder="Sort" value="<?php echo $sort; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Icon <?php echo form_error('icon') ?></label>
            <input type="text" class="form-control form-control-xs" name="icon" id="icon" placeholder="Icon" value="<?php echo $icon; ?>" />
        </div>
	    <input type="hidden" name="id_inc" value="<?php echo $id_inc; ?>" /> 
	    <button type="submit" class="btn btn-sm btn-primary"><i class="mdi mdi-cloud-done"></i> <?php echo $button ?></button> 
	    <a class="btn btn-sm btn-warning" href="<?php echo site_url('menu') ?>" ><i class="mdi mdi-close"></i> Cancel</a>
	</form>
            </div>                                                      
        </div><!-- end card-->                  
    </div>
</div>
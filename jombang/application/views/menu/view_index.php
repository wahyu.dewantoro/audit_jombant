<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">                       
        <div class="card mb-3">
            <div class="card-body">
                <div class="tools float-right">
                    <form action="<?php echo site_url('menu/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control form-control-xs" name="q" value="<?php echo $q; ?>" placeholder="Pencarian">
                            <span class="input-group-btn">
                            <div class="btn-group">
                            
                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                                <?php if ($q <> '')  { ?>
                                    <a href="<?php echo site_url('menu'); ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
                              <?php } 
                              
                                    if($akses['create']==1){
                                     echo anchor(site_url('menu/create'),'<i class="mdi mdi-plus"></i> Tambah', 'class="btn btn-success btn-sm"');
                                    }
                                 ?>
                            </div>
                        </span>
                    </div>
                </form>
                </div>
                    <table class="table  table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>Nama Menu</th>
								<th>Link Menu</th>
								<th>Parent</th>
								<th>Sort</th>
								<th>Icon</th>
								<th width="20px "> </th>
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($menu_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo ++$start ?></td>
								<td><?php echo $rk->nama_menu ?></td>
								<td><?php echo $rk->link_menu ?></td>
								<td><?php echo $rk->parent ?></td>
								<td><?php echo $rk->sort ?></td>
								<td><?php echo $rk->icon ?></td>
								<td style="text-align:center" >
									<div class="btn-group">
										<?php 
										 if($akses['read']==1){ echo anchor(site_url('menu/read/'.acak($rk->id_inc)),'<i class="mdi mdi-search"></i>','class="btn btn-xs btn-primary"'); }
										 if($akses['update']==1){ echo anchor(site_url('menu/update/'.acak($rk->id_inc)),'<i class="mdi mdi-edit"></i>','class="btn btn-xs btn-success"'); }
										 if($akses['delete']==1){ echo anchor(site_url('menu/delete/'.acak($rk->id_inc)),'<i class="mdi mdi-delete"></i>','class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'apakah anda yakin untuk menghapus ?\')"'); }
										?>
									</div>
								</td>
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					<button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
					<div class="float-right">
						<?php echo $pagination ?>
					</div>
            </div>                                                      
        </div><!-- end card-->                  
    </div>
</div>
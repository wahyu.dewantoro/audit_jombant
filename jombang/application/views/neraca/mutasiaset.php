<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <table>
                            <tr>
                                <td><strong>OPD</strong></td>
                                <td>: <?= $nm_unit ?></td>
                            </tr>
                            <tr>
                                <td><strong>Akun</strong></td>
                                <td>: <?= $akun ?></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered" border='1'>
                                <thead>
                                  <tr>
                                      <th colspan="3">Akun</th>
                                      <th colspan="3">Rekening</th>
                                      <th>Nilai</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($neraca_data as $rk){?>
                                        <tr style="font-size:1.2rem">
                                          <td colspan="3"><strong><?php echo $rk->akun_akrual_2?></strong></td>
                                          <td colspan="3"><strong><?php echo $rk->nm_akrual_2?></strong></td>
                                          <td align="right"><strong><?php echo number_format(abs($rk->nilai),'0','','.')?></strong></td>
                                        </tr>
                                    <?php 
                                    $level3=$this->Mneraca->mutasiAsetTiga($nm_unit,$rk->akun_akrual_2);
                                    foreach($level3 as $rn ){  ?>
                                        <tr style="color: green; font-size:1.1rem">
                                          <td></td>
                                          <td colspan="2"><?php echo $rn->akun_akrual_3 ?></td>
                                          <td></td>
                                          <td colspan="2"><?php echo $rn->nm_akrual_3 ?></td>
                                          <td align="right"><?php echo number_format(abs($rn->nilai),'0','','.')?></td>
                                        </tr>
                                        <?php 
                                        $level4=$this->Mneraca->mutasiAsetEmpat($nm_unit,$rn->akun_akrual_3);
                                        
                                        foreach($level4 as $rm){?>
                                            <tr style="color: red;font-size:0.9rem">
                                                <td></td>
                                                <td></td>
                                                <td><i><?php echo $rm->akun_akrual_4 ?></i></td>
                                                <td></td>
                                                <td></td>
                                                <td ><i><?php echo $rm->nm_akrual_4 ?></i></td>
                                                <td align="right"><i><?php echo number_format(abs($rm->nilai),'0','','.')?></i></td>
                                              </tr>
                                    <?php } } } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- end card-->
    </div>
</div>  
<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Neraca.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Neraca</h3>
<table class="tablee" border="1">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>SKPD</th>
								<th>Sub Unit</th>
								<th>Saldo Awal Debet</th>
								<th>Saldo Awal Kredit</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1; foreach ($neraca_data as $rk) {?>
                            <tr>
                                <td valign="top" width="10px" class="text-center"><?php echo $no++; ?></td>
								<td valign="top" ><?php echo $rk->kd_skpd ?></td>
								<td valign="top"><?php echo $rk->nm_sub_unit ?></td>
								<td align="right"><?php echo number_format($rk->saldo_awal_debet,'2',',','.') ?></td>
								<td align="right"><?php echo number_format($rk->saldo_awal_kredit,'2',',','.') ?></td>
                            </tr>
                        <?php
                        } ?>
						</tbody>
                        <tfoot>
                            <tr>
								<th colspan="3" align="right">TOTAL</th>
                                <td class="text-right"><?= $debet ?></td>
                                <td class="text-right"><?= $kredit ?></td>
                            </tr>
                        </tfoot>
					</table>
<?php

header("Content-type: application/octet-stream");

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');header("Content-Disposition: attachment; filename=Neraca ".$kd_skpd." ".str_replace('.',' ', str_replace(',', '', $nm_sub_unit)).".xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Neraca</h3>
<table class="tablee" border="1">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
                                <th>Tahun</th>
								<th>Jurnal</th>
								<th>Bukti</th>
                                <th>Tanggal Bukti</th>
								<th>BKU</th>
								<th>Program</th>
								<th>Kegiatan</th>
								<th>Keterangan</th>
								<th>Akrual 1 - 4</th>
								<th>Akrual 5</th>
								<th>Saldo Awal Debet</th>
								<th>Saldo Awal Kredit</th>
								<th>Mutasi Debet</th>
								<th>Mutasi Kredit</th>
								<th>Penyesuaian Debet</th>
								<th>Penyesuaian Kredit</th>
								<th>Gabungan Debet</th>
								<th>Gabungan Kredit</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1; foreach ($neraca_data as $rk) {?>
                            <tr>
                                <td valign="top" width="10px" class="text-center"><?php echo $no++; ?></td>
								<td valign="top" ><?php echo $rk->Tahun ?></td>
                                <td valign="top" style='mso-number-format:"\@"'><?php echo $rk->Kd_Jurnal ?><br><?php echo $rk->Nm_Jurnal ?></td>
								<td valign="top"><?php echo $rk->No_Bukti ?></td>
								<td valign="top" style='mso-number-format:"\@"'><?php echo date_indo(date('Y-m-d',strtotime($rk->Tgl_Bukti))) ?></td>
								<td valign="top"><?php echo $rk->No_BKU ?></td>
								<td valign="top"><?php echo $rk->Ket_Program ?></td>
								<td valign="top"><?php echo $rk->Ket_Kegiatan ?></td>
								<td valign="top"><?php echo $rk->Keterangan ?></td>
								<td valign="top" style='mso-number-format:"\@"'><span style="float:left;"><?php echo $rk->Akun_Akrual_1 ?></span><br>
									<?php echo $rk->Nm_Akrual_1 ?><br>
									<?php echo $rk->Akun_Akrual_2 ?><br>
									<?php echo $rk->Nm_Akrual_2 ?><br>
									<?php echo $rk->Akun_Akrual_3 ?><br>
									<?php echo $rk->Nm_Akrual_3 ?><br>
									<?php echo $rk->Akun_Akrual_4 ?><br>
									<?php echo $rk->Nm_Akrual_4 ?>
								</td>
								<td valign="top"  class="cell-detail"><?php echo $rk->Akun_Akrual_5 ?><br>
									<?php echo $rk->Nm_Akrual_5 ?></td>
								</td>
								<td valign="top" align="right"><?php echo number_format($rk->Saldo_Awal_Debet,'2',',','.') ?></td>
								<td valign="top" align="right"><?php echo number_format($rk->Saldo_Awal_Kredit,'2',',','.') ?></td>
								<td valign="top" align="right"><?php echo number_format($rk->Mutasi_Debet,'2',',','.') ?></td>
								<td valign="top" align="right"><?php echo number_format($rk->Mutasi_Kredit,'2',',','.') ?></td>
								<td valign="top" align="right"><?php echo number_format($rk->Penyesuaian_Debet,'2',',','.') ?></td>
								<td valign="top" align="right"><?php echo number_format($rk->Penyesuaian_Kredit,'2',',','.') ?></td>
								<td valign="top" align="right"><?php echo number_format($rk->Gabungan_Debet,'2',',','.') ?></td>
								<td valign="top" align="right"><?php echo number_format($rk->Gabungan_Kredit,'2',',','.') ?></td>
                            </tr>
                        <?php
                        } ?>
						</tbody>
                        <tfoot>
                            <tr>
								<th colspan="16" align="right">TOTAL DEBET</th>
                                <td class="text-right"><?= $debet ?></td>
								<th  align="right">TOTAL KREDIT</th>
                                <td class="text-right"><?= $kredit ?></td>
                            </tr>
                        </tfoot>
					</table>
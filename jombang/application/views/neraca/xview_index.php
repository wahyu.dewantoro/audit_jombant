<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
                <form action="<?php echo site_url('neraca/index'); ?>" method="get">
                    <div class="row">
                        <div class="col-md-4">
                                <label><strong>SKPD/OPD</strong></label>
                                <select class="form-control form-control-sm select2" name="q" data-placeholder="Silahkan pilih">
                                <option value=""></option>
                                <?php foreach($subunit as $su){?>
                                <option <?php if($su->nm_sub_unit==$q){echo "selected";}?> value="<?= $su->nm_sub_unit ?>"><?= $su->nm_sub_unit ?></option>
                                <?php } ?>
                                </select>
                            </div>
                        <div width="300px" style="padding-left:15px;padding-right: 15px;">
                                <div class="form-group">
                                    <label><strong>Tanggal Bukti</strong></label>
                                    <div class="row">
                                        <div width="120px" style="padding-left:20px;padding-right: 15px;">
                                            <input style="width:85px" type="text" name="tanggal1" value="<?= $tanggal1 ?>" placeholder="Tgl Mulai" class="form-control form-control-xs datepicker"  >
                                        </div>
                                        <div width="120px" style="padding-left:15px;padding-right: 15px;">
                                            <input style="width:85px" type="text" name="tanggal2" value="<?= $tanggal2 ?>" placeholder="Tgl Akhir" class="form-control form-control-xs datepicker"  >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="col-md-3">
                        <label><strong>Pencarian</strong></label>
                        <div class="input-group">
                            <input type="text" class="form-control form-control-xs" name="q2" value="<?= $q2 ?>" placeholder="Pencarian">
                            <span class="input-group-btn">
                                <div class="btn-group">
                                    <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                                    <?php if ($q <> '' || $q2 <> '')  { ?>
                                        <a href="<?php echo site_url('neraca'); ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
                                <?php }
                                    ?>
                                </div>
                            </span>
                        </div>
                        </div>
                    </div>
                </form>

                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered " id="table2">
                        <thead>
                            <tr>
                                <th width="10px">No</th>

								<th>SKPD</th>
								<th>Aset</th>
								<th>Kewajiban + Ekuitas</th>
								<th width="10px">Detail</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($neraca_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
                                <td class="cell-detail"><?php echo $rk->kd_skpd ?><span class="cell-detail-description"><?php echo $rk->nm_sub_unit ?></span>
                                </td>
								<td align ="right"><?php echo number_format($rk->saldo_awal_debet,'2',',','.') ?></td>
								<td align ="right"><?php echo number_format($rk->saldo_awal_kredit,'2',',','.') ?></td>
								<td align="center">
									<form action="<?= base_url().'neraca/read'?>">
										<input type='hidden' name='kd_skpd'  value='<?= $rk->kd_skpd?>' >
										<input type='hidden' name='nm_sub_unit'  value='<?= $rk->nm_sub_unit?>' >
										<button class="btn btn-xs btn-primary"><i class="mdi mdi-search"></i></button>
									</form>
								</td>
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					</div>
					<!-- <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo number_format($total_rows,'0','','.') ?></button> -->

					<!-- <div class="float-right">
						<?php echo $pagination ?>
					</div> -->
                    <!-- <div class="float-right">
                        <div class="btn-group">
                            <button  class="btn page-link  btn-space btn-success" disabled>Total Debet : <?php echo $debet ?></button>
                            <button  class="btn page-link btn-space btn-warning" disabled>Total Kredit : <?php echo $kredit ?></button>
                        </div>
                    </div> -->
            </div>
        </div><!-- end card-->
    </div>
</div>
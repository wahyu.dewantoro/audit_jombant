<?php

// Panggil class PHPExcel nya
$csv = new PHPExcel();

// Settingan awal fil excel

// Buat header tabel nya pada baris ke 1
$csv->setActiveSheetIndex(0)->setCellValue('A1', "NO");
$csv->setActiveSheetIndex(0)->setCellValue('B1', "Tahun");
$csv->setActiveSheetIndex(0)->setCellValue('C1', "No Bukti");
$csv->setActiveSheetIndex(0)->setCellValue('D1', "Tanggal Bukti");
$csv->setActiveSheetIndex(0)->setCellValue('E1', "Keterangan");
$csv->setActiveSheetIndex(0)->setCellValue('F1', "Nilai");
$csv->setActiveSheetIndex(0)->setCellValue('G1', "Kode rekening");
$csv->setActiveSheetIndex(0)->setCellValue('H1', "Rekening");
$csv->setActiveSheetIndex(0)->setCellValue('I1', "D / K");

$no = 1; // Untuk penomoran tabel, di awal set dengan 1
$numrow = 2; // Set baris pertama untuk isi tabel adalah baris ke 2
$nm_unit="";
foreach ($pajak_data as $rk) {

    $csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
    $csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $rk->Tahun);
    $csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $rk->No_Bukti);
    $csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, date_indo(date('Y-m-d',strtotime($rk->Tgl_Bukti))));
    $csv->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $rk->Keterangan );
    $csv->setActiveSheetIndex(0)->setCellValue('F'.$numrow, number_format($rk->Nilai,'0',',','.'));
    $csv->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $rk->Kd_Rek_Gabung);
    $csv->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $rk->Nm_Rek_5);
    $csv->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $rk->D_K);

    $no++; // Tambah 1 setiap kali looping
    $numrow++; // Tambah 1 setiap kali looping
}

// $csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('E'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValueExplicit('F'.$numrow, "Total Nilai ".$nilai);

// Set orientasi kertas jadi LANDSCAPE
$csv->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

// Set judul file excel nya
// $csv->getActiveSheet(0)->setTitle("Neraca");
// $csv->setActiveSheetIndex(0);

// Proses file excel
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment; filename=Pajak ".$kd_skpd." ".str_replace('.',' ', str_replace(',', '', $nm_sub_unit)).".csv");
header('Cache-Control: max-age=0');

$write = new PHPExcel_Writer_CSV($csv);
$write->save('php://output');
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
                <form action="<?php echo site_url('pajak/index'); ?>"  method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <label><strong>SKPD/OPD</strong></label>
                            <select class="form-control select2 form-control-sm" name="q">
                                <option value="">Pilih SKPD/OPD </option>
                                <?php foreach($subunit as $su){?>
                                <option <?php if($su->nm_unit==$q){echo "selected";}?> value="<?= $su->nm_unit ?>"><?= $su->kd_skpd.' - '.$su->nm_unit ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    <div width="300px" style="padding-left:15px;padding-right: 15px;">
                        <div class="form-group">
                            <label><strong>Tanggal Bukti</strong></label>
                            <div class="row">
                                <div width="120px" style="padding-left:20px;padding-right: 15px;">
                                    <input style="width:85px" type="text" name="tanggal1" value="<?= $tanggal1 ?>" placeholder="Tgl Mulai" class="form-control form-control-xs datepicker"  >
                                </div>
                                <div width="120px" style="padding-left:15px;padding-right: 15px;">
                                    <input style="width:85px" type="text" name="tanggal2" value="<?= $tanggal2 ?>" placeholder="Tgl Akhir" class="form-control form-control-xs datepicker"  >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                                <div class="form-group">
                                    <label style="height: 13px;"><strong> </strong></label><br>
                                        <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                                        <?php if ($q <> '')  { ?>
                                            <a href="<?php echo site_url('pajak'); ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
                                      <?php }
                                      ?>
                                </div>
                            </div>
                    </div>
                </form>

                	<div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered" id="table2">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>SKPD</th>
								<th>Sub Unit</th>
								<th>Nilai</th>
								<th width="10px">Detail</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php
                            $tj=0;
                             foreach ($pajak_data as $rk)  { 
                                $tj+=$rk->nilai;
                                ?>
                            <tr>
								<td  align="center"><?php echo ++$start ?></td>
								<td><?php echo $rk->kd_skpd ?></td>
								<td><?php echo $rk->nm_sub_unit ?></td>
								<td align="right"><?php echo number_format($rk->nilai,'2',',','.') ?></td>
								<td align="center">
                                    <form action="<?= base_url().'pajak/read'?>">
                                        <input type='hidden' name='kd_skpd' value='<?= $rk->kd_skpd ?>'>
                                        <input type='hidden' name='nm_sub_unit' value='<?= $rk->nm_sub_unit ?>'>
                                       <input type="hidden" name="tanggal1" value="<?= $tanggal1 ?>">
                                       <input type="hidden" name="tanggal2" value="<?= $tanggal2 ?>">
                                        <button class="btn btn-xs btn-primary"><i class="mdi mdi-search"></i></button>
                                    </form>
                                </td>
							</tr>
							<?php  }   ?>
						</tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3">Jumlah</td>
                                <td align="right"><strong><?= number_format($tj,'0','','.') ?></strong></td>
                                <td></td>
                            </tr>
                        </tfoot>
					</table>
					</div>
					
            </div>
        </div><!-- end card-->
    </div>
</div>
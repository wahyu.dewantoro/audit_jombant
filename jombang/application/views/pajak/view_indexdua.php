<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            <div class="row">
            	<div class="col-md-4">
            		<table>
            			<tr>
            				<td>SKPD</td>
            				<td>: <strong><?=$kd_skpd ?></td>
            			</tr>
            			<tr  valign="top">
            				<td>Sub Unit</td>
            				<td>: <strong><?= $nm_sub_unit ?></td>
            			</tr>
            		</table>
            	</div>
            	<div class="col-md-8">
            		<div class="">
	                    <form action="<?php echo $action ?>"  method="get">
	                    	<div class="row">
								<div width="300px" style="padding-left:15px;padding-right: 15px;">
			                    	<div class="form-group">
			                    		<label><strong>Tanggal Bukti</strong></label>
			                    		<div class="row">
			                    			<div width="120px" style="padding-left:20px;padding-right: 15px;">
			                    				<input style="width:85px" type="text" name="tanggal1" value="<?= $tanggal1 ?>" placeholder="Tgl Mulai" class="form-control form-control-xs datepicker"  >
			                    			</div>
			                    			<div width="120px" style="padding-left:20px;padding-right: 15px;">
			                    				<input style="width:85px" type="text" name="tanggal2" value="<?= $tanggal2 ?>" placeholder="Tgl Akhir" class="form-control form-control-xs datepicker"  >
			                    			</div>
			                    		</div>


			                    	</div>
			                    </div>
	                    		<div class="col-md-4">
	                    			<div class="form-group">
	                    				<label><strong>Rekening</strong></label>
	                    				<select class="form-control form-control-sm select2" data-placeholder="Semua" name="kd_rek_gabung">
					                    	<option value=""></option>
					                    	<?php foreach($rekening as $rr){?>
					                    	<option <?php if($rr->kd_rek_gabung==$kd_rek_gabung){echo "selected";}?> value="<?= $rr->kd_rek_gabung ?>"><?= $rr->kd_rek_gabung.' - '.$rr->nm_rek_5?></option>
					                    	<?php } ?>
					                    </select>
	                    			</div>
	                    		</div>
	                    		<div class="col-md-4">
	                    			<div class="form-group">
	                    				<label ><strong>Pencarian</strong></label>
	                    				<div class="input-group">
											<input type="hidden"  name="kd_skpd" value="<?= $kd_skpd?>">
											<input type="hidden"  name="nm_sub_unit" value="<?= $nm_sub_unit?>">
				                        	<input type="text" class="form-control form-control-xs" name="q" value="<?php echo $q; ?>" placeholder="Pencarian">
				                            <span class="input-group-btn">
				                            <div class="btn-group">
				                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
				                                <?php if ($q <>''||$kd_rek_gabung<>'' || $tanggal1 <> '' || $tanggal2 <> '')  { ?>
				                                    <a href="<?php echo $action; ?>" class="btn btn-warning"><i class="mdi mdi-close"></i></a>
				                              <?php }
				                                 ?>
												 <!-- <?= anchor($cetak,'<i class="mdi mdi-print"></i> Cetak','class="btn btn-success"')?> -->
				                            </div>
				                        </span>
				                    </div>
	                    			</div>
	                    		</div>
	                    	</div>
	                </form>
	                </div>
            	</div>
            </div>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-bordered">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>Tahun</th>
								<th>No Bukti</th>
								<th>Keterangan</th>
								<th>Nilai</th>
								<th>Rekening</th>
								<th>D / K</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($pajak_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo ++$start ?></td>
								<td><?php echo $rk->Tahun ?></td>
								<td class="cell-detail"><span><?php echo $rk->No_Bukti ?></span><span class="cell-detail-description"><?php echo date_indo(date('Y-m-d',strtotime($rk->Tgl_Bukti))) ?></span></td>
								<td><?php echo $rk->Keterangan ?></td>
								<td align="right"><?php echo number_format($rk->Nilai,'2',',','.') ?></td>
								<td class="cell-detail"><span><?php echo $rk->Kd_Rek_Gabung ?></span><span class="cell-detail-description"><?php echo $rk->Nm_Rek_5 ?></span></td>
								<td align="center"><?php echo $rk->D_K ?></td>
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					</div>
					<button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>

					<div class="float-right">
						<?php echo $pagination ?>
					</div>
					<div class="float-right">
                        <button  class="btn  btn-space btn-success page-link" disabled>Total Nilai : <?php echo $nilai ?></button>
                    </div>
            </div>
        </div><!-- end card-->
    </div>
</div>
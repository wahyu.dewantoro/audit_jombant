<!DOCTYPE html>
<html>

<head>
    <title>PROGRES SIPIL</title>
    <style type="text/css">
        body {
            font-family: "Serif";
            font-size: 13;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
        }

        .center {
            text-align: justify;
            letter-spacing: 0;
        }

        .left {
            margin-left: 500px
        }


        .table {
            border-collapse: collapse;
        }

        .table tbody tr {
            border: none;
        }

        .table thead th {
            border: none;

        }

        .table tfoot td {
            border: none;
        }
    </style>
</head>

<body>
    <h3>Detail Kegiatan</h3>

    <table width="100%" border='0'>
        <tr>
            <th align="left">OPD</th>
            <td style="font-size:9px">></td>
            <td style="width: 20%; white-space: nowrap;" ><?= $kode_skpd ?> <?= $nama_skpd ?></td>
            <td style="width: 17%; white-space: nowrap;" class="item"><strong>Pagu Anggaran</strong></td>
            <td style="font-size:9px">></td>
            <td><?= 'Rp.' . angka($pagu_anggaran) ?></td>
            <td class="item"><strong>Nomer SPK</strong></td>
            <td style="font-size:9px">></td>
            <td><?= $no_spk ?></td>
        </tr>
        <tr>
            <td class="item"><strong>Jenis Belanja</strong></td>
            <td style="font-size:9px">></td>
            <td><?= $kode_belanja ?> <?= $jenis_belanja ?></td>
            <td class="item"><strong>Harga Perkiraan Sendiri</strong></td>
            <td style="font-size:9px">></td>
            <td><?= 'Rp.' . angka($harga_perkiraan) ?></td>
            <td class="item"><strong>Tanggal SPK</strong></td>
            <td style="font-size:9px">></td>
            <td><?= date_indo($tanggal_spk) ?></td>
        </tr>
        <tr>

            <td class="item"><strong>Kegiatan</strong></td>
            <td style="font-size:9px">></td>
            <td><?= $nama_kegiatan ?></td>
            <td class="item"><strong>pelaksana</strong></td>
            <td style="font-size:9px">></td>
            <td><?= $pelaksana ?></td>
            <td class="item"><strong>Mulai Pekerjaan SPK</strong></td>
            <td style="font-size:9px">></td>
            <td><?= date_indo($mulai_pekerjaan_spk) ?></td>
        </tr>
        <tr>

            <td class="item"><strong>Nama PPK</strong></td>
            <td style="font-size:9px">></td>
            <td><?= $nama_ppk ?></td>
            <td class="item"><strong>No SPMK</strong></td>
            <td style="font-size:9px">></td>
            <td><?= $no_spmk ?></td>
            <td class="item"><strong>Nilai SPK</strong></td>
            <td style="font-size:9px">></td>
            <td>Rp.<?= angka($nilai_spk) ?></td>
        </tr>
    </table>

    <h3>Data Progres</h3>
         <table width="100%" border="1" cellpadding="0" cellspacing="0">
    <thead>
             <tr>
               <th width="5%">No</th>
               <th>Tanggal</th>
               <th>Nilai</th>
               <th width="15%">File</th>
               <th>Deskripsi</th>
               <th>Akumulasi</th>
               <th>Persentase</th>
             </tr>
           </thead>
           <tbody>
             <?php if (!empty($progresa)) { ?>
               <?php $akm = 0;
                  $dd = 1;
                  foreach ($progresa as $pp) {
                    $akm += $pp->nilai_progres; ?>
                 <tr>
                   <td align="center"><?= $dd ?> </td>
                   <td><?= mediumdate_indo(date('Y-m-d', strtotime($pp->tanggal_progres))) ?></td>
                   <td><?= angka($pp->nilai_progres) ?></td>
                   <td>
                     <?php $rft = $this->db->query("select * from PEKERJAAN_SIPIL_LAPORAN_FILE where PEKERJAAN_SIPIL_LAPORAN_ID='" . $pp->id . "'")->result();
                          foreach ($rft as $trf) { ?>
                       <?php if ($trf->nama_file <> '') {
                                echo anchor('progressipil/download?file=' . urlencode(str_replace('/', 'zz',  $trf->nama_file)), '<i class="mdi mdi-attachment-alt"></i> Dokumen');
                              } else {
                                echo "-";
                              } ?> <br>
                     <?php } ?>
                   </td>
                   <td><?= $pp->deskripsi_progres <> '' ? $pp->deskripsi_progres : ''; ?></td>
                   <td align="right"><?= angka($akm) ?></td>
                   <td align="right"><?=$pp->persentasi_total; ?> %</td>
                 </tr>
               <?php $dd++;
                  } ?>
             <?php } else { ?>
               <tr>
                 <td colspan="6">Data tidak ada</td>
               </tr>
             <?php } ?>
           </tbody>
    </table>
    <canvas id="canvas"></canvas>
</body>
<script src="<?= base_url() ?>Chart.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url() ?>utils.js"></script>

<?php
if ($nilai_addedum > 0) {
	$tt = $nilai_addedum;
} else {
	$tt = $nilai_spk;
}
?>
<script>
	var lineChartData = {

		datasets: [{
			label: 'realisasi (Dalam Persentase)',
			borderColor: window.chartColors.red,
			backgroundColor: window.chartColors.red,
			fill: false,
			data: [
				<?php $tmp = 0;
				foreach ($progres as $rl) {
					$tmp += $rl->persentasi_total; ?>
					<?= $tmp ?>,
				<?php } ?>
				<?php if ($tmp != $tt) { ?>
					null,
				<?php } ?>
			],
			yAxisID: 'y-axis-1',
		},{
			label: 'Target (Dalam Persentase)',
			borderColor: window.chartColors.blue,
			backgroundColor: window.chartColors.blue,
			fill: false,
			fill: false,
			data: [
				<?php $tmp = 0;
				foreach ($progresjj as $rl) {
					$tmp += $rl->persentase; ?>
					<?= $tmp ?>,
				<?php } ?>
				<?php if ($tmp != $tt) { ?>
					null,
				<?php } ?>
			],
			yAxisID: 'y-axis-1',
		}],
		labels: [
			<?php foreach ($progresj as $zx) { ?> '<?= gabungTanggal2($zx->awal_minggu,$zx->akhir_minggu) ?>',
			<?php } ?>
			<?php if ($tmp != $tt) { ?> ''
			<?php } ?>
		],
        options: {
            animation: {
                onComplete: function () {
                    window.JSREPORT_READY_TO_START = true
                }
            }
        }
	};
	window.onload = function() {
		var ctx = document.getElementById('canvas').getContext('2d');
		window.myLine = Chart.Line(ctx, {
			data: lineChartData,
			options: {
				responsive: true,
				hoverMode: 'index',
				stacked: false,
				title: {
					display: true,
					text: 'Progres Pekerjaan (Nilai Kontrak  : <?= angka($tt) ?>)'
				},
				scales: {
					yAxes: [{
							type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
							display: true,


							position: 'left',
							id: 'y-axis-1',
							ticks: {
								suggestedMin: 0,
								suggestedMax: 	100,
							}
						},

					],
				}
			}
		});
	};

	document.getElementById('randomizeData').addEventListener('click', function() {
		lineChartData.datasets.forEach(function(dataset) {
			dataset.data = dataset.data.map(function() {
				return randomScalingFactor();
			});
		});

		window.myLine.update();
	});
</script>
</html>

<table width="100%">
    <tr>
        <td align="left" width="20%">OPD</td>
        <td align="left">:</td>
        <td align="left"><?=$header->nama_skpd?></td>
    </tr>
    <tr>
        <td align="left">Program</td>
        <td align="left">:</td>
        <td align="left"><?=$header->ket_prog?></td>
    </tr>
    <tr>
        <td align="left">Kegiatan</td>
        <td align="left">:</td>
        <td align="left"><?=$header->ket_keg?></td>
    </tr>
    <tr>
        <td align="left">Sub Kegiatan</td>
        <td align="left">:</td>
        <td align="left"><?=$header->sub_kegiatan?></td>
    </tr>
    <tr>

    
</table>
<table class="table  table-striped table-hover table-bordered" id="myTable" >
    <thead>
        <tr>
            <th>No</th>
            <th>Minggu</th>
            <th>Tanggal Awal</th>
            <th>Tanggal Akhir</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 1;
                            foreach ($res as $rk) { ?>
        <tr>
            <td align="center"><?= $no ?></td>
            <td><?= $rk->minggu ?></td>
            <td align="left"><?= date_indo($rk->awal_minggu) ?></td>
            <td align="left"><?= date_indo($rk->akhir_minggu) ?></td>
        </tr>
        <?php $no++;
                            } ?>
    </tbody>
</table>

<script src="<?= base_url() ?>assets/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.html5.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.flash.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.print.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons/js/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/datatables/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>

      <script type="text/javascript">
$(document).ready( function () {
    $('#myTable').DataTable({
        "lengthChange": false,
        "searching": false
    });
} );
    </script>
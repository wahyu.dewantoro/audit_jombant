<script>
    $(document).ready(function() {
        console.log("ready!");
        $('#formprogres').submit(function() {
            $('.loading').show();
        });

        $('#persentasi_total').on('change, keyup', function() {
            var currentInput = $('#persentasi_total').val();
            // alert(currentInput);
            var fixedInput = currentInput.replace(/[A-Za-z!@#$%^&*()]/g, '');
            $('#persentasi_total').val(fixedInput);
            // console.log(fixedInput);
        });
    });
</script>
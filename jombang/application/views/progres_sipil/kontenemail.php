<table width="100%">
    <tbody>
        <tr>
            <td align="center" rowspan="4"><img width="70px" src="<?= base_url().'jombang.jpg'  ?>" ></td>
            <td align="center"><h3>PEMERINTAH KABUPATEN JOMBANG</h3></td>
        </tr>
        <tr>
            <td align="center"><h3>INSPEKTORAT </h3></td>
        </tr>
        <tr>
            <td align="center">ALAMAT : JL.  GATOT SUBROTO 169  TEPL. (0321) 861424 Kode Pos 61411</td>
        </tr>
        <tr>
            <td align="center">J O M B A N G</td>
        </tr>
    </tbody>
</table>
<hr>
<b>Pemberitahuan Hasil Pekerjaan Konstruksi</b>
<p>Sehubungan dengan kontrak pekerjaan <b><?= $sub_kegiatan ?></b> antara PPK <?= $nama_ppk ?></b> dengan Penyedia <b?<?= $nama_pengawas ?></b> dengan kontrak / Surat Perjanjian Kerja No. <?= $no_sk_pengawas ?> tanggal <?= date_indo($tgl_sk_pengawas) ?> <?php if ($no_addedum_pengawas <> '') { ?> dan addendum kontrak No. <?= $no_addedum_pengawas ?> tanggal <?= date_indo($tgl_addedum_pengawas) ?> <?php } ?> </p> <p>1. Berdasarkan Kontrak, kontrak dinyatakan kritis apabila</p>
<ol type="a">
    <li>dalam periode I (rencana fisik pelaksanaan 0% - 70% dari Kontrak), realisasi fisik pelaksanaan terlambat lebih besar 10% dari rencana.</li>
    <li>dalam periode II (rencana fisik pelaksanaan 70% - 100% dari Kontrak), realisasi fisik pelaksanaan terlambat lebih besar 5% dari rencana. </li>
    <li>rencana fisik pelaksanaan 70% - 100% dari Kontrak, realisasi fisik pelaksanaan terlambat kurang dari 5% dari rencana dan akan melampaui tahun anggaran berjalan.</li>
</ol>
<p>Berdasarkan evaluasi data yang telah masuk ke sistem E-Audit, diketahui bahwa terdapat keterlambatan pekerjaan sebesar <?= $deviasi ?> % dari rencana <?= $perencanaan ?> % dengan realisasi <?= $realisasi ?> %. Kondisi ini telah melebihi ambang batas maksimal sebesar <?php if ($perencanaan <= 70 && $deviasi > 10) {
                                                                                                                                                                                                                                                                                    echo "10";
                                                                                                                                                                                                                                                                                }
                                                                                                                                                                                                                                                                                if ($perencanaan > 70 && $deviasi > 5) {
                                                                                                                                                                                                                                                                                    echo '5';
                                                                                                                                                                                                                                                                                } ?> %</p> keterlambatan dari rencana.</p>


<hr>
<b>Resume</b>

<table>
    <tr>
        <td>OPD</td>
        <td width="1%">:</td>
        <td><?= $nama_skpd ?></td>
    </tr>
    <tr>
        <td>Program</td>
        <td width="1%">:</td>
        <td><?= $ket_prog ?></td>
    </tr>
    <tr>
        <td>Kegiatan</td>
        <td width="1%">:</td>
        <td><?= $ket_keg ?></td>
    </tr>
    <tr>
        <td>Sub Kegiatan</td>
        <td width="1%">:</td>
        <td><?= $sub_kegiatan ?></td>
    </tr>
    <tr>
        <td>Pekerjaan</td>
        <td width="1%">:</td>
        <td>Minggu ke <?php echo  $minggu_ke . ' / ' . gabungTanggal2($awal_minggu, $akhir_minggu)  ?> </td>
    </tr>

    <tr>
        <td>Perencanaan Progres</td>
        <td width="1%">:</td>
        <td><?= $perencanaan  ?> %</td>
    </tr>
    <tr>
        <td>Progres Pekerjaan</td>
        <td width="1%">:</td>
        <td><?= $realisasi  ?>%</td>
    </tr>
    <tr>
        <td>Deviasi</td>
        <td width="1%">:</td>
        <td>- <?= $deviasi  ?>%</td>
    </tr>
</table>
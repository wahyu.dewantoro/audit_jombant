<style>
  @media print {
    body * {
      visibility: hidden;
    }

    #section-to-print,
    #section-to-print * {
      visibility: visible;
    }

    #section-to-print {
      position: absolute;
      left: 0;
      top: 0;
    }
  }
</style>

<div class="row hidden-print" id="section-to-print">
  <div class="col-md-8">
    <div class="tab-container">
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item"><a class="nav-link active" href="#home" data-toggle="tab" role="tab"><i class="mdi mdi-file-text"></i> Pekerjaan</a></li>
        <li class="nav-item"><a class="nav-link" href="#profile" data-toggle="tab" role="tab"><i class="mdi mdi-library"></i> Jasa Konsultansi</a></li>
        <li class="nav-item"><a class="nav-link" href="#messages" data-toggle="tab" role="tab"><i class="mdi mdi-eye"></i> Jasa Konstruksi</a></li>
        <li class="nav-item"><a class="nav-link" href="#tatausaha" data-toggle="tab" role="tab"><i class="mdi mdi-file-text"></i> Administrasi</a></li>
        <li class="nav-item"><a class="nav-link" href="#jadwal" data-toggle="tab" role="tab"><i class="mdi mdi-calendar"></i> Perencanaan</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane" id="jadwal" role="tabpanel">
          <div class="row">
            <?php foreach ($perencanaan as $pr) { ?>
              <div class="col-md-6">
                <div class="card card-border card-contrast">
                  <div class="card-header card-header-contrast">
                    Minggu Ke <?= $pr->minggu ?>
                  </div>
                  <div class="card-body">
                    <p>Pekerjaan <?= mediumdate_indo($pr->awal_minggu) ?> s/d <?= mediumdate_indo($pr->akhir_minggu) ?></p>
                    <p><b>Kumulatif perencanaan</b> <?= $pr->persentase <> '' ? $pr->persentase : 0; ?>%</p>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </div>
        <div class="tab-pane" id="tatausaha" role="tabpanel">

        </div>
        <div class="tab-pane active" id="home" role="tabpanel">
          <?php if (cekjadwal($id) == 0) { ?>
            <div class="alert alert-warning alert-dismissible" role="alert">
              <!-- <button class="close" type="button" data-dismiss="alert" aria-label="Close"><span class="mdi mdi-close" aria-hidden="true"></span></button> -->
              <div class="icon"><span class="mdi mdi-alert-triangle"></span></div>
              <div class="message"><strong>Warning!</strong> Perencanaan progres mingguan belum di isi. {!! anchor('sipil/jadwal/' . acak($id), '<i class="mdi mdi-edit"></i> Lengkapi', 'class="btn btn-sm btn-success"')!!}</div>
            </div>
          <?php } ?>

          <table class="no-border no-strip skills">
            <tbody class="no-border-x no-border-y">
              <tr>
                <td class="item">OPD</td>
                <td class="icon">:</td>
                <td><?= $kode_skpd ?> <?= $nama_skpd ?></td>
              </tr>
              <tr>
                <td class="item">Program</td>
                <td class="icon">:</td>
                <td><?= $ket_prog ?></td>
              </tr>
              <tr>
                <td class="item">Kegiatan</td>
                <td class="icon">:</td>
                <td><?= $ket_keg ?></td>
              </tr>
              <tr>
                <td class="item">Sub Kegiatan</td>
                <td class="icon">:</td>
                <td><?= $sub_kegiatan ?></td>
              </tr>
              <tr>
                <td class="item"> Anggaran</td>
                <td class="icon">:</td>
                <td><?= 'Rp.' . angka($pagu_anggaran) ?></td>
              </tr>
              <tr>
                <td class="item">Harga Perkiraan Sendiri</td>
                <td class="icon">:</td>
                <td><?= 'Rp.' . angka($harga_perkiraan) ?></td>
              </tr>
            </tbody>
          </table>

        </div>
        <div class="tab-pane" id="profile" role="tabpanel">

          <table class="no-border no-strip skills">
            <tbody class="no-border-x no-border-y">
              <tr>
                <td class="item">Nama</td>
                <td class="icon">:</td>
                <td><?= $nama_konsultansi ?></td>
              </tr>
              <tr>
                <td class="item">No Kontrak</td>
                <td class="icon">:</td>
                <td><?= $no_sk_konsultansi ?></td>
              </tr>
              <tr>
                <td class="item">Tanggal Kontrak</td>
                <td class="icon">:</td>
                <td><?= $tgl_sk_konsultansi ?></td>
              </tr>
              <tr>
                <td class="item">Nilai Kontrak</td>
                <td class="icon">:</td>
                <td>Rp.<?= angka($nilai_konsultansi) ?></td>
              </tr>
              <tr>
                <td class="item">Periode Kontrak</td>
                <td class="icon">:</td>
                <td><?= date_indo($tgl_mulai_konsultansi) . ' s/d ' . date_indo($tgl_selesai_konsultansi) ?></td>
              </tr>
            </tbody>
          </table>

          <?php if ($no_addedum_konsultansi) { ?>
            <hr>
            <table class="no-border no-strip skills">
              <tbody class="no-border-x no-border-y">

                <tr>
                  <td class="item">No Addedum</td>
                  <td class="icon">:</td>
                  <td><?= $no_addedum_konsultansi ?></td>
                </tr>
                <tr>
                  <td class="item">Tanggal Addedum</td>
                  <td class="icon">:</td>
                  <td><?= $tgl_addedum_konsultansi ?></td>
                </tr>
                <tr>
                  <td class="item">Nilai Addedum</td>
                  <td class="icon">:</td>
                  <td>Rp.<?= angka($nilai_addedum_konsultansi) ?></td>
                </tr>
                <tr>
                  <td class="item">Periode Addedum</td>
                  <td class="icon">:</td>
                  <td><?= date_indo($tgl_mulai_addedum_konsultansi) . ' s/d ' . date_indo($tgl_selesai_addedum_konsultansi) ?></td>
                </tr>
              </tbody>
            </table>

          <?php } ?>

        </div>
        <div class="tab-pane" id="messages" role="tabpanel">
          <table class="no-border no-strip skills">
            <tbody class="no-border-x no-border-y">
              <tr>
                <td class="item">Nama</td>
                <td class="icon">:</td>
                <td><?= $nama_pengawas ?></td>
              </tr>
              <tr>
                <td class="item">No Kontrak</td>
                <td class="icon">:</td>
                <td><?= $no_sk_pengawas ?></td>
              </tr>
              <tr>
                <td class="item">Tanggal Kontrak</td>
                <td class="icon">:</td>
                <td><?= $tgl_sk_pengawas ?></td>
              </tr>
              <tr>
                <td class="item">Nilai Kontrak</td>
                <td class="icon">:</td>
                <td>Rp.<?= angka($nilai_pengawas) ?></td>
              </tr>
              <tr>
                <td class="item">Periode Kontrak</td>
                <td class="icon">:</td>
                <td><?= date_indo($tgl_mulai_pengawas) . ' s/d ' . date_indo($tgl_selesai_pengawas) ?></td>
              </tr>
            </tbody>
          </table>

          <?php if ($no_addedum_pengawas) { ?>
            <hr>
            <table class="no-border no-strip skills">
              <tbody class="no-border-x no-border-y">

                <tr>
                  <td class="item">No Addedum</td>
                  <td class="icon">:</td>
                  <td><?= $no_addedum_pengawas ?></td>
                </tr>
                <tr>
                  <td class="item">Tanggal Addedum</td>
                  <td class="icon">:</td>
                  <td><?= $tgl_addedum_pengawas ?></td>
                </tr>
                <tr>
                  <td class="item">Nilai Addedum</td>
                  <td class="icon">:</td>
                  <td>Rp.<?= angka($nilai_addedum_pengawas) ?></td>
                </tr>
                <tr>
                  <td class="item">Periode Addedum</td>
                  <td class="icon">:</td>
                  <td><?= date_indo($tgl_mulai_addedum_pengawas) . ' s/d ' . date_indo($tgl_selesai_addedum_pengawas) ?></td>
                </tr>
              </tbody>
            </table>

          <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card  card-border-color card-border-color-primary card-table">
      <div class="card-header ">
        Data Progres
        <div class="tools ">
          <!-- <button class="btn btn-danger toCanvas"><i class="mdi mdi-image"></i> Capture</button> -->

          <a class="btn btn-xs btn-success" href="<?= base_url('progressipil/cetakprogres/' . acak($id)) ?>"><i class="mdi mdi-print"></i> Cetak</a>
          <?php
          if ($akses['create'] == 1) {
            echo anchor('progressipil/progres/' . acak($id), '<i class="mdi mdi-calendar-check"></i> Progress', ' class="btn btn-xs btn-warning"');
          }  ?>

          <?php //anchor('progressipil/cetak/' . acak($id), '<i class="mdi mdi-collection-pdf"></i> Cetak', 'class="btn btn-danger"') 
          ?>
        </div>
      </div>
      <div class="card-body">

        <table class="table table-striped table-bordered" border="1" colpadding="0" rowpadding="0">
          <thead>
            <tr>
              <th>No</th>
              <th>Minggu</th>
              <th>Kumulatif</th>
              <th></th>

            </tr>
          </thead>
          <tbody>
            <?php if (!empty($progresa)) { ?>
              <?php $akm = 0;
                $dd = 1;
                foreach ($progresa as $pp) {

                  ?>
                <tr>
                  <td align="center"><?= $dd ?> </td>
                  <td class="cell-detail"> Ke <?= $pp->minggu_ke ?>
                    <span class="cell-detail-description"> <?= gabungTanggal2($pp->awal_minggu, $pp->awal_minggu) ?> </span>
                  </td>
                  <td class="cell-detail">Realisasi : <?= $pp->realisasi ?> %
                    <span class="cell-detail-description">Perencanaan : <?= $pp->perencanaan ?> % </span>
                    <span class="cell-detail-description">Deviasi : <?= $pp->deviasi > 0 ? $pp->deviasi : 0 ?> % </span>
                  </td>
                  <th></th>

                </tr>
              <?php $dd++;
                } ?>
            <?php } else { ?>
              <tr>
                <td colspan="7">Data tidak ada</td>
              </tr>
            <?php } ?>
          </tbody>
        </table>


      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="card  card-border-color card-border-color-primary card-table">
      <div class="card-header ">
        <div class="tools">
          <!-- <button class="btn btn-danger" id="downloadPdf" onclick="print()" download="image.png">Cetak</button> -->
        </div>
      </div>
      <div class="card-body" id="reportPage">
        <canvas id="canvas"></canvas>
      </div>
    </div>
  </div>
</div>
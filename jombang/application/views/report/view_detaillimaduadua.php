<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">                       
        <div class="card mb-3">
            <div class="card-body">
            
                  <table class="table table-striped table-hover table-bordered" id="table1">
                    <thead>
                      <tr>
                        <th width="15x">No</th>
                        <th>Tanggal SP2D</th>
                        <th>No SP2D</th>
                        <th>Jenis</th>
                        
                        <th>Rekening</th>
                        <th>Nilai</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; foreach($data as $rk){?>
                        <tr>
                            <td align="center"><?= $no++ ?></td>
                             <td><?= date_indo(date('Y-m-d',strtotime($rk->tgl_sp2d))) ?> </td>
                             <td><?= $rk->no_sp2d ?> </td>
                             <td align="center"><?= $rk->jenis_sp2d ?> </td>
                             <td class="cell-detail"><?= $rk->kd_rek_gabung ?> <span class="cell-detail-description"><?= $rk->nm_rek_5 ?> </span></td>
                             
                             <td align="right"><?= number_format($rk->nilai,'2',',','.') ?> </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                  </table>
            </div>                                                      
        </div><!-- end card-->                  
    </div>
</div>
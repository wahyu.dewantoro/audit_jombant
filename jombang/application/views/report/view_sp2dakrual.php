<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            <div class="table-responsive">
                  <table class="table table-striped table-hover table-bordered" id="table1">
                    <thead>
                      <tr>
                        <th width="15x">No</th>
                        <th>Kode</th>
                        <th>Rekening</th>
                        <th>Nilai</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; foreach($data as $rk){?>
                        <tr>
                             <td align="center"><?= $no++ ?></td>
                             <td><?= $rk->kd_rek_gabung ?></td>
                             <td><?= $rk->nm_rek_5 ?></td>
                             <td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
                             <td align="center"><?= anchor('report/detailakrualsp2d?q='.urlencode($rk->kd_rek_gabung),'<i class="mdi mdi-search"></i>','class="btn btn-xs btn-primary"') ?></td>
                        </tr> 
                        <?php } ?>
                    </tbody>
                  </table>
                  </div>
            </div>
        </div><!-- end card-->
    </div>
</div>
<?php

// Panggil class PHPExcel nya
$csv = new PHPExcel();

// Settingan awal fil excel

// Buat header tabel nya pada baris ke 1
$csv->setActiveSheetIndex(0)->setCellValue('A1', "NO;"); // Set kolom A1 dengan tulisan "NO"
$csv->setActiveSheetIndex(0)->setCellValue('B1', "Tahun"); // Set kolom B1 dengan tulisan "NIS"
$csv->setActiveSheetIndex(0)->setCellValue('C1', "Jurnal"); // Set kolom C1 dengan tulisan "NAMA"
$csv->setActiveSheetIndex(0)->setCellValue('D1', "Tanggal Bukti"); // Set kolom E1 dengan tulisan "TELEPON"
$csv->setActiveSheetIndex(0)->setCellValue('E1', "keterangan"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('F1', "Akrual 1-4"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('G1', "Akrual 5"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('H1', "Debet"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('I1', "Kredit"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"

$no = 1; // Untuk penomoran tabel, di awal set dengan 1
$numrow = 2; // Set baris pertama untuk isi tabel adalah baris ke 2
$nm_unit="";
foreach ($saldoawal_data as $rk) {

    $csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
    $csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $rk->Tahun);
    $csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $rk->Nm_Jurnal );
    $csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, date_indo(date('Y-m-d',strtotime($rk->Tgl_Bukti))));
    $csv->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $rk->Keterangan);
    $csv->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $rk->Akun_Akrual_1." ".$rk->Nm_Akrual_1." ".$rk->Akun_Akrual_2." ".$rk->Nm_Akrual_2." ".$rk->Akun_Akrual_3." ".$rk->Nm_Akrual_3." ".$rk->Akun_Akrual_4." ".$rk->Nm_Akrual_4);
    $csv->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $rk->Akun_Akrual_5." ".$rk->Nm_Akrual_5);
    $csv->setActiveSheetIndex(0)->setCellValue('H'.$numrow, number_format($rk->Saldo_Awal_Debet,'2',',','.'));
    $csv->setActiveSheetIndex(0)->setCellValue('I'.$numrow, number_format($rk->Saldo_Awal_Kredit,'2',',','.'));

    $no++; // Tambah 1 setiap kali looping
    $numrow++; // Tambah 1 setiap kali looping
}

// Set orientasi kertas jadi LANDSCAPE
$csv->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

// Set judul file excel nya
// $csv->getActiveSheet(0)->setTitle("Neraca");
// $csv->setActiveSheetIndex(0);

// Proses file excel
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment; filename=Saldo Awal ".$kd_skpd." ".str_replace('.',' ', str_replace(',', '', $nm_sub_unit)).".csv");
header('Cache-Control: max-age=0');

$write = new PHPExcel_Writer_CSV($csv);
$write->save('php://output');
?>
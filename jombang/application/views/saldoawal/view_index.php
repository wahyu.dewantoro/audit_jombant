<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
                <form action="<?php echo site_url('saldoawal/index'); ?>"  method="get">
                    <div class="row">
                    <div class="col-md-4">
                        <select class="form-control form-control-sm select2" name="q" data-placeholder="Pilih SKPD/OPD">
                            <option value=""></option>
                            <?php foreach($subunit as $su){?>
                            <option <?php if($su->nm_sub_unit==$q){echo "selected";}?> value="<?= $su->nm_sub_unit ?>"><?= $su->label ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <div class="btn-group">
                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                                <?php if ($q <> '')  { ?>
                                    <a href="<?php echo site_url('saldoawal'); ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
                              <?php }
                                    if($akses['create']==1){
                                     echo anchor(site_url('saldoawal/create'),'<i class="mdi mdi-plus"></i> Tambah', 'class="btn btn-success btn-sm"');
                                    }
                                ?>
                        </div>
                    </div>
                    </div>
                </form>

                	<div class="table-reaponsive">
                    <table class="table  table-bordered table-striped table-hover" id="table2">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th width="100px">Kode SKPD</th>
								<th>Sub Unit</th>
								<th>Debet</th>
								<th>Kredit</th>
                                <th width="10px">Detail</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($saldoawal_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo ++$start ?></td>
								<td><?= $rk->kd_skpd ?></td>
								<td><?= $rk->nm_sub_unit ?></td>
								<td align="right"><?= number_format($rk->debet,'2',',','.'); ?></td>
								<td align="right"><?= number_format($rk->kredit,'2',',','.'); ?></td>
                                <td align="center">
                                    <form action="<?= base_url().'saldoawal/read' ?>">
                                        <input type='hidden' name='kd_skpd' value='<?= $rk->kd_skpd ?>' >
                                        <input type='hidden' name='nm_sub_unit' value='<?= $rk->nm_sub_unit ?>' >
                                        <button class="btn btn-xs btn-primary"><i class='mdi mdi-search'></i></button>
                                    </form>
                                </td>
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					</div>
					<!-- <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button> -->

					<!-- <div class="float-right">
						<?php echo $pagination ?>
					</div> -->
                    <!-- <div class="float-right">
                        <div class="btn-group">
                        <button  class="btn page-link btn-space btn-success" disabled>Total Debet : <?php echo $debet ?></button>
                        <button  class="btn page-link btn-space btn-warning" disabled>Total Kredit : <?php echo $kredit ?></button>
                        </div>
                    </div> -->
            </div>
        </div><!-- end card-->
    </div>
</div>
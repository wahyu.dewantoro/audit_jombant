<script src="<?= base_url() ?>assets/lib/raphael/raphael.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/morrisjs/morris.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		Morris.Line({
			element: 'bismillah',
			parseTime: false,
			data: [
				<?php
				$a = 0;
				$b = 0;
				foreach ($realisasi as $ra) {
					if ($ra->pendapatan != 0) {
						$a += $ra->pendapatan;
					} else {
						$a = 0;
					}

					if ($ra->belanja != 0) {
						$b += $ra->belanja;
					} else {
						$b = 0;
					}

					?> {
						y: '<?= $ra->nama_bulan ?>',
						a: <?= $a ?>,
						b: <?= $b ?>
					},
				<?php } ?>
			],
			xkey: 'y',
			ykeys: ['a', 'b'],
			labels: ['Pendapatan', 'Belanja'],
			yLabelFormat: function(y) {
				var d = y / 1000000000;
				var dasar = parseFloat(d.toString()).toLocaleString('en');
				var bahan = dasar.toString();
				var satu = bahan.replace('.', '#');
				var dua = satu.replace(',', '.');
				var tiga = dua.replace('#', ',');
				return tiga;
			}
		});

		var heights = $(".card-contrast").map(function() {
				return $(this).height();
			}).get(),
			maxHeight = Math.max.apply(null, heights);
		$(".card-contrast").height(maxHeight);
	});
</script>
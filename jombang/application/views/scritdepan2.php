<script src="<?= base_url() ?>assets/lib/raphael/raphael.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>assets/lib/morrisjs/morris.min.js" type="text/javascript"></script>
<script type="text/javascript">
var $arrColors = ['#34495E', '#26B99A',  '#666', '#3498DB'];
	$(document).ready(function() {
		Morris.Bar({
			element: 'grafik1',
			parseTime: false,
			data: [
				<?php
				$a = 0;
				$b = 0;
				foreach ($grafik1 as $gf1) {

					?> {
						y: '<?= $gf1->Nama_Desa ?> <?= str_replace('Kecamatan ','',$gf1->Nama_Kecamatan) ?>' ,
						a: <?= $gf1->nilai ?>
					},
				<?php } ?>
			],
			xkey: 'y',
			ykeys: ['a'],
			labels: ['Pendapatan'],
			yLabelFormat: function(y) {
				var d = y / 1000000000;
				var dasar = parseFloat(d.toString()).toLocaleString('en');
				var bahan = dasar.toString();
				var satu = bahan.replace('.', '#');
				var dua = satu.replace(',', '.');
				var tiga = dua.replace('#', ',');
				return tiga;
			}
		});

		var heights = $(".card-contrast").map(function() {
				return $(this).height();
			}).get(),
			maxHeight = Math.max.apply(null, heights);
		$(".card-contrast").height(maxHeight);
		Morris.Bar({
			element: 'grafik2',
			parseTime: false,
			data: [
				<?php
				$a = 0;
				$b = 0;
				foreach ($grafik2 as $gf2) {

					?> {
						y: '<?= $gf2->Nama_Desa ?> <?= str_replace('Kecamatan ','',$gf2->Nama_Kecamatan) ?>' ,
						a: <?= $gf2->nilai ?>
					},
				<?php } ?>
			],
			xkey: 'y',
			ykeys: ['a'],
			labels: ['Pendapatan'],
			barColors:['#34495E'],
			yLabelFormat: function(y) {
				var d = y / 1000000000;
				var dasar = parseFloat(d.toString()).toLocaleString('en');
				var bahan = dasar.toString();
				var satu = bahan.replace('.', '#');
				var dua = satu.replace(',', '.');
				var tiga = dua.replace('#', ',');
				return tiga;
			}
		});

		var heights = $(".card-contrast").map(function() {
				return $(this).height();
			}).get(),
			maxHeight = Math.max.apply(null, heights);
		$(".card-contrast").height(maxHeight);
	});
	Morris.Bar({
			element: 'grafik3',
			parseTime: false,
			data: [
				<?php
				$a = 0;
				$b = 0;
				foreach ($grafik3 as $gf3) {

					?> {
						y: '<?= $gf3->Nama_Desa ?> <?= str_replace('Kecamatan ','',$gf3->Nama_Kecamatan) ?>' ,
						a: <?= $gf3->nilai ?>
					},
				<?php } ?>
			],
			xkey: 'y',
			ykeys: ['a'],
			labels: ['Belanja'],
			barColors:['#3498DB'],
			yLabelFormat: function(y) {
				var d = y / 1000000000;
				var dasar = parseFloat(d.toString()).toLocaleString('en');
				var bahan = dasar.toString();
				var satu = bahan.replace('.', '#');
				var dua = satu.replace(',', '.');
				var tiga = dua.replace('#', ',');
				return tiga;
			}
		});

		var heights = $(".card-contrast").map(function() {
				return $(this).height();
			}).get(),
			maxHeight = Math.max.apply(null, heights);
		$(".card-contrast").height(maxHeight);
		Morris.Bar({
			element: 'grafik4',
			parseTime: false,
			data: [
				<?php
				$a = 0;
				$b = 0;
				foreach ($grafik4 as $gf4) {

					?> {
						y: '<?= $gf4->Nama_Desa ?> <?= str_replace('Kecamatan ','',$gf4->Nama_Kecamatan) ?>' ,
						a: <?= $gf4->nilai ?>
					},
				<?php } ?>
			],
			xkey: 'y',
			ykeys: ['a'],
			labels: ['Belanja'],
			barColors:['#666'],
			yLabelFormat: function(y) {
				var d = y / 1000000000;
				var dasar = parseFloat(d.toString()).toLocaleString('en');
				var bahan = dasar.toString();
				var satu = bahan.replace('.', '#');
				var dua = satu.replace(',', '.');
				var tiga = dua.replace('#', ',');
				return tiga;
			}
		});

		var heights = $(".card-contrast").map(function() {
				return $(this).height();
			}).get(),
			maxHeight = Math.max.apply(null, heights);
		$(".card-contrast").height(maxHeight);
		Morris.Bar({
			element: 'grafik5',
			parseTime: false,
			data: [
				<?php
				$a = 0;
				$b = 0;
				foreach ($grafik5 as $gf5) {

					?> {
						y: '<?= $gf5->Nama_Desa ?> <?= str_replace('Kecamatan ','',$gf5->Nama_Kecamatan) ?>' ,
						a: <?= $gf5->persen ?>
					},
				<?php } ?>
			],
			xkey: 'y',
			ykeys: ['a'],
			labels: ['Realisasi'],
			barColors:['#26B99A'],

		});

		var heights = $(".card-contrast").map(function() {
				return $(this).height();
			}).get(),
			maxHeight = Math.max.apply(null, heights);
		$(".card-contrast").height(maxHeight);
		Morris.Bar({
			element: 'grafik6',
			parseTime: false,
			data: [
				<?php
				$a = 0;
				$b = 0;
				foreach ($grafik6 as $gf6) {

					?> {
						y: '<?= $gf6->Nama_Desa ?> <?= str_replace('Kecamatan ','',$gf6->Nama_Kecamatan) ?>' ,
						a: <?= $gf6->persen ?>
					},
				<?php } ?>
			],
			xkey: 'y',
			ykeys: ['a'],
			labels: ['Realisasi'],
			barColors:['#9b5cd4'],
		});

		var heights = $(".card-contrast").map(function() {
				return $(this).height();
			}).get(),
			maxHeight = Math.max.apply(null, heights);
		$(".card-contrast").height(maxHeight);
</script>
<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Data_Pekerjaan.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>

<h2>Data Pekerjaan</h2>
<table class="table table-bordered table-striped" border="1" cellspacing="0" cellpadding="0">
    <thead>
        <tr>
            <th>No</th>
            <th>OPD</th>
            <th>Jenis Belanja</th>
            <th>Kegiatan</th>
            <th>Sub Kegiatan</th>
            <th>Pagu</th>
            <th>Nilai SPK</th>
            <th>HPS</th>
            <th>Persentase </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $sipil) {$persentase=number_format($sipil->nilai_spk/$sipil->harga_perkiraan*100,2,",",".");  ?>
            <tr>
                <td valign="top" width="10px" align="center"><?= ++$start ?></td>
                <td valign="top" class="cell-detail"><?= $sipil->kode_skpd; ?>
                    <span class="cell-detail-description"><?= $sipil->nama_skpd; ?></span></td>
                <td valign="top" class="cell-detail"><?= $sipil->kode_belanja; ?> <span class="cell-detail-description"><?= $sipil->jenis_belanja; ?></span></td>
                <td valign="top" ><?= $sipil->nama_kegiatan; ?></td>
                <td valign="top" ><?= $sipil->sub_kegiatan; ?></td>
                <td valign="top" align="right"><?= $sipil->pagu_anggaran; ?></td>
                <td valign="top" align="right"><?= $sipil->nilai_spk; ?></td>
                <td valign="top" align="right"><?= $sipil->harga_perkiraan; ?></td>
               <td valign="top" align="right"><?= $persentase ?> % </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
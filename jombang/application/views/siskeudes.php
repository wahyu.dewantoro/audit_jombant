
<div class="row">

    <div class=" col-lg-6 col-xl-6">
      <div class="card card-border card-contrast">
        <div style="background-color:#6da2f6; " class="card-header card-header-contrast"><img src="<?= base_url().'assets/img/belanjadua.png'?>" height="50px"> <b style="color:#fff;font-size:1.5rem">Pendapatan Terendah</b>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Desa</th>
                  <th>Kecamatan</th>
                  <th>Nilai</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; foreach($pendapatandua as $pena){?>
                  <tr>
                    <td class="text-center"><?=$no++?></td>
                    <td><?= $pena->Nama_Desa?></td>
                    <td><?= str_replace('Kecamatan ','',ucwords(strtolower($pena->Nama_Kecamatan))) ?></td>
                    <td align="center"><?= number_format($pena->nilai,2,',','.') ?></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <?= anchor('welcome/siskeudes?jenis='.urlencode('pendapatan').'&type='.urlencode('rendah'),"Lihat semua >>")?>
          </div>
        </div>
      </div>
    </div>
    <div class=" col-lg-6 col-xl-6">
      <div class="card card-border card-contrast">
        <div style="background-color:#3bbf5e; " class="card-header card-header-contrast"><img src="<?= base_url().'assets/img/incomedua.png'?>" height="50px"> <b style="color:#fff;font-size:1.5rem">Pendapatan Tertinggi</b>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Desa</th>
                  <th>Kecamatan</th>
                  <th>Nilai</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; foreach($pendapatan as $pen){?>
                  <tr>
                    <td class="text-center"><?=$no++?></td>
                    <td><?= $pen->Nama_Desa?></td>
                    <td><?= str_replace('Kecamatan ','',ucwords(strtolower($pen->Nama_Kecamatan))) ?></td>
                    <td align="center"><?= number_format($pen->nilai,2,',','.') ?></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>

            <?= anchor('welcome/siskeudes?jenis='.urlencode('pendapatan').'&type='.urlencode('tinggi'),"Lihat semua >>")?>

          </div>
        </div>
      </div>
    </div>

    <div class=" col-lg-6 col-xl-6">
      <div class="card card-border card-contrast">
        <div style="background-color:#f7c771; " class="card-header card-header-contrast"><img src="<?= base_url().'assets/img/income.png'?>" height="50px"> <b style="color:#fff; font-size:1.5rem">Belanja Terendah</b>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Desa</th>
                  <th>Kecamatan</th>
                  <th>Nilai</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; foreach($belanjadua as $bela){?>
                  <tr>
                    <td class="text-center"><?=$no++?></td>
                    <td><?= $bela->Nama_Desa?></td>
                    <td><?= str_replace('Kecamatan ','',ucwords(strtolower($bela->Nama_Kecamatan))) ?></td>
                    <td align="center"><?= number_format($bela->nilai,2,',','.') ?></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <?= anchor('welcome/siskeudes?jenis='.urlencode('belanja').'&type='.urlencode('rendah'),"Lihat semua >>")?>
          </div>
        </div>
      </div>
    </div>
    <div class=" col-lg-6 col-xl-6">
      <div class="card card-border card-contrast">
        <div style="background-color:#ed7065; " class="card-header card-header-contrast"><img src="<?= base_url().'assets/img/belanja.png'?>" height="50px"> <b style="color:#fff; font-size:1.5rem">Belanja Tertinggi</b>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Desa</th>
                  <th>Kecamatan</th>
                  <th>Nilai</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; foreach($belanja as $bel){?>
                  <tr>
                    <td class="text-center"><?=$no++?></td>
                    <td><?= $bel->Nama_Desa?></td>
                    <td><?= str_replace('Kecamatan ','',ucwords(strtolower($bel->Nama_Kecamatan))) ?></td>
                    <td align="center"><?= number_format($bel->nilai,2,',','.') ?></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <?= anchor('welcome/siskeudes?jenis='.urlencode('belanja').'&type='.urlencode('tinggi'),"Lihat semua >>")?>
          </div>
        </div>
      </div>
    </div>

    <div class=" col-lg-6 col-xl-6">
      <div class="card card-border card-contrast">
        <div style="background-color:#6da2f6; " class="card-header card-header-contrast"><img src="<?= base_url().'assets/img/belanjadua.png'?>" height="50px"> <b style="color:#fff;font-size:1.5rem">Realisasi Terendah</b>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Desa</th>
                  <th>Kecamatan</th>
                  <th>Pendapatan</th>
                  <th>Realisasi</th>
                  <th>%</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; foreach($persenrealisasidua as $persenb){?>
                  <tr>
                    <td class="text-center"><?=$no++?></td>
                    <td><?= $persenb->Nama_Desa?></td>
                    <td><?= str_replace('Kecamatan ','',ucwords(strtolower($persenb->Nama_Kecamatan))) ?></td>
                    <td align="center"><?= number_format($persenb->pendapatan,2,',','.') ?></td>
                    <td align="center"><?= number_format($persenb->realisasi,2,',','.') ?></td>
                    <td align="center"><?= number_format($persenb->persen,2,',','.') ?></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <?= anchor('welcome/siskeudes?jenis='.urlencode('realisasi').'&type='.urlencode('rendah'),"Lihat semua >>")?>
          </div>
        </div>
      </div>
    </div>
    <div class=" col-lg-6 col-xl-6">
      <div class="card card-border card-contrast">
        <div style="background-color:#f7c771; " class="card-header card-header-contrast"><img src="<?= base_url().'assets/img/income.png'?>" height="50px"> <b style="color:#fff; font-size:1.5rem">Realisasi Tertinggi</b>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Desa</th>
                  <th>Kecamatan</th>
                  <th>Pendapatan</th>
                  <th>Realisasi</th>
                  <th>%</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; foreach($persenrealisasi as $persena){?>
                  <tr>
                    <td class="text-center"><?=$no++?></td>
                    <td><?= $persena->Nama_Desa?></td>
                    <td><?= str_replace('Kecamatan ','',ucwords(strtolower($persena->Nama_Kecamatan))) ?></td>
                    <td align="center"><?= number_format($persena->pendapatan,2,',','.') ?></td>
                    <td align="center"><?= number_format($persena->realisasi,2,',','.') ?></td>
                    <td align="center"><?= number_format($persena->persen,2,',','.') ?></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
            <?= anchor('welcome/siskeudes?jenis='.urlencode('realisasi').'&type='.urlencode('tinggi'),"Lihat semua >>")?>
          </div>
        </div>
      </div>
    </div>
    <div class="col-12 col-lg-12 col-xl-12">
      <div class="card card-border-color card-border-color-dark">
        <div class="card-header card-header-divider">
          <span class="title">Pendapatan Tertinggi (Dalam Milyar)</span>
        </div>
        <div class="card-body">
          <div id="grafik1" style="height: 250px;"></div>
        </div>
      </div>
    </div>
    <div class="col-12 col-lg-12 col-xl-12">
      <div class="card card-border-color card-border-color-dark">
        <div class="card-header card-header-divider">
          <span class="title">Pendapatan Terendah (Dalam Milyar)</span>
        </div>
        <div class="card-body">
          <div id="grafik2" style="height: 250px;"></div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-12 col-xl-12">
      <div class="card card-border-color card-border-color-dark">
        <div class="card-header card-header-divider">
          <span class="title">Belanja Tertinggi (Dalam Milyar)</span>
        </div>
        <div class="card-body">
          <div id="grafik3" style="height: 250px;"></div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-12 col-xl-12">
      <div class="card card-border-color card-border-color-dark">
        <div class="card-header card-header-divider">
          <span class="title">Belanja Terendah (Dalam Milyar)</span>
        </div>
        <div class="card-body">
          <div id="grafik4" style="height: 250px;"></div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-12 col-xl-12">
      <div class="card card-border-color card-border-color-dark">
        <div class="card-header card-header-divider">
          <span class="title">Realisasi Tertinggi </span>
        </div>
        <div class="card-body">
          <div id="grafik5" style="height: 250px;"></div>
        </div>
      </div>
    </div>

    <div class="col-12 col-lg-12 col-xl-12">
      <div class="card card-border-color card-border-color-dark">
        <div class="card-header card-header-divider">
          <span class="title">Realisasi Terendah </span>
        </div>
        <div class="card-body">
          <div id="grafik6" style="height: 250px;"></div>
        </div>
      </div>
    </div>
</div>
<div>
</div>

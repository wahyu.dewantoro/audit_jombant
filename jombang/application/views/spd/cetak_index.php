<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Surat Penyediaan Dana (SPD).xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Surat Penyediaan Dana (SPD)</h3>
<table class="tablee" border="1">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>Kode SKPD</th>
								<th> Unit</th>
								<th>Sub Unit</th>
								<th>Jumlah</th>
								<th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1;
                        $jml=0;
                        foreach ($spd_data as $rk)  {
                            $jml=$jml+str_replace(".","",number_format($rk->nilai,'0','','.'));
                            ?>
                            <tr>
                                <td valign="top" width="10px" class="text-center"><?php echo $no++; ?></td>
								<td valign="top" ><?php echo $rk->kd_skpd ?></td>
								<td valign="top" ><?php echo $rk->nm_unit ?></td>
								<td valign="top" ><?php echo $rk->nm_sub_unit ?></td>
								<td valign="top" ><?php echo number_format($rk->jum,'0',',','.') ?></td>
							 	<td valign="top"  align="right"><?php echo number_format($rk->nilai,'0',',','.') ?></td>
                            </tr>
                        <?php
                        } ?>
						</tbody>
                        <tfoot>
                            <tr>
								<th colspan="5" align="right">TOTAL</th>
                                <td class="text-right"><?php echo number_format($jml,'0','','.' ) ?></td>
                            </tr>
                        </tfoot>
					</table>
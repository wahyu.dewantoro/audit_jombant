<?php

// Panggil class PHPExcel nya
$csv = new PHPExcel();

// Settingan awal fil excel

// Buat header tabel nya pada baris ke 1
$csv->setActiveSheetIndex(0)->setCellValue('A1', "NO"); // Set kolom A1 dengan tulisan "NO"
$csv->setActiveSheetIndex(0)->setCellValue('B1', "SKPD"); // Set kolom B1 dengan tulisan "NIS"
$csv->setActiveSheetIndex(0)->setCellValue('C1', "Unit"); // Set kolom C1 dengan tulisan "NAMA"
$csv->setActiveSheetIndex(0)->setCellValue('D1', "Sub Unit"); // Set kolom C1 dengan tulisan "NAMA"
$csv->setActiveSheetIndex(0)->setCellValue('E1', "Jumlah "); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('F1', "Nilai"); // Set kolom E1 dengan tulisan "TELEPON"

$no = 1; // Untuk penomoran tabel, di awal set dengan 1
$numrow = 2; // Set baris pertama untuk isi tabel adalah baris ke 2
$nm_unit="";
$jml=0;
foreach ($spd_data as $rk) {
    $jml=$jml+str_replace(".","",number_format($rk->nilai,'0','','.'));
    $csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
    $csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $rk->kd_skpd);
    $csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $rk->nm_unit);
    $csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $rk->nm_sub_unit);
    $csv->setActiveSheetIndex(0)->setCellValue('E'.$numrow, number_format($rk->jum,'0',',','.') );
    $csv->setActiveSheetIndex(0)->setCellValueExplicit('F'.$numrow, number_format($rk->nilai,'0',',','.'));
    $no++; // Tambah 1 setiap kali looping
    $numrow++; // Tambah 1 setiap kali looping
}

$csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, null);
$csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, null);
$csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, null);
$csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, null);
$csv->setActiveSheetIndex(0)->setCellValue('E'.$numrow, null);
$csv->setActiveSheetIndex(0)->setCellValueExplicit('F'.$numrow, "Total Nilai ".number_format($jml,'0',',','.'));

// Set orientasi kertas jadi LANDSCAPE
$csv->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

// Set judul file excel nya
// $csv->getActiveSheet(0)->setTitle("Neraca");
// $csv->setActiveSheetIndex(0);

// Proses file excel
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="Surat Penyediaan Dana (SPD).csv"'); // Set nama file excel nya
header('Cache-Control: max-age=0');

$write = new PHPExcel_Writer_CSV($csv);
$write->save('php://output');
?>
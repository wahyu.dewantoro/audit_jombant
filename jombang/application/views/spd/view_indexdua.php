<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            <div class="row">
            	<div class="col-md-4">
    				<table valign="top">
    					<tr>
                            <td><strong>OPD</strong></td>
                            <td>: <?= $kd_skpd?> <?php if($nm_unit==$nm_sub_unit){echo $nm_unit;}else{echo $nm_unit.' / '.$nm_sub_unit;}?></td>
                        </tr>
						<tr valign="top">
    						<td><strong>Periode</strong> </td>
    						<td>: <?= $tanggal1." - ".$tanggal2 ?></td>
    					</tr>
    				</table>
            	</div>
            </div>			<hr>
			<div class="row">

				</div>
                	<div class="table-responsive ">
                    <table class="table table-striped table-hover table-bordered" id="table1" >
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								
								<th>No SPD</th>
								<th>Uraian</th>
								<th>Jumlah</th>
								<th>Nilai</th>
								<th width="10px"></th>
                            </tr>
                        </thead>
                        <tbody>
							<?php 
                            $tj=0;
                            $tn=0;
                            foreach ($spd_data as $rk)  {
                                $tj+=$rk->jumlah;
                                $tn+=$rk->nilai;
                             ?>
                            <tr>
								<td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
								<td class="cell-detail"><?= $rk->no_spd ?> <span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime($rk->tgl_spd))) ?></span></td>
								<td><?= $rk->uraian ?></td>
								<td align="center"><?= $rk->jumlah ?></td>
								<td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
								<td align="center">
									<form action="<?= base_url().'spd/readdua' ?>">
										<input type="hidden" name="kd_skpd" value="<?= $rk->kd_skpd ?>">
                                        <input type="hidden" name="nm_sub_unit" value="<?= $rk->nm_sub_unit ?>">
                                        <input type="hidden" name="nm_unit" value="<?= $rk->nm_unit ?>">
                                        <input type="hidden" name="no_spd" value="<?= $rk->no_spd ?>">
                                        <input type="hidden" name="tgl_spd" value="<?= $rk->tgl_spd ?>">
                                       <input type="hidden" name="tanggal1" value="<?= $tanggal1 ?>">
                                       <input type="hidden" name="tanggal2" value="<?= $tanggal2 ?>">
										<button class="btn btn-xs btn-primary"><i class="mdi mdi-search"></i></button>
									</form>

								</td>
							</tr>
							<?php  }   ?>
						</tbody>
                        <tfoot>
                            <tr>
                                <td colspan="3"></td>
                                <td align="center"><strong><?= $tj ?></strong></td>
                                <td align="right"><strong><?= number_format($tn,'0','','.') ?></strong></td>
                                <td></td>
                            </tr>
                        </tfoot>
					</table>
					</div>
					<!-- <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php //echo number_format($total_rows,'0','','.' )?></button>
					<div class="float-right">
						<?php //echo $pagination ?>
					</div> -->
					<!-- <div class="float-right">
						<button  class="btn page-link btn-space btn-success" disabled>Total Nilai : <?php //echo $nilai ?></button>
					</div> -->
            </div>
        </div><!-- end card-->
    </div>
</div>
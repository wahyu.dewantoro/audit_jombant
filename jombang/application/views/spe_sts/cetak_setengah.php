<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Surat Tanda Setoran (STS).xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Surat Tanda Setoran (STS) <br>
SKPD :<?= $kd_skpd?><br>
Unit :<?= $nm_unit?><br>
Sub Unit :<?= $nm_sub_unit?><br>
Tanggal STS :<?= $tanggal1." - ".$tanggal2?><br> </h3>
<table class="tablee" border="1">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th width="100px">Tanggal STS</th>
								<th>No STS</th>
								<th>Keterangan</th>
								<th>Jumlah</th>
								<th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1; foreach ($spe_sts_data as $rk) {?>
                            <tr>
                                <td valign="top" width="10px" class="text-center"><?php echo $no++; ?></td>
								<td valign="top"><?= date_indo(date('Y-m-d',strtotime($rk->tgl_sts))) ?> </td>
								<td valign="top"><?php echo $rk->no_sts ?></td>
								<td valign="top"><?php echo $rk->keterangan ?></td>
								<td valign="top"><?php echo number_format($rk->jum,'0',',','.') ?></td>
								<td valign="top"><?php echo number_format($rk->nilai,'0',',','.') ?></td>
                            </tr>
                        <?php
                        } ?>
						</tbody>
                        <!-- <tfoot>
                            <tr>
								<th colspan="3" align="right">TOTAL</th>
                                <td class="text-right"><?php echo $nilai ?></td>
                            </tr>
                        </tfoot> -->
					</table>
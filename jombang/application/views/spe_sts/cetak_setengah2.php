<?php

// Panggil class PHPExcel nya
$csv = new PHPExcel();

// Settingan awal fil excel

// Buat header tabel nya pada baris ke 1
$csv->setActiveSheetIndex(0)->setCellValue('A1', "NO"); // Set kolom A1 dengan tulisan "NO"
$csv->setActiveSheetIndex(0)->setCellValue('B1', "Tanggal STS"); // Set kolom B1 dengan tulisan "NIS"
$csv->setActiveSheetIndex(0)->setCellValue('C1', "No STS"); // Set kolom C1 dengan tulisan "NAMA"
$csv->setActiveSheetIndex(0)->setCellValue('D1', "Keterangan"); // Set kolom C1 dengan tulisan "NAMA"
$csv->setActiveSheetIndex(0)->setCellValue('E1', "Jumlah "); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('F1', "Nilai"); // Set kolom E1 dengan tulisan "TELEPON"

$no = 1; // Untuk penomoran tabel, di awal set dengan 1
$numrow = 2; // Set baris pertama untuk isi tabel adalah baris ke 2
$nm_unit="";
foreach ($spe_sts_data as $rk) {

    $csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
    $csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, date_indo(date('Y-m-d',strtotime($rk->tgl_sts))));
    $csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $rk->no_sts);
    $csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $rk->keterangan);
    $csv->setActiveSheetIndex(0)->setCellValue('E'.$numrow, number_format($rk->jum,'0',',','.') );
    $csv->setActiveSheetIndex(0)->setCellValueExplicit('F'.$numrow, number_format($rk->nilai,'0',',','.'));
    $no++; // Tambah 1 setiap kali looping
    $numrow++; // Tambah 1 setiap kali looping
}

// $csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('E'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValueExplicit('F'.$numrow, "Total Nilai ".$nilai);

// Set orientasi kertas jadi LANDSCAPE
$csv->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

// Set judul file excel nya
// $csv->getActiveSheet(0)->setTitle("Neraca");
// $csv->setActiveSheetIndex(0);

// Proses file excel
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment; filename=Surat Tanda Setoran (STS) ".$kd_skpd." ".str_replace('.',' ', str_replace(',', '', $nm_sub_unit)).".csv");
header('Cache-Control: max-age=0');

$write = new PHPExcel_Writer_CSV($csv);
$write->save('php://output');
?>
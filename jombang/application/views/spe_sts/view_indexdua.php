<style type="text/css">
  #wgtmsr{
	 width:150px;
	}

	#wgtmsr option{
	  width:150px;
	}


</style>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            <div class="row">
            	<div class="col-md-4">
    				<table valign="top">
    					<tr>
    						<td><strong>OPD</strong></td>
    						<td>: <?= $kd_skpd?> <?php if($nm_unit==$nm_sub_unit){echo $nm_unit;}else{echo $nm_unit.' / '.$nm_sub_unit;}?></td>
    					</tr>
    					<tr valign="top">
    						<td><strong>No STS</strong></td>
    						<td>: <?= $no_sts?></td>
    					</tr>
                        <tr valign="top">
                            <td><strong>Tanggal STS</strong></td>
                            <td>: <?= date_indo(date('Y-m-d',strtotime($sts->tgl_sts))) ?></td>
                        </tr>
    				</table>
            	</div>
                <div class="col-md-4">
                    <table>
                        <tr>
                            <td><strong>Program</strong></td>
                            <td>: <?= $sts->ket_program?></td>
                        </tr>
                        <tr>
                            <td><strong>Kegiatan</strong></td>
                            <td>: <?= $sts->ket_kegiatan?></td>
                        </tr>
                        <tr>
                            <td><strong>Keterangan</strong></td>
                            <td>: <?= $sts->keterangan?></td>
                        </tr>
                    </table>
                </div>
            </div>
            		<hr>
             
                	<div class="table-responsive ">
                    <table class="table table-striped table-hover table-bordered" id="table2" >
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								
								<th>Kode Rekening</th>
								<th>Nama Rekening</th>
                                <th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php   foreach ($spe_sts_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo++$start?></td>
								<td><?= $rk->kd_rek_gabung ?></td>
								<td><?= $rk->nm_rek_5 ?></td>
                                <td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
							</tr>
							<?php  }  ?>
							
						</tbody>
					</table>
					</div>
					
            </div>
        </div><!-- end card-->
    </div>
</div>
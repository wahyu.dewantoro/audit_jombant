<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
                 <div class="row">
                <div class="col-md-3">
                    <table valign="top">
                        <tr valign="top">
                            <td><strong>OPD</strong></td>
                            <td>: <?= $kd_skpd?> <?php if($nm_unit==$nm_sub_unit){echo $nm_unit;}else{echo $nm_unit.' / '.$nm_sub_unit;}?></td>
                        </tr>
						<tr valign="top">
    						<td><strong>Periode</strong></td>
    						<td>:<?= $tanggal1 ." - ".$tanggal2?></td>
    					</tr>
                    </table>
                </div>
            </div>          <hr>
                	<div class="table-responsive noSwipe">
                    <table class="table table-striped table-hover table-bordered" id="table1" >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No STS</th>
                                <th>Keterangan </th>
                                <th>Jumlah</th>
                                <th>Nilai</th>
                                <th width="10px"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $tj=0;
                            $tn=0;
                            foreach($data as $rk){
                                $tj+=$rk->jum;
                                $tn+=$rk->nilai;
                                ?>
                            <tr>
                                <td align="center"><?= ++$start ?></td>

                                
                                <td class="cell-detail"><?= $rk->no_sts ?> <span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime($rk->tgl_sts))) ?> </span></td>
                                <td><?= $rk->keterangan  ?> </td>
                                <td align="center"><?= $rk->jum ?> </td>
                                <td align="right"><?= number_format($rk->nilai,'0','','.') ?> </td>
                                <td align="center">
                                    <form action="<?= base_url().'spe_sts/read' ?>">
                                        <input type="hidden" name="kd_skpd" value="<?= $rk->kd_skpd ?>">
                                        <input type="hidden" name="nm_sub_unit" value="<?= $rk->nm_sub_unit ?>">
                                        <input type="hidden" name="nm_unit" value="<?= $rk->nm_unit ?>">
                                        <input type="hidden" name="no_sts" value="<?= $rk->no_sts ?>">
                                       <input type="hidden" name="tanggal1" value="<?= $tanggal1 ?>">
                                       <input type="hidden" name="tanggal2" value="<?= $tanggal2 ?>">
                                        <button class="btn btn-xs btn-primary"><i class="mdi mdi-search"></i></button>
                                    </form>

                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <td colspan="4"><strong>Jumlah</strong></td>
                            <td align="center"><strong><?= $tj?></strong></td>
                            <td align="right"><strong><?= number_format($tn,0,'','.')?></strong></td>
                        </tfoot>
					</table>
					</div>


            </div>
        </div><!-- end card-->
    </div>
</div>
<?php

// Panggil class PHPExcel nya
$csv = new PHPExcel();

// Settingan awal fil excel

// Buat header tabel nya pada baris ke 1
$csv->setActiveSheetIndex(0)->setCellValue('A1', "NO;"); // Set kolom A1 dengan tulisan "NO"
$csv->setActiveSheetIndex(0)->setCellValue('B1', "SKPD"); // Set kolom B1 dengan tulisan "NIS"
$csv->setActiveSheetIndex(0)->setCellValue('C1', "Unit"); // Set kolom C1 dengan tulisan "NAMA"
$csv->setActiveSheetIndex(0)->setCellValue('D1', "Sub Unit"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('E1', "Jumlah GU"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('F1', "Nilai GU"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('G1', "Jumlah TU"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('H1', "Nilai TU"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('I1', "Jumlah NIHIL"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('J1', "Nilai Nihil"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"

$no = 1; // Untuk penomoran tabel, di awal set dengan 1
$numrow = 2; // Set baris pertama untuk isi tabel adalah baris ke 2
$nm_unit="";
$jnsgu=0;
$nilaigu=0;
$jnstu=0;
$nilaitu=0;
$jnsnihil=0;
$nilainihil=0;
foreach ($spj_data as $rk)  {
    $jnsgu+=$rk->jnsgu;
    $nilaigu+=$rk->nilaigu;
    $jnstu+=$rk->jnstu;
    $nilaitu+=$rk->nilaitu;
    $jnsnihil+=$rk->jnsnihil;
    $nilainihil+=$rk->nilainihil;

    $csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
    $csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $rk->kd_skpd);
    $csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $rk->nm_unit);
    $csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $rk->nm_sub_unit);
    $csv->setActiveSheetIndex(0)->setCellValue('E'.$numrow,  $rk->jnsgu);
    $csv->setActiveSheetIndex(0)->setCellValue('F'.$numrow, number_format($rk->nilaigu,'0','','.'));
    $csv->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $rk->jnstu);
    $csv->setActiveSheetIndex(0)->setCellValue('H'.$numrow, number_format($rk->nilaitu,'0','','.'));
    $csv->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $rk->jnsnihil);
    $csv->setActiveSheetIndex(0)->setCellValue('J'.$numrow, number_format($rk->nilainihil,'0','','.'));

    $no++; // Tambah 1 setiap kali looping
    $numrow++; // Tambah 1 setiap kali looping
}
$csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "");
$csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, "");
$csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, "");
$csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, "Jumlah");
$csv->setActiveSheetIndex(0)->setCellValue('E'.$numrow,  $jnsgu);
$csv->setActiveSheetIndex(0)->setCellValue('F'.$numrow, number_format($nilaigu,'0','','.'));
$csv->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $jnstu);
$csv->setActiveSheetIndex(0)->setCellValue('H'.$numrow, number_format($nilaitu,'0','','.'));
$csv->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $jnsnihil);
$csv->setActiveSheetIndex(0)->setCellValue('J'.$numrow, number_format($nilainihil,'0','','.'));
// Set orientasi kertas jadi LANDSCAPE
$csv->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

// Set judul file excel nya
// $csv->getActiveSheet(0)->setTitle("Neraca");
// $csv->setActiveSheetIndex(0);

// Proses file excel
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment; filename=Surat Pertanggung Jawaban (SPJ).csv");
header('Cache-Control: max-age=0');

$write = new PHPExcel_Writer_CSV($csv);
$write->save('php://output');
?>
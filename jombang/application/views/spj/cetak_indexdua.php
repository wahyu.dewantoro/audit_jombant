<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=SPJ.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3><?= $title ?><br>
SKPD :<?= $kd_skpd?><br>
Unit :<?= $nm_unit?><br>
Sub Unit :<?= $nm_sub_unit?><br>
Tanggal SPJ :<?= $tanggal1." - ".$tanggal2?><br>
</h3>

<table class="tablee" border="1">
<thead>
                            <tr>
                                <th width="10px">No</th>
								<th>No SPJ</th>
                <th>Keterangan</th>
                <th>Jenis</th>
								<th>Jumlah</th>
								<th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1; foreach ($spj_data as $rk) {?>
                            <tr>
                                <td valign="top" width="10px" class="text-center"><?php echo $no++; ?></td>
                                <td valign="top" ><?php echo $rk->no_spj ?></td>
                                <td valign="top" ><?php echo $rk->keterangan ?></td>
                                <td valign="top" ><?php echo $rk->jenis_spj ?></td>
                                 <td valign="top"><?= $rk->jum ?></td>
                                <td valign="top"  align="right"><?php echo number_format($rk->nilai,'0',',','.') ?></td>
                            </tr>
                        <?php
                        } ?>
						</tbody>
</table>
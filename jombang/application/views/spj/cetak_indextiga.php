<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=SPJ.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3><?= $title ?><br>
SKPD :<?= $kd_skpd?><br>
Unit :<?= $nm_unit?><br>
Sub Unit :<?= $nm_sub_unit?><br>
No SPJ :<?= $no_spj?><br>
</h3>


<table class="tablee" border="1">
                      <thead>
                            <tr>
                              <th>No</th>
                              <th>Program</th>
                              <th>Kegiatan</th>
                              <th>Tanggal SPJ</th>
                              <th>No spj</th>
                              <th>Tanggal Bukti</th>
                              <td>No Bukti</td>
                              <th>Tanggal Pengesahan</th>
                              <th>Pengesahan</th>
                              <th>Keterangan</th>
                              <th>Jenis</th>
                              <th>Nilai</th>
                              <th>Kode Rekening</th>
                              <th>Rekening</th>
                              <th>Uraian</th>
                            </tr>
                        </thead>
                        <tbody>
              <?php foreach ($spj_data as $rk)  { ?>
                            <tr>
                <td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
                <td><?= $rk->ket_program ?></td>
                <td><?= $rk->ket_kegiatan ?></td>
                <td><?= date_indo(date('Y-m-d',strtotime( $rk->tgl_spj))) ?></td>
                <td><?= $rk->no_spj ?></td>
                <td><?= date_indo(date('Y-m-d',strtotime( $rk->tgl_bukti))) ?></td>
                <td><?= $rk->no_bukti ?></td>
                <td><?= date_indo(date('Y-m-d',strtotime( $rk->tgl_pengesahan))) ?></td>
                <td><?= $rk->no_pengesahan ?></td>
                <td><?= $rk->keterangan ?></td>
                <td><?= $rk->jenis_spj ?></td>
                <td><?= $rk->nilai ?></td>
                <td><?= $rk->kd_rek_gabung ?></td>
                <td><?= $rk->nm_rek_5 ?></td>
                <td><?= $rk->uraian ?></td>
              </tr>
              <?php  }   ?>
            </tbody>
</table>
<?php

// Panggil class PHPExcel nya
$csv = new PHPExcel();

// Settingan awal fil excel

// Buat header tabel nya pada baris ke 1
$csv->setActiveSheetIndex(0)->setCellValue('A1', "NO");
$csv->setActiveSheetIndex(0)->setCellValue('B1', "Program");
$csv->setActiveSheetIndex(0)->setCellValue('C1', "Kegiatan");
$csv->setActiveSheetIndex(0)->setCellValue('D1', "No SPJ");
$csv->setActiveSheetIndex(0)->setCellValue('E1', "Tanggal SPJ");
$csv->setActiveSheetIndex(0)->setCellValue('F1', "Bukti");
$csv->setActiveSheetIndex(0)->setCellValue('G1', "Tanggal Bukti");
$csv->setActiveSheetIndex(0)->setCellValue('H1', "Pengesahan");
$csv->setActiveSheetIndex(0)->setCellValue('I1', "Tanggal Pengesahan");
$csv->setActiveSheetIndex(0)->setCellValue('J1', "Keterangan");
$csv->setActiveSheetIndex(0)->setCellValue('K1', "Jenis");
$csv->setActiveSheetIndex(0)->setCellValue('L1', "Nilai");
$csv->setActiveSheetIndex(0)->setCellValue('M1', "Kode rekening");
$csv->setActiveSheetIndex(0)->setCellValue('N1', "Rekening");
$csv->setActiveSheetIndex(0)->setCellValue('O1', "Uraian");

$no = 1; // Untuk penomoran tabel, di awal set dengan 1
$numrow = 2; // Set baris pertama untuk isi tabel adalah baris ke 2
$nm_unit="";
foreach ($spj_data as $rk) {

    $csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
    $csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $rk->ket_program);
    $csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $rk->ket_kegiatan);
    $csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $rk->no_spj);
    $csv->setActiveSheetIndex(0)->setCellValue('E'.$numrow, date_indo(date('Y-m-d',strtotime($rk->tgl_spj))));
    $csv->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $rk->no_bukti);
    $csv->setActiveSheetIndex(0)->setCellValue('G'.$numrow, date_indo(date('Y-m-d',strtotime($rk->tgl_bukti))));
    $csv->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $rk->no_pengesahan);
    $csv->setActiveSheetIndex(0)->setCellValue('I'.$numrow, date_indo(date('Y-m-d',strtotime($rk->tgl_pengesahan))));
    $csv->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $rk->keterangan);
    $csv->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $rk->jenis_spj);
    $csv->setActiveSheetIndex(0)->setCellValueExplicit('L'.$numrow, number_format($rk->nilai,'0',',','.'));
    $csv->setActiveSheetIndex(0)->setCellValue('M'.$numrow, $rk->kd_rek_gabung);
    $csv->setActiveSheetIndex(0)->setCellValue('N'.$numrow, $rk->nm_rek_5);
    $csv->setActiveSheetIndex(0)->setCellValue('O'.$numrow, $rk->uraian);

    $no++; // Tambah 1 setiap kali looping
    $numrow++; // Tambah 1 setiap kali looping
}

// $csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('E'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValueExplicit('F'.$numrow, "Total Nilai ".$nilai);

// Set orientasi kertas jadi LANDSCAPE
$csv->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

// Set judul file excel nya
// $csv->getActiveSheet(0)->setTitle("Neraca");
// $csv->setActiveSheetIndex(0);

// Proses file excel
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment; filename=Surat Pertanggung Jawaban (SPJ) ".$kd_skpd." ".str_replace('.',' ', str_replace(',', '', $nm_sub_unit)).".csv");
header('Cache-Control: max-age=0');

$write = new PHPExcel_Writer_CSV($csv);
$write->save('php://output');
?>
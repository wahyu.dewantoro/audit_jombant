<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
                    <form action="<?php echo site_url('spj'); ?>"   method="get">
                        <div class="row">
                        <div class="col-md-4">
                                <label><strong>SKPD/OPD</strong></label>
                                    <select class="form-control form-control-sm select2" name="q" data-placeholder="Pilih SKPD/OPD">
                                        <option value=""></option>
                                        <?php foreach($subunit as $su){?>
                                        <option <?php if($su->nm_unit==$q){echo "selected";}?> value="<?= $su->nm_unit ?>"><?= $su->kd_skpd.' - '.$su->nm_unit ?></option>
                                        <?php } ?>
                                    </select>
                            </div>
                            <div width="300px" style="padding-left:15px;padding-right: 15px;">
                                <div class="form-group">
                                    <label><strong>Tanggal SPJ</strong></label>
                                    <div class="row">
                                        <div width="120px" style="padding-left:20px;padding-right: 15px;">
                                            <input style="width:85px" type="text" name="tanggal1" value="<?= $tanggal1 ?>" placeholder="Tgl Mulai" class="form-control form-control-xs datepicker"  >
                                        </div>
                                        <div width="120px" style="padding-left:15px;padding-right: 15px;">
                                            <input style="width:85px" type="text" name="tanggal2" value="<?= $tanggal2 ?>" placeholder="Tgl Akhir" class="form-control form-control-xs datepicker"  >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label style="height: 13px;"><strong> </strong></label><br>
                                        <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                                        <?php if ($q <> '')  { ?>
                                            <a href="<?php echo site_url('spm'); ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
                                      <?php }
                                      ?>
                                </div>
                            </div>
                        </div>
                    </form>

                <div class="table-responsive">
                    <table class="table  table-striped table-hover table-bordered" id="table2">
                        <thead>
                            <tr>
                                <th rowspan="2" width="10px">No</th>
                                <th rowspan="2">OPD</th>
                                <th colspan="2">GU</th>
                                <th colspan="2">TU</th>
                                <th colspan="2">NIHIL</th>
                            </tr>
                            <tr>
                                <th>Jumlah</th>
                                <th>Nilai</th>
                                <th>Jumlah</th>
                                <th>Nilai</th>
                                <th>Jumlah</th>
                                <th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $jnsup=0;
                            $nilaiup=0;
                            $jnsgu=0;
                            $nilaigu=0;
                            $jnstu=0;
                            $nilaitu=0;
                            $jnsls=0;
                            $nilails=0;
                            $jnsnihil=0;
                            $nilainihil=0;
                            foreach ($spj_data as $rk)  {

                                $jnsgu+=$rk->jnsgu;
                                $nilaigu+=$rk->nilaigu;
                                $jnstu+=$rk->jnstu;
                                $nilaitu+=$rk->nilaitu;

                                $jnsnihil+=$rk->jnsnihil;
                                $nilainihil+=$rk->nilainihil;
                                ?>
                            <tr>
                                <td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
                                <td class="cell-detail"><?= $rk->nm_sub_unit ?><span class="cell-detail-description"><?= $rk->nm_unit ?> </span> <span class="cell-detail-description"><?= $rk->kd_skpd ?> </span></td>

                                <td align="center">
                                    <?php if($rk->jnsgu<>''){
                                            echo anchor('spj/read?nm_unit='.urlencode($rk->nm_unit).'&nm_sub_unit='.urlencode($rk->nm_sub_unit).'&kd_skpd='.urlencode($rk->kd_skpd).'&q='.urlencode('gu').'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2),$rk->jnsgu);
                                    }else{ ?>
                                    -
                                    <?php } ?>
                                </td>
                                <td align="right"><?= number_format($rk->nilaigu,'0','','.') ?></td>
                                <td align="center">
                                    <?php if($rk->jnstu<>''){
                                            echo anchor('spj/read?nm_unit='.urlencode($rk->nm_unit).'&nm_sub_unit='.urlencode($rk->nm_sub_unit).'&kd_skpd='.urlencode($rk->kd_skpd).'&q='.urlencode('tu').'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2),$rk->jnstu);
                                    }else{ ?>
                                    -
                                    <?php } ?>
                                </td>
                                <td align="right"><?= number_format($rk->nilaitu,'0','','.') ?></td>
                                <td align="center">
                                    <?php if($rk->jnsnihil<>''){
                                            echo anchor('spj/read?nm_unit='.urlencode($rk->nm_unit).'&nm_sub_unit='.urlencode($rk->nm_sub_unit).'&kd_skpd='.urlencode($rk->kd_skpd).'&q='.urlencode('nihil').'&tanggal1='.urlencode($tanggal1).'&tanggal2='.urlencode($tanggal2),$rk->jnsnihil);
                                    }else{ ?>
                                    -
                                    <?php } ?>
                                </td>
                                <td align="right"><?= number_format($rk->nilainihil,'0','','.') ?></td>
                                
                            </tr>
                            <?php  }   ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2"><strong> Jumlah</strong></td>
                                <td align="center"><strong> <?= $jnsgu ?> </strong></td>
                                <td align="right"><strong> <?= number_format($nilaigu,'0','','.') ?></strong> </td>
                                <td align="center"><strong> <?= $jnstu ?> </strong></td>
                                <td align="right"><strong> <?= number_format($nilaitu,'0','','.') ?></strong> </td>
                                <td align="center"><strong> <?= $jnsnihil ?> </strong></td>
                                <td align="right"><strong> <?= number_format($nilainihil,'0','','.') ?></strong> </td>
                                
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div><!-- end card-->
    </div>
</div>
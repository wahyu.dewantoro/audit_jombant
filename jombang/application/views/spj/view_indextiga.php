<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            <div class="row">
            	<div class="col-md-6">
    				<table valign="top">
    					<tr>
                            <td><strong>OPD</strong></td>
                            <td>:</td>
                            <td> <?= $kd_skpd?> <?php if($nm_unit==$nm_sub_unit){echo $nm_unit;}else{echo $nm_unit.' / '.$nm_sub_unit;}?></td>
                        </tr>

                        <tr valign="top">
                            <td width="20%"><strong>No SPJ</strong></td>
                            <td width="1%">:</td>
                            <td><?= $no_spj?></td>
                        </tr>
                        <tr valign="top">
                            <td width="20%"><strong>Tanggal SPJ</strong></td>
                            <td width="1%">:</td>
                            <td><?= date_indo(date('Y-m-d',strtotime($spj->tgl_spj))) ?></td>
                        </tr>
    				</table>
            	</div>
            </div>
			<hr>

                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered" id="table2">
                        <thead>
                            <tr>
								<th>No</th>
								<th>Program</th>
                                <th>Kegiatan</th>
                                <!-- <th>SPJ</th> -->
                                <th>Bukti</th>
                                <th>Pengesahan</th>
                                <th>Keterangan</th>
                                <th>Jenis</th>
                                <th>Uraian</th>
                                <th>Rekening</th>
                                <th>Nilai</th>

                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($spj_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
                                <td> <?= $rk->ket_program ?></td>
                                <td> <?= $rk->ket_kegiatan ?></td>
                                <td class="cell-detail"> <?= $rk->no_bukti ?> <span class="cell-detail-description"> <?= date_indo(date('Y-m-d',strtotime($rk->tgl_bukti))) ?></span></td>

                                <td class="cell-detail"> <?= $rk->no_pengesahan ?> <span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime($rk->tgl_pengesahan))) ?></span></td>
                                <td> <?= $rk->keterangan ?></td>
                                <td align="center"> <?= $rk->jenis_spj ?></td>
                                <td><?= $rk->uraian ?></td>
                                <td class="cell-detail"> <?= $rk->kd_rek_gabung ?>
                                
                                 <span class="cell-detail-description"><?= $rk->nm_rek_5 ?></span></td>
                                <td align="right"> <?= number_format($rk->nilai,0,'','.') ?></td>

							</tr>
							<?php  }   ?>
						</tbody>
					</table>
				</div>

            </div>
        </div><!-- end card-->
    </div>
</div>
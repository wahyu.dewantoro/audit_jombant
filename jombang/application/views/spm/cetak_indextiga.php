<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=SPM.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3><?= $title ?><br>
SKPD :<?= $kd_skpd?><br>
Unit :<?= $nm_unit?><br>
Sub Unit :<?= $nm_sub_unit?><br>
No SPM :<?= $no_spm?><br>
</h3>


<table class="tablee" border="1">
                      <thead>
                            <tr>
                              <th>No</th>
                              <th>No spm</th>
                              <th>Kode Rekening</th>
                              <th>Rekening</th>
                              <th>Nilai</th>
                              <th>Program</th>
                              <th>Kegiatan</th>
                            </tr>
                        </thead>
                        <tbody>
              <?php foreach ($spm_data as $rk)  { ?>
                            <tr>
                <td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
                <td class="cell-detail"><?= $rk->no_spm ?><span class="cell-detail-description"></span></td>
                <td><?= $rk->kd_rek_gabung ?></td>
                <td class="cell-detail"><span class="cell-detail-description"><?= $rk->nm_rek_5 ?></span></td>
                <td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
                <td><?= $rk->ket_program ?></td>
                <td><?= $rk->ket_kegiatan ?></td>
              </tr>
              <?php  }   ?>
            </tbody>
</table>
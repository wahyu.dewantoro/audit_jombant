<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            <div class="row">
            	<div class="col-md-4">
    				<table valign="top">
    					<tr valign="top">
                            <td ><strong>OPD</strong></td>
                            <td>:</td>
                            <td> <?= $kd_skpd?> <?php if($nm_unit==$nm_sub_unit){echo $nm_unit;}else{echo $nm_unit.' / '.$nm_sub_unit;}?></td>
                        </tr>
                        <tr valign="top">
                            <td ><strong>No SPM</strong></td>
                            <td>:</td>
                            <td><?= $no_spm ?></td>
                        </tr>
                        <tr valign="top">
                            <td ><strong>Tanggal SPM</strong></td>
                            <td>:</td>
                            <td><?= date_indo(date('Y-m-d',strtotime($spm->tgl_spm)))?></td>
                        </tr>
    				</table>
            	</div>
                <div class="col-md-4">
                    <table valign="top">
                        <tr valign="top">
                            <td ><strong>Program</strong></td>
                            <td>:</td>
                            <td><?= $spm->ket_program ?></td>
                        </tr>
                        <tr valign="top">
                            <td ><strong>Kegiatan</strong></td>
                            <td>:</td>
                            <td><?= $spm->ket_kegiatan ?></td>
                        </tr>
                    </table>
                </div>
            </div>
			<hr>
			
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered" id="table2">
                        <thead>
                            <tr>
								<th>No</th>
								
								<th>Rekening</th>
								<th>Nilai</th>
								
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($spm_data as $rk)  { ?>
                            <tr>
								<td width="10px" align="center"><?php echo number_format(++$start,'0','','.') ?></td>
								
								<td class="cell-detail"><?= $rk->kd_rek_gabung ?><span class="cell-detail-description"><?= $rk->nm_rek_5 ?></span></td>
								<td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
								
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
				</div>
					
            </div>
        </div><!-- end card-->
    </div>
</div>
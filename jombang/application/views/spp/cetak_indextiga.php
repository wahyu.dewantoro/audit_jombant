<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Surat Permintaan Pembayaran (SPP).xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Surat Permintaan Pembayaran (SPP)<br>
SKPD :<?= $kd_skpd?><br>
Unit :<?= $nm_unit?><br>
Sub Unit :<?= $nm_sub_unit?><br>
No SPP :<?= $no_spp?><br>
</h3>


<table class="tablee" border="1">
                      <thead>
                            <tr>
                              <th>No</th>
                              <th>Tanggal</th>
                              <th>No spp</th>
                              <th>Kode</th>
                              <th>Rekening</th>
                              <th>Nilai</th>
                              <th>Program</th>
                              <th>Kegiatan</th>
                            </tr>
                        </thead>
                        <tbody>
              <?php foreach ($spp_data as $rk)  { ?>
                            <tr>
                <td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
                <td><?= date_indo(date('Y-m-d',strtotime( $rk->tgl_spp))) ?></td>
                <td class="cell-detail"><?= $rk->no_spp ?><span class="cell-detail-description"></span></td>
                <td><?= $rk->kd_rek_gabung ?></td>
                <td class="cell-detail"><span class="cell-detail-description"><?= $rk->nm_rek ?></span></td>
                <td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
                <td><?= $rk->ket_program ?></td>
                <td><?= $rk->ket_kegiatan ?></td>
              </tr>
              <?php  }   ?>
            </tbody>
</table>
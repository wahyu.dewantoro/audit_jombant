<?php

// Panggil class PHPExcel nya
$csv = new PHPExcel();

// Settingan awal fil excel

// Buat header tabel nya pada baris ke 1
$csv->setActiveSheetIndex(0)->setCellValue('A1', "NO");
$csv->setActiveSheetIndex(0)->setCellValue('B1', "Tanggal SPP");
$csv->setActiveSheetIndex(0)->setCellValue('C1', "No SPP");
$csv->setActiveSheetIndex(0)->setCellValue('D1', "Kode Rekening");
$csv->setActiveSheetIndex(0)->setCellValue('E1', "Nama Rekening");
$csv->setActiveSheetIndex(0)->setCellValue('F1', "Nilai");
$csv->setActiveSheetIndex(0)->setCellValue('G1', "Program");
$csv->setActiveSheetIndex(0)->setCellValue('H1', "Kegiatan");

$no = 1; // Untuk penomoran tabel, di awal set dengan 1
$numrow = 2; // Set baris pertama untuk isi tabel adalah baris ke 2
$nm_unit="";
foreach ($spp_data as $rk) {

    $csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
    $csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, date_indo(date('Y-m-d',strtotime($rk->tgl_spp))));
    $csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $rk->no_spp);
    $csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $rk->kd_rek_gabung);
    $csv->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $rk->nm_rek);
    $csv->setActiveSheetIndex(0)->setCellValueExplicit('F'.$numrow, number_format($rk->nilai,'0',',','.'));
    $csv->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $rk->ket_program);
    $csv->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $rk->ket_kegiatan);

    $no++; // Tambah 1 setiap kali looping
    $numrow++; // Tambah 1 setiap kali looping
}

// $csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('E'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValueExplicit('F'.$numrow, "Total Nilai ".$nilai);

// Set orientasi kertas jadi LANDSCAPE
$csv->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

// Set judul file excel nya
// $csv->getActiveSheet(0)->setTitle("Neraca");
// $csv->setActiveSheetIndex(0);

// Proses file excel
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment; filename=Surat Permintaan Pembayaran (SPP) ".$kd_skpd." ".str_replace('.',' ', str_replace(',', '', $nm_sub_unit)).".csv");
header('Cache-Control: max-age=0');

$write = new PHPExcel_Writer_CSV($csv);
$write->save('php://output');
?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            <div class="row">
            	<div class="col-md-6">
    				<table >
    					<tr valign="top">
                            <td><strong>OPD</strong></td>
                            <td>: <?= $kd_skpd?> <?php if($nm_unit==$nm_sub_unit){echo $nm_unit;}else{echo $nm_unit.' / '.$nm_sub_unit;}?></td>
                        </tr>
                        <tr valign="top">
                            <td><strong>Periode</strong></td>
                            <td>:<?= $tanggal1 ." - ".$tanggal2?></td>
                        </tr>
    				</table>
            	</div>
            </div>
			<hr>

                <div class="table-responsive">
                    <table class="table   table-striped table-hover table-bordered" id="table1">
                        <thead>
                           <tr>
                           		<th>No</th>
								<th>No SPP</th>
								<th>Uraian</th>
								<th>Jenis</th>
								<th>Jumlah</th>
								<th>Nilai</th>
								<th></th>
                           </tr>
                        </thead>
                        <tbody>
							<?php 
                            $tn=0;
                            $tj=0;
                            foreach ($spp_data as $rk)  { 
                                $tn+=$rk->nilai;
                                $tj+=$rk->jum;
                                ?>
                            <tr>
								<td  align="center"><?php echo ++$start ?></td>
								<td class="cell-detail"><?= $rk->no_spp ?>
                                    <span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime($rk->tgl_spp))) ?></span>
                                </td>
								<td><?= $rk->uraian ?></td>
								<td align="center"><?= $rk->jenis_spp ?></td>
								<td align="center"><?= $rk->jum ?></td>
								<td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
								<th align="center">
									<form action="<?= base_url().'spp/readdua'?>">
										<input type='hidden' name='kd_skpd' value='<?= $rk->kd_skpd?>'>
										<input type='hidden' name='nm_unit' value='<?= $nm_unit ?>'>
										<input type='hidden' name='nm_sub_unit' value='<?= $rk->nm_sub_unit?>'>
										<input type='hidden' name='no_spp' value='<?= $rk->no_spp?>'>
                                       <input type="hidden" name="tanggal1" value="<?= $tanggal1 ?>">
                                       <input type="hidden" name="tanggal2" value="<?= $tanggal2 ?>">
										<button class="btn btn-xs btn-primary"><i class="mdi mdi-search"></i></button>
									</form>
								</th>
							</tr>
							<?php  }   ?>
						</tbody>
                        <tfoot>
                            <tr>
                                <td colspan="4">Jumlah</td>
                                <td align="center"><strong><?= number_format($tj,0,'','.')?></strong></td>
                                <td align="right"><strong><?= number_format($tn,0,'','.')?></strong></td>
                                <td></td>

                            </tr>
                        </tfoot>
					</table>
				</div>

            </div>
        </div><!-- end card-->
    </div>
</div>
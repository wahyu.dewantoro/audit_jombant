<?php

// Panggil class PHPExcel nya
$csv = new PHPExcel();

// Settingan awal fil excel

// Buat header tabel nya pada baris ke 1
$csv->setActiveSheetIndex(0)->setCellValue('A1', "NO;"); // Set kolom A1 dengan tulisan "NO"
$csv->setActiveSheetIndex(0)->setCellValue('B1', "SKPD"); // Set kolom B1 dengan tulisan "NIS"
$csv->setActiveSheetIndex(0)->setCellValue('C1', "Unit"); // Set kolom C1 dengan tulisan "NAMA"
$csv->setActiveSheetIndex(0)->setCellValue('D1', "Sub Unit"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('E1', "Jumlah UP"); // Set kolom E1 dengan tulisan "TELEPON"
$csv->setActiveSheetIndex(0)->setCellValue('F1', "Nilai UP"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('G1', "Jumlah GU"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('H1', "Nilai GU"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('I1', "Jumlah TU"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('J1', "Nilai TU"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('K1', "Jumlah LS"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('L1', "Nilai LS"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('M1', "Jumlah NIHIL"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"
$csv->setActiveSheetIndex(0)->setCellValue('N1', "Nilai Nihil"); // Set kolom D1 dengan tulisan "JENIS KELAMIN"

$no = 1; // Untuk penomoran tabel, di awal set dengan 1
$numrow = 2; // Set baris pertama untuk isi tabel adalah baris ke 2
$nm_unit="";
$jnsup=0;
$nilaiup=0;
$jnsgu=0;
$nilaigu=0;
$jnstu=0;
$nilaitu=0;
$jnsls=0;
$nilails=0;
$jnsnihil=0;
$nilainihil=0;
foreach ($sppd_data as $rk)  {
    $jnsup+=$rk->jnsup;
    $nilaiup+=$rk->nilaiup;
    $jnsgu+=$rk->jnsgu;
    $nilaigu+=$rk->nilaigu;
    $jnstu+=$rk->jnstu;
    $nilaitu+=$rk->nilaitu;
    $jnsls+=$rk->jnsls;
    $nilails+=$rk->nilails;
    $jnsnihil+=$rk->jnsnihil;
    $nilainihil+=$rk->nilainihil;

    $csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
    $csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $rk->kd_skpd);
    $csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $rk->nm_unit);
    $csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $rk->nm_sub_unit);
    $csv->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $rk->jnsup);
    $csv->setActiveSheetIndex(0)->setCellValue('F'.$numrow, number_format($rk->nilaiup,'0','','.') );
    $csv->setActiveSheetIndex(0)->setCellValue('G'.$numrow,  $rk->jnsgu);
    $csv->setActiveSheetIndex(0)->setCellValue('H'.$numrow, number_format($rk->nilaigu,'0','','.'));
    $csv->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $rk->jnstu);
    $csv->setActiveSheetIndex(0)->setCellValue('J'.$numrow, number_format($rk->nilaitu,'0','','.'));
    $csv->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $rk->jnsls );
    $csv->setActiveSheetIndex(0)->setCellValue('L'.$numrow, number_format($rk->nilails,'0','','.') );
    $csv->setActiveSheetIndex(0)->setCellValue('M'.$numrow, $rk->jnsnihil);
    $csv->setActiveSheetIndex(0)->setCellValue('N'.$numrow, number_format($rk->nilainihil,'0','','.'));

    $no++; // Tambah 1 setiap kali looping
    $numrow++; // Tambah 1 setiap kali looping
}
$csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "");
$csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, "");
$csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, "");
$csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, "Jumlah");
$csv->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $jnsup);
$csv->setActiveSheetIndex(0)->setCellValue('F'.$numrow, number_format($nilaiup,'0','','.') );
$csv->setActiveSheetIndex(0)->setCellValue('G'.$numrow,  $jnsgu);
$csv->setActiveSheetIndex(0)->setCellValue('H'.$numrow, number_format($nilaigu,'0','','.'));
$csv->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $jnstu);
$csv->setActiveSheetIndex(0)->setCellValue('J'.$numrow, number_format($nilaitu,'0','','.'));
$csv->setActiveSheetIndex(0)->setCellValue('K'.$numrow, $jnsls );
$csv->setActiveSheetIndex(0)->setCellValue('L'.$numrow, number_format($nilails,'0','','.') );
$csv->setActiveSheetIndex(0)->setCellValue('M'.$numrow, $jnsnihil);
$csv->setActiveSheetIndex(0)->setCellValue('N'.$numrow, number_format($nilainihil,'0','','.'));
// Set orientasi kertas jadi LANDSCAPE
$csv->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

// Set judul file excel nya
// $csv->getActiveSheet(0)->setTitle("Neraca");
// $csv->setActiveSheetIndex(0);

// Proses file excel
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment; filename=Surat Perintah Pencairan Dana (SP2D).csv");
header('Cache-Control: max-age=0');

$write = new PHPExcel_Writer_CSV($csv);
$write->save('php://output');
?>
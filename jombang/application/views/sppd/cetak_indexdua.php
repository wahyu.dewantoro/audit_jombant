<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Surat Perintah Pencairan Dana (SP2D).xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<h3><?= $title ?><br>
SKPD :<?= $kd_skpd?><br>
Unit :<?= $nm_unit?><br>
Sub Unit :<?= $nm_sub_unit?><br>
Tanggal SP2D :<?= $tanggal1." - ".$tanggal2?><br>
</h3>

<table class="table   table-striped table-hover table-bordered" border="1">
<thead>
                            <tr>
                                <th width="10px">No</th>
								<th>No SP2D</th>
                				<th>Keterangan</th>
								<th>Jenis</th>
								<th>Jumlah</th>
								<th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1; foreach ($sppd_data as $rk) {?>
                            <tr>
                                <td valign="top" width="10px" class="text-center"><?php echo $no++; ?></td>
                                <td valign="top" ><?php echo $rk->no_sp2d ?></td>
                                <td valign="top" ><?php echo $rk->keterangan ?></td>
                                <td valign="top" ><?php echo $rk->jenis_sp2d ?></td>
                                 <td valign="top"><?= $rk->jum ?></td>
                                <td valign="top"  align="right"><?php echo number_format($rk->nilai,'0',',','.') ?></td>
                            </tr>
                        <?php
                        } ?>
						</tbody>
</table>
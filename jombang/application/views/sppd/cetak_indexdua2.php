<?php

// Panggil class PHPExcel nya
$csv = new PHPExcel();

// Settingan awal fil excel

// Buat header tabel nya pada baris ke 1
$csv->setActiveSheetIndex(0)->setCellValue('A1', "NO");
$csv->setActiveSheetIndex(0)->setCellValue('C1', "No SP2D");
$csv->setActiveSheetIndex(0)->setCellValue('D1', "Keterangan");
$csv->setActiveSheetIndex(0)->setCellValue('E1', "Jenis");
$csv->setActiveSheetIndex(0)->setCellValue('F1', "Jumlah");
$csv->setActiveSheetIndex(0)->setCellValue('G1', "Nilai");

$no = 1; // Untuk penomoran tabel, di awal set dengan 1
$numrow = 2; // Set baris pertama untuk isi tabel adalah baris ke 2
$nm_unit="";
foreach ($sppd_data as $rk) {

    $csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
    $csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $rk->no_sp2d);
    $csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $rk->keterangan);
    $csv->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $rk->jenis_sp2d);
    $csv->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $rk->jum);
    $csv->setActiveSheetIndex(0)->setCellValueExplicit('G'.$numrow, number_format($rk->nilai,'0',',','.'));

    $no++; // Tambah 1 setiap kali looping
    $numrow++; // Tambah 1 setiap kali looping
}

// $csv->setActiveSheetIndex(0)->setCellValue('A'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('B'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('C'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('D'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValue('E'.$numrow, null);
// $csv->setActiveSheetIndex(0)->setCellValueExplicit('F'.$numrow, "Total Nilai ".$nilai);

// Set orientasi kertas jadi LANDSCAPE
$csv->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

// Set judul file excel nya
// $csv->getActiveSheet(0)->setTitle("Neraca");
// $csv->setActiveSheetIndex(0);

// Proses file excel
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment; filename=Surat Perintah Pencairan Dana (SP2D) ".$kd_skpd." ".str_replace('.',' ', str_replace(',', '', $nm_sub_unit)).".csv");
header('Cache-Control: max-age=0');

$write = new PHPExcel_Writer_CSV($csv);
$write->save('php://output');
?>
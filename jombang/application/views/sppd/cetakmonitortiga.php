<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=document.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<h3><?= $title ?>
</h3>

<table class="table   table-striped table-hover table-bordered" border="1">
    <thead>
        <tr>
			<th width="10px">No</th>
                                <th>Kode SKPD</th>
                                <th>Unit</th>
                                <th>SUb Unit</th>
                                <th>No SP2D</th>
                                <th>Tanggal SP2D</th>
                                <th>Keterangan</th>
                                <th>Nilai</th>
                                <th>Penerima</th>
                                <th>Rekening</th>
                                <th>NPWP</th>
        </tr>
    </thead>
    <tbody>
		<?php $start=0; foreach ($sppd_data as $rk)  { ?>
        <tr>
			<td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
			<td><?= $rk->kd_skpd ?></td>
			<td><?= $rk->nm_unit ?></td>
			<td><?= $rk->nm_sub_unit ?></td>                                
			<td class="cell-detail"><?= $rk->no_sp2d ?> </td>
			<td><?= date_indo(date('Y-m-d',strtotime($rk->tgl_sp2d))) ?></td>
			<td><?= $rk->keterangan ?></td>
			<td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
			<td><?= $rk->nm_penerima ?></td>
			<td><?= $rk->rek_penerima ?></td>
			<td><?= $rk->npwp ?></td>
		</tr>
		<?php  }   ?>
	</tbody>
</table>
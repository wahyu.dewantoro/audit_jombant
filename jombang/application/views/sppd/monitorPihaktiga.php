<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            
            <div class="row">
                <div class="col-md-12">
                    <form action="<?php echo $action ?>" method="get">
                        <div class="row">
                            <div class="col-md-5">
                                <select name="nm_unit" class="form-control form-control-sm select2" data-placeholder="Silahkan pilih">
                                    <option value=""></option>
                                    <?php foreach($unit as $su){?>
                                    <option <?php if($su->nm_unit==$nm_unit){echo "selected";}?> value="<?= $su->nm_unit ?>"><?= $su->kd_skpd.' - '.$su->nm_unit ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                     <div class="input-group">
                                        <input type="text" class="form-control form-control-xs" name="q" value="<?php echo $q; ?>" placeholder="Pencarian">
                                        <span class="input-group-btn">
                                        <div class="btn-group">
                                            <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                                            <?php if ($q <> '' || $nm_unit<>'')  { ?>
                                                <a href="<?php echo $action ; ?>" class="btn btn-warning"><i class="mdi mdi-close"></i></a>
                                              <?php } ?>
                                              

                                        </div>
                                    </span>
                                </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                </div>
                <div class="table-responsive">
                    <table class="table   table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
                                <th>OPD</th>
                                <th>SP2D</th>
                                <th>SPM</th>
                                <th>Keterangan</th>
                                <th>Nilai</th>
                                <th>Penerima</th>
                                <th>Rekening</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php   foreach ($sppd_data as $rk)  { ?>
                            <tr>

                                <td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
                                <td class="cell-detail">
                                    <?= $rk->nm_sub_unit ?>
                                    <span class="cell-detail-description"><?= $rk->kd_skpd ?></span>
                                </td>                                
                                <td class="cell-detail"><?= $rk->no_sp2d ?> <span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime($rk->tgl_sp2d))) ?></span></td>
                                <td class="cell-detail"><?= $rk->no_spm ?> <span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime($rk->tgl_spm))) ?></span></td>
                                <td><?= $rk->keterangan ?></td>
                                <td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
                                <td class="cell-detail"><?= $rk->nm_penerima ?>
                                    <span class="cell-detail-description">NPWP: <?= $rk->npwp ?></span>
                                </td>
                                <td><?= $rk->rek_penerima ?></td>
                            </tr>
                            <?php   }   ?>
                        </tbody>
                    </table>
                </div>
                    <button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo number_format($total_rows,'0','','.'); ?></button>
                    <div class="float-right">
                        <?php echo $pagination ?>
                    </div>
                    
            </div>
        </div><!-- end card-->
    </div>
</div>
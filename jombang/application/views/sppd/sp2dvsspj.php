<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <div class="card">
            <div class="card-body">
                <form action="<?php echo $action ?>" class="" method="get">
                    <div class="row">
                        <div class="col-md-8">
                            <select class="form-control form-control-sm select2" name="q" data-placeholder='Silahkan pilih SKPD/OPD'>
                                <option value=""></option>
                                <?php foreach ($subunit as $su) { ?>
                                    <option <?php if ($su->kd_skpd == $q) {
                                                    echo "selected";
                                                } ?> value="<?= $su->kd_skpd ?>"><?= $su->nm_sub_unit ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-primary" type="submit"><i class="mdi mdi-search"></i> Cari</button>
                            <?php if ($q <> '') { ?>
                                <a href="<?php echo $action ?>" class="btn btn-warning"><i class="mdi mdi-close"></i> Reset</a>
                            <?php }
                            ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php if ($q <> '') { ?>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="card">
                <div class="card-body">
                    <form action="<?php echo base_url() . 'sppd/prosesmappingspj' ?>" method="post">
                        <input type="hidden" name="sppd" id="sppd">
                        <div id="res"></div>
                        <button class="btn  btn-primary"><i class="mdi mdi-cloud-done"></i> Submit</button>
                    </form>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<?php if ($q <> '') { ?>
    <div class="row">
        <div class="col-md-6">
            <div class="card card-contrast">
                <div class="card-header card-header-contrast card-header-featured">SP2D</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>

                                    <th>Tanggal</th>
                                    <th>SP2D</th>
                                    <th>keterangan</th>
                                    <th>Nilai</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($sppd as $rn) { ?>
                                    <tr>
                                        <!-- <td><?= $rn->tahun ?></td> -->

                                        <td><?= date_indo(date('Y-m-d', strtotime($rn->tgl_sp2d))) ?></td>
                                        <td><?= $rn->no_sp2d ?></td>
                                        <td><?= $rn->keterangan ?></td>
                                        <td align="right"><?= number_format($rn->nilai, '0', '', '.') ?></td>
                                        <td>
                                            <label class="custom-control custom-radio">
                                                <input class="custom-control-input satu" type="radio" value="<?= $rn->tahun . '#' . $rn->kd_skpd . '#' . $rn->no_sp2d ?>" name="sp2d"><span class="custom-control-label"></span>
                                            </label>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-contrast">
                <div class="card-header card-header-contrast card-header-featured">SPJ</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <th></th>
                                <th>Tanggal </th>
                                <th>No SPJ</th>
                                <th>Uraian</th>
                                <th>Nilai</th>
                            </thead>
                            <tbody>
                                <?php foreach ($spj as $rk) { ?>
                                    <tr>
                                        <td>
                                            <label class="custom-control custom-checkbox">
                                                <input class="custom-control-input dua" type="checkbox" name="spj[]" value="<?php echo $rk->no_spj ?>"><span class="custom-control-label"></span>
                                            </label>
                                        </td>
                                        <td><?= date_indo(date('Y-m-d', strtotime($rk->tgl_spj))) ?></td>
                                        <td><?= $rk->no_spj ?></td>
                                        <td><?= $rk->keterangan ?></td>
                                        <td align="right"><?= number_format($rk->nilai, '0', '', '.') ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card card-contrast">
                <div class="card-header card-header-contrast card-header-featured">SP2D VS SPJ</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped  table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="3">SP2D</th>
                                    <th colspan="3">SPJ</th>
                                    <th rowspan="2">Selisih</th>
                                    <th rowspan="2"></th>
                                </tr>
                                <tr>
                                    <th>Nomor</th>
                                    <th>Keterangan</th>
                                    <th>Nilai</th>
                                    <th>Nomor</th>
                                    <th>Keterangan</th>
                                    <!-- <th>Nilai</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $awal = 0;
                                    $akhir = 0;
                                    foreach ($mapping as $rkk) {

                                        $start_date = new DateTime(date('Y-m-d', strtotime($rkk->tgl_sp2d)));
                                        $end_date   = new DateTime(date('Y-m-d', strtotime($rkk->tgl_spj)));
                                        $interval   = $start_date->diff($end_date);
                                        /*$awal=$rkk->sp2d_nilai;
                                    $akhir+=$rkk->spj_nilai;*/
                                        ?>
                                    <tr <?php if ($interval->days > 30) { ?> class="table-danger" <?php } ?>>

                                        <td class="cell-detail"><?= $rkk->no_sp2d ?> <span class="cell-detail-description"><?= date_indo(date('Y-m-d', strtotime($rkk->tgl_sp2d))) ?></span></td>
                                        <td><?= $rkk->ket_sp2d ?></td>
                                        <td align="right"><?= number_format($rkk->nilai_sp2d, '0', '', '.') ?></td>

                                        <td class="cell-detail"><?= $rkk->no_spj ?> <span class="cell-detail-description"><?= date_indo(date('Y-m-d', strtotime($rkk->tgl_spj))) ?></span></td>
                                        <td><?= $rkk->ket_spj ?></td>
                                        <td align="right"><?= number_format($rkk->nilai_spj, '0', '', '.') ?></td>
                                        <td align="right"><?= number_format(($rkk->nilai_sp2d) - ($rkk->nilai_spj), '0', '', '.') ?></td>
                                        <th><?= anchor('sppd/hapusmaping?p=' . $rkk->id, '<i class="mdi mdi-delete"></i>', 'class="btn btn-xs btn-danger" onclick="return confirm(\'Apakah anda yakin?\')"'); ?></th>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <!-- <tfoot>
                                     <tr>
                                         <td colspan="2"><strong>Jumlah SP2D</strong></td>
                                         <td align="right"><?= number_format($awal, '2', ',', '.') ?></td>
                                         <td colspan="2"><strong>Jumlah SPJ</strong></td>
                                         <td align="right"><?= number_format($akhir, '2', ',', '.') ?></td>
                                         <td align="right"><?= number_format($awal - $akhir, '2', ',', '.') ?></td>
                                     </tr>
                                 </tfoot> -->
                        </table>
                        <!-- Awal : <?= $awal ?>
                             Akhir : <?= $akhir ?> -->

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>
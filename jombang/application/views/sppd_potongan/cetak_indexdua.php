<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Potongan Surat Perintah Pencairan Dana (SP2D).xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Potongan Surat Perintah Pencairan Dana (SP2D)<br>
SKPD :<?= $kd_skpd?><br>
Unit :<?= $nm_unit?><br>
Sub Unit :<?= $nm_sub_unit?><br>
Tanggal SP2D :<?= $tanggal1." - ".$tanggal2?><br>
</h3>

<table class="tablee" border="1">
  <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>Tanggal SP2D</th>
								<th>No SP2D</th>
                <th>Keterangan</th>
								<th>Jumlah</th>
								<th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        $no=1; foreach ($sppd_potongan_data as $rk) {?>
                            <tr>
                                <td valign="top" width="10px" class="text-center"><?php echo $no++; ?></td>
                                <td valign="top" style='mso-number-format:"\@"'><?= date_indo(date('Y-m-d',strtotime($rk->tgl_sp2d))) ?></td>
                                <td valign="top" ><?php echo $rk->no_sp2d ?></td>
                                <td valign="top" ><?php echo $rk->keterangan ?></td>
                                 <td valign="top"><?= $rk->jum ?></td>
                                <td valign="top"  align="right"><?php echo number_format($rk->nilai,'0',',','.') ?></td>
                            </tr>
                        <?php
                        } ?>
						</tbody>
</table>
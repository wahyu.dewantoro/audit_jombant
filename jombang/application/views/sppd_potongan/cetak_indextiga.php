<?php

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=Potongan Surat Perintah Pencairan Dana (SP2D).xls");

header("Pragma: no-cache");

header("Expires: 0");

?>
<style type="text/css">
.table tbody tr td.cell-detail .cell-detail-description {
    display: block;
    font-size: .8462rem;
    color: #999;
}
  tablee{
    border-collapse:collapse;
    border: 1px solid black !important;;
  }
  tablee td{
    border: 1px solid black !important;;
  }
  tablee tr{
    border: 1px solid black !important;;
  }
  tablee th{
    border: 1px solid black !important;;
  }
  tablee tbody{
    border: 1px solid black !important;;
  }
</style>
<h3>Potongan Surat Perintah Pencairan Dana (SP2D)<br>
SKPD :<?= $kd_skpd?><br>
Unit :<?= $nm_unit?><br>
Sub Unit :<?= $nm_sub_unit?><br>
No SP2D :<?= $no_sp2d?><br>
</h3>


<table class="tablee" border="1">
                      <thead>
                            <tr>
                              <th>No</th>
                              <th>Tanggal</th>
                              <th>No SP2D</th>
                              <th>Keterangan</th>
                              <th>Nilai</th>
                              <th>Potongan</th>
                              <th>Kode Rekening</th>
                              <th>Rekening</th>
                              <th>Bukti</th>
                            </tr>
                        </thead>
                        <tbody>
              <?php foreach ($sppd_potongan_data as $rk)  { ?>
                            <tr>
                <td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
                <td><?= date_indo(date('Y-m-d',strtotime( $rk->tgl_sp2d))) ?></td>
                <td class="cell-detail"><?= $rk->no_sp2d ?><span class="cell-detail-description"></span></td>
                <td><?= $rk->keterangan ?></td>
                <td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
                <td><?= $rk->nm_pot ?></td>
                <td><?= $rk->kd_rek_gabung ?></td>
                <td><?= $rk->nm_rek_5 ?></td>
                <td><?= $rk->id_bukti ?></td>
              </tr>
              <?php  }   ?>
            </tbody>
</table>
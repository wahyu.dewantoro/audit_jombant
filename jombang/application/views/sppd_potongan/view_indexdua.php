<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            <div class="row">
            	<div class="col-md-6">
    				<table valign="top">
    					<tr valign="top">
                            <td><strong>OPD</strong></td>
                            <td>: <?= $kd_skpd?> <?php if($nm_unit==$nm_sub_unit){echo $nm_unit;}else{echo $nm_unit.' / '.$nm_sub_unit;}?></td>
                        </tr>
                        <tr valign="top">
                            <td><strong>Periode</strong></td>
                            <td>:<?= $tanggal1 ." - ".$tanggal2?></td>
                        </tr>
    				</table>
            	</div>
            </div>			<hr>

                	<div class="table-responsive ">
                    <table class="table table-striped table-hover table-bordered"  id="table1">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>No SP2D</th>
								<th>Keterangan</th>
								<th>Jumlah</th>
								<th>Nilai</th>
								<th></th>
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($sppd_potongan_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
								
								<td class="cell-detail"><?= $rk->no_sp2d ?><span class="cell-detail-description"><?= date_indo(date('Y-m-d',strtotime( $rk->tgl_sp2d))) ?></span></td>
								<td><?= $rk->keterangan ?></td>
								<td align="center"><?= $rk->jum ?></td>
								<td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
								<th align="center">
									<form action="<?= base_url().'sppd_potongan/readdua'?>">
										<input type='hidden' name='no_sp2d' value='<?= $rk->no_sp2d?>' >
										<input type='hidden' name='kd_skpd' value='<?= $rk->kd_skpd?>' >
										<input type='hidden' name='nm_unit' value='<?= $rk->nm_unit?>' >
										<input type='hidden' name='nm_sub_unit' value='<?= $rk->nm_sub_unit?>' >
                                       <input type="hidden" name="tanggal1" value="<?= $tanggal1 ?>">
                                       <input type="hidden" name="tanggal2" value="<?= $tanggal2 ?>">
										<button class="btn btn-xs btn-primary"><i class="mdi mdi-search"></i></button>
									</form>
								</th>
							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					</div>

            </div>
        </div><!-- end card-->
    </div>
</div>
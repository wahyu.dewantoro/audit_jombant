<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card mb-3">
            <div class="card-body">
            <div class="row">
            	<div class="col-md-6">
    				<table valign="top">
    					<tr valign="top">
                            <td ><strong>OPD</strong></td>
                            <td>:</td>
                            <td> <?= $kd_skpd?> <?php if($nm_unit==$nm_sub_unit){echo $nm_unit;}else{echo $nm_unit.' / '.$nm_sub_unit;}?></td>
                        </tr>
                        <tr valign="top">
                            <td width="20%"><strong>No SP2D</strong></td>
                            <td width="1%">:</td>
                            <td><?= $no_sp2d?></td>
                        </tr>
                        <tr valign="top">
                            <td width="20%"><strong>Tanggal SP2D</strong></td>
                            <td width="1%">:</td>
                            <td><?= date_indo(date('Y-m-d',strtotime( $sppd->tgl_sp2d))) ?></td>
                        </tr>
                        <tr valign="top">
                            <td width="20%"><strong>Keterangan</strong></td>
                            <td width="1%">:</td>
                            <td><?= $sppd->keterangan?></td>
                        </tr>
    				</table>
            	</div>
            </div>			<hr>
			 
                	<div class="table-responsive ">
                    <table class="table table-striped table-hover table-bordered"  id="table1">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								 <th>Potongan</th>
                                <th>Rekening</th>
                                 <th>Nilai</th>
								 <th>Bukti</th>
								
								
                            </tr>
                        </thead>
                        <tbody>
							<?php foreach ($sppd_potongan_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo number_format(++$start,'0','','.') ?></td>
								<td><?= $rk->nm_pot ?></td>
								<td class="cell-detail"><?= $rk->kd_rek_gabung ?> <span class="cell-detail-description"><?= $rk->nm_rek_5 ?></span></td>
								<td align="right"><?= number_format($rk->nilai,'0','','.') ?></td>
								<td><?= $rk->id_bukti ?></td>

							</tr>
							<?php  }   ?>
						</tbody>
					</table>
					</div>
					
            </div>
        </div><!-- end card-->
    </div>
</div>
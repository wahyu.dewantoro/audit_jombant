<style type="text/css">     
  #wgtmsr{
     width:150px;   
    }

    #wgtmsr option{
      width:150px;   
    }


</style>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">                       
        <div class="card mb-3">
            <div class="card-body">
                
                    <div class="table-responsive">
                    <table class="table  table-bordered table-striped table-hover" id="table1">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>Kode SKPD</th>
                                <th>Urusan</th>
                                <th>Bidang</th>
                                <th>Unit</th>
                                <th>Sub Unit</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php $start=0;  foreach ($subunit_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo ++$start ?></td>
                                <td><?= $rk->kd_skpd ?></td>
                                <td><?= $rk->nm_urusan ?></td>
                                <td><?= $rk->nm_bidang ?></td>
                                <td><?= $rk->nm_unit ?></td>
                                <td><?= $rk->nm_sub_unit ?></td>
							</tr>
							<?php  }    ?>
                           
						</tbody>
					</table>
                    </div>
					
            </div>                                                      
        </div><!-- end card-->                  
    </div>
</div>
<style type="text/css">     
  #wgtmsr{
     width:150px;   
    }

    #wgtmsr option{
      width:150px;   
    }


</style>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">                       
        <div class="card mb-3">
            <div class="card-body">
                
                    <div class="table-responsive">
                    <table class="table  table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="10px">No</th>
								<th>Kode Unit</th>
                                <th>Urusan</th>
                                <th>Bidang</th>
                                <th>Unit</th>
                                <th>Jumlah</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
							<?php  foreach ($subunit_data as $rk)  { ?>
                            <tr>
								<td  align="center"><?php echo ++$start ?></td>
                                <td><?= $rk->kode_unit ?></td>
                                <td><?= $rk->nm_urusan ?></td>
                                <td><?= $rk->nm_bidang ?></td>
                                <td><?= $rk->nm_unit ?></td>
                                <td><?= $rk->jum ?></td>
                                <th>
                                    <?php echo anchor('subunit/read?q='.urlencode($rk->nm_unit),'<i class="mdi mdi-search"></i>','class="btn btn-xs btn-primary"');?>
                                </th>
							</tr>
							<?php  }    ?>
                           
						</tbody>
					</table>
                    </div>
					<button  class="btn  btn-space btn-secondary" disabled>Total Record : <?php echo $total_rows ?></button>
					<div class="float-right">
						<?php echo $pagination ?>
					</div>
            </div>                                                      
        </div><!-- end card-->                  
    </div>
</div>
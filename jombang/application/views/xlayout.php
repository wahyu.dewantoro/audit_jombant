
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?= base_url() ?>assets/img/logo-fav.png">
    <title><?= $title ?></title>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/material-design-icons/css/material-design-iconic-font.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/jqvmap/jqvmap.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/app.css" type="text/css"/>
  </head>
  <body>
    <div class="be-wrapper be-fixed-sidebar">
      <nav class="navbar navbar-expand fixed-top be-top-header">
        <div class="container-fluid">
          <div class="be-navbar-header"><a class="navbar-brand" href="<?= base_url() ?>"></a>
          </div>
          <div class="be-right-navbar">
            <ul class="nav navbar-nav float-right be-user-nav">
              <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-expanded="false"><img src="<?= base_url() ?>assets/img/avatar.png" alt="Avatar"><span class="user-name"><?= get_userdata('audit_nama') ?></span></a>
                <div class="dropdown-menu" role="menu">     
                  <div class="user-info">
                    <div class="user-name"><?= get_userdata('audit_nama') ?></div>
                    <div class="user-position online">Available</div>
                  </div><a class="dropdown-item" href="<?= base_url('auth/logout')?>"><span class="icon mdi mdi-power"></span>Logout</a>
                </div>
              </li>
            </ul>
            
          </div>
        </div>
      </nav>
      
      <!-- left menu -->
      <?php $this->load->view('sidebar');?>
      <div class="be-content">
        <div class="page-head">
            <h2 class="page-head-title"><?= $title ?>
              <div style="float:right">

              <?php 
                if(isset($kembali)){
                  echo  anchor($kembali,'<i class="mdi mdi-flip-to-back"></i> Kembali','class="btn btn-sm btn-primary"');    
                }
              ?>
              
            </div>
            </h2>

          </div>
          <div class="main-content container-fluid">
          <?= $this->session->flashdata('message')?>
            <?= $contents ?>
            
          </div>
      </div>
    </div>
    
    <script src="<?= base_url() ?>assets/lib/jquery/jquery.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/js/app.js" type="text/javascript"></script>

    <script src="<?= base_url() ?>assets/lib/jquery.sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/countup/countUp.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <!--
    <script src="<?= base_url() ?>assets/lib/jqvmap/jquery.vmap.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>assets/lib/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script> -->
    <script type="text/javascript">
      // menu aktif
      $(function() {
          var base='<?= base_url() ?>'
          var seg='<?= strtolower($this->uri->segment(1)); ?>';
        $('.sidebar-elements a[href~="' + base+seg + '"]').parents('li').addClass('active');
      });



      $(document).ready(function(){
      	//-initialize the javascript
      	App.init();
      	App.dashboard();
      
      });
    </script>
  </body>
</html>